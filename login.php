<?php include('../paginas_include/variables-generales.php'); 

session_start();
if($_SESSION) {
  $redireccion = $Servidor_url.'PANELADMINISTRADOR/'; 
  header('Location: '.$redireccion);
  exit;
}

?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Shopifunny   &raquo; Ingresar</title>
  <meta name="HandheldFriendly" content="True">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">
  <meta name="format-detection" content="telephone=no">
  <meta http-equiv="cleartype" content="on">

  <link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/reset.css"> <!-- CSS reset -->
  <link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/font-awesome-4.7.0/css/font-awesome.min.css" />
  <link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/nuevohome.css">
  <script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/variables-generales.js"></script>            
  <script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/modernizr.js"></script> <!-- Modernizr -->
  <link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/login.css"> <!-- Gem style -->
  <link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Gem style -->

  <link rel="apple-touch-icon" sizes="57x57" href="<?php echo $Servidor_url;?>apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="<?php echo $Servidor_url;?>apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="<?php echo $Servidor_url;?>apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="<?php echo $Servidor_url;?>apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="<?php echo $Servidor_url;?>apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="<?php echo $Servidor_url;?>apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="<?php echo $Servidor_url;?>apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="<?php echo $Servidor_url;?>apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="<?php echo $Servidor_url;?>apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo $Servidor_url;?>android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $Servidor_url;?>favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="<?php echo $Servidor_url;?>favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $Servidor_url;?>favicon-16x16.png">
  <link rel="manifest" href="<?php echo $Servidor_url;?>manifest.json">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="<?php echo $Servidor_url;?>ms-icon-144x144.png">
  
  <script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/jquery-2.1.1.js"></script>
  <style type="text/css">
    #btn_login {
      background-color: #FF9800 !important;
    }
    .error {
      background: #EF9A9A;
      padding: 13px;
      color: #ffffff;
      text-align: center;
    }

    body {
      background: #fff;
    }

    .logo_redondo {
      width: 200px;
      margin-top: 60px;
    }

  </style>
</script>
</head>
<body>
  <div class="parte_abajo">
    <center>
      <img src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/img/logo-grande2.png" class="logo_redondo">
    </center>
    <div class="formulario_adentro"> <!-- log in form -->
      <form class="cd-form" method="post" action="<?php echo $Servidor_url;?>PANELADMINISTRADOR/php/loguear-usuario.php">
        <?php if($_GET['error']) { ?>
        <div class="error">
          <p>Los datos ingresados son incorrectos</p>
        </div>
        <?php } ?>
        <p class="fieldset">
          <label class="image-replace cd-username" for="signin-email">Usuario o E-mail</label>
          <input class="full-width has-padding has-border" style="color:#f90; border-color: #f90" name="usuario" required id="signin-email" type="text" placeholder="Usuario o E-mail">
        </p>
        <p class="fieldset">
          <label class="image-replace cd-password" for="signin-password">Contraseña</label>
          <input class="full-width has-padding has-border" style="color:#f90; border-color: #f90" name="contrasena" required id="signin-password"  type="password"  placeholder="Contraseña">
          <a href="#0" class="hide-password">Mostrar</a>
        </p>
        <div>
          <input type="hidden" name="redirigir" value="<?php echo trim($_GET['redirigir']); ?>"/>
        </div>
        <p class="fieldset">
          <div class="contenedor_boton">
            <p class="fieldset">
              <input class="full-width" type="submit" id="btn_login" value="Continuar">
            </p>
          </div>              
        </p>
        <br>
      </form>
      <!-- <a href="#0" class="cd-close-form">Close</a> -->
    </div> <!-- cd-login -->  
    <div class="clear"></div>

  </div>

  <script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/login.js"></script>

</body>
</html>
