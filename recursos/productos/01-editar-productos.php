<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');

$producto = trim($_GET['producto']);
$imagen_cargada = trim($_GET['imagen_cargada']);

if(!$producto) {
	$redirigir = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/productos/00-cargar-producto.php';
	header('location:'.$redirigir);
	exit;
}

conectar2('shopifun', "admin");

$query_rs_imagen = "SELECT id_foto, nombre_foto, fecha_carga, recorte_foto_nombre, recorte_foto_miniatura FROM fotos_publicaciones WHERE id_publicacion = $producto ORDER BY id_foto DESC ";
$rs_imagen = mysql_query($query_rs_imagen)or die(mysql_error());
$row_rs_imagen = mysql_fetch_assoc($rs_imagen);
$totalrow_rs_imagen = mysql_num_rows($rs_imagen);

if($imagen_cargada) {
	$id_foto = $row_rs_imagen['id_foto'];
	$redireccionar = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/productos/03-recortar-foto.php?foto='.$id_foto.'&producto='.$producto;
	header('location:'.$redireccionar);
	exit;
}
//consultar en la base de datos
$query_rs_noticias = "SELECT * FROM productos WHERE id_producto = $producto";
$rs_noticias = mysql_query($query_rs_noticias)or die(mysql_error());
$row_rs_noticias = mysql_fetch_assoc($rs_noticias);
$totalrow_rs_noticias = mysql_num_rows($rs_noticias);

$esquinero = $row_rs_noticias['producto_esquinero'];
$categoria = $row_rs_noticias['producto_categoria'];
$titulo = $row_rs_noticias['producto_titulo'];

$url_origen = $row_rs_noticias['producto_url_origen'];
$descripcion = $row_rs_noticias['producto_descripcion'];
$tags = $row_rs_noticias['producto_tags'];
$precio_dolar = $row_rs_noticias['producto_precio_dolar'];
$precio_dolar_comparado = $row_rs_noticias['producto_precio_dolar_comparado'];
$precio_shipping_dolar = $row_rs_noticias['producto_precio_shipping_dolar'];
$precio_euro =  $row_rs_noticias['producto_precio_euro'];
$precio_euro_comparado =  $row_rs_noticias['producto_precio_euro_comparado'];
$precio_shipping_euro =  $row_rs_noticias['producto_precio_shipping_euro'];
$stock =  $row_rs_noticias['producto_stock'];
$ventas =  $row_rs_noticias['producto_ventas'];

$producto_talles = $row_rs_noticias['producto_talles'];

$talles = explode(',', $producto_talles);

foreach ($talles as $valor) {
	$array_talles[$valor] = 1;
}

$check_atemporal = NULL;
$fecha_noticia_style = NULL;

if($fecha_noticia=='0000-00-00') {
	$check_atemporal = 'checked';
	$fecha_noticia_style = 'display:none';
	$fecha_noticia_value = date('Y-m-d');
}
//consultar en la base de datos
$query_rs_cuerpo = "SELECT id_cuerpo, orden, tamano_texto, cuerpo_tipo, contenido FROM productos_cuerpo WHERE id_producto = $producto ORDER BY orden ASC ";
$rs_cuerpo = mysql_query($query_rs_cuerpo)or die(mysql_error());
$row_rs_cuerpo = mysql_fetch_assoc($rs_cuerpo);
$totalrow_rs_cuerpo = mysql_num_rows($rs_cuerpo);

//consultar en la base de datos
$query_rs_imagenes = "SELECT id_foto, recorte_foto_nombre FROM fotos_publicaciones WHERE id_publicacion = $producto ";
$rs_imagenes = mysql_query($query_rs_imagenes)or die(mysql_error());
$row_rs_imagenes = mysql_fetch_assoc($rs_imagenes);
$totalrow_rs_imagenes = mysql_num_rows($rs_imagenes);

do {
	$id_foto = $row_rs_imagenes['id_foto'];
	$recorte_foto_nombre = $row_rs_imagenes['recorte_foto_nombre'];

	$array_imagenes[$id_foto] = $recorte_foto_nombre;
} while($row_rs_imagenes = mysql_fetch_assoc($rs_imagenes));


//consultar en la base de datos
$query_rs_colores = "SELECT id_color, color, color_codigo  FROM colores";
$rs_colores = mysql_query($query_rs_colores)or die(mysql_error());
$row_rs_colores = mysql_fetch_assoc($rs_colores);
$totalrow_rs_colores = mysql_num_rows($rs_colores);


//consultar en la base de datos
$query_rs_esquineros = "SELECT id_esquinero, esquinero_nombre FROM esquineros ORDER BY esquinero_nombre ASC";
$rs_esquineros = mysql_query($query_rs_esquineros)or die(mysql_error());
$row_rs_esquineros = mysql_fetch_assoc($rs_esquineros);
$totalrow_rs_esquineros = mysql_num_rows($rs_esquineros);

//consultar en la base de datos
$query_rs_categorias = "SELECT id_categoria, categoria_nombre FROM categorias categoria_nombre ORDER BY categoria_nombre ASC ";
$rs_categorias = mysql_query($query_rs_categorias)or die(mysql_error());
$row_rs_categorias = mysql_fetch_assoc($rs_categorias);
$totalrow_rs_categorias = mysql_num_rows($rs_categorias);


//consultar en la base de datos
$query_rs_talles = "SELECT id_talle, talle FROM talles ";
$rs_talles = mysql_query($query_rs_talles)or die(mysql_error());
$row_rs_talles = mysql_fetch_assoc($rs_talles);
$totalrow_rs_talles = mysql_num_rows($rs_talles);

desconectar();

?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form3.css?v=2"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/popup.css"> <!-- Resource style -->
	<script src="<?php echo $Servidor_url;?>PANELADMINISTRADOR/js/nicEdit2/nicEdit.js" type="text/javascript"></script>
	<link rel="stylesheet" href="<?php echo $Servidor_url;?>PANELADMINISTRADOR/css/checkbox/style.css?v=3"> <!-- Resource style -->

</head>
<style type="text/css">
.btn_eliminar {
	text-align: right;
	width: 100%;
}

a {
	cursor: pointer;
}
.boton_verde a{
	background: #48b617;
	color: #fff;
}
.boton_verde a:hover {
	background: #235d09 !important;
	color: #f6ff05;
}	
.boton_rojo a{
	background: #c40000;
	color: #fff;
}
.boton_rojo a:hover {
	background: #9e0101 !important;
	color: #f6ff05;
}
h3 {
	margin-bottom: 5px;
	font-weight: bold;
}

.portada {
	color: #2E7D32;
	font-weight: bold;
}

.rojo {
	color: #F44336;
	font-weight: bold;
}

.verde {
	color: #2E7D32;
	font-weight: bold;
}
a {
	cursor: pointer;
}
.input_video {
	width: 100%;
	padding: 10px;
	margin-top: 15px;
}

.input_texto {
	width: 100%;
	height: 200px;
	padding: 10px;
	margin-top: 15px;
}


.video-container {
	position: relative;
	padding-bottom: 56.25%;
	padding-top: 30px; height: 0; overflow: hidden;
}

.video-container iframe,
.video-container object,
.video-container embed {
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
}
.video_youtube {
	width: 100%;
}

#formTexto {
	margin-top: -150px;
	padding: 20px;
}	

.img_delete {
	width: 30px;
}

.td_delete {
	padding: 10px;
	text-align: right;
	width: 30px;
}
.td_delete img {
	width: 30px;
	display: block;
}
.txt_categoria_elegida {
	padding: 10px;
	color: #2c97de;
	display: block;
}
.grupo_categoria {
	margin-left: -20px!important;
}

#section_categoria {
	background: #a7a7a7;
	padding: 30px;
	color: #fff;
}

#section_categoria h3 {
	font-size: 24px;
}
.select_class {
	background: #eeeeee !important;
}
.delete_categoria {
	float: right;
}
.delete_categoria img{
	width: 35px;
	margin-top: -5px;
}
.boton_amarillo {
	background: #f90;
}
.boton_amarillo:hover {
	background: red;
	color: #fff;
}

.input_mitad {
	width: 50% !important;
	float: left;
}
</style>
</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper">
			<div class="cd-popup" id="popup_agregar" role="alert">
				<div class="cd-popup-container">
					<p>¿Qué elemento querés agregar?</p>
					
					<div style="padding:10px">
						<a class="vc_btn_largo vc_btn_verde vc_btn_3d" onclick="agregar_texto()">
							<span class="fa-stack fa-lg pull-left">
								<i class="fa fa-circle fa-stack-2x"></i>
								<i class="fa fa-plus fa-stack-1x fa-inverse"></i>
							</span>
							<b>Agregar Texto</b>
						</a>
						
						<a class="vc_btn_largo vc_btn_amarillo vc_btn_3d" onclick="agregar_imagen()">
							<span class="fa-stack fa-lg pull-left">
								<i class="fa fa-circle fa-stack-2x"></i>
								<i class="fa fa-camera fa-stack-1x fa-inverse"></i>
							</span>
							<b>Agregar Imagen</b>
						</a>
						<a class="vc_btn_largo vc_btn_rojo vc_btn_3d" onclick="agregar_video()">
							<span class="fa-stack fa-lg pull-left">
								<i class="fa fa-circle fa-stack-2x"></i>
								<i class="fa fa-youtube-play fa-stack-1x fa-inverse"></i>
							</span>
							<b>Agregar Video de youtube</b>
						</a>
					</div>
					<a href="#0" class="cd-popup-close img-replace"></a>
				</div> <!-- cd-popup-container -->
			</div> <!-- cd-popup -->
			<div class="cd-popup" id="popup_video" role="alert">
				<div class="cd-popup-container">
					<p>Copiá y pegá acá el link de tu video<br>
						<input class="input_video" type="text" id="input_video" placeholder="Ej: https://www.youtube.com/watch?v=yH2HIPOb-cE" /></p>
						<ul class="cd-buttons">
							<li id="btn_confirmar_noticia"><a onclick="confirmar_video_youtube()">Continuar</a></li>
							<li><a onclick="cerrar_popup()">Cancelar</a></li>
						</ul>
						<a href="#0" class="cd-popup-close img-replace"></a>
					</div> <!-- cd-popup-container -->
				</div> <!-- cd-popup -->

				<div class="cd-popup" id="popup_borrar_elemento" role="alert">
					<div class="cd-popup-container">
						<p>¿Seguro querés borrar este elemento?<br>
							<ul class="cd-buttons">
								<li id="btn_confirmar_borrado_elemento"><a>Si</a></li>
								<li><a onclick="cerrar_popup()">Cancelar</a></li>
							</ul>
							<a href="#0" class="cd-popup-close img-replace"></a>
						</div> <!-- cd-popup-container -->
					</div> <!-- cd-popup -->
					<div class="cd-popup" id="popup_direccion" role="alert">
						<div class="cd-popup-container">
							<p>¿Estás seguro de querer borrar esta dirección?</p>
							<ul class="cd-buttons">
								<li id="btn_confirmar_direccion"></li>
								<li><a onclick="cerrar_popup()">No</a></li>
							</ul>
							<a href="#0" class="cd-popup-close img-replace"></a>
						</div> <!-- cd-popup-container -->
					</div> <!-- cd-popup -->
					<div class="contenedor">

						<div >					<!-- Contenido de la Pagina-->	

							<div class="cd-form floating-labels">
								<section id="crear_categoria" >							
									<fieldset >
										<form onsubmit="return validar_formulario()" id="myForm" action="<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/productos/php/01-editar-producto-db.php" method="POST">
											<input type="hidden" name="accion" id="accion" />
											<input type="hidden" name="accionDato" id="accionDato" />

											<a href="<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/productos/02-ficha-producto.php?producto=<?php echo $producto;?>" class="vc_btn_largo vc_btn_rojo vc_btn_3d" style="width:250px;float:right">
												<span class="fa-stack fa-lg pull-left">
													<i class="fa fa-circle fa-stack-2x"></i>
													<i class="fa fa-calendar-o fa-stack-1x fa-inverse"></i>
												</span>
												<b>Ficha del producto</b>
											</a>
											<legend id="txt_nueva_categoria">Editar Producto</legend>
											<div class="icon">
												<label class="cd-label" for="cd-company">Esquinero</label>
												<p class="cd-select">
													<select name="esquinero" class="select_class" id="esquinero" >
														<option value="0">Elegí un esquinero</option>					
														<?php do { 
															$id_esquinero = $row_rs_esquineros['id_esquinero'];
															$esquinero_nombre = $row_rs_esquineros['esquinero_nombre'];

															$selected = '';

															if($id_esquinero==$esquinero) {
																$selected = 'selected';
															}
															?>

															<option <?php echo $selected; ?> value="<?php echo $id_esquinero; ?>"><?php echo $esquinero_nombre; ?></option>					

															<?php } while($row_rs_esquineros = mysql_fetch_assoc($rs_esquineros)); ?>
														</select></p>
													</div> 			 
													<div class="icon">
														<label class="cd-label" for="cd-company">Categoría</label>
														<p class="cd-select">
															<select name="categoria" required class="select_class" id="categoria" >
																<option value="0">Elegí una categoría</option>					<?php do { 
																	$id_categoria = $row_rs_categorias['id_categoria'];
																	$categoria_nombre = $row_rs_categorias['categoria_nombre'];


																	$selected = '';

																	if($id_categoria==$categoria) {
																		$selected = 'selected';
																	}
																	?>

																	<option <?php echo $selected; ?> value="<?php echo $id_categoria; ?>"><?php echo $categoria_nombre; ?></option>					
																	<?php } while($row_rs_categorias = mysql_fetch_assoc($rs_categorias)); ?>
																</select></p>	
															</select></p>
														</div> 		   
														<div class="icon">
															<label class="cd-label" for="cd-company">URL de Origen</label>
															<input class="company" placeholder="Ej: https://www.aliexpress.com/store/product/Colmi-Sport-Smart-Watch-VS505-Professional-Waterproof-5ATM-Passometer-Like-Smart-Bracelet-Ultra-long-Standby/615486_32794789664.html?spm=2114.12010608.0.0.3a967302xQ2vqQ" type="text" name="url_origen" value="<?php echo $url_origen; ?>" id="url_origen" required>
														</div> 			    

														<div class="icon">
															<label class="cd-label" for="cd-company">Titulo</label>
															<input class="company" type="text" name="titulo" value="<?php echo $titulo; ?>" id="titulo" required>
														</div> 			    

														<div class="icon">
															<label class="cd-label" for="cd-company">Talles</label>

															<?php do { 
																$id_talle = $row_rs_talles['id_talle'];
																$talle = $row_rs_talles['talle'];

																$checked = '';

																if($array_talles[$id_talle]) {
																	$checked = 'checked';
																}
																?>
																<input type="checkbox" <?php echo $checked; ?> name="talle[<?php echo $id_talle; ?>]" id="talle<?php echo $id_talle; ?>" class="css-checkbox" />
																<label for="talle<?php echo $id_talle; ?>" class="css-label"><?php echo $talle; ?></label> <br>

																<?php } while($row_rs_talles = mysql_fetch_assoc($rs_talles)); ?>


															</div> 
															<br><br>
															<div class="icon">
																<label class="cd-label" for="cd-textarea">Descripción</label>
																<textarea class="message" required name="descripcion" id="descripcion" ><?php echo $descripcion; ?></textarea>
															</div>
															<div class="icon">
																<label class="cd-label" for="cd-company">Tags</label>
																<input class="company" type="text" value="<?php echo $tags; ?>" name="tags" id="tags" required>
															</div> 

															<div class="icon">
																<label class="cd-label" for="cd-company">Precio Dolar</label>
																<input class="company input_mitad" type="number" step="0.01" placeholder="Precio Dolar Venta"  value="<?php echo $precio_dolar; ?>" name="precio_dolar" id="precio_dolar" required>
																<input class="company input_mitad" type="number" step="0.01" placeholder="Precio Dolar Comparado" value="<?php echo $precio_dolar; ?>" name="precio_dolar_comparado" id="precio_dolar_comparado" required>
															</div>
															<div class="icon">
																<label class="cd-label" for="cd-company">Precio Shipping Dolar</label>
																<input class="company" type="number" step="0.01"  value="<?php echo $precio_shipping_dolar; ?>" name="precio_shipping_dolar" id="precio_shipping_dolar" required>
															</div> 
															<div class="icon">
																<label class="cd-label" for="cd-company">Precio Euro</label>
																<input class="company input_mitad" type="number" step="0.01" placeholder="Precio Euro Venta"  value="<?php echo $precio_euro; ?>" name="precio_euro" id="precio_euro" required>
																<input class="company input_mitad" type="number" step="0.01" placeholder="Precio Euro Comparado" value="<?php echo $precio_euro_comparado; ?>" name="precio_euro_comparado" id="precio_euro_comparado" required>
															</div> 
															<div class="icon">
																<label class="cd-label" for="cd-company">Precio Shipping Euro</label>
																<input class="company" type="number" step="0.01" value="<?php echo $precio_shipping_euro; ?>" name="precio_shipping_euro" id="precio_shipping_euro" required>
															</div> 
															<div class="icon">
																<label class="cd-label" for="cd-company">Stock</label>
																<input class="company" type="number" value="<?php echo $stock; ?>" name="stock" id="stock" required>
															</div> 

															<div class="icon">
																<label class="cd-label" for="cd-company">Ventas</label>
																<input class="company" type="number" value="<?php echo $ventas; ?>" name="ventas" id="ventas" required>
															</div> 

															<input  type="hidden" id="elementos_contador" value="1">
															<input  type="hidden" id="producto" name="producto" value="<?php echo $producto; ?>">
															<br>												
															<div id="cuerpo">
																<h3>Cuerpo:</h3>
																<table class="table table-striped">
																	<tbody>
																		<?php
																		$ruta_img = $Servidor_url.'APLICACION/Imagenes/notas/recortes/';
																		$i=1;
																		do {
																			$id_cuerpo = $row_rs_cuerpo['id_cuerpo'];
																			$cuerpo_tipo = $row_rs_cuerpo['cuerpo_tipo'];
																			$contenido = $row_rs_cuerpo['contenido'];

																			$orden = $row_rs_cuerpo['orden'];

																			if($cuerpo_tipo=="imagen") {
																				$imagen = $ruta_img.$array_imagenes[$contenido];
																				?>
																				<input type="hidden" name="elemento[<?php echo $id_cuerpo;?>]" value="<?php echo $contenido; ?>" >
																				<tr>
																					<td width="60"><input type="text" name="orden[<?php echo $id_cuerpo;?>]"  maxlength="2" value="<?php echo $i; ?>"></td>
																					<td><img src="<?php echo $imagen; ?>" width="100%"></td>
																					<td><a onclick="borrar_elemento(<?php echo $id_cuerpo; ?>)" ><img src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/img/delete.png" class="img_delete"></a></td>
																				</tr>
																				<?php
																			}

																			if($cuerpo_tipo=="texto") {
																				?>
																				<tr>
																					<td width="60"><input type="text" name="orden[<?php echo $id_cuerpo;?>]" maxlength="2" value="<?php echo $i; ?>">
																					</td>

																					<td>
																						<textarea name="elemento[<?php echo $id_cuerpo;?>]" id="area<?php echo $i;?>"><?php echo $contenido; ?></textarea>
																						<a onclick="editar_html(<?php echo $id_cuerpo; ?>)">Editar HTML del texto</a>
																						<br><br>
																					</td>
																					<td><a onclick="borrar_elemento(<?php echo $id_cuerpo; ?>)" ><img src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/img/delete.png" class="img_delete"></a></td>
																					<script type="text/javascript">
																						bkLib.onDomLoaded(function() {
																							new nicEditor({fullPanel : true}).panelInstance('area<?php echo $i;?>');
																						});
																					</script>
																				</tr>	  
																				<?php }

																				if($cuerpo_tipo=="video") { ?>
																				<input type="hidden" name="elemento[<?php echo $id_cuerpo;?>]" value="<?php echo $contenido; ?>" >
																				<tr>
																					<td width="60"><input type="text" name="orden[<?php echo $id_cuerpo;?>]" maxlength="2" value="<?php echo $i; ?>"></td>
																					<td>
																						<div class="video_youtube">
																							<div class="video-container">
																								<iframe src="https://www.youtube.com/embed/<?php echo $contenido;?>" frameborder="0" ></iframe>
																							</div>
																						</div>
																					</td>
																					<td width="60"><a onclick="borrar_elemento(<?php echo $id_cuerpo; ?>)" ><img src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/img/delete.png" class="img_delete"></a></td>
																				</tr>


																				<?php }
																				$i++;
																			} while($row_rs_cuerpo = mysql_fetch_assoc($rs_cuerpo));
																			?>

																		</tbody>
																	</table>
																</div>
																<img src="" id="imagen"/>
																<a class="vc_btn_largo vc_btn_verde vc_btn_3d" onclick="agregar_elemento()">
																	<span class="fa-stack fa-lg pull-left">
																		<i class="fa fa-circle fa-stack-2x"></i>
																		<i class="fa fa-plus fa-stack-1x fa-inverse"></i>
																	</span>
																	<p>Agregar elemento al cuerpo</p>
																</a>
																<br><br>

															</fieldset>	
														</section>    	

														<a name="botonGuardar"></a>
														<div class="alinear_centro">
															<input type="submit" value="Guardar" id="btn_nueva_categoria">
														</div>
													</form>
												</div>
											</div> <!-- .content-wrapper -->
										</main> 
										<?php include('../../includes/pie-general.php');?>
										<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
										<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/popup.js"></script> <!-- Resource jQuery -->
										<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/jquery.ddslick.min.js"></script> <!-- Resource jQuery -->

										<script type="text/javascript">
											function agregar_elemento() {
												$('#popup_agregar').addClass('is-visible');
											}
											function cerrar_popup() {
												$('.cd-popup').removeClass('is-visible');
											}

											function eliminar_elemento(elemento) {
												$('#elemento_'+elemento).html('');
											}

											function agregar_imagen() {
												var elemento = document.getElementById("elementos_contador").value;

												cerrar_popup();	
												window.open('<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/productos/popUps/01-cargar-imagen.php?noticia=<?php echo $noticia; ?>&id_administrador=<?php echo $id_administrador;?>','popup','width=400,height=400');

												document.getElementById("elementos_contador").value = parseInt(elemento)+1;		
											}

											function agregar_video(){
												$('.cd-popup').removeClass('is-visible');
												$('#popup_video').addClass('is-visible');
											}

											function agregar_texto(){
												$('#accion').val("AgregarTexto");
												document.getElementById("myForm").submit();
											}

											function imagen_lista() {
												alert('imagen cargada :D');
											}

											function confirmar_video_youtube() {
												var video = document.getElementById("input_video").value;
												$('#accion').val("AgregarVideo");
												$('#accionDato').val(video);

												document.getElementById("myForm").submit();
											}


											function borrar_elemento(elemento) {
												$('#popup_borrar_elemento').addClass('is-visible');

												$('#btn_confirmar_borrado_elemento').html('<a onclick="confirmar_borrado_elemento('+elemento+')">Sí</a>');
											}

											function confirmar_borrado_elemento(elemento) {
												$('#accion').val("BorrarElemento");
												$('#accionDato').val(elemento);

												document.getElementById("myForm").submit();
											}

											function editar_html(cuerpo) {
												window.open('<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/productos/popUps/editar-html-cuerpo.php?cuerpo='+cuerpo,'popup','width=800,height=600');
											}
										</script>
									</body>
									</html>