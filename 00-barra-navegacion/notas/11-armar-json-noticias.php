<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');

$pagina= trim($_GET['pagina']);

$limite = 50;
$pagina_get = $pagina;
if(!$pagina_get) {
	$pagina_get=1;
}
if($pagina) {
	$pagina = $pagina-1;
}
$arranca = $pagina*$limite;

conectar2("wavi", "sitioweb");

//consultar en la base de datos
$query_rs_grupo_noticias = "SELECT id_grupo, grupo_nombre, incluir_en_json, cantidad_de_notas FROM grupo_noticias ORDER BY orden ASC ";
$rs_grupo_noticias = mysql_query($query_rs_grupo_noticias)or die(mysql_error());
$row_rs_grupo_noticias = mysql_fetch_assoc($rs_grupo_noticias);
$totalrow_rs_grupo_noticias = mysql_num_rows($rs_grupo_noticias);

do {
	$id_grupo = $row_rs_grupo_noticias['id_grupo'];
	$grupo_nombre = $row_rs_grupo_noticias['grupo_nombre'];
	$incluir_en_json = $row_rs_grupo_noticias['incluir_en_json'];
	$cantidad_de_notas = $row_rs_grupo_noticias['cantidad_de_notas'];

	$array_grupos[$id_grupo] = $grupo_nombre;
	$array_grupos_incluir_en_json[$id_grupo] = $incluir_en_json;
	$array_grupos_cantidad_de_notas[$id_grupo] = $cantidad_de_notas;

} while ($row_rs_grupo_noticias = mysql_fetch_assoc($rs_grupo_noticias));

$array_grupos = array_filter($array_grupos);

desconectar();
?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->

	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/paginacion.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/checkbox/style.css?v=3"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/popup.css"> <!-- Resource style -->


	<style type="text/css">
		.rojo {
			color: #F44336;
			font-weight: bold;
		}

		.verde {
			color: #2E7D32;
			font-weight: bold;
		}

		.checkbox {
			z-index: 999;
		}
		h2 {
			font-size: 20px;
		}

		.input_popup {
			width: 100%;
			padding: 10px;
		}

		#agrupadas table td {
			background-color: #FFF9C4;
		}
		#agrupadas table .td_header {
			background-color: #FFC107;
			color: #fff;
		}


		.n_notas {
			background-color: #E91E63;
			color: #fff;
			width: 50px;
			height: 50px;
			padding: 15px;
			border-radius: 50%;
			cursor: pointer;
		}

		.n_notas:hover {
			color: #FFF176;
			background-color: #F48FB1;
		}
	</style>
</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<div class="cd-popup" id="popup_cantidad_notas" role="alert">
			<div class="cd-popup-container">
				<p>Cantidad de notas por página<br><br>
					<input type="hidden" id="input_id_grupo" />
					<input class="input_popup" name="n_notas" id="input_n_notas" type="text" id="input_popup">
				</p>
				<ul class="cd-buttons" >
					<li><a onclick="enviar_n_notas()">Guardar</a></li>
					<li><a onclick="cerrar_popup()">Cancelar</a></li>
				</ul>
				<a href="#0" class="cd-popup-close img-replace"></a>
			</div> <!-- cd-popup-container -->
		</div> <!-- cd-popup -->
		
	</div> <!-- cd-popup -->
	<?php include('../../includes/barra-navegacion.php'); ?>
	<div class="content-wrapper" style="max-width:100%">
		<div class="cd-form floating-labels" style="max-width:100%">
			<section style="margin: 0 auto;">
				<form method="POST" id="formGuardarCambios" action="<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/wavi-noticias/php/19-armar-json-noticias-db.php">
					<center>	
						<div id="div_json">
							<p><b><a onclick="GenerarJson()">Guardar lista en un archivo Json</a></b> </p>
						</div>
						<br>
						<br>
						<br>
						<br>
						<br>
						<a class="vc_btn_largo vc_btn_amarillo vc_btn_3d" style="max-width:300px" onclick="enviar_guardar_cambios()">
							<span class="fa-stack fa-lg pull-left">
								<i class="fa fa-circle fa-stack-2x"></i>
								<i class="fa fa-exclamation-circle fa-stack-1x fa-inverse"></i>
							</span>
							<p>Guardar Cambios</p>
						</a>


					</center>

					<br>
					<section id="agrupadas">
						<table class="table table-striped" >
							<thead class="tabla_encabezado">
								<tr>
									<th><b>Incluir</b></th>
									<th><b>Nº notas</b></th>
									<th><b>Grupo</b></th>
								</tr>
							</thead>
							<tbody>
								<?php if($array_grupos) {
									foreach ($array_grupos as $id_grupo => $grupo_nombre) {  
										$checked = null;

										if($array_grupos_incluir_en_json[$id_grupo]) {
											$checked = 'checked';
										}

										$cantidad = $array_grupos_cantidad_de_notas[$id_grupo];
										?>
										<tr>
											<td width="100" data-href="grupo<?php echo $id_grupo; ?>"><input type="checkbox" <?php echo $checked; ?> name="grupo[<?php echo $id_grupo; ?>]" id="grupo<?php echo $id_grupo; ?>" class="css-checkbox" /><label for="grupo<?php echo $id_grupo; ?>" class="css-label"></label></td>
											<td  width="150" style="padding: 20px">
												<input type="hidden" name="cantidad_notas[<?php echo $id_grupo;?>]"  id="input_cantidad_notas_<?php echo $id_grupo;?>" value="<?php echo $cantidad; ?>"/>
												<a id="cantidad_notas_<?php echo $id_grupo;?>" onclick="cantidad_notas(<?php echo $id_grupo; ?>)" class="n_notas"><?php echo $cantidad; ?></a>
											</td>
											<td data-href="grupo<?php echo $id_grupo; ?>"><h2><?php echo $grupo_nombre; ?></h2></td></tr>

											<?php }} ?>
										</tbody>
									</table>	
								</form>
							</section>

						</div>
					</div> <!-- .content-wrapper -->
				</main> 
				<?php include('../../includes/pie-general.php');?>
				<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
				<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/popup.js"></script> <!-- Resource jQuery -->
				<script type="text/javascript">
					$('td[data-href]').on("click", function() {
						var grupo = $(this).data('href');
						if(document.getElementById(grupo).checked) {
							document.getElementById(grupo).checked = false;
						} else {
							document.getElementById(grupo).checked = true;
						}
					});

					function cerrar_popup() {
						$('.cd-popup').removeClass('is-visible');
					}


					function cantidad_notas(id_grupo) {
						var notas = $('#input_cantidad_notas_'+id_grupo).val();

						$('#input_id_grupo').val(id_grupo);
						$('#input_n_notas').val(notas);
						$('#popup_cantidad_notas').addClass('is-visible');
					}

					function enviar_n_notas() {
						var id_grupo = $('#input_id_grupo').val();
						var n_notas = $('#input_n_notas').val();

						$('#cantidad_notas_'+id_grupo).text(n_notas);
						$('#input_cantidad_notas_'+id_grupo).val(n_notas);
						cerrar_popup();
					}

					function enviar_guardar_cambios() {
						document.getElementById("formGuardarCambios").submit();
					}


					function GenerarJson() {
						$('#div_json').html("<b>Generando Json</b>, esperá unos segundos");
						$.ajax({
							url: "<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-noticias/ajax/generar-json-noticias.php",
							success: function (resultado) {
								$('#div_json').html(resultado);										
							}
						});	
					}
				</script>
			</body>
			</html>