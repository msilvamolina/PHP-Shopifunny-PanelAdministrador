<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');

$pagina= trim($_GET['pagina']);

$limite = 50;
$pagina_get = $pagina;
if(!$pagina_get) {
	$pagina_get=1;
}
if($pagina) {
	$pagina = $pagina-1;
}
$arranca = $pagina*$limite;

conectar2("wavi", "sitioweb");

//consultar en la base de datos
$query_rs_noticias = "SELECT noticias.id_noticia, noticias.noticia_titulo, noticias.noticia_publicada, noticias.fecha_publicacion, noticias.foto_portada, diarios.diario_nombre, diarios.diario_imagen FROM noticias, diarios WHERE noticias.id_diario = diarios.id_diario AND noticias.noticia_publicada = 1 AND noticias.id_grupo = 0 ORDER BY noticias.id_noticia DESC LIMIT $arranca,$limite";
$rs_noticias = mysql_query($query_rs_noticias)or die(mysql_error());
$row_rs_noticias = mysql_fetch_assoc($rs_noticias);
$totalrow_rs_noticias = mysql_num_rows($rs_noticias);

//consultar en la base de datos
$query_rs_noticias_agrupadas = "SELECT noticias.id_noticia, noticias.noticia_titulo, noticias.id_grupo, noticias.foto_portada, diarios.diario_nombre, diarios.diario_imagen  FROM noticias, diarios WHERE id_grupo > 0 AND noticias.id_diario = diarios.id_diario ORDER BY orden ASC";
$rs_noticias_agrupadas = mysql_query($query_rs_noticias_agrupadas)or die(mysql_error());
$row_rs_noticias_agrupadas = mysql_fetch_assoc($rs_noticias_agrupadas);
$totalrow_rs_noticias_agrupadas = mysql_num_rows($rs_noticias_agrupadas);

do {
	$id_noticia = $row_rs_noticias_agrupadas['id_noticia'];
	$noticia_titulo = $row_rs_noticias_agrupadas['noticia_titulo'];
	$id_grupo = $row_rs_noticias_agrupadas['id_grupo'];

	$foto_portada = $row_rs_noticias_agrupadas['foto_portada'];
	$diario_imagen = $row_rs_noticias_agrupadas['diario_imagen'];

	if(!$array_noticias_grupos[$id_grupo]) {
		$array_noticias_grupos[$id_grupo] = $id_noticia;	
	} else {
		$array_noticias_grupos[$id_grupo] .= '-'.$id_noticia;
	}
	
	$array_noticias_agrupadas[$id_noticia] = $noticia_titulo;
	$array_noticias_foto_portada[$id_noticia] = $foto_portada;
	$array_noticias_diario_imagen[$id_noticia] = $diario_imagen;

} while($row_rs_noticias_agrupadas = mysql_fetch_assoc($rs_noticias_agrupadas));

$pagina_actual_variables = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/wavi-noticias/04-noticias.php?';
if($usuario) {
	$pagina_actual_variables = $pagina_actual_variables.'usuario='.$usuario.'&';
}
if($q) {
	$pagina_actual_variables = $pagina_actual_variables.'q='.$q.'&';
}
$pagina_siguiente = $pagina+2;
$pagina_anterior = $pagina;
$disabled_siguiente = null;
$disabled_anterior = null;
$link_siguiente = $pagina_actual_variables.'pagina='.$pagina_siguiente;
$link_anterior = $pagina_actual_variables.'pagina='.$pagina_anterior;
if($pagina_anterior<=0) {
	$disabled_anterior = 'disabled';
	$link_anterior = null;
}

if(!$totalrow_rs_noticias) {
	$disabled_siguiente = 'disabled';
	$link_siguiente = null;
}
//consultar en la base de datos
$query_rs_fotos = "SELECT id_foto, recorte_foto_miniatura FROM fotos_publicaciones ";
$rs_fotos = mysql_query($query_rs_fotos)or die(mysql_error());
$row_rs_fotos = mysql_fetch_assoc($rs_fotos);
$totalrow_rs_fotos = mysql_num_rows($rs_fotos);

$ruta = $Servidor_url.'APLICACION/Imagenes/notas/recortes/';

do {
	$id_foto = $row_rs_fotos['id_foto'];
	$nombre_foto = $row_rs_fotos['recorte_foto_miniatura'];

	$array_fotos[$id_foto] = $ruta.$nombre_foto;
} while($row_rs_fotos = mysql_fetch_assoc($rs_fotos));


//consultar en la base de datos
$query_rs_grupo_noticias = "SELECT id_grupo, grupo_nombre FROM grupo_noticias ORDER BY orden ASC ";
$rs_grupo_noticias = mysql_query($query_rs_grupo_noticias)or die(mysql_error());
$row_rs_grupo_noticias = mysql_fetch_assoc($rs_grupo_noticias);
$totalrow_rs_grupo_noticias = mysql_num_rows($rs_grupo_noticias);

do {
	$id_grupo = $row_rs_grupo_noticias['id_grupo'];
	$grupo_nombre = $row_rs_grupo_noticias['grupo_nombre'];

	$array_grupos[$id_grupo] = $grupo_nombre;
} while ($row_rs_grupo_noticias = mysql_fetch_assoc($rs_grupo_noticias));

$array_grupos = array_filter($array_grupos);

desconectar();
?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->

	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/paginacion.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/checkbox/style.css?v=3"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/popup.css"> <!-- Resource style -->


	<style type="text/css">
		.rojo {
			color: #F44336;
			font-weight: bold;
		}

		.verde {
			color: #2E7D32;
			font-weight: bold;
		}

		.checkbox {
			z-index: 999;
		}
		h2 {
			font-size: 20px;
		}

		.input_popup {
			width: 100%;
			padding: 10px;
		}

		#agrupadas table td {
			background-color: #FFF9C4;
		}
		#agrupadas table .td_header {
			background-color: #FFC107;
			color: #fff;
		}
	</style>
</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<div class="cd-popup" id="popup_agregar_lista" role="alert">
			<div class="cd-popup-container">
				<p>Modificar grupo<br><br>
					<input class="input_popup" name="grupo_nombre" type="text" id="input_popup">
					<input name="id_grupo"  type="hidden" id="input_popup_id">
				</p>
				<ul class="cd-buttons" >
					<li><a onclick="enviar_editar_grupo()">Guardar</a></li>
					<li><a onclick="cerrar_popup()">Cancelar</a></li>
				</ul>
				<a href="#0" class="cd-popup-close img-replace"></a>
			</div> <!-- cd-popup-container -->
		</div> <!-- cd-popup -->
		<div class="cd-popup" id="popup_eliminar_grupo" role="alert">
			<div class="cd-popup-container">
				<p>¿Seguro querés eliminar este grupo?
					<div id="popup_nombre_grupo" style="color:red">Nombre del grupo</div><br><br>
					Se desvincularán todas las noticias vinculadas a este grupo
					<input type="hidden" id="input_popup_borrar_id">
				</p>
				<ul class="cd-buttons" >
					<li><a onclick="confirmar_eliminar_grupo()">Si, estoy seguro</a></li>
					<li><a onclick="cerrar_popup()">Cancelar</a></li>
				</ul>
				<a href="#0" class="cd-popup-close img-replace"></a>
			</div> <!-- cd-popup-container -->
		</div> <!-- cd-popup -->
		<div class="cd-popup" id="popup_cambiar_grupo" role="alert">
			<div class="cd-popup-container">
				<p><select class="input_popup" name="select_grupo_noticias" onchange="inspeccionar_select_popup()" class="budget" id="select_grupo_noticias_popup" required>	
					<option value="-1">Elegír un grupo de noticias</option>
					<option value="0">[+] Crear grupo</option>
					<?php foreach ($array_grupos as $id_grupo => $grupo_nombre) { ?>
					<option value="<?php echo $id_grupo; ?>"><?php echo $grupo_nombre; ?></option>
					<?php } ?>
				</select><br><br>
				<input type="hidden" id="input_popup_accion">
				<input class="input_popup" type="text" style="display:none" id="input_popup_cambiar_grupo">
			</p>
			<ul class="cd-buttons" >
				<li><a onclick="cambiar_vinculaciones()">Guardar</a></li>
				<li><a onclick="cerrar_popup()">Cancelar</a></li>
			</ul>
			<a href="#0" class="cd-popup-close img-replace"></a>
		</div> <!-- cd-popup-container -->
	</div> <!-- cd-popup -->
	<div class="cd-popup" id="popup_agregar_nuevo_grupo" role="alert">
		<div class="cd-popup-container">
			<p>¿Cómo se va a llamar?<br><br>
				<input class="input_popup" type="hidden" id="input_nuevo_grupo_noticia">
				<input class="input_popup" type="text" id="input_nuevo_grupo">
			</p>
			<ul class="cd-buttons" >
				<li><a onclick="confirmar_creacion_grupo()">Guardar</a></li>
				<li><a onclick="cerrar_popup()">Cancelar</a></li>
			</ul>
			<a href="#0" class="cd-popup-close img-replace"></a>
		</div> <!-- cd-popup-container -->
	</div> <!-- cd-popup -->
	<?php include('../../includes/barra-navegacion.php'); ?>
	<div class="content-wrapper" style="max-width:100%">
		<div class="cd-form floating-labels" style="max-width:100%">
			<section style="margin: 0 auto;">
				<form method="POST" id="formQuitarVinculaciones" action="<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/wavi-noticias/php/14-desagrupar-notas-db.php">
					<center>	
						<a class="vc_btn_largo vc_btn_amarillo vc_btn_3d" style="max-width:300px" onclick="quitar_vinculaciones()">
							<span class="fa-stack fa-lg pull-left">
								<i class="fa fa-circle fa-stack-2x"></i>
								<i class="fa fa-exclamation-circle fa-stack-1x fa-inverse"></i>
							</span>
							<p>Quitar Vinculaciones</p>
						</a>
						<a class="vc_btn_largo vc_btn_verde vc_btn_3d" style="max-width:300px" onclick="cambiar_grupo('desagrupar')">
							<span class="fa-stack fa-lg pull-left">
								<i class="fa fa-circle fa-stack-2x"></i>
								<i class="fa fa-exchange fa-stack-1x fa-inverse"></i>
							</span>
							<p>Cambiar de grupo</p>
						</a>
					</center>
					<input type="hidden" name="grupos_accion" id="grupos_accion" value="0" />
					<input type="hidden" name="grupos_id_grupo" id="grupos_id_grupo" value="0" />
					<input type="hidden" name="grupos_grupo_nombre" id="grupos_grupo_nombre" value="0" />
					<br>
					<section id="agrupadas">
						<table class="table table-striped" >
							<thead class="tabla_encabezado">
								<tr>
									<th><b>Elegir</b></th>
									<th><b>#</b></th>
									<th><b>Diario</b></th>
									<th><b>Portada</b></th>
									<th><b>Nombre</b></th>
								</tr>
							</thead>
							<tbody>
								<?php if($array_grupos) {
									foreach ($array_grupos as $id_grupo => $grupo_nombre) {  ?>
									<tr>
										<td class="td_header">
											<a onclick="editar_grupo(<?php echo $id_grupo; ?>, '<?php echo $grupo_nombre; ?>')">
												<span class="fa-stack fa-lg pull-left">
													<i style="color:#9C27B0" class="fa fa-circle fa-stack-2x"></i>
													<i class="fa fa-edit fa-stack-1x fa-inverse"></i>
												</a>
											</span></td>
											<td  class="td_header">
												<a onclick="borrar_grupo(<?php echo $id_grupo; ?>, '<?php echo $grupo_nombre; ?>')">
													<span class="fa-stack fa-lg pull-left">
														<i style="color:#E91E63" class="fa fa-circle fa-stack-2x"></i>
														<i class="fa fa-trash fa-stack-1x fa-inverse"></i>
													</a>
												</span></td>
												<td colspan="3"  class="td_header"><h2><?php echo $grupo_nombre; ?></h2></td></tr>
												<?php if($array_noticias_grupos[$id_grupo]) {
													$array = explode('-', $array_noticias_grupos[$id_grupo]);
													foreach ($array as $id_noticia) {
														$url_diario = $Servidor_url.'APLICACION/Imagenes/diarios/';

														$noticia_titulo = $array_noticias_agrupadas[$id_noticia];

														$foto_portada = $array_noticias_foto_portada[$id_noticia];
														$diario_imagen = $array_noticias_diario_imagen[$id_noticia];

														$imagen = $Servidor_url.'PANELADMINISTRADOR/img/icono-imagen.png';
														$nombre_imagen = '<span class="no_hay_imagen">(no hay imagen)</span>';

														if($foto_portada) {
															$imagen = $array_fotos[$foto_portada];
														}

														$super_class = null;
														if($total_categorias_vinculadas!=$total_categorias_subgrupo) {
															$super_class = 'categorias_con_subgrupos';
														}
														?>
														<tr class="<?php echo $super_class; ?>" data-href="noticia<?php echo $id_noticia; ?>">
															<td ><input type="checkbox" name="noticia[<?php echo $id_noticia; ?>]" id="noticia<?php echo $id_noticia; ?>" class="css-checkbox" /><label for="noticia<?php echo $id_noticia; ?>" class="css-label2"></label></td>

															<td><?php echo $id_noticia; ?></td>
															<td><img src="<?php echo $url_diario.$diario_imagen; ?>"  width="50"></td>
															<td><img src="<?php echo $imagen; ?>"  width="100"></td>
															<td><?php echo $noticia_titulo; ?></td>
														</tr>		
														<?php }}}} ?>
													</tbody>
												</table>	
											</form>
										</section>
										<?php if($totalrow_rs_noticias) { ?>
										<form method="POST" id="formAgruparNotas" action="<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/wavi-noticias/php/13-agrupar-notas-db.php">
											<input type="hidden" name="agrupar_nuevo_grupo_id_noticia" id="agrupar_nuevo_grupo_id_noticia" value="0" />
											<input type="hidden" name="agrupar_accion" id="agrupar_accion" value="0" />
											<input type="hidden" name="agrupar_id_grupo" id="agrupar_id_grupo" value="0" />
											<input type="hidden" name="agrupar_grupo_nombre" id="agrupar_grupo_nombre" value="0" />

											<br><br>
											<center>	
												<a class="vc_btn_largo vc_btn_rojo vc_btn_3d" style="max-width:300px" onclick="cambiar_grupo('agrupar')">
													<span class="fa-stack fa-lg pull-left">
														<i class="fa fa-circle fa-stack-2x"></i>
														<i class="fa fa-object-group fa-stack-1x fa-inverse"></i>
													</span>
													<p>Agrupar CheckBox</p>
												</a>
												<a class="vc_btn_largo vc_btn_verde vc_btn_3d" style="max-width:300px" onclick="guardar_selects()">
													<span class="fa-stack fa-lg pull-left">
														<i class="fa fa-circle fa-stack-2x"></i>
														<i class="fa fa-object-group fa-stack-1x fa-inverse"></i>
													</span>
													<p>Guardar Selects</p>
												</a>
											</center>
											<br>
											<table class="table table-striped">
												<thead class="tabla_encabezado">
													<tr>
														<th><b>Elegir</b></th>
														<th><b>#</b></th>
														<th><b>Diario</b></th>
														<th><b>Portada</b></th>
														<th><b>Nombre</b></th>
													</tr>
												</thead>
												<tbody>
													<?php 
													$url_diario = $Servidor_url.'APLICACION/Imagenes/diarios/';

													do {

														$id_noticia = $row_rs_noticias['id_noticia'];
														$noticia_titulo = $row_rs_noticias['noticia_titulo'];
														$noticia_publicada = $row_rs_noticias['noticia_publicada'];
														$fecha_publicacion = $row_rs_noticias['fecha_publicacion'];
														$foto_portada = $row_rs_noticias['foto_portada'];
														$diario_imagen = $row_rs_noticias['diario_imagen'];

														$imagen = $Servidor_url.'PANELADMINISTRADOR/img/icono-imagen.png';
														$nombre_imagen = '<span class="no_hay_imagen">(no hay imagen)</span>';

														if($foto_portada) {
															$imagen = $array_fotos[$foto_portada];
														}

														$super_class = null;
														if($total_categorias_vinculadas!=$total_categorias_subgrupo) {
															$super_class = 'categorias_con_subgrupos';
														}

														$array_ids_noticias[] = $id_noticia;
														?>
														<tr class="<?php echo $super_class; ?>" >
															<td data-href="noticia<?php echo $id_noticia; ?>"><input type="checkbox" name="noticia[<?php echo $id_noticia; ?>]" id="noticia<?php echo $id_noticia; ?>" class="css-checkbox" /><label for="noticia<?php echo $id_noticia; ?>" class="css-label"></label></td>
															<td data-href="noticia<?php echo $id_noticia; ?>"><?php echo $id_noticia; ?></td>
															<td data-href="noticia<?php echo $id_noticia; ?>"><img src="<?php echo $url_diario.$diario_imagen; ?>"  width="50"></td>
															<td data-href="noticia<?php echo $id_noticia; ?>"><img src="<?php echo $imagen; ?>"  width="100"></td>
															<td><?php echo $noticia_titulo; ?><br>
																<p><select onchange="inspeccionar_varios_selects(this.value, <?php echo $id_noticia;?>)" class="input_popup" name="select_grupo_individual[<?php echo $id_noticia;?>]">
																	<option value="-1">Elegír un grupo de noticias</option>
																	<option value="0">[+] Crear grupo</option>
																	<?php foreach ($array_grupos as $id_grupo => $grupo_nombre) { ?>
																	<option value="<?php echo $id_grupo; ?>"><?php echo $grupo_nombre; ?></option>
																	<?php } ?>
																</select>
															</td>														</tr>		
															<?php } while($row_rs_noticias = mysql_fetch_assoc($rs_noticias)); ?>	          	
														</tbody>
													</table>	
												</section>
											</form>	     
											<?php } else { ?>
											<p>No hay notas sin agrupar</p>
											<?php }?>           
										</div>
									</div> <!-- .content-wrapper -->
								</main> 
								<?php include('../../includes/pie-general.php');?>
								<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
								<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/popup.js"></script> <!-- Resource jQuery -->
								<script type="text/javascript">
									$('tr[data-href]').on("click", function() {
										var noticia = $(this).data('href');
										if(document.getElementById(noticia).checked) {
											document.getElementById(noticia).checked = false;
										} else {
											document.getElementById(noticia).checked = true;
										}
									});
									$('td[data-href]').on("click", function() {
										var noticia = $(this).data('href');
										if(document.getElementById(noticia).checked) {
											document.getElementById(noticia).checked = false;
										} else {
											document.getElementById(noticia).checked = true;
										}
									});

									function inspeccionar_select() {
										var select_grupo_noticias = $("#select_grupo_noticias").val();
										if(select_grupo_noticias==0) {
											$("#div_titulo").show();
										} else {
											$("#div_titulo").hide();
										}
									}

									function editar_grupo(id_grupo, grupo_nombre) {
										$("#input_popup").val(grupo_nombre);
										$("#input_popup_id").val(id_grupo);
										$('#popup_agregar_lista').addClass('is-visible');
									}
									function cerrar_popup() {
										$('.cd-popup').removeClass('is-visible');
									}

									function borrar_grupo(id_grupo, grupo_nombre) {
										$("#popup_nombre_grupo").text(grupo_nombre);
										$("#input_popup_borrar_id").val(id_grupo);										
										$('#popup_eliminar_grupo').addClass('is-visible');
									}

									function enviar_editar_grupo() {
										var id_grupo = document.getElementById("input_popup_id").value;
										var grupo_nombre = document.getElementById("input_popup").value;

										var sendInfo = {
											id_grupo: id_grupo,
											grupo_nombre: grupo_nombre
										};

										$.ajax({
											type: "POST",
											url: "<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/wavi-noticias/php/15-editar-grupo-db.php",
											success: function (resultado) {
												window.location.reload();			
											}, data: sendInfo
										});
									}

									function confirmar_eliminar_grupo() {
										var id_grupo = document.getElementById("input_popup_borrar_id").value;

										var sendInfo = {
											id_grupo: id_grupo,
										};

										$.ajax({
											type: "POST",
											url: "<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/wavi-noticias/php/16-borrar-grupo-db.php",
											success: function (resultado) {
												window.location.reload();			
											}, data: sendInfo
										});
									}

									function quitar_vinculaciones() {
										$("#grupos_accion").val("quitar");					
										$("#grupos_id_grupo").val("0");					
										$("#grupos_grupo_nombre").val("0");										
										document.getElementById("formQuitarVinculaciones").submit();
									}

									function cambiar_vinculaciones() {
										var id_grupo = $("#select_grupo_noticias_popup").val();
										var grupo_nombre = $("#input_popup_cambiar_grupo").val();
										var accion = $("#input_popup_accion").val();

										if(accion=="desagrupar") {
											$("#grupos_accion").val("cambiar");					
											$("#grupos_id_grupo").val(id_grupo);					
											$("#grupos_grupo_nombre").val(grupo_nombre);										
											document.getElementById("formQuitarVinculaciones").submit();
										} else if(accion=="agrupar") {
											$("#agrupar_id_grupo").val(id_grupo);					
											$("#agrupar_grupo_nombre").val(grupo_nombre);										
											document.getElementById("formAgruparNotas").submit();
										}

									}

									function cambiar_grupo(accion) {									
										$('#popup_cambiar_grupo').addClass('is-visible');
										$("#input_popup_accion").val(accion);
									}

									function inspeccionar_select_popup() {
										var select_grupo_noticias = $("#select_grupo_noticias_popup").val();
										if(select_grupo_noticias==0) {
											$("#input_popup_cambiar_grupo").show();
										} else {
											$("#input_popup_cambiar_grupo").hide();
											$("#input_popup_cambiar_grupo").val("");					
										}
									}

									function inspeccionar_varios_selects(valor, id_noticia) {
										if(valor==0) {
											$('#popup_agregar_nuevo_grupo').addClass('is-visible');
											$('#input_nuevo_grupo_noticia').val(id_noticia);
										}
									}

									function confirmar_creacion_grupo() {
										var grupo_nombre = $("#input_nuevo_grupo").val();
										var id_noticia = $("#input_nuevo_grupo_noticia").val();

										$('#agrupar_nuevo_grupo_id_noticia').val(id_noticia);
										$('#agrupar_accion').val("guardar_selects");
										$('#agrupar_grupo_nombre').val(grupo_nombre);
										document.getElementById("formAgruparNotas").submit();
									}

									function guardar_selects() {
										$('#agrupar_accion').val("guardar_selects");
										document.getElementById("formAgruparNotas").submit();
									}
								</script>
							</body>
							</html>