<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');

$noticia = trim($_GET['noticia']);
$imagen_cargada = trim($_GET['imagen_cargada']);

if(!$noticia) {
	$redirigir = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/notas/00-cargar-nota.php';
	header('location:'.$redirigir);
	exit;
}

conectar2('moebius', "ProyectoMoebius");

$query_rs_imagen = "SELECT id_foto, nombre_foto, fecha_carga, recorte_foto_nombre, recorte_foto_miniatura FROM fotos_publicaciones WHERE id_publicacion = $noticia ORDER BY id_foto DESC ";
$rs_imagen = mysql_query($query_rs_imagen)or die(mysql_error());
$row_rs_imagen = mysql_fetch_assoc($rs_imagen);
$totalrow_rs_imagen = mysql_num_rows($rs_imagen);

if($imagen_cargada) {
	$id_foto = $row_rs_imagen['id_foto'];
	$redireccionar = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/notas/03-recortar-foto.php?foto='.$id_foto.'&noticia='.$noticia;
	header('location:'.$redireccionar);
	exit;
}
//consultar en la base de datos
$query_rs_noticias = "SELECT noticia_cartucho, noticia_titulo, noticia_bajada, fecha_noticia, noticia_palabras_claves FROM noticias WHERE id_noticia = $noticia";
$rs_noticias = mysql_query($query_rs_noticias)or die(mysql_error());
$row_rs_noticias = mysql_fetch_assoc($rs_noticias);
$totalrow_rs_noticias = mysql_num_rows($rs_noticias);

$noticia_cartucho = $row_rs_noticias['noticia_cartucho'];
$noticia_titulo = $row_rs_noticias['noticia_titulo'];
$noticia_bajada = $row_rs_noticias['noticia_bajada'];
$noticia_palabras_claves = $row_rs_noticias['noticia_palabras_claves'];
$fecha_noticia = $row_rs_noticias['fecha_noticia'];
$fecha_noticia_value = $fecha_noticia;

$check_atemporal = NULL;
$fecha_noticia_style = NULL;

if($fecha_noticia=='0000-00-00') {
	$check_atemporal = 'checked';
	$fecha_noticia_style = 'display:none';
	$fecha_noticia_value = date('Y-m-d');
}
//consultar en la base de datos
$query_rs_cuerpo = "SELECT id_cuerpo, orden, tamano_texto, cuerpo_tipo, contenido FROM noticias_cuerpo WHERE id_noticia = $noticia ORDER BY orden ASC ";
$rs_cuerpo = mysql_query($query_rs_cuerpo)or die(mysql_error());
$row_rs_cuerpo = mysql_fetch_assoc($rs_cuerpo);
$totalrow_rs_cuerpo = mysql_num_rows($rs_cuerpo);

//consultar en la base de datos
$query_rs_imagenes = "SELECT id_foto, recorte_foto_nombre FROM fotos_publicaciones WHERE id_publicacion = $noticia ";
$rs_imagenes = mysql_query($query_rs_imagenes)or die(mysql_error());
$row_rs_imagenes = mysql_fetch_assoc($rs_imagenes);
$totalrow_rs_imagenes = mysql_num_rows($rs_imagenes);

do {
	$id_foto = $row_rs_imagenes['id_foto'];
	$recorte_foto_nombre = $row_rs_imagenes['recorte_foto_nombre'];

	$array_imagenes[$id_foto] = $recorte_foto_nombre;
} while($row_rs_imagenes = mysql_fetch_assoc($rs_imagenes));

desconectar();

?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form3.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/popup.css"> <!-- Resource style -->
	<script src="<?php echo $Servidor_url;?>PANELADMINISTRADOR/js/nicEdit2/nicEdit.js" type="text/javascript"></script>

</head>
<style type="text/css">
	.btn_eliminar {
		text-align: right;
		width: 100%;
	}

	a {
		cursor: pointer;
	}
	.boton_verde a{
		background: #48b617;
		color: #fff;
	}
	.boton_verde a:hover {
		background: #235d09 !important;
		color: #f6ff05;
	}	
	.boton_rojo a{
		background: #c40000;
		color: #fff;
	}
	.boton_rojo a:hover {
		background: #9e0101 !important;
		color: #f6ff05;
	}
	h3 {
		margin-bottom: 5px;
		font-weight: bold;
	}

	.portada {
		color: #2E7D32;
		font-weight: bold;
	}

	.rojo {
		color: #F44336;
		font-weight: bold;
	}

	.verde {
		color: #2E7D32;
		font-weight: bold;
	}
	a {
		cursor: pointer;
	}
	.input_video {
		width: 100%;
		padding: 10px;
		margin-top: 15px;
	}

	.input_texto {
		width: 100%;
		height: 200px;
		padding: 10px;
		margin-top: 15px;
	}


	.video-container {
		position: relative;
		padding-bottom: 56.25%;
		padding-top: 30px; height: 0; overflow: hidden;
	}
	
	.video-container iframe,
	.video-container object,
	.video-container embed {
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
	}
	.video_youtube {
		width: 100%;
	}

	#formTexto {
		margin-top: -150px;
		padding: 20px;
	}	

	.img_delete {
		width: 30px;
	}

	.td_delete {
		padding: 10px;
		text-align: right;
		width: 30px;
	}
	.td_delete img {
		width: 30px;
		display: block;
	}
	.txt_categoria_elegida {
		padding: 10px;
		color: #2c97de;
		display: block;
	}
	.grupo_categoria {
		margin-left: -20px!important;
	}

	#section_categoria {
		background: #a7a7a7;
		padding: 30px;
		color: #fff;
	}

	#section_categoria h3 {
		font-size: 24px;
	}
	.select_class {
		background: #eeeeee !important;
	}
	.delete_categoria {
		float: right;
	}
	.delete_categoria img{
		width: 35px;
		margin-top: -5px;
	}
	.boton_amarillo {
		background: #f90;
	}
	.boton_amarillo:hover {
		background: red;
		color: #fff;
	}
</style>
</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper">
			<div class="cd-popup" id="popup_agregar" role="alert">
				<div class="cd-popup-container">
					<p>¿Qué elemento querés agregar?</p>
					
					<div style="padding:10px">
						<a class="vc_btn_largo vc_btn_verde vc_btn_3d" onclick="agregar_texto()">
							<span class="fa-stack fa-lg pull-left">
								<i class="fa fa-circle fa-stack-2x"></i>
								<i class="fa fa-plus fa-stack-1x fa-inverse"></i>
							</span>
							<b>Agregar Texto</b>
						</a>
						
						<a class="vc_btn_largo vc_btn_amarillo vc_btn_3d" onclick="agregar_imagen()">
							<span class="fa-stack fa-lg pull-left">
								<i class="fa fa-circle fa-stack-2x"></i>
								<i class="fa fa-camera fa-stack-1x fa-inverse"></i>
							</span>
							<b>Agregar Imagen</b>
						</a>
						<a class="vc_btn_largo vc_btn_rojo vc_btn_3d" onclick="agregar_video()">
							<span class="fa-stack fa-lg pull-left">
								<i class="fa fa-circle fa-stack-2x"></i>
								<i class="fa fa-youtube-play fa-stack-1x fa-inverse"></i>
							</span>
							<b>Agregar Video de youtube</b>
						</a>
					</div>
					<a href="#0" class="cd-popup-close img-replace"></a>
				</div> <!-- cd-popup-container -->
			</div> <!-- cd-popup -->
			<div class="cd-popup" id="popup_video" role="alert">
				<div class="cd-popup-container">
					<p>Copiá y pegá acá el link de tu video<br>
						<input class="input_video" type="text" id="input_video" placeholder="Ej: https://www.youtube.com/watch?v=yH2HIPOb-cE" /></p>
						<ul class="cd-buttons">
							<li id="btn_confirmar_noticia"><a onclick="confirmar_video_youtube()">Continuar</a></li>
							<li><a onclick="cerrar_popup()">Cancelar</a></li>
						</ul>
						<a href="#0" class="cd-popup-close img-replace"></a>
					</div> <!-- cd-popup-container -->
				</div> <!-- cd-popup -->

				<div class="cd-popup" id="popup_borrar_elemento" role="alert">
					<div class="cd-popup-container">
						<p>¿Seguro querés borrar este elemento?<br>
							<ul class="cd-buttons">
								<li id="btn_confirmar_borrado_elemento"><a>Si</a></li>
								<li><a onclick="cerrar_popup()">Cancelar</a></li>
							</ul>
							<a href="#0" class="cd-popup-close img-replace"></a>
						</div> <!-- cd-popup-container -->
					</div> <!-- cd-popup -->
					<div class="cd-popup" id="popup_direccion" role="alert">
						<div class="cd-popup-container">
							<p>¿Estás seguro de querer borrar esta dirección?</p>
							<ul class="cd-buttons">
								<li id="btn_confirmar_direccion"></li>
								<li><a onclick="cerrar_popup()">No</a></li>
							</ul>
							<a href="#0" class="cd-popup-close img-replace"></a>
						</div> <!-- cd-popup-container -->
					</div> <!-- cd-popup -->
					<div class="contenedor">

						<div >					<!-- Contenido de la Pagina-->	

							<div class="cd-form floating-labels">
								<section id="crear_categoria" >							
									<fieldset >
										<form onsubmit="return validar_formulario()" id="myForm" action="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/notas/php/01-editar-nota-db.php" method="POST">
											<input type="hidden" name="accion" id="accion" />
											<input type="hidden" name="accionDato" id="accionDato" />

											<a href="<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/notas/02-ficha-nota.php?noticia=<?php echo $noticia;?>" class="vc_btn_largo vc_btn_rojo vc_btn_3d" style="width:250px;float:right">
												<span class="fa-stack fa-lg pull-left">
													<i class="fa fa-circle fa-stack-2x"></i>
													<i class="fa fa-calendar-o fa-stack-1x fa-inverse"></i>
												</span>
												<b>Ficha de la noticia</b>
											</a>
											<legend id="txt_nueva_categoria">Editar Nota</legend>
											<div class="icon">
												<label class="cd-label" for="cd-company">Cartucho</label>
												<input class="company" type="text" name="cartucho" value="<?php echo $noticia_cartucho; ?>" id="titulo" required>
											</div> 			    
											<div class="icon">
												<label class="cd-label" for="cd-company">Titulo</label>
												<input class="company" type="text" name="titulo" value="<?php echo $noticia_titulo; ?>" id="titulo" required>
											</div> 			    
											<div class="icon">
												<label class="cd-label" for="cd-textarea">Bajada</label>
												<textarea class="message" required name="bajada" id="bajada" ><?php echo $noticia_bajada; ?></textarea>
											</div>
											<div class="icon">
												<label class="cd-label" for="cd-company">Palabras claves</label>
												<input class="company" type="text" value="<?php echo $noticia_palabras_claves; ?>" name="palabras_claves" id="palabras_claves" required>
											</div> 									    
											<div class="icon" id="fecha_noticia" style="<?php echo $fecha_noticia_style; ?>">
												<label class="cd-label" for="cd-company">Fecha noticia</label>
												<input class="company" type="date" value="<?php echo $fecha_noticia_value;?>" step="1" name="fecha_noticia" id="fecha_noticia" required>
											</div>

											<input  type="hidden" id="elementos_contador" value="1">
											<input  type="hidden" id="noticia" name="noticia" value="<?php echo $noticia; ?>">
											<br>												
											<div id="cuerpo">
												<h3>Cuerpo:</h3>
												<table class="table table-striped">
													<tbody>
														<?php
														$ruta_img = $Servidor_url.'APLICACION/Imagenes/notas/recortes/';
														$i=1;
														do {
															$id_cuerpo = $row_rs_cuerpo['id_cuerpo'];
															$cuerpo_tipo = $row_rs_cuerpo['cuerpo_tipo'];
															$contenido = $row_rs_cuerpo['contenido'];

															$orden = $row_rs_cuerpo['orden'];

															if($cuerpo_tipo=="imagen") {
																$imagen = $ruta_img.$array_imagenes[$contenido];
																?>
																<input type="hidden" name="elemento[<?php echo $id_cuerpo;?>]" value="<?php echo $contenido; ?>" >
																<tr>
																	<td width="60"><input type="text" name="orden[<?php echo $id_cuerpo;?>]"  maxlength="2" value="<?php echo $i; ?>"></td>
																	<td><img src="<?php echo $imagen; ?>" width="100%"></td>
																	<td><a onclick="borrar_elemento(<?php echo $id_cuerpo; ?>)" ><img src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/img/delete.png" class="img_delete"></a></td>
																</tr>
																<?php
															}

															if($cuerpo_tipo=="texto") {
																?>
																<tr>
																	<td width="60"><input type="text" name="orden[<?php echo $id_cuerpo;?>]" maxlength="2" value="<?php echo $i; ?>">
																	</td>

																	<td>
																		<textarea name="elemento[<?php echo $id_cuerpo;?>]" id="area<?php echo $i;?>"><?php echo $contenido; ?></textarea>
																		<a onclick="editar_html(<?php echo $id_cuerpo; ?>)">Editar HTML del texto</a>
																		<br><br>
																	</td>
																	<td><a onclick="borrar_elemento(<?php echo $id_cuerpo; ?>)" ><img src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/img/delete.png" class="img_delete"></a></td>
																	<script type="text/javascript">
																		bkLib.onDomLoaded(function() {
																			new nicEditor({fullPanel : true}).panelInstance('area<?php echo $i;?>');
																		});
																	</script>
																</tr>	  
																<?php }

																if($cuerpo_tipo=="video") { ?>
																<input type="hidden" name="elemento[<?php echo $id_cuerpo;?>]" value="<?php echo $contenido; ?>" >
																<tr>
																	<td width="60"><input type="text" name="orden[<?php echo $id_cuerpo;?>]" maxlength="2" value="<?php echo $i; ?>"></td>
																	<td>
																		<div class="video_youtube">
																			<div class="video-container">
																				<iframe src="https://www.youtube.com/embed/<?php echo $contenido;?>" frameborder="0" ></iframe>
																			</div>
																		</div>
																	</td>
																	<td width="60"><a onclick="borrar_elemento(<?php echo $id_cuerpo; ?>)" ><img src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/img/delete.png" class="img_delete"></a></td>
																</tr>


																<?php }
																$i++;
															} while($row_rs_cuerpo = mysql_fetch_assoc($rs_cuerpo));
															?>

														</tbody>
													</table>
												</div>
												<img src="" id="imagen"/>
												<a class="vc_btn_largo vc_btn_verde vc_btn_3d" onclick="agregar_elemento()">
													<span class="fa-stack fa-lg pull-left">
														<i class="fa fa-circle fa-stack-2x"></i>
														<i class="fa fa-plus fa-stack-1x fa-inverse"></i>
													</span>
													<p>Agregar elemento al cuerpo</p>
												</a>
												<br><br>

											</fieldset>	
										</section>    	

										<a name="botonGuardar"></a>
										<div class="alinear_centro">
											<input type="submit" value="Guardar" id="btn_nueva_categoria">
										</div>
									</form>
								</div>
							</div> <!-- .content-wrapper -->
						</main> 
						<?php include('../../includes/pie-general.php');?>
						<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
						<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/popup.js"></script> <!-- Resource jQuery -->
						<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/jquery.ddslick.min.js"></script> <!-- Resource jQuery -->

						<script type="text/javascript">
							function agregar_elemento() {
								$('#popup_agregar').addClass('is-visible');
							}
							function cerrar_popup() {
								$('.cd-popup').removeClass('is-visible');
							}

							function eliminar_elemento(elemento) {
								$('#elemento_'+elemento).html('');
							}

							function agregar_imagen() {
								var elemento = document.getElementById("elementos_contador").value;

								cerrar_popup();	
								window.open('<?php echo $Servidor_url;?>PANELADMINISTRADOR//00-barra-navegacion/notas/popUps/01-cargar-imagen.php?noticia=<?php echo $noticia; ?>&id_administrador=<?php echo $id_administrador;?>','popup','width=400,height=400');

								document.getElementById("elementos_contador").value = parseInt(elemento)+1;		
							}

							function agregar_video(){
								$('.cd-popup').removeClass('is-visible');
								$('#popup_video').addClass('is-visible');
							}

							function agregar_texto(){
								$('#accion').val("AgregarTexto");
								document.getElementById("myForm").submit();
							}

							function imagen_lista() {
								alert('imagen cargada :D');
							}

							function confirmar_video_youtube() {
								var video = document.getElementById("input_video").value;
								$('#accion').val("AgregarVideo");
								$('#accionDato').val(video);

								document.getElementById("myForm").submit();
							}

		
							function borrar_elemento(elemento) {
								$('#popup_borrar_elemento').addClass('is-visible');

								$('#btn_confirmar_borrado_elemento').html('<a onclick="confirmar_borrado_elemento('+elemento+')">Sí</a>');
							}

							function confirmar_borrado_elemento(elemento) {
								$('#accion').val("BorrarElemento");
								$('#accionDato').val(elemento);

								document.getElementById("myForm").submit();
							}

							function editar_html(cuerpo) {
								window.open('<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/notas/popUps/editar-html-cuerpo.php?cuerpo='+cuerpo,'popup','width=800,height=600');
							}
						</script>
					</body>
					</html>