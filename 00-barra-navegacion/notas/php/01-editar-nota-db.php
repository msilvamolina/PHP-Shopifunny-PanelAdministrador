<?php include('../../../../paginas_include/variables-generales.php'); 
include('../../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../../php/verificar-permisos.php');

$accion = trim($_POST['accion']);
$accionDato = trim($_POST['accionDato']);

$noticia = trim($_POST['noticia']);
$cartucho = trim($_POST['cartucho']);
$titulo = trim($_POST['titulo']);
$bajada = trim($_POST['bajada']);
$palabras_claves = trim($_POST['palabras_claves']);

$fecha_noticia = trim($_POST['fecha_noticia']);

//arrays
$elemento = $_POST['elemento'];
$orden = $_POST['orden'];
$tamano_texto = $_POST['tamano_texto'];

$usuario_que_crea = $id_administrador;

if(!$noticia) {
	$redireccion = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/notas/00-cargar-nota.php';
	header('Location: '.$redireccion);
	exit;
}


$cartucho = arreglar_datos_db($cartucho);
$titulo = arreglar_datos_db($titulo);
$bajada = arreglar_datos_db($bajada);
$palabras_claves = arreglar_datos_db($palabras_claves);

conectar2('moebius', "ProyectoMoebius");
$fecha_actual = date('Y-m-d H:i:s');
mysql_query("UPDATE noticias SET noticia_cartucho='$cartucho',noticia_titulo='$titulo', noticia_bajada='$bajada',noticia_palabras_claves='$palabras_claves', fecha_noticia='$fecha_noticia', fecha_modificacion='$fecha_actual',usuario_que_modifica='$id_administrador', ip_visitante_modificacion='$ip_visitante'
	WHERE id_noticia='$noticia'");

foreach ($elemento as $id_elemento => $contenido) {
	if($id_elemento) {
		$elemento_orden = $orden[$id_elemento];

		$tamano = 0;
		if($tamano_texto[$id_elemento]) {
			$tamano = $tamano_texto[$id_elemento];
		}

		mysql_query("UPDATE noticias_cuerpo SET orden='$elemento_orden', tamano_texto='$tamano', contenido='$contenido' WHERE id_cuerpo='$id_elemento'");
	}
}

//borramos las direcciones que borro el usuario
//cargamos las direcciones

$redireccion = $Servidor_url.'PANELADMINISTRADOR//00-barra-navegacion/notas/01-editar-nota.php?noticia='.$noticia."#botonGuardar";


if($accion) {
	if($accion=="AgregarTexto") {
		$cuerpo_tipo = "texto";

		$query_rs_consulta = "SELECT orden FROM noticias_cuerpo WHERE id_noticia = $noticia ORDER BY orden DESC LIMIT 1";
		$rs_consulta = mysql_query($query_rs_consulta)or die(mysql_error());
		$row_rs_consulta = mysql_fetch_assoc($rs_consulta);
		$totalrow_rs_consulta = mysql_num_rows($rs_consulta);

		$orden = 1;
		if($totalrow_rs_consulta) {
			$orden = $row_rs_consulta['orden'];
			$orden++;
		}

		$query = "INSERT INTO noticias_cuerpo (id_noticia, orden, cuerpo_tipo, usuario_que_carga, ip_visitante) 
		VALUES ('$noticia','$orden','$cuerpo_tipo','$id_administrador', '$ip_visitante')";
		$result = mysql_query($query);
	}
	if($accion=="BorrarElemento") {
		$elemento = $accionDato;
		if($elemento) {

			$query_rs_elemento = "SELECT cuerpo_tipo, contenido FROM noticias_cuerpo WHERE id_cuerpo = $elemento AND id_noticia = $noticia";
			$rs_elemento = mysql_query($query_rs_elemento)or die(mysql_error());
			$row_rs_elemento = mysql_fetch_assoc($rs_elemento);
			$totalrow_rs_elemento = mysql_num_rows($rs_elemento);

			$cuerpo_tipo = $row_rs_elemento['cuerpo_tipo'];
			$contenido = $row_rs_elemento['contenido'];

			mysql_query("DELETE FROM noticias_cuerpo WHERE id_cuerpo=$elemento");

			if($cuerpo_tipo=='imagen') {
				$foto = $contenido;
		//consultar en la base de datos
				$query_rs_imagen = "SELECT nombre_foto, recorte_foto_nombre, recorte_foto_miniatura FROM fotos_publicaciones WHERE id_foto = $foto";
				$rs_imagen = mysql_query($query_rs_imagen)or die(mysql_error());
				$row_rs_imagen = mysql_fetch_assoc($rs_imagen);
				$totalrow_rs_imagen = mysql_num_rows($rs_imagen);

				$nombre_foto = $row_rs_imagen['nombre_foto'];
				$recorte_foto_nombre = $row_rs_imagen['recorte_foto_nombre'];
				$recorte_foto_miniatura = $row_rs_imagen['recorte_foto_miniatura'];


				$storeFolder = '../../../../APLICACION/Imagenes/notas/';
				$storeFolder2 = $storeFolder.'recortes/';

				unlink($storeFolder.$nombre_foto);
				unlink($storeFolder2.$recorte_foto_nombre);
				unlink($storeFolder2.$recorte_foto_miniatura);

				mysql_query("DELETE FROM fotos_publicaciones WHERE id_foto=$foto");
			}
		}
	}
	if($accion=="AgregarVideo") {
		$video = $accionDato;
		if($video) {
			$explorar = explode('v=', $video);
			$explorar2 = explode('&', $explorar[1]);

			$video = $explorar2[0];

			$cuerpo_tipo = "video";

			$query_rs_consulta = "SELECT orden FROM noticias_cuerpo WHERE id_noticia = $noticia ORDER BY orden DESC LIMIT 1";
			$rs_consulta = mysql_query($query_rs_consulta)or die(mysql_error());
			$row_rs_consulta = mysql_fetch_assoc($rs_consulta);
			$totalrow_rs_consulta = mysql_num_rows($rs_consulta);

			$orden = 1;
			if($totalrow_rs_consulta) {
				$orden = $row_rs_consulta['orden'];
				$orden++;
			}

			$query = "INSERT INTO noticias_cuerpo (id_noticia, orden, cuerpo_tipo, contenido, usuario_que_carga, ip_visitante) 
			VALUES ('$noticia','$orden','$cuerpo_tipo','$video','$id_administrador', '$ip_visitante')";
			$result = mysql_query($query);
		}
	}
}

desconectar();

header('Location: '.$redireccion);
exit;

?>