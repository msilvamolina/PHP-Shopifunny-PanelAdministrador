<!DOCTYPE html>
<html>
<head>
<html lang="es" class="no-js">
<head>
	<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>sistemaV3/css/dropzone.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>sistemaV3/css/dropzone2.css"> <!-- Resource style -->	
	<script src="<?php echo $Servidor_url; ?>sistemaV3/js/dropzone.js"></script>
</head>
<body>
<center>
	<div id="imagen_cargando" style="display:none;margin-top:40px">
			<img src="https://www.paradigmapublicidad.com/sistemaV3/img/loader.gif">
	</div>
</center>
<div  id="contenedor_drag_drop">
		<div id="dropzone"><form action="http://www.wavi.com.ar/sistemaParadigma/php/01-cargar-imagen-db.php" class="dropzone needsclick" id="myAwesomeDropzone">
	  <input type="hidden" name="codigo" value="<?php echo $codigo; ?>">

	  <div class="fallback" style="display:none">
    <input name="file" type="file" multiple />
  </div>
	  <div class="dz-message needsclick">
	  	<img src="https://www.paradigmapublicidad.com/sistemaV3/img/icono-drag-drop.png" class="icono_drag_drop">
	    Arrastrá la imagen a este recuadro para subirla.
	  </div>
</form></div>
<script src="jquery/jquery-2.1.4.js"></script>

<script type="text/javascript">
	Dropzone.options.myAwesomeDropzone = {
  init: function() {
    this.on("complete", function(file) {
     	imagen_cargada();
     });
  },
	accept: function(file, done) {
	    if ((file.type != "image/jpeg") && (file.type != "image/png")){
	        alert("Error! Solo podés cargar imágenes JPG y PNG");
	        this.removeAllFiles();
	    } else { done(); }
	}
};

function imagen_cargada() {
	$('#contenedor_drag_drop').hide();
	$('#imagen_cargando').show();
	$.ajax({
		url: "02-mostrar-imagen-noticia.php?codigo=<?php echo $codigo;?>",
		success: function (resultado) {
			$('#imagen_cargando').html(resultado);
			//refrescamos para que se borren los temporales
		    setInterval( function() {
				borrar_temporales();	
			}, 1500 );
		}
	});	
}

function borrar_temporales() {
	$.ajax({
		url: "php/02-borrar-temporales.php?noticia=<?php echo $noticia;?>",
		success: function (resultado) {
			//$('#imagen_cargando').html(resultado);
			parent.opener.location = 'https://www.paradigmapublicidad.com/sistemaV3/00-barra-navegacion/libres-del-sur/02-ficha-nota.php?noticia=<?php echo $noticia; ?>&imagen_cargada=1';
			self.close();
		}
	});	
}

</script>
</body>
</html>