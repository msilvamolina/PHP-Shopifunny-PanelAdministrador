<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');

$arranca = $pagina*$limite;

conectar2("wavi", "sitioweb");

//consultar en la base de datos
$query_rs_grupo_noticias = "SELECT id_grupo, grupo_nombre FROM grupo_noticias ORDER BY orden ASC ";
$rs_grupo_noticias = mysql_query($query_rs_grupo_noticias)or die(mysql_error());
$row_rs_grupo_noticias = mysql_fetch_assoc($rs_grupo_noticias);
$totalrow_rs_grupo_noticias = mysql_num_rows($rs_grupo_noticias);

do {
	$id_grupo = $row_rs_grupo_noticias['id_grupo'];
	$grupo_nombre = $row_rs_grupo_noticias['grupo_nombre'];

	$array_grupos[$id_grupo] = $grupo_nombre;
} while ($row_rs_grupo_noticias = mysql_fetch_assoc($rs_grupo_noticias));

$array_grupos = array_filter($array_grupos);

desconectar();
?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<link rel='stylesheet' href='<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/drag/styles.css' type='text/css' media='all' />
	<?php include('../../includes/head-general.php'); ?>

	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->

	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/paginacion.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/checkbox/style.css?v=3"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/popup.css"> <!-- Resource style -->
	<link href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/drag/jquery-ui.css" rel="stylesheet">
	<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/drag/jquery-1.7.2.min.js"></script>
	<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/drag/jquery-ui.min.js"></script>
	<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/drag/jquery.ui.touch-punch.min.js"></script>

	<script type="text/javascript">

		$(function() {
			$( "#test-list" ).sortable({
				placeholder: "ui-state-highlight",
				opacity: 0.6,
				update: function(event, ui) {
					document.getElementById("boton_submit").disabled = true;
					$('#boton_submit').addClass('boton_trabajando');

					var order = $('#test-list').sortable('serialize');
					var sendInfo = {
						order: order,
					};

					$.ajax({
						type: "POST",
						url: "<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/wavi-noticias/ajax/01-guardar-orden-noticias-db.php",
						success: function (resultado) {
							$('#input_resultado').val(resultado);
							document.getElementById("boton_submit").disabled = false;
							$('#boton_submit').removeClass('boton_trabajando');
						}, data: sendInfo
					});

				}
			});
			$( "#test-list" ).disableSelection(); 

			$( "#test-list" ).sortable({
				cancel: ".ui-state-disabled"
			});

		});

	</script>
	<style type="text/css">
		.rojo {
			color: #F44336;
			font-weight: bold;
		}

		.verde {
			color: #2E7D32;
			font-weight: bold;
		}

		.checkbox {
			z-index: 999;
		}
		h2 {
			font-size: 20px;
		}

		.input_popup {
			width: 100%;
			padding: 10px;
		}

		#agrupadas table td {
			background-color: #FFF9C4;
		}
		#agrupadas table .td_header {
			background-color: #FFC107;
			color: #fff;
		}

		.noticias_principales {
			background:#FC0;
			padding:20px;
			padding-top:1px;
		}


		.noticias_varias {
			background:#ccc;
			padding:20px;
			margin-top:20px;
			padding-top:1px;	
		}

		.appendoButtons{
			font-size:26px;
		}
		#sortable { list-style-type: none; margin: 0; padding: 0; width: 60%; }
		#sortable li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1.4em; height: 18px; }
		#sortable li span { position: absolute; margin-left: -1.3em; }

		.botones_noticias a{
			background:#F30;
			color:#fff;
			padding:10px;
			font-weight:bold;
			text-decoration:none;
		}

		.botones_noticias a:hover{
			background:#606;
		}

		.lista_header {
			background:#f90!important; 
			color:#fff;
		}

		.boton_submit {

		}

		#contenedor_boton {
			width: 100%;
			text-align: right;
		}
		.clear {
			float: none;
			clear: both;
		}

	</style>
</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper" style="max-width:100%">
			<div class="cd-form floating-labels" style="max-width:100%">
				<div id="resultado">
					<form method="POST" id="formQuitarVinculaciones" action="<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/wavi-noticias/php/17-guardar-orden-grupo-db.php">
						<input type="hidden" required value="" name="orden" id="input_resultado">
						<div id="contenedor_boton">
							<input type="submit" value="Guardar orden" id="boton_submit" class="boton_submit">
						</div>
					</form>
				</div>
				<?php if($_GET['ok']) { ?>
				<div class="alert alert-success" role="alert">
					Los cambios se guardaron correctamente a las <?php echo date('H:i:s'); ?>
				</div>
				<?php } ?>
				<ul id="test-list">

					<?php foreach ($array_grupos as $id_grupo => $grupo_nombre) { ?>
					<li id="listItem_GRUPO<?php echo $id_grupo; ?>" class="lista_header" >
					<img src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/drag/arrow.png" alt="move" width="16" height="16" class="handle" />
					<strong><?php echo $grupo_nombre; ?></strong></li>
					<?php } ?>
				</ul>
			</div> <!-- .content-wrapper -->
		</main> 
		<script type="text/javascript">
			document.getElementById("boton_submit").disabled = true;
			$('#boton_submit').addClass('boton_trabajando');
		</script>
	</body>
	</html>