<?php include('../../../../paginas_include/variables-generales.php'); 
include('../../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../../php/verificar-permisos.php');

$order = trim($_POST['order']);
$response = null;

if($order) {
	$array = explode('listItem[]=', $order);
	foreach ($array as $valor) {
		$valor = str_replace('&', '', $valor);
		if(!$response) {
			$response = $valor;
		} else {
			$response .= '-'.$valor;
		}
	}
}

echo $response;

?>