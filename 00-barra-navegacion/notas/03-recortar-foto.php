<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');

$id_foto = trim($_GET['foto']);
$id_noticia = $_GET['noticia'];
$sala = $_GET['sala'];
$evento = $_GET['evento'];

$reemplazar = $_GET['reemplazar'];
$origen = $_GET['origen'];

conectar2('moebius', "ProyectoMoebius");

if($id_foto) {
	$query_rs_noticia = "SELECT recorte_foto_nombre, recorte_foto_miniatura, nombre_foto, recorte_foto_x, recorte_foto_y, recorte_foto_w, recorte_foto_h  FROM fotos_publicaciones WHERE id_foto = $id_foto";
	$rs_noticia = mysql_query($query_rs_noticia)or die(mysql_error());
	$row_rs_noticia = mysql_fetch_assoc($rs_noticia);
	$totalrow_rs_noticia = mysql_num_rows($rs_noticia);
	$recorte_anterior = $row_rs_noticia['recorte_foto_nombre'];
	$recorte_anterior_miniatura = $row_rs_noticia['recorte_foto_miniatura'];
	desconectar();
	$foto_seleccionada = $row_rs_noticia['nombre_foto'];
} else {
	$redirigir = $Servidor_url.'PANELADMINISTRADOR/error';
	header('location:'.$redirigir);
	exit;
}

$url_imagenes = $Servidor_url.'APLICACION/Imagenes/notas/';

if($sala) {
	$url_imagenes = $Servidor_url.'APLICACION/Imagenes/salas/';
}

if($evento) {
	$url_imagenes = $Servidor_url.'APLICACION/Imagenes/eventos/';
}
?>
<!doctype html>
<html lang="es" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>

	<script src="<?php echo $Servidor_url;?>PANELADMINISTRADOR/js/jcrop/js/jquery.min.js"></script>

	<script src="<?php echo $Servidor_url;?>PANELADMINISTRADOR/js/jcrop/js/jquery.Jcrop.js"></script>
	<link rel="stylesheet" href="<?php echo $Servidor_url;?>PANELADMINISTRADOR/js/jcrop/demo_files/main.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $Servidor_url;?>PANELADMINISTRADOR/js/jcrop/demo_files/demos.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $Servidor_url;?>PANELADMINISTRADOR/js/jcrop/css/jquery.Jcrop.css" type="text/css" />
	<style type="text/css">
		.boton_verde a{
			background: #48b617;
			color: #fff;
		}
		.boton_verde a:hover {
			background: #235d09 !important;
			color: #f6ff05;
		}	
		.boton_rojo a{
			background: #c40000;
			color: #fff;
		}
		.boton_rojo a:hover {
			background: #9e0101 !important;
			color: #f6ff05;
		}

		h3 {
			margin-bottom: 5px;
			font-weight: bold;
		}
	</style>
</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<!-- Contenido de la Pagina-->	
		<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper">

			<div class="cd-form floating-labels" style="max-width:1600px">
				<br><br>
				<center>
					<?php if(($recorte_anterior)&&($reemplazar!=1)) { ?>
					<div class="alert alert-warning" role="alert">
						<strong>Atención!</strong>, esta Imagen ya fue recortada anteriormente, si continúa reemplazará este recorte
					</div>
					<center>
						<a class="vc_btn_largo vc_btn_amarillo vc_btn_3d" style="max-width:300px" href="<?php echo $_SERVER['PHP_SELF']; ?>?foto=<?php echo $id_foto;?>&noticia=<?php echo $id_noticia; ?>&sala=<?php echo $sala; ?>&reemplazar=1&origen=<?php echo $origen;?>">
							<span class="fa-stack fa-lg pull-left">
								<i class="fa fa-circle fa-stack-2x"></i>
								<i class="fa fa-crop fa-stack-1x fa-inverse"></i>
							</span>
							<p>Nuevo recorte</p>
						</a>
					</center>
					<br><br>
					<h3>Recorte Miniatura</h3>
					<p><img src="<?php echo $url_imagenes; ?>recortes/<?php echo $recorte_anterior_miniatura; ?>" width="280" height="175"/></p>
					<br><br>
					<h3>Recorte Grande</h3>
					<p><img src="<?php echo $url_imagenes; ?>recortes/<?php echo $recorte_anterior; ?>" width="637" height="399"/></p>
					<?php }else{ ?>
					<img src="<?php echo $url_imagenes.$foto_seleccionada; ?>" id="cropbox"/>
					<form action="<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/notas/php/03-recortar-foto-db.php" method="post" onsubmit="return checkCoords();">
						<input type="hidden" name="id_noticia" value="<?php echo $id_noticia; ?>"/>
						<input type="hidden" name="id_evento" value="<?php echo $evento; ?>"/>
						<input type="hidden" name="id_sala" value="<?php echo $sala; ?>"/>
						<input type="hidden" name="origen" value="<?php echo $origen; ?>"/>
						<input type="hidden" name="id_foto" value="<?php echo $id_foto; ?>"/>
						<input type="hidden" id="x" name="x" />
						<input type="hidden" id="y" name="y" />
						<input type="hidden" id="w" name="w" />
						<input type="hidden" id="h" name="h" />
						<br>
						<p><center><input type="submit" value="Recortar Imagen" data-inline="true" data-theme="d" id="btn_cargar_noticia" name="btn_cargar_noticia" /></center></p>
					</form>
					<?php } ?>
				</center>
			</div>
		</div> <!-- .content-wrapper -->
	</main> 
	<script type="text/javascript">
		$(function(){
			$('#cropbox').Jcrop({
				aspectRatio: 91 / 57,
				setSelect:   [ 0,0,637,399 ],
				minSize: [0,0],
				onSelect: updateCoords
			});
		});
		function updateCoords(c)
		{
			$('#x').val(c.x);
			$('#y').val(c.y);
			$('#w').val(c.w);
			$('#h').val(c.h);
		};
		function checkCoords()
		{
			if (parseInt($('#w').val())) return true;
			alert('Seleccioná una región antes de cortar');
			return false;
		};
	</script>
</body>
</html>