<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');

$evento = $_GET['evento'];

if(!$evento) {
	exit;
}
?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/popup.css"> <!-- Resource style -->
	<script src="https://www.amoralasabiduria.com/robotgeneral/RobotNumerOne/js/cuenta-regresiva.php?minutos=0.1" type="text/javascript" charset="utf-8" async defer></script>

	<style type="text/css">
	body {
		background: #262626 !important;
	}

	.se-pre-con {
		position: fixed;
		left: 0px;
		top: 0px;
		width: 100%;
		height: 100%;
		z-index: 9999;
		background: url('<?php echo $Servidor_url;?>PANELADMINISTRADOR/img/newloader.jpg') center no-repeat #262626;
	}
</style>
</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<!-- Contenido de la Pagina-->	
		<div class="se-pre-con"></div>

	</main> 
	<?php include('../../includes/pie-general.php');?>
	<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
	<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/popup.js"></script> <!-- Resource jQuery -->
	<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/jquery.ddslick.min.js"></script> <!-- Resource jQuery -->
	<?php include('../../includes/firebase.php');?>

	<script type="text/javascript">
		<?php 
		conectar2('moebius', "ProyectoMoebius");

		$query_rs_eventos = "SELECT * FROM eventos WHERE id_evento = $evento";
		$rs_eventos = mysql_query($query_rs_eventos)or die(mysql_error());
		$row_rs_eventos = mysql_fetch_assoc($rs_eventos);
		$totalrow_rs_eventos = mysql_num_rows($rs_eventos);

		if($totalrow_rs_eventos) {

			$id_evento = $row_rs_eventos['id_evento'];
			$evento_key = $row_rs_eventos['evento_key'];

			$id_sala = $row_rs_eventos['id_sala'];
			$evento_tipo = $row_rs_eventos['evento_tipo'];
			$evento_nombre = $row_rs_eventos['evento_nombre'];
			$evento_descripcion = $row_rs_eventos['evento_descripcion'];
			$evento_fecha = $row_rs_eventos['evento_fecha'];
			$usuario_que_carga = $row_rs_eventos['usuario_que_carga'];
			$ip_visitante = $row_rs_eventos['ip_visitante'];
			$fecha_carga = $row_rs_eventos['fecha_carga'];
			$usuario_que_modifica = $row_rs_eventos['usuario_que_modifica'];
			$ip_visitante_modificacion = $row_rs_eventos['ip_visitante_modificacion'];
			$fecha_modificacion = $row_rs_eventos['fecha_modificacion'];

			$evento_nombre = arreglar_datos_db($evento_nombre);
			$evento_descripcion = arreglar_datos_db($evento_descripcion);

			$explorar_fecha = explode(' ', $evento_fecha);
			$fecha_sola = $explorar_fecha[0];
			$hora_comienzo = $explorar_fecha[1]; ?>

			var postData = {
				id_evento: "<?php echo $id_evento; ?>",
				id_sala: "<?php echo $id_sala; ?>"		
			};

			var newPostKey = firebase.database().ref().push().key;

			  // Write the new post's data simultaneously in the posts list and the user's post list.
			  var updates = {};
			  updates['/eventos/' + newPostKey] = postData;

			  if(firebase.database().ref().update(updates)) {
			  	setInterval( function() {
			  		location.href ="<?php echo $Servidor_url ?>PANELADMINISTRADOR/00-barra-navegacion/eventos/01-editar-evento.php?evento=<?php echo $evento;?>&key="+newPostKey;
			  	}, 1500 );
			  }
			  <?php } ?>
			  desconectar();
			</script>
		</body>
		</html>