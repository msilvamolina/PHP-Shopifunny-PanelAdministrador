<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');
$evento = trim($_GET['evento']);
$redirigir = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/eventos/02-eventos.php';
$imagen_cargada = trim($_GET['imagen_cargada']);

if(!$evento) {
	header('location:'.$redirigir);
	exit;
} 

$evento_anterior = "#";
$evento_siguiente = "#";

conectar2('moebius', "ProyectoMoebius");

//consultar en la base de datos
$query_rs_eventos = "SELECT *  FROM eventos WHERE id_evento = $evento ";
$rs_eventos = mysql_query($query_rs_eventos)or die(mysql_error());
$row_rs_eventos = mysql_fetch_assoc($rs_eventos);
$totalrow_rs_eventos = mysql_num_rows($rs_eventos);


if(!$totalrow_rs_eventos) {
	header('location:'.$redirigir);
	exit;
} 

//consultar en la base de datos
$query_rs_evento_anterior = "SELECT id_evento FROM eventos WHERE id_evento < $evento  ORDER BY id_evento DESC LIMIT 1";
$rs_evento_anterior = mysql_query($query_rs_evento_anterior)or die(mysql_error());
$row_rs_evento_anterior = mysql_fetch_assoc($rs_evento_anterior);
$totalrow_rs_evento_anterior = mysql_num_rows($rs_evento_anterior);

if($totalrow_rs_evento_anterior) {
	$evento_anterior = $row_rs_evento_anterior['id_evento'];
}

//consultar en la base de datos
$query_rs_evento_siguiente = "SELECT id_evento FROM eventos WHERE id_evento > $evento   ORDER BY id_evento ASC LIMIT 1";
$rs_evento_siguiente = mysql_query($query_rs_evento_siguiente)or die(mysql_error());
$row_rs_evento_siguiente = mysql_fetch_assoc($rs_evento_siguiente);
$totalrow_rs_evento_siguiente = mysql_num_rows($rs_evento_siguiente);

if($totalrow_rs_evento_siguiente) {
	$evento_siguiente = $row_rs_evento_siguiente['id_evento'];
}

$id_evento = $row_rs_eventos['id_evento'];
$id_sala = $row_rs_eventos['id_sala'];
$evento_tipo = $row_rs_eventos['evento_tipo'];
$evento_nombre = $row_rs_eventos['evento_nombre'];
$evento_descripcion = $row_rs_eventos['evento_descripcion'];
$evento_fecha = $row_rs_eventos['evento_fecha'];

$evento_key = $row_rs_eventos['evento_key'];
$sincronizacion_firebase = $row_rs_eventos['sincronizacion_firebase'];
$usuario_que_sincroniza = $row_rs_eventos['usuario_que_sincroniza'];

$usuario_que_carga = $row_rs_eventos['usuario_que_carga'];
$usuario_que_modifica = $row_rs_eventos['usuario_que_modifica'];
$fecha_modificacion = $row_rs_eventos['fecha_modificacion'];
$fecha_carga = $row_rs_eventos['fecha_carga'];
//consultar en la base de datos
$query_rs_sala = "SELECT * FROM salas WHERE id_sala = $id_sala ";
$rs_sala = mysql_query($query_rs_sala)or die(mysql_error());
$row_rs_sala = mysql_fetch_assoc($rs_sala);
$totalrow_rs_sala = mysql_num_rows($rs_sala);

$nombre_sala = $row_rs_sala['sala_nombre'];
$sala_direccion_calle = $row_rs_sala['sala_direccion_calle'];
$sala_direccion_numero = $row_rs_sala['sala_direccion_numero'];
$sala_direccion_piso = $row_rs_sala['sala_direccion_piso'];
$sala_direccion_dpto = $row_rs_sala['sala_direccion_dpto'];
$sala_latitud = $row_rs_sala['sala_latitud'];
$sala_longitud = $row_rs_sala['sala_longitud'];
$sala_ciudad = $row_rs_sala['sala_ciudad'];
$sala_provincia = $row_rs_sala['sala_provincia'];

$id_ciudad = $row_rs_sala['sala_ciudad'];
$id_provincia = $row_rs_sala['sala_provincia'];

	//consultar en la base de datos
$query_rs_imagen = "SELECT id_foto, nombre_foto, fecha_carga, recorte_foto_nombre, recorte_foto_miniatura FROM fotos_publicaciones WHERE id_evento = $evento ORDER BY id_foto DESC ";
$rs_imagen = mysql_query($query_rs_imagen)or die(mysql_error());
$row_rs_imagen = mysql_fetch_assoc($rs_imagen);
$totalrow_rs_imagen = mysql_num_rows($rs_imagen);

if($imagen_cargada) {
	$id_foto = $row_rs_imagen['id_foto'];
	$redireccionar = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/notas/03-recortar-foto.php?origen=ficha&foto='.$id_foto.'&evento='.$evento;
	header('location:'.$redireccionar);
	exit;
}

if($totalrow_rs_imagen) {
	$id_foto_portada = $row_rs_imagen['id_foto'];
	$foto_portada = $row_rs_imagen['recorte_foto_nombre'];
}

//consultar en la base de datos
$query_rs_ciudad = "SELECT id_ciudad, ciudad_nombre FROM ciudades ";
$rs_ciudad = mysql_query($query_rs_ciudad)or die(mysql_error());
$row_rs_ciudad = mysql_fetch_assoc($rs_ciudad);
$totalrow_rs_ciudad = mysql_num_rows($rs_ciudad);
do {
	$id_ciudad_1 = $row_rs_ciudad['id_ciudad'];
	$ciudad_nombre_1 = $row_rs_ciudad['ciudad_nombre'];
	$array_ciudades[$id_ciudad_1] = $ciudad_nombre_1;
} while($row_rs_ciudad = mysql_fetch_assoc($rs_ciudad));
$ciudad_nombre = $array_ciudades[$id_ciudad];
	//consultar en la base de datos
$query_rs_provincia = "SELECT id_provincia, provincia_nombre  FROM provincias ";
$rs_provincia = mysql_query($query_rs_provincia)or die(mysql_error());
$row_rs_provincia = mysql_fetch_assoc($rs_provincia);
$totalrow_rs_provincia = mysql_num_rows($rs_provincia);
do {
	$id_provincia_1 = $row_rs_provincia['id_provincia'];
	$provincia_nombre_1 = $row_rs_provincia['provincia_nombre'];
	$array_provincias[$id_provincia_1] = $provincia_nombre_1;
} while($row_rs_provincia = mysql_fetch_assoc($rs_provincia));
$provincia_nombre = $array_provincias[$id_provincia];

$direccion_a_mostrar = null;

if($sala_direccion_calle) {
	$direccion_a_mostrar = $sala_direccion_calle.' '.$sala_direccion_numero;

	if($sala_direccion_piso) {
		$direccion_a_mostrar .= ', <b>Piso:</b> '.$sala_direccion_piso;
	}
	if($sala_direccion_dpto) {
		$direccion_a_mostrar .= ', <b>Dpto:</b> "'.$sala_direccion_dpto.'"';
	}

	$direccion_a_mostrar .= ',<br>'.$array_ciudades[$sala_ciudad].', '.$array_provincias[$sala_provincia];
}

//consultar en la base de datos
$query_rs_usuario = "SELECT id_usuario, usuario_nombre, usuario_apellido FROM usuarios WHERE administrador = 1 ";
$rs_usuario = mysql_query($query_rs_usuario)or die(mysql_error());
$row_rs_usuario = mysql_fetch_assoc($rs_usuario);
$totalrow_rs_usuario = mysql_num_rows($rs_usuario);
do {
	$id_usuario = $row_rs_usuario['id_usuario'];
	$usuario_nombre = $row_rs_usuario['usuario_nombre'];
	$usuario_apellido = $row_rs_usuario['usuario_apellido'];
	$array_usuarios[$id_usuario] = $usuario_nombre.' '.$usuario_apellido;
} while($row_rs_usuario = mysql_fetch_assoc($rs_usuario));


//consultar en la base de datos
$query_rs_tarifas = "SELECT tarifa, tarifa_descripcion FROM tarifas_eventos WHERE id_evento = $evento";
$rs_tarifas = mysql_query($query_rs_tarifas)or die(mysql_error());
$row_rs_tarifas = mysql_fetch_assoc($rs_tarifas);
$totalrow_rs_tarifas = mysql_num_rows($rs_tarifas);

desconectar();

$array_datos['Descripción'] = $evento_descripcion;
$array_datos['Tipo'] = $evento_tipo;
$array_datos['Usuario que cargó']['usuario='.$usuario_que_modifica] = $array_usuarios[$usuario_que_carga];
$array_datos['Fecha de carga'] = nombre_fecha_min($fecha_carga);
if($fecha_modificacion) {
	$array_datos['Usuario que modificó']['usuario='.$usuario_que_modifica] = $array_usuarios[$usuario_que_modifica];
	$array_datos['Fecha modificación'] =  nombre_fecha_min($fecha_modificacion);
}
$link_redireccion = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/salas/03-ficha-sala.php?';

$ruta_imagenes = $Servidor_url.'APLICACION/Imagenes/eventos/';

$imagen = $Servidor_url.'PANELADMINISTRADOR/img/icono-imagen.png';
$nombre_imagen = '<span class="no_hay_imagen">(no hay imagen)</span>';
$icono_boton = 'add.png';
$link_boton = 'agregar_imagen(1)';

if($foto_portada) {
	$imagen = $ruta_imagenes.'recortes/'.$foto_portada;
	$icono_boton = 'delete.png';
	$link_boton = 'borrar_imagen('.$id_foto_portada.')';
}


?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/paginacion.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/popup.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/barra-pasos.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/negocios.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/fichas.css"> <!-- Resource style -->

	<script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?key=AIzaSyA51ZllTKq7CDMNNYUro72e6TnJ8RtEQa4'></script>

	<style type="text/css">
	.tabla_martin {
		width: 100%;
		max-width: 800px;
		margin: 0 auto;
		margin-bottom: 50px;
	}
	.tabla_celda {
		width: 100%;
		text-align: center;
		padding: 10px;
	}	
	.tabla_martin_fila {
		background: #ccc;
	}
	.alinear_derecha {
		text-align: center;
	}
	.alinear_derecha {
		background: #ff6000;
		color: #fff;
		padding: 10px;
	}
	@media only screen and (min-width: 768px) {
		.tabla_celda {
			text-align: left;
			width: 50%;
			float: left;
		}
		.alinear_derecha {
			text-align: right;
		}
	}

	h2 {
		padding-bottom: 0px;
		font-size: 26px;
		text-align: center;
	}
	.vacio {
		color: #c6c6c6;
	}
	.boton_verde a{
		background: #48b617;
		color: #fff;
	}
	.boton_verde a:hover {
		background: #235d09 !important;
		color: #f6ff05;
	}	
	.boton_rojo a{
		background: #c40000;
		color: #fff;
	}
	.boton_rojo a:hover {
		background: #9e0101 !important;
		color: #f6ff05;
	}		
	h2 span {
		background: red;
		padding: 5px;
		border-radius: 5px;
		color: #fff;
	}
	.h2_direcciones {
		padding: 20px;
		padding-bottom: 0px;
	}
	h2 .h2_direcciones {
		margin-top: 40px;
		display: block;
	}
	.clear {
		clear: both;
	}
	.sin_fondo {
		background: #fff !important;
		color: #ff6000!important;
	}
	.fondo_celeste {
		background: #3a86f6 !important;
	}

	.area_texto{
		width: 100%;
	}
	#otro_problema {
		display: none;
	}
	#reportar_problema {
		width: 100%;
		max-width: 300px;
		margin: 0 auto;
	}
	#reportar_problema li{
		padding: 10px;
		background: #790054;
		color: #fff;
		cursor: pointer;
	}
	#reportar_problema li:nth-of-type(2n){
		background: #c400a3;
	}	
	.area_texto {
		color: #000;
		height: 200px;
	}

	.mostrar_otro_poblema {
		display: none;
	}
	#reportar_problema {
		display: none;
	}
	.ul_encabezado {
		color:#ffea00 !important;
		font-size: 20px;
		text-align: center;
	}

	.negocio_reportado {
		width: 100%;
		max-width: 600px;
		margin: 20px auto;
	}	
	.mapa{
		width: 100%;
		height: 300px;
	}	

	.usuario_avatar {
		width: 50px;
		border-radius: 50%;
	}
	td {
		cursor: pointer;
	}

	.fa-toggle-on {
		font-size: 30px;
		color: #03af4f;
		cursor: pointer;
	}
	.fa-toggle-off {
		font-size: 30px;
		color: #f98c96;
		cursor: pointer;
	}	

	a {
		cursor: pointer;
	}

	.select_class {
		width: 100%;
		padding: 10px;
	}

	.imagen_portada {
		background: #8de4e1;
		height: auto;
		width: 100%;
	}

	.fondo_verde {
		color: #FFF9C4;
		background: #8BC34A;
	}

	.fondo_verde2 {
		background: #CDDC39;
	}
	.fondo_purpura {
		background: #7E57C2;
	}
	.fondo_purpura2 {
		background: #B39DDB;
		color: #FFF9C4;
	}

	.fondo_naranja2 {
		background: #FFB74D;
	}
</style>
</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<div class="cd-popup" id="popup_categoria" role="alert">
			<div class="cd-popup-container">
				<p>¿Estás seguro de querer borrar esta imagen?</p>
				<ul class="cd-buttons">
					<li id="btn_confirmar_categoria"><a onclick="">Sí</a></li>
					<li><a onclick="cerrar_popup()">No</a></li>
				</ul>
				<a href="#0" class="cd-popup-close img-replace"></a>
			</div> <!-- cd-popup-container -->
		</div> <!-- cd-popup -->		

		<div class="cd-popup" id="popup_borrar" role="alert">
			<div class="cd-popup-container">
				<p>¿Estás seguro de querer borrar este negocio?</p>
				<ul class="cd-buttons">
					<li id="btn_confirmar_borrado"><a onclick="confirmar_borrado()">Sí</a></li>
					<li><a onclick="cerrar_popup()">No</a></li>
				</ul>
				<a href="#0" class="cd-popup-close img-replace"></a>
			</div> <!-- cd-popup-container -->
		</div> <!-- cd-popup -->
		<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper" >
			<!-- Contenido de la Pagina-->
			<nav role="navigation">
				<ul class="cd-pagination">
					<li class="button"><a href="<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/eventos/03-ficha-evento.php?evento=<?php echo $evento_anterior; ?>">Anterior</a></li>
					<li class="button"><a  href="<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/eventos/03-ficha-evento.php?evento=<?php echo $evento_siguiente; ?>">Siguiente</a></li>
				</ul>
			</nav> <!-- cd-pagination-wrapper -->		

			<h2><span><?php echo $id_evento.'</span><br><br>'.$evento_nombre.'<br><br><span class="color_naranja">'.nombre_fecha_min($evento_fecha); ?></span></h2>

			<nav role="navigation">
				<ul class="cd-pagination">
					<li class="button boton_verde"><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/eventos/01-editar-evento.php?evento=<?php echo $evento; ?>">Editar</a></li>		
					<li class="button boton_rojo"><a href="#" onclick="borrar_negocio()">Borrar</a></li>	
				</ul>
			</nav> <!-- cd-pagination-wrapper -->
			
			<center>
				<?php 
				if(strtotime($fecha_modificacion) > strtotime($fecha_carga)) {
					if(strtotime($fecha_modificacion) > strtotime($sincronizacion_firebase)) { ?>
					<a class="vc_btn_largo vc_btn_amarillo vc_btn_3d" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/eventos/04-sincronizar-evento-app.php?evento=<?php echo $evento;?>" style="max-width:300px">
						<span class="fa-stack fa-lg pull-left">
							<i class="fa fa-circle fa-stack-2x"></i>
							<i class="fa fa-refresh fa-stack-1x fa-inverse"></i>
						</span>
						<p>Sincronizar con la app</p>
					</a>
					<?php } else { ?>
					<b>Este evento se encuentra sincronizado con la app</b><br><br>

					Fue sincronizado por <b><?php echo $array_usuarios[$usuario_que_sincroniza]; ?></b> el día <b><?php echo nombre_fecha($sincronizacion_firebase); ?></b><br><br> con el key <b style="color: red"><?php echo $evento_key; ?></b>
					<?php } } ?>
				</center>

				<br><br><br>

				<div class="tabla_martin">
					<div class="imagen_portada">
						<div class="imagen_delete">
							<a href="#" onclick="<?php echo $link_boton; ?>">
								<img src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/img/<?php echo $icono_boton;?>" class="icono_boton">
							</a>
						</div>
						<center>
							<img src="<?php echo $imagen; ?>" class="imagen_portada2" />
						</center>
					</div>
					<br><br>
					<div clas="tabla_martin_fila">
						<div class="tabla_celda alinear_derecha ">
							Nombre del evento
						</div>
						<div class="tabla_celda fondo_naranja2"><?php echo $evento_nombre; ?></div>
						<div class="tabla_celda alinear_derecha fondo_purpura">
							Fecha del evento
						</div>
						<div class="tabla_celda fondo_purpura2"><?php echo nombre_fecha_min($evento_fecha); ?></div>
					</div>

					<?php foreach ($array_datos as $nombre => $dato) { ?>
					<div clas="tabla_martin_fila">
						<div class="tabla_celda alinear_derecha sin_fondo"><?php echo $nombre; ?></div>

						<div class="tabla_celda"><?php
						if(is_array($dato)) {
							foreach ($dato as $clave => $valor) {
								if($valor) {
									$explorar_clave = explode('https://', $clave);
									if($explorar_clave[1]) {
										$link = $clave;
									} else {
										$link = $link_redireccion.$clave;
									}
									echo '<a href="'.$link.'">'.$valor.'</a>';
								} else {
									echo '<i class="vacio">(Vacío)</i>';
								}
							}
						} else {
							if($dato) {
								echo $dato;
							} else {
								echo '<i class="vacio">(Vacío)</i>';
							} 
						}	?></div>
					</div>
					<?php } ?>
					<div class="clear"></div>

					<?php if($totalrow_rs_tarifas) { ?>
					<h2 class="h2_direcciones">Tarifas</h2>	
					<br>	
					<?php do {
						$tarifa = $row_rs_tarifas['tarifa'];
						$tarifa_descripcion = $row_rs_tarifas['tarifa_descripcion']; ?>
						<div clas="tabla_martin_fila">

							<div class="tabla_celda fondo_verde alinear_derecha"><?php echo formato_moneda($tarifa); ?></div>

							<div class="tabla_celda fondo_verde2"><?php echo $tarifa_descripcion; ?></div>
							<div class="clear"></div>
						</div>	
						<?php } while ($row_rs_tarifas = mysql_fetch_assoc($rs_tarifas)); ?>
						<?php } ?>
						<?php if($id_sala) { ?>
						<h2 class="h2_direcciones">Lugar</h2>	
						<br>	
						<div class="tabla_martin">
							<?php $i=1;	?>
							<div clas="tabla_martin_fila">
								<a target="_blank" href="<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/salas/03-ficha-sala.php?sala=<?php echo $id_sala; ?>">
									<div class="tabla_celda alinear_derecha"><?php echo $nombre_sala; ?></div>

									<div class="tabla_celda sin_fondo"><?php echo $direccion_a_mostrar; ?></div>
								</a>
								<div class="clear"></div>
							</div>
							<?php if($sala_longitud) { ?>	
							<div clas="tabla_martin_fila sin_fondo">
								<center>
									<div class="mapa" id="map_canvas1"></div><br><br>

									<a class="vc_btn_largo vc_btn_amarillo vc_btn_3d" style="max-width:300px"  target="_blank" 
									href="http://maps.google.com/?cbll=<?php echo $sala_latitud; ?>,<?php echo $sala_longitud; ?>&cbp=12,20.09,,0,5&layer=c">
									<span class="fa-stack fa-lg pull-left">
										<i class="fa fa-circle fa-stack-2x"></i>
										<i class="fa fa-street-view fa-stack-1x fa-inverse"></i>
									</span>
									<p>Ver en Google Street View</p>
								</a>
							</center><br><br>	
						</div>									
						<?php } } ?>
					</div>
				</div> <!-- .content-wrapper -->
			</main> 
			<?php include('../../includes/pie-general.php');?>
			<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
			<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/popup.js"></script> <!-- Resource jQuery -->
			<script type="text/javascript">
				function borrar_negocio() {
					$('#popup_borrar').addClass('is-visible');
				}
				function confirmar_borrado() {
					window.open('<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/eventos/php/02-borrar-evento-db.php?evento=<?php echo $evento;?>', '_self');
				}
				function cerrar_popup() {
					$('.cd-popup').removeClass('is-visible');
				}

				function quitar_reporte() {
					$('#popup_reporte').addClass('is-visible');
				}


				<?php if($sala_latitud) { ?>
					initialize();

					function initialize() {
						geocoder = new google.maps.Geocoder();


						var latlng1 = new google.maps.LatLng(<?php echo $sala_latitud; ?>, <?php echo $sala_longitud; ?>);
						var myOptions1 = {
							zoom: 18,
							center: latlng1,
							mapTypeId: google.maps.MapTypeId.ROADMAP
						}

						map1 = new google.maps.Map(document.getElementById("map_canvas1"), myOptions1);

						marker1 = new google.maps.Marker({
							position: latlng1,
							map: map1,
							draggable: false
						});
					}
					<?php } ?>

					function agregar_imagen(portada) {
						window.open('<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/notas/popUps/02-cargar-imagen-evento.php?evento=<?php echo $evento; ?>','popup','width=800,height=400');
					}
					function borrar_imagen(imagen) {
						$('#popup_categoria').addClass('is-visible');
						$('#btn_confirmar_categoria').html('<a href="<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/eventos/php/03-borrar-imagen-evento.php?evento=<?php echo $evento;?>&foto='+imagen+'">Sí</a>');
					}
				</script>
			</body>
			</html>