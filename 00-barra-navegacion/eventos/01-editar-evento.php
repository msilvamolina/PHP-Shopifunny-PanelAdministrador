<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');

$evento = trim($_GET['evento']);

if(!$evento) {
	$redireccion = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/eventos/00-cargar-evento.php';
	header('Location: '.$redireccion);
	exit;
}

conectar2('moebius', "ProyectoMoebius");

//consultar en la base de datos
$query_rs_evento = "SELECT id_sala, evento_key, evento_tipo, evento_nombre, evento_fecha, evento_descripcion  FROM eventos WHERE id_evento = $evento ";
$rs_evento = mysql_query($query_rs_evento)or die(mysql_error());
$row_rs_evento = mysql_fetch_assoc($rs_evento);
$totalrow_rs_evento = mysql_num_rows($rs_evento);

$evento_key = $row_rs_evento['evento_key'];
$evento_lugar = $row_rs_evento['id_sala'];
$evento_tipo = $row_rs_evento['evento_tipo'];
$evento_nombre = $row_rs_evento['evento_nombre'];
$evento_fecha = $row_rs_evento['evento_fecha'];
$evento_descripcion = $row_rs_evento['evento_descripcion'];

//consultar en la base de datos
$query_rs_salas = "SELECT id_sala, sala_nombre FROM salas ORDER BY sala_nombre ASC ";
$rs_salas = mysql_query($query_rs_salas)or die(mysql_error());
$row_rs_salas = mysql_fetch_assoc($rs_salas);
$totalrow_rs_salas = mysql_num_rows($rs_salas);

//consultar en la base de datos
$query_rs_tarifas = "SELECT id_tarifa, tarifa, tarifa_descripcion FROM tarifas_eventos WHERE id_evento = $evento ";
$rs_tarifas = mysql_query($query_rs_tarifas)or die(mysql_error());
$row_rs_tarifas = mysql_fetch_assoc($rs_tarifas);
$totalrow_rs_tarifas = mysql_num_rows($rs_tarifas);

desconectar();

if(!$totalrow_rs_evento) {
	$redireccion = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/eventos/00-cargar-evento.php';
	header('Location: '.$redireccion);
	exit;
}

$tipos_eventos[] = "Gratis";
$tipos_eventos[] = "Patrocinado";
$tipos_eventos[] = "Premium";


$explorar_fecha = explode(' ', $evento_fecha);
$fecha_actual = $explorar_fecha[0];
$hora_actual = $explorar_fecha[1];
?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css?v=2"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/popup.css"> <!-- Resource style -->

	<style type="text/css">
	.contenedor{
		text-align: center;
		margin: 0 auto;
		padding-top: 40px;
	}
	.bender{
		width: 100%;
		max-width: 134px;
	}

	h2 {
		margin-top: 10px;
		font-size: 26px;
	}
	.td_delete {
		padding: 10px;
		text-align: right;
		width: 30px;
	}
	.td_delete img {
		width: 30px;
		display: block;
	}	
	.cd-form {
		text-align: left;
	}

	.grupo_categoria {
		margin-left: -20px!important;
	}

	#section_categoria {
		background: #a7a7a7;
		padding: 30px;
		color: #fff;
	}

	#section_categoria h3 {
		font-size: 24px;
	}
	.select_class {
		background: #eeeeee !important;
	}

	.section_informacion_bender {
		background: #a7a7a7;
		padding: 30px;
		color: #fff;		
	}

	#section_informacion_bender {
		line-height: 18px;
	}
	.bender_chiquito {
		width: 30px;
	}

	.info_bender {
		margin-bottom: 20px;
	}
	.delete_categoria {
		float: right;
	}
	.delete_categoria img{
		width: 35px;
		margin-top: -5px;
	}
	#negocio_repetido {
		width: 100%;
		padding: 30px;
		background: #464646;
		color:#fff;
	}
	#negocio_repetido b {
		color: #e6d461;
	}

	#negocio_repetido span {
		color: #f92672;
	}

	#negocio_repetido h3 {
		color: #a6db29;
		font-size: 32px;
		margin-bottom: 10px;
	}

	.campo_fecha_hora {
		padding: 6px;
		border: 1px solid #B3E5FC;
		background-color: #ffffff;
		border-radius: .25em;
		box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.08);
		width: 49.5%;
	}
</style>
</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper">

			<div class="contenedor">

				<div >					<!-- Contenido de la Pagina-->	
					<div class="cd-form floating-labels">
						<section id="crear_categoria" >							
							<fieldset >
								<?php if($_GET['ok']) { ?>
								<div class="alert alert-success" role="alert">
									Los cambios se guardaron correctamente a las <?php echo date('H:i:s'); ?>
								</div>
								<?php } ?>
								<form id="myForm" onsubmit="return validar_formulario()" action="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/eventos/php/01-editar-evento-db.php" method="POST">
									<input  type="hidden" id="evento" name="evento" value='<?php echo $evento; ?>'>

									<a href="<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/eventos/03-ficha-evento.php?evento=<?php echo $evento;?>" class="vc_btn_largo vc_btn_rojo vc_btn_3d" style="width:250px;float:right">
										<span class="fa-stack fa-lg pull-left">
											<i class="fa fa-circle fa-stack-2x"></i>
											<i class="fa fa-calendar-o fa-stack-1x fa-inverse"></i>
										</span>
										<b>Ficha del evento</b>
									</a>
									<legend id="txt_nueva_categoria">Editar Evento</legend>

									<div class="icon">
										<label class="cd-label" for="cd-company">Nombre del evento</label>
										<input class="company" type="text" name="nombre" value="<?php echo $evento_nombre; ?>" id="nueva_categoria_nombre" required>
									</div> 			    
									<div class="icon">
										<label class="cd-label" for="cd-textarea">Descripción</label>
										<textarea class="message" name="descripcion" id="nueva_categoria_descripcion" ><?php echo $evento_descripcion; ?></textarea>
									</div>

									<p class="cd-select icon"><label class="cd-label" for="cd-company">Tipo de evento</label>
										<select name="evento_tipo" required class="budget"   >									
											<?php foreach ($tipos_eventos as $tipo) {
												$elegido = null;
												if($evento_tipo==$tipo) {
													$elegido = 'selected';
												}
												if($tipo) {
													echo '<option value="'.$tipo.'" '.$elegido.'>'.$tipo.'</option>';
												}
											}
											?>
										</select></p>
										<br><br>

										<p class="cd-select icon"><label class="cd-label" >Lugar</label>
											<select name="evento_lugar" required class="budget">
												<option value=""></option>	
												<?php
												do {
													$id_sala = $row_rs_salas['id_sala'];
													$sala_nombre = $row_rs_salas['sala_nombre'];

													$elegido = null;
													if($evento_lugar==$id_sala) {
														$elegido = 'selected';
													}
													echo '<option value="'.$id_sala.'" '.$elegido.'>'.$sala_nombre.'</option>';
												} while ($row_rs_salas = mysql_fetch_assoc($rs_salas));
												?>
											</select></p>
											<div class="icon" id="fecha_noticia" >
												<label class="cd-label" for="cd-company">Fecha evento</label>
												<input class="campo_fecha_hora company" type="date" value="<?php echo $fecha_actual; ?>" step="1" name="fecha"   required>
												<input class="campo_fecha_hora company" type="time" value="<?php echo $hora_actual; ?>" step="1" name="hora"  required>
											</div>
											<br>
											<legend id="txt_nueva_categoria">Tarifas</legend>
											<table width="100%"  id="telefonos">
												<?php 
												$i=1; 
												if($totalrow_rs_tarifas) { 
													do { 
														$id_tarifa = $row_rs_tarifas['id_tarifa'];
														$tarifa = $row_rs_tarifas['tarifa'];
														$tarifa_descripcion = $row_rs_tarifas['tarifa_descripcion'];

														?>
														<input name="tarifa_ids[]" type="hidden" value="<?php echo $id_tarifa;?>" />
														<tr id="telefono_agregado_<?php echo $i; ?>">
															<td><input  type="number" name="tarifa[]" step="0.01" value="<?php echo $tarifa; ?>" required placeholder="Precio (Ej: 150 ó 150,60)" ></td>

															<td><input  type="text" name="tarifa_descripcion[]" value="<?php echo $tarifa_descripcion; ?>" required placeholder="Descripción"></td>

															<td class="td_delete" width="60"><a onclick="eliminar_telefono(<?php echo $i; ?>, <?php echo $id_tarifa; ?>)"><img src="<?php echo $Servidor_url;?>PANELADMINISTRADOR/img/delete.png"></a></td></tr>


															<?php $i++; } while($row_rs_tarifas = mysql_fetch_assoc($rs_tarifas)); } ?>										
														</table>
														<input  type="hidden" id="tarifas_contador" value='<?php echo $i; ?>'>

														<div class="alinear_centro">
															<a class="boton_azul boton_amarillo" onclick="agregar_tarifa()">[+] Agregar tarifa</a>
														</div>				    
														<br><br><br>

														<div class="alinear_centro">
															<input type="submit" value="Guardar Cambios">
														</div>
													</div>


												</form>
											</fieldset>
										</section>

										<br><br><br><br><br>
									</div>

								</div> <!-- .content-wrapper -->


							</main> 
							<?php include('../../includes/pie-general.php');?>
							<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
							<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/popup.js"></script> <!-- Resource jQuery -->
							<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/jquery.ddslick.min.js"></script> <!-- Resource jQuery -->
							<?php include('../../includes/firebase.php');?>

							<script type="text/javascript">

								function cerrar_popup() {
									$('.cd-popup').removeClass('is-visible');
								}

								function agregar_tarifa() {
									var telefonos_contador = document.getElementById("tarifas_contador").value;
									var codigo_area = '$';

									var variable = '<tr id="telefono_agregado_'+telefonos_contador+'">';
									variable = variable+'<td><input  type="number" name="tarifa[]" step="0.01" value="'+codigo_area+'" required placeholder="Precio (Ej: 150 ó 150,60)" ></td>';
									variable = variable+'<td><input  type="text" name="tarifa_descripcion[]" required placeholder="Descripción"></td>';
									variable = variable+'<td class="td_delete" width="60"><a onclick="eliminar_telefono('+telefonos_contador+', 0)"><img src="<?php echo $Servidor_url;?>PANELADMINISTRADOR/img/delete.png"></a></td></tr>';

									document.getElementById("tarifas_contador").value = parseInt(telefonos_contador)+1;		
									$('#telefonos').append(variable);
								}
								function eliminar_telefono(telefono, id_tarifa) {
									$('#telefono_agregado_'+telefono).html('');
									var variable = '<input type="hidden" name="eliminar_tarifa[]" value="'+id_tarifa+'">';
									$('#telefonos').append(variable);
								}	

								function validar_formulario() {
									var error = null;	

									if(error) {
										alert(error);
										return false;
									} else {
										return true;
									}
								}
							</script>
						</body>
						</html>