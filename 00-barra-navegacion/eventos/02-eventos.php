<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');

$pagina= trim($_GET['pagina']);

$limite = 50;
$pagina_get = $pagina;
if(!$pagina_get) {
	$pagina_get=1;
}
if($pagina) {
	$pagina = $pagina-1;
}
$arranca = $pagina*$limite;

conectar2('moebius', "ProyectoMoebius");

//consultar en la base de datos
$query_rs_noticias = "SELECT * FROM eventos  ORDER BY id_evento DESC LIMIT $arranca,$limite";
$rs_noticias = mysql_query($query_rs_noticias)or die(mysql_error());
$row_rs_noticias = mysql_fetch_assoc($rs_noticias);
$totalrow_rs_noticias = mysql_num_rows($rs_noticias);

$pagina_actual_variables = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/eventos/02-eventos.php?';
if($usuario) {
	$pagina_actual_variables = $pagina_actual_variables.'usuario='.$usuario.'&';
}
if($q) {
	$pagina_actual_variables = $pagina_actual_variables.'q='.$q.'&';
}
$pagina_siguiente = $pagina+2;
$pagina_anterior = $pagina;
$disabled_siguiente = null;
$disabled_anterior = null;
$link_siguiente = $pagina_actual_variables.'pagina='.$pagina_siguiente;
$link_anterior = $pagina_actual_variables.'pagina='.$pagina_anterior;
if($pagina_anterior<=0) {
	$disabled_anterior = 'disabled';
	$link_anterior = null;
}

if(!$totalrow_rs_noticias) {
	$disabled_siguiente = 'disabled';
	$link_siguiente = null;
}
//consultar en la base de datos
$query_rs_fotos = "SELECT id_foto, id_evento, id_sala, recorte_foto_miniatura FROM fotos_publicaciones WHERE id_evento > 0";
$rs_fotos = mysql_query($query_rs_fotos)or die(mysql_error());
$row_rs_fotos = mysql_fetch_assoc($rs_fotos);
$totalrow_rs_fotos = mysql_num_rows($rs_fotos);

$ruta = $Servidor_url.'APLICACION/Imagenes/eventos/recortes/';

do {
	$id_evento = $row_rs_fotos['id_evento'];
	$nombre_foto = $row_rs_fotos['recorte_foto_miniatura'];

	$array_fotos[$id_evento] = $ruta.$nombre_foto;
} while($row_rs_fotos = mysql_fetch_assoc($rs_fotos));

$query_rs_ciudad = "SELECT id_ciudad, ciudad_nombre, id_provincia FROM ciudades ORDER BY ciudad_nombre ASC ";
$rs_ciudad = mysql_query($query_rs_ciudad)or die(mysql_error());
$row_rs_ciudad = mysql_fetch_assoc($rs_ciudad);
$totalrow_rs_ciudad = mysql_num_rows($rs_ciudad);

do {
	$id_ciudad = $row_rs_ciudad['id_ciudad'];
	$ciudad_nombre = $row_rs_ciudad['ciudad_nombre'];

	$array_ciudades[$id_ciudad] = $ciudad_nombre;

} while($row_rs_ciudad = mysql_fetch_assoc($rs_ciudad));

//consultar en la base de datos
$query_rs_provincias = "SELECT id_provincia, provincia_nombre FROM provincias ORDER BY provincia_nombre ASC ";
$rs_provincias = mysql_query($query_rs_provincias)or die(mysql_error());
$row_rs_provincias = mysql_fetch_assoc($rs_provincias);
$totalrow_rs_provincias = mysql_num_rows($rs_provincias);

do {
	$id_provincia = $row_rs_provincias['id_provincia'];
	$provincia_nombre = $row_rs_provincias['provincia_nombre'];

	$array_provincias[$id_provincia] = $provincia_nombre;
} while($row_rs_provincias = mysql_fetch_assoc($rs_provincias));

//consultar en la base de datos
$query_rs_salas = "SELECT id_sala, sala_nombre, sala_direccion_calle, sala_direccion_numero, sala_direccion_dpto, sala_direccion_piso, sala_ciudad, sala_provincia, sala_longitud, sala_latitud, sala_direccion_google_maps  FROM salas ";
$rs_salas = mysql_query($query_rs_salas)or die(mysql_error());
$row_rs_salas = mysql_fetch_assoc($rs_salas);
$totalrow_rs_salas = mysql_num_rows($rs_salas);

do {
	$id_sala = $row_rs_salas['id_sala'];
	$sala_nombre = $row_rs_salas['sala_nombre'];
	$sala_direccion_calle = $row_rs_salas['sala_direccion_calle'];
	$sala_direccion_numero = $row_rs_salas['sala_direccion_numero'];
	$sala_direccion_dpto = $row_rs_salas['sala_direccion_dpto'];
	$sala_direccion_piso = $row_rs_salas['sala_direccion_piso'];
	$sala_ciudad = $row_rs_salas['sala_ciudad'];
	$sala_provincia = $row_rs_salas['sala_provincia'];
	$sala_longitud = $row_rs_salas['sala_longitud'];
	$sala_latitud = $row_rs_salas['sala_latitud'];
	$sala_direccion_google_maps =  $row_rs_salas['sala_direccion_google_maps'];
	$direccion_a_mostrar = null;

	if($sala_direccion_calle) {
		$direccion_a_mostrar = $sala_direccion_calle.' '.$sala_direccion_numero;

		if($sala_direccion_piso) {
			$direccion_a_mostrar .= ', <b>Piso:</b> '.$sala_direccion_piso;
		}
		if($sala_direccion_dpto) {
			$direccion_a_mostrar .= ', <b>Dpto:</b> "'.$sala_direccion_dpto.'"';
		}

		$direccion_a_mostrar .=', '.$array_ciudades[$sala_ciudad].', '.$array_provincias[$sala_provincia];

		$array_direccion_mostrar[$id_sala] = $direccion_a_mostrar;
	}

	$array_sala_latitud[$id_sala] = $sala_latitud;
	$array_sala_longitud[$id_sala] = $sala_longitud;

	$array_sala_direccion_google_maps[$id_sala] = $sala_direccion_google_maps;

	$array_sala_nombre[$id_sala] = $sala_nombre;
} while ($row_rs_salas = mysql_fetch_assoc($rs_salas));

desconectar();
?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/paginacion.css"> <!-- Resource style -->

	<style type="text/css">
		.rojo {
			color: #F44336;
			font-weight: bold;
		}

		.verde {
			color: #2E7D32;
			font-weight: bold;
		}

		.evento_nombre {
			color: #E91E63;
		}

		.verde2 {
			color: #8BC34A;
			font-weight: bold;
		}
	</style>
</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper" style="max-width:100%">
			<!-- Contenido de la Pagina-->
			<nav role="navigation">
				<ul class="cd-pagination">
					<li class="button"><a class="<?php echo $disabled_anterior; ?>" href="<?php echo $link_anterior; ?>">Anterior</a></li>
					<li class="button"><a  class="<?php echo $disabled_siguiente; ?>"  href="<?php echo $link_siguiente; ?>">Siguiente</a></li>
				</ul>
			</nav> <!-- cd-pagination-wrapper -->	
			<div class="cd-form floating-labels" style="max-width:100%">
				<legend id="txt_nueva_categoria">Eventos</legend>

				<?php if($totalrow_rs_noticias) { ?>
				<table class="table table-striped">
					<thead class="tabla_encabezado">
						<tr>
							<th><b>#</b></th>
							<th><b>Foto</b></th>
							<th><b>Ubicación</b></th>
							<th><b>Nombre</b></th>
							<th><b>Fechadel evento</b></th>
						</tr>
					</thead>
					<tbody>
						<?php 

						do { 
							$id_evento = $row_rs_noticias['id_evento'];
							$id_sala = $row_rs_noticias['id_sala'];
							$evento_fecha = $row_rs_noticias['evento_fecha'];

							$sala_latitud = $array_sala_latitud[$id_sala];
							$sala_longitud = $array_sala_longitud[$id_sala];

							$evento_nombre = $row_rs_noticias['evento_nombre'];

							$direccion_a_mostrar = $array_direccion_mostrar[$id_sala];
							$sala_direccion_google_maps = $array_sala_direccion_google_maps[$id_sala];

							$sala_nombre = $array_sala_nombre[$id_sala];

							$imagen = $Servidor_url.'PANELADMINISTRADOR/img/icono-imagen.png';
							$nombre_imagen = '<span class="no_hay_imagen">(no hay imagen)</span>';

							if($array_fotos[$id_evento]) {
								$imagen = $array_fotos[$id_evento];
							}

							$latitud_longitud = $sala_latitud.','.$sala_longitud;
							$url_ubicacion = "http://maps.googleapis.com/maps/api/staticmap?center=".$latitud_longitud."&zoom=16&size=150x100&scale=1&markers=color:red%7Clabel:%7C".$latitud_longitud;	
							?>
							<tr class="<?php echo $super_class; ?>" data-href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/eventos/03-ficha-evento.php?evento=<?php echo $id_evento; ?>">
								<td><?php echo $id_evento; ?></td>
								<td><img src="<?php echo $imagen; ?>"  width="100"></td>
								<td width="150">
									<?php if($sala_latitud) { ?>
									<img src="<?php echo $url_ubicacion; ?>" alt="<?php echo $sala_direccion_google_maps; ?>" title="<?php echo $sala_direccion_google_maps; ?>">
									<?php } ?>
								</td>
								<td><b><span class="evento_nombre"><?php echo $evento_nombre; ?></span></b><br>
									<b><?php echo $sala_nombre; ?></b><br>
									<?php echo $direccion_a_mostrar;?></td>


									<td class="verde2"><?php echo nombre_fecha($evento_fecha); ?></td>
								</tr>		
								<?php } while($row_rs_noticias = mysql_fetch_assoc($rs_noticias)); ?>	          	
							</tbody>
						</table>		     
						<?php } else { ?>
						<p>No hay eventos</p>
						<?php }?>           
					</div>
					<nav role="navigation">
						<ul class="cd-pagination">
							<li class="button"><a class="<?php echo $disabled_anterior; ?>" href="<?php echo $link_anterior; ?>">Anterior</a></li>
							<li class="button"><a  class="<?php echo $disabled_siguiente; ?>"  href="<?php echo $link_siguiente; ?>">Siguiente</a></li>
						</ul>
					</nav> <!-- cd-pagination-wrapper -->	
				</div> <!-- .content-wrapper -->
			</main> 
			<?php include('../../includes/pie-general.php');?>
			<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
			<script type="text/javascript">
				$('tr[data-href]').on("click", function() {
					document.location = $(this).data('href');
				});
			</script>
		</body>
		</html>