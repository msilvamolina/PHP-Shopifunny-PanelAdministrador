<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');
?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/popup.css"> <!-- Resource style -->
	<style type="text/css">
	.btn_eliminar {
		text-align: right;
		width: 100%;
	}

	a {
		cursor: pointer;
	}
</style>
</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper">
			<div class="contenedor">
				<div >					<!-- Contenido de la Pagina-->	
					<div class="cd-form floating-labels">
						<section id="crear_categoria" >							
							<fieldset >
								<form id="form-convalidaciones">
									<legend id="txt_nueva_categoria">Nuevo Evento</legend>
									<div class="icon">
										<label class="cd-label" for="cd-company">Nombre</label>
										<input class="company" type="text" name="nombreEvento" id="nombreEvento" required>
									</div> 			    

									<div class="alinear_centro">
										<input type="submit" value="Crear Evento" id="boton-enviar-convalidacion">
									</div>
								</form>

								<table id="tabla-Convalidaciones"></table>
							</fieldset>	
						</section>    	

					</div>
				</div>
			</div> <!-- .content-wrapper -->
		</main> 
		<?php include('../../includes/pie-general.php');?>
		<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
		<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/popup.js"></script> <!-- Resource jQuery -->
		<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/jquery.ddslick.min.js"></script> <!-- Resource jQuery -->
		<?php include('../../includes/firebase.php');?>
		
		<script type="text/javascript" >
			window.onload = inicializar;

			var formConvalidaciones;
			var refConvalidaciones; 
			var tablaConvalidaciones;
			var CREATE = "Añadir Convalidación";
			var UPDATE = "Modificar Convalidación";
			var modo = CREATE;
			var refConvalidacionAEditar;

			function inicializar () {
				formConvalidaciones = document.getElementById("form-convalidaciones");
				formConvalidaciones.addEventListener("submit", enviarConvalidacionAFirebase, false);

				tablaConvalidaciones = document.getElementById("tabla-Convalidaciones");
				refConvalidaciones = firebase.database().ref().child("Eventos");

				mostrarConvalidacionesDeFirebase();
			}

			function mostrarConvalidacionesDeFirebase() {
				refConvalidaciones.on("value", function(snap){
					var datos = snap.val();

					var filasAMostrar  = "";
					for (var key in datos) {
						filasAMostrar += "<tr>" +
						"<td>" + key + "</td>" +
						"<td>" + datos[key].nombreEvento + "</td>" +
						'<td> ' +
						'<button class="btn btn-default editar" data-convalidacion="' + key + '">' +
						'<span class="glyphicon glyphicon-pencil"></span>' +
						'</button>' +
						'</td>' +
						'<td> ' +
						'<button class="btn btn-danger borrar" data-convalidacion="' + key + '">' +
						'<span class="glyphicon glyphicon-trash"></span>' +
						'</button>' +
						'</td>' +
						"</tr>";
					}

					tablaConvalidaciones.innerHTML = filasAMostrar;

					if(filasAMostrar != "") {
						var elementosEditables = document.getElementsByClassName("editar");

						for (var i = 0; i < elementosEditables.length; i++) {
							elementosEditables[i].addEventListener("click", editarConvalidacionDeFirebase, false);
						}
						var elementosBorrables = document.getElementsByClassName("borrar");

						for (var i = 0; i < elementosBorrables.length; i++) {
							elementosBorrables[i].addEventListener("click", borrarConvalidacionDeFirebase, false);
						}
					} 
				});
			}

			function editarConvalidacionDeFirebase() {
				var keyDeConvalidacionAEditar = this.getAttribute("data-convalidacion");
				refConvalidacionAEditar = refConvalidaciones.child(keyDeConvalidacionAEditar);

				refConvalidacionAEditar.once("value", function(snap){
					var datos = snap.val();
					document.getElementById("nombreEvento").value = datos.nombreEvento;
				});
				modo = UPDATE;
				document.getElementById("boton-enviar-convalidacion").value = modo;
			}

			function borrarConvalidacionDeFirebase() {
				var keyDeConvalidacionABorrar = this.getAttribute("data-convalidacion");

				var refConvalidacionABorrar = refConvalidaciones.child(keyDeConvalidacionABorrar);

				refConvalidacionABorrar.remove();
			}

			function enviarConvalidacionAFirebase(event) {
				event.preventDefault();

				switch(modo) {
					case CREATE:
					refConvalidaciones.push({
						nombreEvento: event.target.nombreEvento.value
					});
					formConvalidaciones.reset();
					break;

					case UPDATE:
					refConvalidacionAEditar.update({
						nombreEvento: event.target.nombreEvento.value
					});
					formConvalidaciones.reset();
					modo = CREATE;
					document.getElementById("boton-enviar-convalidacion").value = modo;

					break;
				}
			}			
		</script>
	</body>
	</html>