<?php include('../../../../paginas_include/variables-generales.php'); 
include('../../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../../php/verificar-permisos.php');

$key = trim($_POST['key']);
$id_evento = trim($_POST['id_evento']);

if(!$key || !$id_evento) {
	$array_post['respuesta'] = "error";
	header('Content-Type: application/json');
	print_r(json_encode($array_post));
	exit;
}

conectar2('moebius', "ProyectoMoebius");
$fecha_actual = date('Y-m-d H:i:s');

mysql_query("UPDATE eventos SET evento_key='$key', sincronizacion_firebase='$fecha_actual', usuario_que_sincroniza='$id_administrador' WHERE id_evento='$id_evento'");
desconectar();

$array_post['respuesta'] = "OK";

header('Content-Type: application/json');
print_r(json_encode($array_post));
?>