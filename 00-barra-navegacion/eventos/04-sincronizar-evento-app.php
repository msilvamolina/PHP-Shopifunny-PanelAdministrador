<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');

$evento = trim($_GET['evento']);

if(!$evento) {
	exit;
}
?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/popup.css"> <!-- Resource style -->
	<style type="text/css">
	body {
		background: #262626 !important;
	}

	.se-pre-con {
		position: fixed;
		left: 0px;
		top: 0px;
		width: 100%;
		height: 100%;
		z-index: 9999;
		background: url('<?php echo $Servidor_url;?>PANELADMINISTRADOR/img/newloader.jpg') center no-repeat #262626;
	}
</style>
</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<!-- Contenido de la Pagina-->	
		<div class="se-pre-con"></div>

	</main> 
	<?php include('../../includes/pie-general.php');?>
	<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
	<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/popup.js"></script> <!-- Resource jQuery -->
	<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/jquery.ddslick.min.js"></script> <!-- Resource jQuery -->
	<?php include('../../includes/firebase.php');?>

	<script type="text/javascript">
		<?php 
		conectar2('moebius', "ProyectoMoebius");

		$query_rs_eventos = "SELECT * FROM eventos WHERE id_evento = $evento";
		$rs_eventos = mysql_query($query_rs_eventos)or die(mysql_error());
		$row_rs_eventos = mysql_fetch_assoc($rs_eventos);
		$totalrow_rs_eventos = mysql_num_rows($rs_eventos);

		if($totalrow_rs_eventos) {
			$id_evento = $row_rs_eventos['id_evento'];
			$evento_key = $row_rs_eventos['evento_key'];

			$id_sala = $row_rs_eventos['id_sala'];
			$evento_tipo = $row_rs_eventos['evento_tipo'];
			$evento_nombre = $row_rs_eventos['evento_nombre'];
			$evento_descripcion = $row_rs_eventos['evento_descripcion'];
			$evento_fecha = $row_rs_eventos['evento_fecha'];
			$usuario_que_carga = $row_rs_eventos['usuario_que_carga'];
			$ip_visitante = $row_rs_eventos['ip_visitante'];
			$fecha_carga = $row_rs_eventos['fecha_carga'];
			$usuario_que_modifica = $row_rs_eventos['usuario_que_modifica'];
			$ip_visitante_modificacion = $row_rs_eventos['ip_visitante_modificacion'];
			$fecha_modificacion = $row_rs_eventos['fecha_modificacion'];


			$evento_latitud = $row_rs_eventos['evento_latitud'];
			$evento_longitud = $row_rs_eventos['evento_longitud'];
			$evento_gridref = $row_rs_eventos['evento_gridref'];

			$evento_nombre = arreglar_datos_db2($evento_nombre);
			$evento_descripcion = arreglar_datos_db2($evento_descripcion);

	//consultar en la base de datos
			$query_rs_imagen = "SELECT id_foto, nombre_foto, fecha_carga, recorte_foto_nombre, recorte_foto_miniatura FROM fotos_publicaciones WHERE id_evento = $evento ORDER BY id_foto DESC ";
			$rs_imagen = mysql_query($query_rs_imagen)or die(mysql_error());
			$row_rs_imagen = mysql_fetch_assoc($rs_imagen);
			$totalrow_rs_imagen = mysql_num_rows($rs_imagen);

			$id_foto_portada = 0;
			$foto_portada = "";

			$ruta_imagenes = $Servidor_url.'APLICACION/Imagenes/eventos/recortes/';

			if($totalrow_rs_imagen) {
				$id_foto_portada = $row_rs_imagen['id_foto'];
				$foto_portada = $ruta_imagenes.$row_rs_imagen['recorte_foto_nombre'];
			}


//consultar en la base de datos
			$query_rs_tarifas = "SELECT id_tarifa, tarifa, tarifa_descripcion, usuario_que_crea, ip_visitante, fecha_carga, usuario_que_modifica, ip_visitante_modificacion, fecha_modificacion FROM tarifas_eventos WHERE id_evento = $evento";
			$rs_tarifas = mysql_query($query_rs_tarifas)or die(mysql_error());
			$row_rs_tarifas = mysql_fetch_assoc($rs_tarifas);
			$totalrow_rs_tarifas = mysql_num_rows($rs_tarifas);

			$explorar_fecha = explode(' ', $evento_fecha);
			$fecha_sola = $explorar_fecha[0];
			$hora_comienzo = $explorar_fecha[1]; ?>

			var refEventos = firebase.database().ref().child("Eventos"); 

			var refConvalidaciones; 

			<?php if($evento_key) { ?>
				var evento_key = "<?php echo $evento_key; ?>";
			<?php } else { ?>
				var evento_key = refEventos.push().key;
			<?php } ?>	

					window.onload = inicializar(evento_key);

					function inicializar (evento_key) {
						refConvalidaciones = refEventos.child(evento_key);

						<?php if($totalrow_rs_tarifas) { ?>
							<?php do { 
								$id_tarifa = $row_rs_tarifas['id_tarifa'];
								$tarifa = $row_rs_tarifas['tarifa'];
								$tarifa_descripcion = $row_rs_tarifas['tarifa_descripcion'];
								$array_tarifa[] = $id_tarifa; ?>

								var tarifa<?php echo $id_tarifa;?> = {
									tarifa: <?php echo $tarifa;?>,
									tarifa_descripcion: "<?php echo $tarifa_descripcion;?>"
								};

								<?php } while ($row_rs_tarifas = mysql_fetch_assoc($rs_tarifas)); ?>

								var tarifasTodas = {
									<?php foreach ($array_tarifa as $value) { ?>
										<?php echo $value; ?>: tarifa<?php echo $value; ?>,
										<?php } ?>
										tarifa0: {}
									};
									<?php } else { ?>
										var tarifasTodas = {};
										<?php } ?>


										var sala = {
											id_sala: <?php echo $id_sala; ?>,
											evento_latitud: "<?php echo $evento_latitud;?>",
											evento_longitud: "<?php echo $evento_longitud;?>",
											evento_gridref: "<?php echo $evento_gridref;?>"
										};

										<?php if($id_foto_portada) { ?>
											var foto = {
												id_foto_portada: <?php echo $id_foto_portada; ?>,
												foto_portada: "<?php echo $foto_portada;?>"
											};
											<?php } else { ?>
												var foto = {};
												<?php } ?>	

												refConvalidaciones.update({
													id_evento: <?php echo $id_evento; ?>,
													sala: sala,
													evento_tipo: "<?php echo $evento_tipo; ?>",
													evento_nombre: "<?php echo $evento_nombre; ?>",
													evento_descripcion: "<?php echo $evento_descripcion; ?>",
													evento_fecha: "<?php echo $evento_fecha; ?>",
													usuario_que_carga: <?php echo $usuario_que_carga; ?>,
													ip_visitante: "<?php echo $ip_visitante; ?>",
													fecha_carga: "<?php echo $fecha_carga; ?>",
													usuario_que_modifica: <?php echo $usuario_que_modifica; ?>,
													ip_visitante_modificacion: "<?php echo $ip_visitante_modificacion; ?>",
													fecha_modificacion: "<?php echo $fecha_modificacion; ?>",
													fecha_sola: "<?php echo $fecha_sola; ?>",
													hora_comienzo: "<?php echo $hora_comienzo; ?>",
													foto_portada: foto,
													tarifas: tarifasTodas
												});
												enviarDatoAServidor(evento_key);
											}



											<?php } ?> 

											function enviarDatoAServidor(keypepe) {
												var sendInfo = {
													key: keypepe,
													id_evento: "<?php echo $id_evento; ?>"
												};

												$.ajax({
													type: "POST",
													url: "<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/eventos/json/04-sincronizar-evento-json.php",
													dataType: "json",
													success: function (data) {
														response = JSON.stringify(data);
														var json = jQuery.parseJSON(response);
														if(json.respuesta == "OK") {
															setInterval( function() {
																window.location.href = "<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/eventos/03-ficha-evento.php?evento=<?php echo $id_evento; ?>" 	
															}, 1500 );
														} else {
															alert(json.respuesta);
														}							
													},
													data: sendInfo
												});
											}
										</script>
									</body>
									</html>