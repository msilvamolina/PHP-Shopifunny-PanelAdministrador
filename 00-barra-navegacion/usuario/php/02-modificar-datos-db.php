<?php include('../../../../paginas_include/variables-generales.php'); 
include('../../../includes/permisos-usuarios.php');
$permisos_pagina = '';

$actualizar_datos = 1;
include('../../../php/verificar-permisos.php');

$nombre_usuario = trim($_POST['nombre_usuario']);
$nombre = trim($_POST['nombre']);
$apellido = trim($_POST['apellido']);
$email = trim($_POST['email']);
$telefono = trim($_POST['telefono']);
$dia = trim($_POST['dia']);
$mes = trim($_POST['mes']);
$ano = trim($_POST['ano']);

$link = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/usuario/02-modificar-datos.php';
$link_ok = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/usuario/02-modificar-datos-ok.php';

if( (!$nombre_usuario) OR (!$nombre) OR (!$apellido) OR (!$email) OR (!$telefono) OR (!$dia)  OR (!$mes) OR (!$ano) ) {
	header('location: '.$link.'?error=1');
	exit;
}

$fecha_nacimiento = $ano.'-'.$mes.'-'.$dia;

$acentos = array('á', 'é', 'í', 'ó', 'ú');
$no_acentos = array('a', 'e', 'i', 'o', 'u');

$nombre_usuario = quitar_acentos($nombre_usuario);

$nombre_usuario = strtolower($nombre_usuario);
$nombre_usuario = str_replace(' ', '-', $nombre_usuario);

$array_eliminar = array('"', "'");
$nombre_usuario = str_replace($array_eliminar, '', $nombre_usuario);
$nombre_usuario = str_replace($acentos, $no_acentos, $nombre_usuario);


conectar2('shopifun', "admin");

//consultar en la base de datos
$query_rs_usuario = "SELECT id_usuario FROM usuarios WHERE usuario_usuario = '$nombre_usuario' ";
$rs_usuario = mysql_query($query_rs_usuario)or die(mysql_error());
$row_rs_usuario = mysql_fetch_assoc($rs_usuario);
$totalrow_rs_usuario = mysql_num_rows($rs_usuario);

if($totalrow_rs_usuario) {
	$id_usuario = $row_rs_usuario['id_usuario'];
	if($id_administrador!=$id_usuario) {
		header('location: '.$link.'?error=usuario');
		exit;		
	}
}

//consultar en la base de datos
$query_rs_email ="SELECT id_usuario FROM usuarios WHERE usuario_email = '$email' ";
$rs_email = mysql_query($query_rs_email)or die(mysql_error());
$row_rs_email = mysql_fetch_assoc($rs_email);
$totalrow_rs_email = mysql_num_rows($rs_email);

if($totalrow_rs_email) {
	$id_usuario = $row_rs_email['id_usuario'];
	if($id_administrador!=$id_usuario) {
		header('location: '.$link.'?error=email');
		exit;		
	}
}

$fecha_modificacion = date('Y-m-d H:i:s');

mysql_query("UPDATE usuarios SET usuario_usuario='$nombre_usuario', usuario_nombre='$nombre', usuario_apellido='$apellido', usuario_email='$email', usuario_telefono='$telefono', usuario_nacimiento='$fecha_nacimiento', ultima_actualizacion_datos='$fecha_modificacion' WHERE id_usuario='$id_administrador'");

desconectar();

//si todo sale bien, actualizamos los datos en las variables asi el sistema no lo jode mas

$_SESSION['administrador_usuario'] = $nombre_usuario;
$_SESSION['administrador_nombre'] = $nombre;
$_SESSION['administrador_apellido'] = $apellido;	
$_SESSION['administrador_email'] = $email;
$_SESSION['administrador_telefono'] = $telefono;
$_SESSION['administrador_fecha_nacimiento'] = $fecha_nacimiento;

header('location: '.$link_ok);
exit;
?>