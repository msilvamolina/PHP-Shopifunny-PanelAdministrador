<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';

$actualizar_datos = 1;
include('../../php/verificar-permisos.php');

conectar2('moebius', "ProyectoMoebius");

//consultar en la base de datos
$query_rs_usaurios = "SELECT *  FROM usuarios WHERE id_usuario = $id_administrador ";
$rs_usaurios = mysql_query($query_rs_usaurios)or die(mysql_error());
$row_rs_usaurios = mysql_fetch_assoc($rs_usaurios);
$totalrow_rs_usaurios = mysql_num_rows($rs_usaurios);

$usuario_usuario = $row_rs_usaurios['usuario_usuario'];
$usuario_nombre = $row_rs_usaurios['usuario_nombre'];
$usuario_apellido = $row_rs_usaurios['usuario_apellido'];
$usuario_email = $row_rs_usaurios['usuario_email'];
$usuario_telefono = $row_rs_usaurios['usuario_telefono'];
$usuario_fecha_nacimiento = $row_rs_usaurios['usuario_nacimiento'];

if($usuario_fecha_nacimiento) {
	$explorar_fecha = explode('-', $usuario_fecha_nacimiento);

	$nacimiento_dia = $explorar_fecha[2];
	$nacimiento_mes = $explorar_fecha[1];
	$nacimiento_ano = $explorar_fecha[0];

}

$array_meses = array('', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
desconectar();
?>
<!doctype html>
<html lang="es" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/barra-pasos.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/negocios.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/popup.css"> <!-- Resource style -->

	<style type="text/css">
		.td_delete {
			padding: 10px;
			text-align: right;
			width: 30px;
		}
		.td_delete img {
			width: 30px;
			display: block;
		}

		.tabla {
			width: 100%;
		}
		.tabla tr td{
			padding: 10px;
		}	

		.tabla tr:nth-of-type(2n) {
			background: #f5e5f2;
		}

		.label_nueva {
			font-size: 14px;
			color: #848484;
			margin-bottom: 5px;
		}
		.label_nueva2 {
			font-size: 14px;
			color: #848484;
			position: absolute;
		}	
		.col-md-4 {
			padding: 0;
		}
	</style>
</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<!-- Contenido de la Pagina-->	
		<!-- Contenido de la Pagina-->
		<div class="cd-popup" id="popup_contrasena" role="alert">
			<div class="cd-popup-container">
				<p>No podés dejar ningún campo vacío</p>
				<ul class="cd-buttons">
					<li style="width:100%"><a onclick="cerrar_popup()">Bueno</a></li>
				</ul>
				<a href="#0" class="cd-popup-close img-replace"></a>
			</div> <!-- cd-popup-container -->
		</div> <!-- cd-popup -->			
		<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper">
			<!-- Contenido de la Pagina-->
			<div class="cd-form floating-labels" style="max-width:1600px">
				<div style="max-width:600px; margin:0 auto;">
					<section id="crear_categoria" >							
						<fieldset >
							<form onsubmit="return comprobar()" action="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/usuario/php/02-modificar-datos-db.php" method="post">
								<legend id="txt_nueva_categoria">Actualizar datos</legend>	
								<?php if($_GET['sistema']) { ?>
								<div class="alert alert-warning" role="alert">
									<strong><?php echo $administrador_nombre; ?>, </strong> Antes de continuar, necesitamos que verifiqués y complestés los datos que faltan
								</div>
								<?php } ?>
								<?php if($_GET['error'] == 'usuario') { ?>
								<div class="error-message">
									<p>El usuario ingresado ya existe</p>
								</div>
								<?php } ?>   
								<?php if($_GET['error'] == 'email') { ?>
								<div class="error-message">
									<p>El e-mail ingresado ya existe</p>
								</div>
								<?php } ?>    		               
								<?php if($_GET['error'] == 1) { ?>
								<div class="error-message">
									<p>No podés dejar campos vacíos</p>
								</div>
								<?php } ?>   					
								<div class="icon">
									<p class="label_nueva">Nombre de usuario. No puede tener espacios en blanco</p>
									<input class="company" type="text" value="<?php echo $usuario_usuario; ?>" name="nombre_usuario" id="nombre_usuario" required>
								</div> 					
								<div class="icon">
									<p class="label_nueva">Nombre</p>
									<input class="company" type="text" value="<?php echo $usuario_nombre; ?>" name="nombre" id="nombre" required>
								</div> 
								<div class="icon">
									<p class="label_nueva">Apellido</p>
									<input class="company" type="text" value="<?php echo $usuario_apellido; ?>" name="apellido" id="apellido" required>
								</div> 

								<div class="icon">
									<p class="label_nueva">E-mail</p>
									<input class="company" type="text" value="<?php echo $usuario_email; ?>" name="email" id="email" required>
								</div> 
								<div class="icon">
									<p class="label_nueva">Teléfono. Ej: 3815434376</p>
									<input class="company" type="text" value="<?php echo $usuario_telefono; ?>" name="telefono" id="telefono" required>
								</div> 		
								<div class="row" >
									<p class="label_nueva2">Fecha de nacimiento</p>

									<div class="col-md-4">
										<p class="cd-select icon">
											<select class="budget" required name="dia" id="dia">
												<option value="0">Día</option>
												<?php $i=1; do {
													$selected=null;
													if($nacimiento_dia==$i) {
														$selected = 'selected';
													}
													?>
													<option value="<?php echo $i; ?>" <?php echo $selected; ?> ><?php echo $i; ?></option>
													<?php $i++; } while($i<=31); ?>
												</select>
											</p>
										</div>
										<div class="col-md-4">
											<p class="cd-select icon">
												<select class="budget" required name="mes" id="mes" >
													<option value="0" selected="1">Mes</option>
													<?php $i=1; do {
														$selected=null;
														if($nacimiento_mes==$i) {
															$selected = 'selected';
														}
														?>
														<option value="<?php echo $i; ?>" <?php echo $selected; ?> ><?php echo $array_meses[$i]; ?></option>
														<?php $i++; } while($i<=12); ?>								
													</select>
												</p>
											</div>
											<div class="col-md-4">
												<p class="cd-select icon">
													<select class="budget" required name="ano" id="ano" >
														<option value="0" selected="1">Año</option>
														<?php $i=2000; do {
															$selected=null;
															if($nacimiento_ano==$i) {
																$selected = 'selected';
															}
															?>
															<option value="<?php echo $i; ?>" <?php echo $selected; ?> ><?php echo $i; ?></option>
															<?php $i= $i-1; } while($i>=1900); ?>								
														</select>
													</p>
												</div>
											</div>				    		    
											<div class="alinear_centro">
												<input type="submit" value="Guardar Cambios" >
											</div>

										</form>

									</fieldset>	
								</section>    	
							</div>	
						</div>
					</div> <!-- .content-wrapper -->
				</main> 
				<?php include('../../includes/pie-general.php');?>
				<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
				<script type="text/javascript">
					
					function cerrar_popup() {
						$('.cd-popup').removeClass('is-visible');
					}

					function comprobar() {
						var nombre_usuario = document.getElementById("nombre_usuario").value; 
						var nombre = document.getElementById("nombre").value;
						var apellido = document.getElementById("apellido").value;
						var email = document.getElementById("email").value; 
						var telefono = document.getElementById("telefono").value;
						var dia = document.getElementById("dia").value; 
						var mes = document.getElementById("mes").value;
						var ano = document.getElementById("ano").value;

						if( (!nombre_usuario) || (!nombre)  || (!apellido) || (!email) || (!telefono) || (dia==0)|| (mes==0)|| (ano==0) ) { 
							$('#popup_contrasena').addClass('is-visible');
							return false;
						} else {
							return true;
						}
					}
				</script>
			</body>
			</html>