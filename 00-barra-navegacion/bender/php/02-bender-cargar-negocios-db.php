<?php include('../../../../paginas_include/variables-generales.php'); 
include('../../../includes/permisos-usuarios.php');
$permisos_pagina = 'permisos_bender';
include('../../../php/verificar-permisos.php');

$codigo = $_POST['codigo'];

$separar_codigo = explode('<span class="m-results-business--position" data-position="ad0"></span>', $codigo);
$separar_codigo2 = explode('<!-- RIGHT COLUMN -->', $separar_codigo[1]);

//separamos todos los negocios
$separador = '<!-- TITULO -->';
$codigo_a_separar = $separar_codigo2[0];

$separar_codigo3 = explode($separador, $codigo_a_separar);

$total_negocios_cargados = 0;
conectar2('paradigm', 'bender');
foreach ($separar_codigo3 as $separador) {
	$separador=trim($separador);
	if($separador) {
		//obtenemos el titulo del negocio
		$separar_titulo = explode('</h3>', $separador);
		$separar_titulo2 = explode('</a>', $separar_titulo[0]);
		$separar_titulo3 = explode('">', $separar_titulo2[0]);
		$dato_titulo = trim($separar_titulo3[2]);
		//$dato_titulo es la variable con el titulo

		//obtenemos la categoria del negocio
		$separar_titulo = explode('class="m-results-business--classification-txt">', $separador);
		$separar_titulo2 = explode('</a>', $separar_titulo[1]);
		$dato_categoria = trim($separar_titulo2[0]);
		//$dato_categoria es la variable con la categoria

		//obtenemos la direccion del negocio
		$separar_titulo = explode('<span itemprop="addressLocality">', $separador);
		$separar_titulo2 = explode('</span>', $separar_titulo[1]);
		$dato_ubicacion = $separar_titulo2[0];
		$separar_titulo3= explode('-',$separar_titulo2[0]);
		$dato_ciudad = trim($separar_titulo3[0]);
		$dato_provincia = trim($separar_titulo3[1]);
		//obtenemos la direccion del negocio
		$separar_titulo = explode('<span itemprop="streetAddress">', $separador);
		$separar_titulo2 = explode('</span>', $separar_titulo[1]);
		$dato_direccion= trim($separar_titulo2[0]);
		//$dato_direccion es la variable con la direccion

		//obtenemos la direccion array del negocio
		$direccion_array = explode('<address itemprop="address">', $separador);

		$array_direccion = array('');
		$todas_las_direccion = null;
		if($direccion_array) {
			$direccion_array[0] = null;
			foreach ($direccion_array as $valor) {
				if($valor) {
					$explorar_valor = explode('</address>', $valor);
					$valor_extraido = $explorar_valor[0];
					$explorar_nuevo_valor = explode('<span>', $valor_extraido);

					if($explorar_nuevo_valor) {
						$valor2 = $explorar_nuevo_valor[1];
						$direccion_sola = trim($valor2);

						$valor3 = $explorar_nuevo_valor[3];
						$provincia_sola = trim($valor3);

						$direccion_sola .= '=>'.$provincia_sola; 

						$array_direccion[] = trim($direccion_sola);
					}
				}
			}
		}
		//$dato_direccion array es la variable con la direccion
		if($array_direccion) {
			$array_direccion = array_unique($array_direccion);
			foreach ($array_direccion as $direccion_sola) {
				if(!$todas_las_direccion) {
					$todas_las_direccion = $direccion_sola;
				} else {
					$todas_las_direccion .= '%%'.$direccion_sola;							
				}
			}
		}


		if($todas_las_direccion) {
			$dato_direccion = $todas_las_direccion;
		} else {
			$dato_direccion .= '=>'.$dato_ubicacion;
		}

		//analizamos los telefonos
		$telefono_array = explode('<div class="m-bip-otras-direcciones--telefonos">', $separador);

		$todos_los_telefonos = null;
		$array_telefonos = array('');
		if($telefono_array) {
			$telefono_array[0] = null;
			foreach ($telefono_array as $valor) {
				$explorar_valor = explode('<p itemprop="telephone">', $valor);
				$explorar_valor2 = explode('</p>', $explorar_valor[1]);

				$telefono = trim($explorar_valor2[0]);

				if($telefono) {
					$telefono = str_replace(' - ', '', $telefono);
					$array_telefonos[] = $telefono;
				}
			}
		}
		if($array_telefonos) {
			$array_telefonos = array_unique($array_telefonos);

			foreach ($array_telefonos as $valor) {
				if(!$todos_los_telefonos) {
					$todos_los_telefonos = $valor;
				} else {
					$todos_los_telefonos .= '%%'.$valor;							
				}
			}
		}


		//$dato_ciudad y dato_provincia son las variable scon la categoria

		//obtenemos la latitud y longitud del negocio
		$separar_titulo = explode('marker-small.png|', $separador);
		$separar_titulo2 = explode('&client', $separar_titulo[1]);
		$separar_titulo3= explode(',',$separar_titulo2[0]);
		$dato_latitud= trim($separar_titulo3[0]);
		$dato_longitud = trim($separar_titulo3[1]);
		//$dato_latitud y dato_longitud son las variable scon la categoria

		//obtenemos la descripcion del negocio
		$separar_titulo = explode('<li itemprop="description">', $separador);
		$separar_titulo2 = explode('</li>', $separar_titulo[1]);
		$dato_descripcion= trim($separar_titulo2[0]);
		//$dato_descripcion es la variable con la direccion


		//obtenemos la telefono del negocio
		$separar_titulo = explode('<span data-single-phone class="m-icon--single-phone" itemprop="telephone">', $separador);
		$separar_titulo2 = explode('</a>', $separar_titulo[1]);
		$separar_titulo3 = explode('</p>', $separar_titulo2[0]);
		$separar_titulo4 = explode('>', $separar_titulo3[0]);

		$dato_telefono = trim($separar_titulo4[1]);
		if(!$dato_telefono) {
			$separar_titulo = explode('itemprop="telephone">', $separador);
			$separar_titulo2 = explode('</p>', $separar_titulo[1]);
			$separar_titulo5 = explode(')', $separar_titulo2[0]);
			$dato_telefono = trim($separar_titulo2[0]);
		}
		//$dato_codigo_area, y  $dato_telefono es la variable con la direccion
		$dato_telefono = str_replace(' - ', '', $dato_telefono);

		if($todos_los_telefonos) {
			$dato_telefono = $todos_los_telefonos;
		}


		//obtenemos los servicios del negocio
		$separar_titulo = explode('<ul class="m-services">', $separador);
		$separar_titulo2 = explode('</ul>', $separar_titulo[1]);
		$dato_servicios = trim($separar_titulo2[0]);
		$dato_servicios = str_replace('<li><i class="m-icon--list-dash"></i>', '', $dato_servicios);
		$explorar_servicios = explode('</li>', $dato_servicios);
		if($explorar_servicios) {
			foreach ($explorar_servicios as $valor) {
				$valor = trim($valor);
				if($valor) {
					if($nuevo_dato_servicio) {
						$nuevo_dato_servicio = $nuevo_dato_servicio.', '.$valor;
					} else {
						$nuevo_dato_servicio = $valor;
					}
				}
			}
		}
		//$dato_direccion es la variable con la direccion

		//obtenemos el sitio web del negocio
		$separar_titulo = explode('<a itemprop="url"', $separador);
		$separar_titulo2 = explode('</a>', $separar_titulo[1]);
		$separar_titulo3 = explode('>', $separar_titulo2[0]);
		$dato_sitio_web= trim($separar_titulo3[1]);
		//$dato_sitio_web es la variable con la direccion

		$dato_titulo = arreglar_datos_db($dato_titulo);
		$dato_categoria = arreglar_datos_db($dato_categoria);
		$dato_direccion = arreglar_datos_db($dato_direccion);
		$dato_descripcion = arreglar_datos_db($dato_descripcion);

		echo '<p>'.$dato_titulo.'</p>';
		echo '<p>'.$dato_telefono.'</p>';		
		echo '<p>'.$dato_direccion.'</p>';

		echo '<br><br><br><br>';
		//cargamos los datos en la base de datos, antes vemos si no hay un negocio exactamente igual
		//consultar en la base de datos
		$query_rs_negocios = "SELECT id_bender_negocio FROM bender_negocios WHERE bender_titulo = '$dato_titulo' AND bender_categoria = '$dato_categoria' AND bender_ciudad = '$dato_ciudad' AND bender_provincia = '$dato_provincia' AND bender_direccion = '$dato_direccion' AND bender_telefono = '$dato_telefono' ";
		$rs_negocios = mysql_query($query_rs_negocios)or die(mysql_error());
		$row_rs_negocios = mysql_fetch_assoc($rs_negocios);
		$totalrow_rs_negocios = mysql_num_rows($rs_negocios);

		
		if(!$totalrow_rs_negocios) {
			 $query3 = "INSERT INTO bender_negocios (bender_titulo, bender_categoria, bender_direccion, bender_ciudad, bender_provincia, bender_sitio_web, bender_latitud, bender_longitud, bender_descripcion, bender_telefono, usuario_que_carga, ip_visitante) 
			 VALUES ('$dato_titulo', '$dato_categoria', '$dato_direccion','$dato_ciudad', '$dato_provincia', '$dato_sitio_web', '$dato_latitud', '$dato_longitud', '$dato_descripcion', '$dato_telefono', '$id_administrador', '$ip_visitante')  ";
			 $result = mysql_query($query3);
		     $id_reporte = mysql_insert_id();

		     $total_negocios_cargados++;
		}


	}
}//termina el array principal
desconectar();


$location = $Servidor_url.'sistemaV3/00-barra-navegacion/bender/03-bender-editar-negocios.php?total_cargados='.$total_negocios_cargados;
header('location:'.$location);
exit;

?>