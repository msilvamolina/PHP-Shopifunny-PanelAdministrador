<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = 'permisos_bender';
include('../../php/verificar-permisos.php');

$bender_negocio = trim($_GET['bender_negocio']);
$total_cargados = trim($_GET['total_cargados']);
$get_categoria = trim($_GET['categoria']);

$get_error = trim($_GET['error']);
conectar2('paradigm', 'guiadeservicios');

//consultar en la base de datos
$query_rs_provincias = "SELECT id_provincia, provincia_nombre FROM provincias ORDER BY provincia_nombre ASC ";
$rs_provincias = mysql_query($query_rs_provincias)or die(mysql_error());
$row_rs_provincias = mysql_fetch_assoc($rs_provincias);
$totalrow_rs_provincias = mysql_num_rows($rs_provincias);
do {
	$id_provincia = $row_rs_provincias['id_provincia'];
	$provincia_nombre = $row_rs_provincias['provincia_nombre'];
	$array_provincias[$id_provincia] = $provincia_nombre;
} while($row_rs_provincias = mysql_fetch_assoc($rs_provincias));

//consultar en la base de datos
$query_rs_array_ciudades = "SELECT id_ciudad, ciudad_nombre  FROM ciudades ORDER BY ciudad_nombre ";
$rs_array_ciudades = mysql_query($query_rs_array_ciudades)or die(mysql_error());
$row_rs_array_ciudades = mysql_fetch_assoc($rs_array_ciudades);
$totalrow_rs_array_ciudades = mysql_num_rows($rs_array_ciudades);

do {
	$id_ciudad = $row_rs_array_ciudades['id_ciudad'];
	$ciudad_nombre = $row_rs_array_ciudades['ciudad_nombre'];
	$array_ciudades_completas[$id_ciudad] = $ciudad_nombre;	
} while($row_rs_array_ciudades = mysql_fetch_assoc($rs_array_ciudades));

//consultar en la base de datos
$query_rs_grupo_categoria = "SELECT id_grupo_categoria, categoria_descripcion, categoria_nombre, categoria_imagen FROM grupo_categorias ORDER BY categoria_nombre ASC";
$rs_grupo_categoria = mysql_query($query_rs_grupo_categoria)or die(mysql_error());
$row_rs_grupo_categoria = mysql_fetch_assoc($rs_grupo_categoria);
$totalrow_rs_grupo_categoria = mysql_num_rows($rs_grupo_categoria);

desconectar();
$WHERE = null;
if($bender_negocio) {
	$WHERE = 'id_bender_negocio = '.$bender_negocio.' AND ';
}
conectar2('paradigm', 'bender');
//consultar en la base de datos
$query_rs_bender_negocio = "SELECT id_bender_negocio, bender_titulo, bender_categoria, bender_direccion, bender_ciudad, bender_provincia, bender_sitio_web, bender_latitud, bender_longitud, bender_descripcion, bender_telefono  FROM bender_negocios WHERE $WHERE usuario_que_carga = $id_administrador AND negocio_corregido IS NULL ORDER BY id_bender_negocio DESC";
$rs_bender_negocio = mysql_query($query_rs_bender_negocio)or die(mysql_error());
$row_rs_bender_negocio = mysql_fetch_assoc($rs_bender_negocio);
$totalrow_rs_bender_negocio = mysql_num_rows($rs_bender_negocio);

if(!$totalrow_rs_bender_negocio) {
	//no hay negocio para editar
	$redirigir = $Servidor_url.'sistemaV3/00-barra-navegacion/bender/02-bender-cargar-negocios.php';
	header('location:'. $redirigir);
	exit;
}
$id_bender_negocio = $row_rs_bender_negocio['id_bender_negocio'];

$bender_titulo = $row_rs_bender_negocio['bender_titulo'];
$bender_categoria = $row_rs_bender_negocio['bender_categoria'];
$bender_direccion = $row_rs_bender_negocio['bender_direccion'];
$bender_ciudad = $row_rs_bender_negocio['bender_ciudad'];
$bender_provincia = $row_rs_bender_negocio['bender_provincia'];
$bender_sitio_web = $row_rs_bender_negocio['bender_sitio_web'];
$bender_latitud = $row_rs_bender_negocio['bender_latitud'];
$bender_longitud = $row_rs_bender_negocio['bender_longitud'];
$bender_descripcion = $row_rs_bender_negocio['bender_descripcion'];
$bender_codigo_area = $row_rs_bender_negocio['bender_codigo_area'];
$bender_telefono = $row_rs_bender_negocio['bender_telefono'];


$bender_descripcion = strtolower($bender_descripcion);
$bender_descripcion = ucfirst($bender_descripcion);

function depurar_direccion($bender_direccion) {
	//depuramos la direccion porque tiene muchos espacios entre los datos
	$caracteres_molestos = array('</span>');
	$bender_direccion = str_replace($caracteres_molestos, '', $bender_direccion);
	$bender_direccion = preg_replace("/\r\n+|\r+|\n+|\t+/i", " ", $bender_direccion);
	$explorar_direccion = explode(' ', $bender_direccion);
	$nueva = null;
	foreach ($explorar_direccion as $valor) {
		if($valor) {
			$valor = trim($valor);
			if($nueva) {
				$nueva = $nueva.' '.$valor;
			} else {
				$nueva = $valor;
			}
		}
	}
	return trim($nueva);
}
if(!$bender_provincia) {
	$bender_provincia = 0;
}
if(!$bender_ciudad) {
	$bender_ciudad = 0;
}
if(!$bender_codigo_area) {
	$bender_codigo_area = 0;
}
desconectar();

conectar2('paradigm', 'guiadeservicios');

$busqueda_subgrupo_categoria = 0;
if(!$get_categoria) {
	$busqueda = $bender_categoria;
	//buscamos la categoria del negocio en nuestra base de datos
	$query_rs_buscar_categoria = "SELECT id_grupo_categoria, MATCH (categoria_nombre, categoria_descripcion) AGAINST ( '$busqueda' IN BOOLEAN MODE) AS BUSQUEDA1 FROM grupo_categorias WHERE (MATCH (categoria_nombre, categoria_descripcion) AGAINST ( '$busqueda' IN BOOLEAN MODE)) ORDER BY id_grupo_categoria DESC";
	$rs_buscar_categoria = mysql_query($query_rs_buscar_categoria)or die(mysql_error());
	$row_rs_buscar_categoria = mysql_fetch_assoc($rs_buscar_categoria);
	$totalrow_rs_buscar_categoria = mysql_num_rows($rs_buscar_categoria);

	$busqueda_grupo_categoria = $row_rs_buscar_categoria['id_grupo_categoria'];

	if($busqueda_grupo_categoria) {
		//consultar en la base de datos
		$query_rs_buscar_subgrupo = "SELECT id_subgrupo, MATCH (subgrupo_nombre, subgrupo_nombre) AGAINST ( '$busqueda' IN BOOLEAN MODE) AS BUSQUEDA1 FROM subgrupos_categorias WHERE id_grupo_dependiente = $busqueda_grupo_categoria AND (MATCH (subgrupo_nombre, subgrupo_nombre) AGAINST ( '$busqueda' IN BOOLEAN MODE)) ORDER BY id_subgrupo DESC";
		$rs_buscar_subgrupo = mysql_query($query_rs_buscar_subgrupo)or die(mysql_error());
		$row_rs_buscar_subgrupo = mysql_fetch_assoc($rs_buscar_subgrupo);
		$totalrow_rs_buscar_subgrupo = mysql_num_rows($rs_buscar_subgrupo);
		
		$busqueda_subgrupo_categoria = $row_rs_buscar_subgrupo['id_subgrupo'];
	}
} else {
	//consultar en la base de datos
	$query_rs_analizar_categorias = "SELECT id_grupo_categoria, id_subgrupo_categoria FROM categorias WHERE id_categoria = $get_categoria ";
	$rs_analizar_categorias = mysql_query($query_rs_analizar_categorias)or die(mysql_error());
	$row_rs_analizar_categorias = mysql_fetch_assoc($rs_analizar_categorias);
	$totalrow_rs_analizar_categorias = mysql_num_rows($rs_analizar_categorias);
	
	$busqueda_grupo_categoria = $row_rs_analizar_categorias['id_grupo_categoria'];
	$busqueda_subgrupo_categoria = $row_rs_analizar_categorias['id_subgrupo_categoria'];
}



$busqueda = $bender_provincia;
//consultar en la base de datos
$query_rs_busqueda_provincia = "SELECT id_provincia, MATCH (provincia_nombre) AGAINST ( '$busqueda' IN BOOLEAN MODE) AS BUSQUEDA1 FROM provincias WHERE (MATCH (provincia_nombre) AGAINST ( '$busqueda' IN BOOLEAN MODE)) ORDER BY id_provincia DESC";
$rs_busqueda_provincia = mysql_query($query_rs_busqueda_provincia)or die(mysql_error());
$row_rs_busqueda_provincia = mysql_fetch_assoc($rs_busqueda_provincia);
$totalrow_rs_busqueda_provincia = mysql_num_rows($rs_busqueda_provincia);

$busqueda_provincia = $row_rs_busqueda_provincia['id_provincia'];


if($busqueda_provincia) {
	//consultar en la base de datos
	$busqueda = '"'.$bender_ciudad.'"';

	$query_rs_busqueda_ciudad = "SELECT id_ciudad, MATCH (ciudad_nombre) AGAINST ( '$busqueda' IN BOOLEAN MODE) AS BUSQUEDA1 FROM ciudades WHERE id_provincia = $busqueda_provincia AND (MATCH (ciudad_nombre) AGAINST ( '$busqueda' IN BOOLEAN MODE)) ORDER BY id_ciudad DESC";
	$rs_busqueda_ciudad = mysql_query($query_rs_busqueda_ciudad)or die(mysql_error());
	$row_rs_busqueda_ciudad = mysql_fetch_assoc($rs_busqueda_ciudad);
	$totalrow_rs_busqueda_ciudad = mysql_num_rows($rs_busqueda_ciudad);
	
	$busqueda_ciudad = $row_rs_busqueda_ciudad['id_ciudad'];

	//consultar en la base de datos
	$query_rs_ciudad = "SELECT id_ciudad, ciudad_nombre, id_provincia FROM ciudades WHERE id_provincia = $busqueda_provincia ORDER BY ciudad_nombre ASC ";
	$rs_ciudad = mysql_query($query_rs_ciudad)or die(mysql_error());
	$row_rs_ciudad = mysql_fetch_assoc($rs_ciudad);
	$totalrow_rs_ciudad = mysql_num_rows($rs_ciudad);
}
	
	$busqueda = $bender_titulo;
	//consultar en la base de datos
	$query_rs_negocios = "SELECT id_negocio, negocio_provincia, negocio_ciudad, negocio_nombre, MATCH (negocio_nombre) AGAINST ( '$busqueda' IN BOOLEAN MODE) AS BUSQUEDA1 FROM negocios WHERE (MATCH (negocio_nombre) AGAINST ( '$busqueda' IN BOOLEAN MODE)) ORDER BY negocio_nombre DESC ";
	$rs_negocios = mysql_query($query_rs_negocios)or die(mysql_error());
	$row_rs_negocios = mysql_fetch_assoc($rs_negocios);
	$totalrow_rs_negocios = mysql_num_rows($rs_negocios);

	do {
		$id_negocio = $row_rs_negocios['id_negocio'];
		$negocio_nombre = $row_rs_negocios['negocio_nombre'];

		$negocio_provincia = $row_rs_negocios['negocio_provincia'];
		$negocio_ciudad = $row_rs_negocios['negocio_ciudad'];

		$negocio_provincia = $array_provincias[$negocio_provincia];
		$negocio_ciudad = $array_ciudades_completas[$negocio_ciudad];

		$array_negocios[$id_negocio] = '<span>'.$negocio_nombre.'</span>, ubicado en: <span>'.$negocio_ciudad.', '.$negocio_provincia.'</span>';
		$array_negocios_simple[$id_negocio] = $negocio_nombre.', ubicado en: '.$negocio_ciudad.', '.$negocio_provincia;

	} while($row_rs_negocios = mysql_fetch_assoc($rs_negocios));

//consultar en la base de datos
$query_rs_negocios_like = "SELECT id_negocio, negocio_provincia, negocio_ciudad, negocio_nombre FROM negocios WHERE negocio_nombre LIKE '$busqueda' ";
$rs_negocios_like = mysql_query($query_rs_negocios_like)or die(mysql_error());
$row_rs_negocios_like = mysql_fetch_assoc($rs_negocios_like);
$totalrow_rs_negocios_like = mysql_num_rows($rs_negocios_like);

$totalrow_rs_negocios = $totalrow_rs_negocios + $totalrow_rs_negocios_like;
do {
		$id_negocio = $row_rs_negocios_like['id_negocio'];
		$negocio_nombre = $row_rs_negocios_like['negocio_nombre'];

		$negocio_provincia = $row_rs_negocios_like['negocio_provincia'];
		$negocio_ciudad = $row_rs_negocios_like['negocio_ciudad'];

		$negocio_provincia = $array_provincias[$negocio_provincia];
		$negocio_ciudad = $array_ciudades_completas[$negocio_ciudad];

		$array_negocios[$id_negocio] = '<span>'.$negocio_nombre.'</span>, ubicado en: <span>'.$negocio_ciudad.', '.$negocio_provincia.'</span>';
		$array_negocios_simple[$id_negocio] = $negocio_nombre.', ubicado en: '.$negocio_ciudad.', '.$negocio_provincia;

} while($row_rs_negocios_like = mysql_fetch_assoc($rs_negocios_like));

//hacemos una busqueda con like también	

desconectar();
?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>sistemaV3/css/form.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>sistemaV3/css/popup.css"> <!-- Resource style -->

	<title>Sistema Paradigma 2.0</title>
	<style type="text/css">
	.contenedor{
		text-align: center;
		margin: 0 auto;
		padding-top: 40px;
	}
	.bender{
		width: 100%;
		max-width: 134px;
	}

	h2 {
		margin-top: 10px;
		font-size: 26px;
	}
	.td_delete {
		padding: 10px;
		text-align: right;
		width: 30px;
	}
	.td_delete img {
		width: 30px;
		display: block;
	}	
	.cd-form {
		text-align: left;
	}

	.grupo_categoria {
		margin-left: -20px!important;
	}

	#section_categoria {
		background: #a7a7a7;
		padding: 30px;
		color: #fff;
	}

	#section_categoria h3 {
		font-size: 24px;
	}
	.select_class {
		background: #eeeeee !important;
	}

	.section_informacion_bender {
		background: #a7a7a7;
		padding: 30px;
		color: #fff;		
	}

	#section_informacion_bender {
		line-height: 18px;
	}
	.bender_chiquito {
		width: 30px;
	}

	.info_bender {
		margin-bottom: 20px;
	}
	.delete_categoria {
		float: right;
	}
	.delete_categoria img{
		width: 35px;
		margin-top: -5px;
	}
	#negocio_repetido {
		width: 100%;
		padding: 30px;
		background: #464646;
		color:#fff;
	}
	#negocio_repetido b {
		color: #e6d461;
	}

	#negocio_repetido span {
		color: #f90;
	}

	#negocio_repetido h3 {
		color: #a6db29;
		font-size: 32px;
		margin-bottom: 10px;
	}

	.error {
		background: #fdcceb;
		padding: 15px;
		border: 1px solid #f61162;
		max-width: 600px;
		margin: 0 auto;
	}
	.error h2{
		margin-bottom: 5px;
		color: #f61162;
	}	

	.ul_negocios {
		text-align:left;
		padding:20px;
		line-height: 15px;
	}

	.ul_negocios li a{
	  color: ;
	}
	/* select alternating items starting with the second item */
	.ul_negocios li:nth-of-type(2n) a{
	  color: #f90 !important;
	}

	a {
		cursor: pointer;
	}

	#negocio_repetido a {
		color: #fff;
	}

	#negocio_repetido a:hover, #negocio_repetido a:hover span {
		color: #fffb94;
	}	
	</style>
</head>
<body>
<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
	<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper">
<div class="cd-popup" id="popup_saltar_negocio" role="alert">
	<div class="cd-popup-container">
		<p>¿Estás seguro de querer saltar este negocio?</p>
		<ul class="cd-buttons">
			<li id="btn_confirmar_saltar_negocio"></li>
			<li><a onclick="cerrar_popup()">No</a></li>
		</ul>
		<a href="#0" class="cd-popup-close img-replace"></a>
	</div> <!-- cd-popup-container -->
</div> <!-- cd-popup -->
<div class="cd-popup" id="popup_ficha_negocio" role="alert">
	<div class="cd-popup-container" >
	<div style="padding:15px">
	<input type="hidden" value="0" id="negocio_elegido" />
		<p>¿Qué queréres hacer con este negocio?</p>
			<button class="vc_btn_largo vc_btn_amarillo vc_btn_3d" onclick="opcion_ver_ficha_negocio()" style="max-width:100%;" >Ver ficha del negocio</button>
			<button class="vc_btn_largo vc_btn_verde vc_btn_3d" onclick="agregar_direccion_telefono()" style="max-width:100%;" >Agregar direcciones a este negocio</button>
		<br><br>
	</div>	
		<ul class="cd-buttons">
			<li style="width:100%"><a onclick="cerrar_popup()">Cancelar</a></li>
		</ul>
		<a href="#0" class="cd-popup-close img-replace"></a>
	</div> <!-- cd-popup-container -->
</div> <!-- cd-popup -->


			<div class="contenedor">

			<img class="bender" src="<?php echo $Servidor_url;?>sistemaV3/00-barra-navegacion/bender/img/bender-circulo.png"> 		
			<?php if($total_cargados) { ?>
				<h2>Listo, cargué <?php echo $total_cargados; ?> negocios</h2>
			<?php } ?>	
		<div >		
		<?php if($get_error) { ?>
			<div class="error">
				<h2>Error!</h2>
			<?php if($get_error=='negocio_repetido') { ?>
				<p>Ya hay un negocio cargado con la misma información que intetaste cargar, lo mejor es saltar este negocio</p>
			<?php } ?>
			<?php if($get_error=='categorias') { ?>
				<p>El negocio tiene que tener una categoría y una subcategoría</p>
			<?php } ?>		
			<?php if($get_error=='provincia') { ?>
				<p>No podés cargar un negocio sin una provincia o ciudad</p>
			<?php } ?>					
			</div>
		<?php } ?>	
						<div class="cd-form floating-labels">
						<section id="crear_categoria" >							
							<fieldset >
								<form onsubmit="return validar_formulario()" action="<?php echo $Servidor_url; ?>sistemaV3/00-barra-navegacion/guia-de-servicios/php/02-nuevo-cargar-negocio.php" method="POST">
					<input type="hidden" value="bender" name="source">			
					<input  type="hidden" id="categoria_contador" value='2'>
					<input type="hidden" name="id_bender_negocio" value="<?php echo $id_bender_negocio; ?>" />
					<input type="hidden" name="pagina_que_manda" value="<?php echo $_SERVER['PHP_SELF']; ?>" />
					<input type="hidden" name="agregar_solo_direccion_telefono" id="agregar_solo_direccion_telefono" value="0" />

					 	<a class="vc_btn_largo vc_btn_verde vc_btn_3d" style="max-width:300px; float:right"
						onclick="saltar_negocio(<?php echo $id_bender_negocio; ?>)">
							<span class="fa-stack fa-lg pull-left">
							  <i class="fa fa-circle fa-stack-2x"></i>
							  <i class="fa fa-external-link-square fa-stack-1x fa-inverse"></i>
							</span>
								<p>Saltar negocio</p>
						</a>	
					<article id="agregar_solo_direccion">			
						<section id="section_informacion_bender">
							 <p>Ahora vamos a analizar el siguiente negocio: <b><?php echo $bender_titulo; ?></b></p>
							 <p>Ubicado en: <b><?php echo $bender_ciudad; ?>, <?php echo $bender_provincia; ?></b></p>
							 <br><p>La información que recogí esta arriba de cada elemento que tenés que corregir</p>
							</section>
							<br><br>			
						<?php if($totalrow_rs_negocios) { ?>				
							<section id="negocio_repetido">
								<h3>¡Importante!</h3>
								<?php if($negocio_de_otra_provincia) { ?>
									Al parecer, este negocio se llama igual que uno que ya está cargado, pero <b>es de otra provincia</b>. Checkéa bien su información antes de continuar.
									<br><br>					
								<?php } else {?>								
									Se encontró un negocio con el mismo nombre. <b>No se recomienda cargar este negocio, a menos que sea de otra provincia. </b>Por favor verificá bien eso.
									<br><br>
								<?php } ?>										

								<b>Negocios ya cargado: </b><br>
							<?php	foreach ($array_negocios as $id_negocio => $negocio) {

									?>
								<a onclick="mostrar_ficha_negocio(<?php echo $id_negocio; ?>)"><?php echo $negocio; ?></a>
								<br>
							<?php } ?>
							<br><br>
							Para ver la <span>Ficha del negocio</span> o <span>Agregarle direcciones</span>, hacé clic en algú negocio
								<br><br>
								Si querés agregarle información como otras direcciones y otros teléfonos. Entonces hacé clic en <b>"Editar"</b>. Sino, en <b>"Saltar este negocio"</b>
								<br><br>
															
							</section>
							<?php } ?>	
							<br><br>	
								<legend id="txt_nueva_categoria">Revisar negocio</legend>

							<p class="info_bender">
							<img class="bender_chiquito" src="<?php echo $Servidor_url;?>sistemaV3/00-barra-navegacion/bender/img/bender-circulo.png"> 
							categoría: <b><?php echo $bender_categoria; ?></b></p>

							<?php if(!$get_categoria) { ?>
								<a href="<?php echo $Servidor_url; ?>sistemaV3/00-barra-navegacion/guia-de-servicios/00-elegir-categoria.php?continuar=<?php echo $Servidor_url; ?>sistemaV3/00-barra-navegacion/bender/03-bender-editar-negocios.php" style="margin-left:35px">Elegir una categoría de la forma antigua</a>
							<?php } else { ?>
								<a href="<?php echo $Servidor_url; ?>sistemaV3/00-barra-navegacion/bender/03-bender-editar-negocios.php" style="margin-left:35px">Volver a la recomendación de Bender</a>
							<?php } ?>
							<input type="hidden" value="<?php echo $busqueda_grupo_categoria; ?>" name="select_grupo_categoria[]" id="select_grupo_categoria_1" />
							<br><br>
							<section id="section_categoria">
								<h3>Elegí una categoría y subcategoría</h3>
								<div id="demoBasic"  ></div>		

								<p class="cd-select">
									<select name="subgrupo[]" class="select_class" id="select_subgrupo_1" >
									<option value="0">Elegí una subcategoría</option>						
								</select></p>
							</section>
							<div id="div_agregar_categorias"></div>
						    <div class="alinear_centro">
							      <a class="boton_azul boton_amarillo" onclick="agregar_categoria()" id="btn_agregar_categoria">[+] Agregar a otra categoría</a>
						    </div>
							<br>
							<p class="info_bender">
							<img class="bender_chiquito" src="<?php echo $Servidor_url;?>sistemaV3/00-barra-navegacion/bender/img/bender-circulo.png"> 
							provincia: <b><?php echo $bender_provincia; ?></b>, ciudad: <b><?php echo $bender_ciudad; ?></b></p>	

							<section id="section_categoria">
							<h3>Elegí una provincia</h3><br>
								<p class="cd-select">
									<select name="provincia" class="select_class" required id="select_provincia" onchange="cargar_ciudades(this.value,0)">									
									<option value="0">Elegí una provincia</option>
									<?php foreach ($array_provincias as $id_provincia => $provincia_nombre) {
										$elegido = null;
										if($id_provincia==$busqueda_provincia) {
											$elegido = 'selected';
										}
										if($id_provincia) {
											echo '<option value="'.$id_provincia.'" '.$elegido.'>'.$provincia_nombre.'</option>';
										}
									}
									?>
								</select></p>
							<br><br>
							<h3>Elegí una ciudad</h3><br>

								<p class="cd-select">
									<select name="ciudad" class="select_class" required id="select_ciudad_0" >									
									<option value="0">Elegí una ciudad</option>
									<?php do{
										$id_ciudad = $row_rs_ciudad['id_ciudad'];
										$ciudad_nombre = $row_rs_ciudad['ciudad_nombre'];
										$elegido = null;
										if($id_ciudad==$busqueda_ciudad) {
											$elegido = 'selected';
										}
										if($id_ciudad) {
											echo '<option value="'.$id_ciudad.'" '.$elegido.'>'.$ciudad_nombre.'</option>';
										}
										$array_ciudades[$id_ciudad]=$ciudad_nombre;
									} while($row_rs_ciudad = mysql_fetch_assoc($rs_ciudad));
									?>
								</select></p>		
							    </section>						      					
							    <div class="icon">
							    	<label class="cd-label" for="cd-company">Nombre del negocio</label>
									<input class="company" type="text" name="nombre" value="<?php echo $bender_titulo; ?>" id="nueva_categoria_nombre" required>
							    </div> 			    
								<div class="icon">
									<label class="cd-label" for="cd-textarea">Descripción del negocio</label>
					      			<textarea class="message" name="descripcion" id="nueva_categoria_descripcion" ><?php echo $bender_descripcion; ?></textarea>
								</div>
								<div class="icon">
							    	<label class="cd-label" for="cd-email">Sitio web</label>
									<input class="email" type="text" value="<?php echo $bender_sitio_web; ?>" name="sitio_web" id="cd-email">
							    </div>				    
			    	<div class="icon">
				    	<label class="cd-label" for="cd-company">Palabras claves</label>
						<input class="company" type="text" name="palabras_claves"  id="nueva_categoria_palabras_claves" >
				    </div> 	
					<div class="icon">
				    	<label class="cd-label" for="cd-email">E-mails</label>
						<input class="email" type="email" name="email" id="cd-email">
				    </div>
					<input value="<?php echo $bender_latitud; ?>" name="latitud" type="hidden" />
					<input value="<?php echo $bender_longitud; ?>" name="longitud" type="hidden" />
					</article>
					<legend id="txt_nueva_categoria">Direcciones</legend>
					<div id="direcciones">
					<?php 
						$explorar_direcciones = explode('%%', $bender_direccion);
					$i=1;
					foreach ($explorar_direcciones as $bender_direccion) {
						$explorar_direccion = explode('=>', $bender_direccion);
						$bender_direccion = depurar_direccion($explorar_direccion[0]);
						$explorar_ubicacion = explode('-', $explorar_direccion[1]);

						$direccion_ciudad = trim($explorar_ubicacion[0]);
						$direccion_provincia = trim($explorar_ubicacion[1]);

						if($bender_direccion) {
					 ?>

					<div id="direccion_agregada_<?php echo $i; ?>">
						<p class="info_bender">
						<img class="bender_chiquito" src="<?php echo $Servidor_url;?>sistemaV3/00-barra-navegacion/bender/img/bender-circulo.png"> 
						provincia: <b><?php echo $direccion_provincia; ?></b>, ciudad: <b><?php echo $direccion_ciudad; ?></b></p>
				    <table width="100%" >
				    	<tr>
				    		<td >
								<p class="cd-select icon">
									<select name="select_provincia[]" class="budget" required onchange="cargar_ciudades(this.value,<?php echo $i; ?>)">									
									<?php foreach ($array_provincias as $id_provincia => $provincia_nombre) {
										$elegido = null;
										if($id_provincia==$busqueda_provincia) {
											$elegido = 'selected';
										}
										if($id_provincia) {
											echo '<option value="'.$id_provincia.'" '.$elegido.'>'.$provincia_nombre.'</option>';
										}
									}
									?>
								</select></p>						    	
				    		</td>
				    		<td >
								<p class="cd-select icon">
									<select name="select_ciudad[]" class="budget" required id="select_ciudad_<?php echo $i; ?>" >									
									<option value="0">Elegí una ciudad</option>
									<?php foreach ($array_ciudades as $id_ciudad => $ciudad_nombre) {
										$elegido = null;
										if($id_ciudad==$busqueda_ciudad) {
											$elegido = 'selected';
										}
										if($id_ciudad) {
											echo '<option value="'.$id_ciudad.'" '.$elegido.'>'.$ciudad_nombre.'</option>';
										}
									}
									?>
								</select></p>					    	
				    		</td>		
				    			<td class="td_delete"><a onclick="eliminar_direccion(<?php echo $i; ?>)"><img src="<?php echo $Servidor_url;?>sistemaV3/img/delete.png"></a></td>					    				    		
				    	</tr>
				    </table>					
				    <table width="100%" >
				    	<tr>
				    		<td width="60%">
								<input  type="text" name="direccion_nombre[]" value="<?php echo $bender_direccion;?>" required placeholder="Dirección" >							
				    		</td>						    				    		
				    	</tr>
				    	<tr><td>&nbsp;</td></tr>
				    </table>
				    </div>
				    <?php $i++;}} ?>
				    <input  type="hidden" id="direcciones_contador" value='<?php echo $i; ?>'>

				    </div>
				    <div class="alinear_centro">
					      <a class="boton_azul boton_amarillo" onclick="agregar_direccion()" id="btn_agregar_direccion">[+] Agregar otra dirección</a>
				    </div>
					<legend id="txt_nueva_categoria">Teléfonos</legend>
					    <table width="100%" id="telefonos">
					    <?php 
					   		$i = 1;
					    	$explorar_telefonos = explode('%%', $bender_telefono);
	
					    	foreach ($explorar_telefonos as $bender_telefono) {
						    	$explorar_telefono_codigo = explode(')', $bender_telefono);

						    	$bender_codigo_area = str_replace('(', '', $explorar_telefono_codigo[0]);
						    	$bender_codigo_area = trim($bender_codigo_area);

						    	$bender_telefono = trim($explorar_telefono_codigo[1]);
					    	 ?>
					    	<tr id="telefono_agregado_<?php echo $i; ?>">
					    		<td>
									<input  type="text" name="cod_area[]"  value="<?php echo $bender_codigo_area;?>" required placeholder="Código de área" >							
							    	
					    		</td>
					    		<td>
									<input  type="text" name="telefono[]" value="<?php echo $bender_telefono;?>" required placeholder="Teléfono">							
					    		</td>		
					    		<td class="td_delete"><a onclick="eliminar_telefono(<?php echo $i; ?>)"><img src="<?php echo $Servidor_url;?>sistemaV3/img/delete.png"></a></td>	    				    				    		
					    	</tr>
					    <?php $i++;}	?>
					    </table>
					    <input  type="hidden" id="telefonos_contador" value='<?php echo $i; ?>'>
				    <div class="alinear_centro">
					      <a class="boton_azul boton_amarillo" onclick="agregar_telefonos()">[+] Agregar teléfono</a>
				    </div>				    
				    <br><br><br>

				   <div class="alinear_centro">
							      	<input type="submit" value="Continuar" id="btn_nueva_categoria">
							    </div>
							    </form>
							    </fieldset>	
							</section>    	
			 									
						</div>
			</div>
		</div> <!-- .content-wrapper -->
	</main> 
<?php include('../../includes/pie-general.php');?>
<script src="<?php echo $Servidor_url; ?>sistemaV3/js/form.js"></script> <!-- Resource jQuery -->
<script src="<?php echo $Servidor_url; ?>sistemaV3/js/jquery.ddslick.min.js"></script> <!-- Resource jQuery -->
<script src="<?php echo $Servidor_url; ?>sistemaV3/js/popup.js"></script> <!-- Resource jQuery -->



<script type="text/javascript">

	function cargar_ciudades(provincia,campo) {	
		if(provincia) {
			$('#select_ciudad_'+campo).html('<option value="0">Cargando ciudades...</option>');
				$.ajax({
					url: "<?php echo $Servidor_url; ?>sistemaV3/00-barra-navegacion/guia-de-servicios/ajax/cargar-ciudades.php?provincia="+provincia,
					success: function (resultado) {
						$('#select_ciudad_'+campo).html(resultado);
					}
				});
		}			
	}	

	function eliminar_direccion(direccion) {
		$('#direccion_agregada_'+direccion).html('');
		
	}
	function eliminar_telefono(telefono) {
		$('#telefono_agregado_'+telefono).html('');
	}	

	function eliminar_categoria(categoria) {
		$('#nueva_categoria_'+categoria).html('');
	}

	function saltar_negocio(bender_negocio) {
		$('#popup_saltar_negocio').addClass('is-visible');
		var variable = '<a onclick="confirmar_saltar_negocio('+bender_negocio+')">Sí</a>';
		$('#btn_confirmar_saltar_negocio').html(variable);		
	}

	function confirmar_saltar_negocio(bender_negocio) {
		window.location.href = "<?php echo $Servidor_url;?>sistemaV3/00-barra-navegacion/bender/php/04-saltar-negocio.php?bender_negocio="+bender_negocio;
	}
	function cerrar_popup() {
		$('.cd-popup').removeClass('is-visible');
	}	
//Dropdown plugin data
var ddData = [
<?php
$url_imagen = $Servidor_url.'sistemaV3/imagenes-subidas-desde-web/categorias/';
	$i = 1;
 do { 
	$id = $row_rs_grupo_categoria['id_grupo_categoria'];
	$nombre = $row_rs_grupo_categoria['categoria_nombre'];
	$imagen = $row_rs_grupo_categoria['categoria_imagen'];
	$descripcion = $row_rs_grupo_categoria['categoria_descripcion'];

	$selected = 'false';

	if($id==$busqueda_grupo_categoria) {
		$selected = 'true';		
	}

	if($imagen) {
		$imagen = $url_imagen.$imagen;
	} else {
		$imagen = $Servidor_url.'sistemaV3/img/icono-imagen.png';
	}

	$descripcion = substr($descripcion, 0, 100);
	$descripcion .= '...';
	?>
	    {
	    	text: "<?php echo $nombre; ?>",
	        value: <?php echo $id; ?>,
	        selected: <?php echo $selected; ?>,
	        description: "<?php echo $descripcion; ?>",
	        imageSrc: "<?php echo $imagen; ?>"
	<?php if($i == $totalrow_rs_grupo_categoria) {
	     echo '}';
	} else {
		echo '},';
	}

 $i++; } while($row_rs_grupo_categoria = mysql_fetch_assoc($rs_grupo_categoria)); ?>
];

//Dropdown plugin data


$('#demoBasic').ddslick({
    data: ddData,
    width: 540,
    imagePosition: "left",
    selectText: "Elegí una categoría",
    onSelected: function (data) {
    	var valor = data.selectedData.value;
        cargar_subgrupo(valor, 1);
    }
});	


function cargar_subgrupo(categoria, numero) {
	document.getElementById("select_grupo_categoria_"+numero).value = categoria;
	var subgrupo_elegido = <?php echo $busqueda_subgrupo_categoria; ?>;
	var resultado = '<option value="0">Cargando subcategorías...</option>';
	$('#select_subgrupo_'+numero).html(resultado);
	$.ajax({
		url: "<?php echo $Servidor_url; ?>sistemaV3/00-barra-navegacion/bender/json/03-categorias-subgrupos.php?categoria="+categoria+"&subgrupo="+subgrupo_elegido,
		success: function (resultado) {
			$('#select_subgrupo_'+numero).html(resultado);
		}
	});	
}

function agregar_telefonos() {
	var telefonos_contador = document.getElementById("telefonos_contador").value;
	var codigo_area = "<?php echo $bender_codigo_area; ?>";
	if(!codigo_area) {
		codigo_area = '0381';
	}
	var variable = '<tr id="telefono_agregado_'+telefonos_contador+'">';
	 	variable = variable+'<td><input  type="text" name="cod_area[]" value="'+codigo_area+'" required placeholder="Código de área" ></td>';
		variable = variable+'<td><input  type="text" name="telefono[]" required placeholder="Teléfono"></td>';
 		variable = variable+'<td class="td_delete"><a onclick="eliminar_telefono('+telefonos_contador+')"><img src="<?php echo $Servidor_url;?>sistemaV3/img/delete.png"></a></td></tr>';
	
	document.getElementById("telefonos_contador").value = parseInt(telefonos_contador)+1;		
	$('#telefonos').append(variable);
}			
function agregar_direccion() {	
	var direccion_contador = document.getElementById("direcciones_contador").value;
	$('#btn_agregar_direccion').addClass('boton_trabajando');			
	document.getElementById("btn_agregar_direccion").disabled = true;
	var provincia = document.getElementById("select_provincia").value ;
	var ciudad = document.getElementById("select_ciudad_0").value ;
			$.ajax({
				url: "<?php echo $Servidor_url; ?>sistemaV3/00-barra-navegacion/guia-de-servicios/ajax/agregar-direccion2.php?provincia="+provincia+"&ciudad="+ciudad+"&contador="+direccion_contador,
				success: function (resultado) {
					$('#direcciones').append(resultado);
					$('#btn_agregar_direccion').removeClass('boton_trabajando');			
					document.getElementById("btn_agregar_direccion").disabled = false;
					document.getElementById("direcciones_contador").value = parseInt(direccion_contador)+1;		
				}
			});			
}

function agregar_categoria() {
	var categoria_contador = document.getElementById("categoria_contador").value;
	$('#btn_agregar_categoria').addClass('boton_trabajando');			
	document.getElementById("btn_agregar_categoria").disabled = true;

	$.ajax({
		url: "<?php echo $Servidor_url; ?>sistemaV3/00-barra-navegacion/guia-de-servicios/ajax/agregar-categoria.php?numero="+categoria_contador,
		success: function (resultado) {
			$('#div_agregar_categorias').append(resultado);	
			document.getElementById("categoria_contador").value = parseInt(categoria_contador)+1;	
			$('#btn_agregar_categoria').removeClass('boton_trabajando');			
			document.getElementById("btn_agregar_categoria").disabled = false;				
		}
	});		
}


function validar_formulario() {
	agregar_solo_direccion_telefono = document.getElementById('agregar_solo_direccion_telefono').value;
		var error = null;	
	if(agregar_solo_direccion_telefono==0) {
		var total = document.getElementsByName('select_grupo_categoria[]').length;
		for (var i = 1; i <= total; i++) {
			var dato =  document.getElementById('select_grupo_categoria_'+i).value;
			if((!dato)||(dato==0)) {
				error = 'No podés dejar categorías vacías';
			}
		}
		var total2 = document.getElementsByName('subgrupo[]').length;
		for (var i = 1; i <= total; i++) {
			var dato =  document.getElementById('select_subgrupo_'+i).value;
			if((!dato)||(dato==0)) {
				error = 'No podés dejar subcategorías vacías';
			}
		}	
	}
	if(error) {
		alert(error);
		return false;	
	} else {
		return true;
	}

}

function agregar_direccion_telefono() {
	var negocio = document.getElementById('negocio_elegido').value;	
	$('.cd-popup').removeClass('is-visible');	
	$('#agregar_solo_direccion').hide();
	document.getElementById('agregar_solo_direccion_telefono').value = negocio;
	
}

function mostrar_ficha_negocio(negocio) {
	 document.getElementById('negocio_elegido').value = negocio;
	$('#popup_ficha_negocio').addClass('is-visible');
}

function opcion_ver_ficha_negocio() {
	var negocio = document.getElementById('negocio_elegido').value;

	window.open('<?php echo $Servidor_url; ?>sistemaV3/00-barra-navegacion/guia-de-servicios/01-negocios-ficha.php?negocio='+negocio);
}

</script>
</body>
</html>