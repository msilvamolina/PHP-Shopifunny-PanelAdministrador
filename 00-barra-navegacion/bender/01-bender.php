<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = 'permisos_bender';
include('../../php/verificar-permisos.php');

?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<title>Sistema Paradigma 2.0</title>
	<style type="text/css">
	.contenedor{
		text-align: center;
		margin: 0 auto;
		padding-top: 40px;
	}
	.bender{
		width: 100%;
		max-width: 258px;
	}

	h2 {
		margin-top: 10px;
		font-size: 26px;
	}
	</style>
</head>
<body>
<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
	<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper">

			<div class="contenedor">

			<img class="bender" src="<?php echo $Servidor_url;?>sistemaV3/00-barra-navegacion/bender/img/bender.jpg"> 		
			<h2>Hola <?php echo $administrador_nombre; ?>,<br>¿Qué hacemos hoy?</h2>

			<center>
			<br><br>
				<a class="vc_btn_largo vc_btn_verde vc_btn_3d" style="max-width:300px"  href="<?php echo $Servidor_url;?>sistemaV3/00-barra-navegacion/bender/02-bender-cargar-negocios.php">
						<p>¡Carguemos negocios Bender!</p>
				</a>
			</center>
			</div>
		</div> <!-- .content-wrapper -->
	</main> 
<?php include('../../includes/pie-general.php');?>

</body>
</html>