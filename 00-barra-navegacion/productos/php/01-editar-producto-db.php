<?php include('../../../../paginas_include/variables-generales.php'); 
include('../../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../../php/verificar-permisos.php');

$accion = trim($_POST['accion']);
$accionDato = trim($_POST['accionDato']);

$producto = trim($_POST['producto']);
$esquinero = trim($_POST['esquinero']);
$categoria = trim($_POST['categoria']);
$titulo = trim($_POST['titulo']);

$url_origen = trim($_POST['url_origen']);
$descripcion = trim($_POST['descripcion']);
$tags = trim($_POST['tags']);
$precio_dolar = trim($_POST['precio_dolar']);
$precio_dolar_comparado = trim($_POST['precio_dolar_comparado']);
$precio_shipping_dolar = trim($_POST['precio_shipping_dolar']);
$precio_euro = trim($_POST['precio_euro']);
$precio_euro_comparado = trim($_POST['precio_euro_comparado']);
$precio_shipping_euro = trim($_POST['precio_shipping_euro']);
$stock = trim($_POST['stock']);
$ventas = trim($_POST['ventas']);

//arrays
$elemento = $_POST['elemento'];
$orden = $_POST['orden'];
$tamano_texto = $_POST['tamano_texto'];

$usuario_que_crea = $id_administrador;

if(!$producto) {
	$redireccion = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/productos/00-cargar-producto.php';
	header('Location: '.$redireccion);
	exit;
}

$lista_talles = null;

foreach ($_POST['talle'] as $id_talle => $value) {
	if(!$lista_talles) {
		$lista_talles = $id_talle;
	} else {
		$lista_talles .= ','.$id_talle;
	}
}

$descripcion = arreglar_datos_db($descripcion);
$titulo = arreglar_datos_db($titulo);
$tags = arreglar_datos_db($tags);

conectar2('shopifun', "admin");
$fecha_actual = date('Y-m-d H:i:s');
mysql_query("UPDATE productos SET producto_titulo='$titulo', producto_descripcion='$descripcion', 	producto_esquinero='$esquinero', producto_categoria='$categoria', producto_talles='$lista_talles',  	producto_precio_dolar='$precio_dolar', 	producto_precio_dolar_comparado='$precio_dolar_comparado', 	producto_precio_euro='$precio_euro', producto_precio_euro_comparado='$precio_euro_comparado', producto_stock='$stock', 	producto_ventas='$ventas', producto_precio_shipping_dolar='$precio_shipping_dolar',  producto_precio_shipping_euro='$precio_shipping_euro', producto_tags='$tags', usuario_que_modifica='$id_administrador', ip_visitante_modificacion='$ip_visitante', fecha_modificacion='$fecha_actual'
	WHERE id_producto='$producto'");

foreach ($elemento as $id_elemento => $contenido) {
	if($id_elemento) {
		$elemento_orden = $orden[$id_elemento];

		$tamano = 0;
		if($tamano_texto[$id_elemento]) {
			$tamano = $tamano_texto[$id_elemento];
		}

		mysql_query("UPDATE productos_cuerpo SET orden='$elemento_orden', tamano_texto='$tamano', contenido='$contenido' WHERE id_cuerpo='$id_elemento'");
	}
}

$redireccion = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/productos/01-editar-productos.php?producto='.$producto."#botonGuardar";


//borramos las direcciones que borro el usuario
//cargamos las direcciones

if($accion) {
	if($accion=="AgregarTexto") {
		$cuerpo_tipo = "texto";

		$query_rs_consulta = "SELECT orden FROM productos_cuerpo WHERE id_producto = $producto ORDER BY orden DESC LIMIT 1";
		$rs_consulta = mysql_query($query_rs_consulta)or die(mysql_error());
		$row_rs_consulta = mysql_fetch_assoc($rs_consulta);
		$totalrow_rs_consulta = mysql_num_rows($rs_consulta);

		$orden = 1;
		if($totalrow_rs_consulta) {
			$orden = $row_rs_consulta['orden'];
			$orden++;
		}

		$query = "INSERT INTO productos_cuerpo (id_producto, orden, cuerpo_tipo, usuario_que_carga, ip_visitante) 
		VALUES ('$producto','$orden','$cuerpo_tipo','$id_administrador', '$ip_visitante')";
		$result = mysql_query($query);
	}
	if($accion=="BorrarElemento") {
		$elemento = $accionDato;
		if($elemento) {

			$query_rs_elemento = "SELECT cuerpo_tipo, contenido FROM productos_cuerpo WHERE id_cuerpo = $elemento AND id_producto = $producto";
			$rs_elemento = mysql_query($query_rs_elemento)or die(mysql_error());
			$row_rs_elemento = mysql_fetch_assoc($rs_elemento);
			$totalrow_rs_elemento = mysql_num_rows($rs_elemento);

			$cuerpo_tipo = $row_rs_elemento['cuerpo_tipo'];
			$contenido = $row_rs_elemento['contenido'];

			mysql_query("DELETE FROM productos_cuerpo WHERE id_cuerpo=$elemento");

			if($cuerpo_tipo=='imagen') {
				$foto = $contenido;
		//consultar en la base de datos
				$query_rs_imagen = "SELECT nombre_foto, recorte_foto_nombre, recorte_foto_miniatura FROM fotos_publicaciones WHERE id_foto = $foto";
				$rs_imagen = mysql_query($query_rs_imagen)or die(mysql_error());
				$row_rs_imagen = mysql_fetch_assoc($rs_imagen);
				$totalrow_rs_imagen = mysql_num_rows($rs_imagen);

				$nombre_foto = $row_rs_imagen['nombre_foto'];
				$recorte_foto_nombre = $row_rs_imagen['recorte_foto_nombre'];
				$recorte_foto_miniatura = $row_rs_imagen['recorte_foto_miniatura'];


				$storeFolder = '../../../../APLICACION/Imagenes/productos/';
				$storeFolder2 = $storeFolder.'recortes/';

				unlink($storeFolder.$nombre_foto);
				unlink($storeFolder2.$nombre_foto);
				unlink($storeFolder2.$recorte_foto_nombre);
				unlink($storeFolder2.$recorte_foto_miniatura);

				mysql_query("DELETE FROM fotos_publicaciones WHERE id_foto=$foto");
			}
		}
	}
	if($accion=="AgregarVideo") {
		$video = $accionDato;
		if($video) {
			$explorar = explode('v=', $video);
			$explorar2 = explode('&', $explorar[1]);

			$video = $explorar2[0];

			$cuerpo_tipo = "video";

			$query_rs_consulta = "SELECT orden FROM productos_cuerpo WHERE id_producto = $producto ORDER BY orden DESC LIMIT 1";
			$rs_consulta = mysql_query($query_rs_consulta)or die(mysql_error());
			$row_rs_consulta = mysql_fetch_assoc($rs_consulta);
			$totalrow_rs_consulta = mysql_num_rows($rs_consulta);

			$orden = 1;
			if($totalrow_rs_consulta) {
				$orden = $row_rs_consulta['orden'];
				$orden++;
			}

			$query = "INSERT INTO productos_cuerpo (id_producto, orden, cuerpo_tipo, contenido, usuario_que_carga, ip_visitante) 
			VALUES ('$producto','$orden','$cuerpo_tipo','$video','$id_administrador', '$ip_visitante')";
			$result = mysql_query($query);
		}
	}
}

desconectar();

header('Location: '.$redireccion);
exit;
?>