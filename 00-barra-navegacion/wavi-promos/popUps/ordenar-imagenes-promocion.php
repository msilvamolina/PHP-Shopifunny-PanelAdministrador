<?php include('../../../../paginas_include/variables-generales.php'); 
include('../../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../../php/verificar-permisos.php');

$promo = $_GET['promo'];

if(!$promo) {
	$redirigir = $Servidor_url."PANELADMINISTRADOR/00-barra-navegacion/wavi-promos/02-crear-promocion.php";
	header('location:'.$redirigir);
	exit;
}

conectar2("wavi", "sitioweb");

//consultar en la base de datos
$query_rs_grupo_noticias = "SELECT id_grupo, grupo_nombre FROM grupo_noticias ORDER BY orden ASC ";
$rs_grupo_noticias = mysql_query($query_rs_grupo_noticias)or die(mysql_error());
$row_rs_grupo_noticias = mysql_fetch_assoc($rs_grupo_noticias);
$totalrow_rs_grupo_noticias = mysql_num_rows($rs_grupo_noticias);

do {
	$id_grupo = $row_rs_grupo_noticias['id_grupo'];
	$grupo_nombre = $row_rs_grupo_noticias['grupo_nombre'];

	$array_grupos[$id_grupo] = $grupo_nombre;
} while ($row_rs_grupo_noticias = mysql_fetch_assoc($rs_grupo_noticias));



$query_rs_imagen = "SELECT id_foto, nombre_foto, fecha_carga, recorte_foto_nombre, recorte_foto_miniatura FROM fotos_publicaciones WHERE id_promocion = $promo ORDER BY orden ASC";
$rs_imagen = mysql_query($query_rs_imagen)or die(mysql_error());
$row_rs_imagen = mysql_fetch_assoc($rs_imagen);
$totalrow_rs_imagen = mysql_num_rows($rs_imagen);

do {
	$id_foto = $row_rs_imagen['id_foto'];
	$array_foto[$id_foto] =  $row_rs_imagen['nombre_foto'];
	$array_recorte_foto_miniatura[$id_foto] =  $row_rs_imagen['recorte_foto_miniatura'];
} while($row_rs_imagen = mysql_fetch_assoc($rs_imagen));

desconectar();

$ruta_imagenes = $Servidor_url.'APLICACION/Imagenes/promos/';

?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<link rel='stylesheet' href='<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/drag/styles.css' type='text/css' media='all' />
	<?php include('../../../includes/head-general.php'); ?>

	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->

	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/paginacion.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/checkbox/style.css?v=3"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/popup.css"> <!-- Resource style -->
	<link href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/drag/jquery-ui.css" rel="stylesheet">
	<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/drag/jquery-1.7.2.min.js"></script>
	<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/drag/jquery-ui.min.js"></script>
	<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/drag/jquery.ui.touch-punch.min.js"></script>


	<script type="text/javascript">
		$(function() {
			$( "#test-list" ).sortable({
				placeholder: "ui-state-highlight",
				opacity: 0.6,
				update: function(event, ui) {
					var order = $('#test-list').sortable('serialize');
					var sendInfo = {
						order: order,
					};

					$('#btn_guardar').hide();
					$.ajax({
						type: "POST",
						url: "<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/wavi-noticias/ajax/01-guardar-orden-noticias-db.php",
						success: function (resultado) {
							$('#input_resultado').val(resultado);
							$('#btn_guardar').show();
						}, data: sendInfo
					});

				}
			});
			$( "#test-list" ).disableSelection(); 

			$( "#test-list" ).sortable({
				cancel: ".ui-state-disabled"
			});

		});

	</script>
	<style type="text/css">
		.rojo {
			color: #F44336;
			font-weight: bold;
		}

		.verde {
			color: #2E7D32;
			font-weight: bold;
		}

		.checkbox {
			z-index: 999;
		}
		h2 {
			font-size: 20px;
		}

		.input_popup {
			width: 100%;
			padding: 10px;
		}

		#agrupadas table td {
			background-color: #FFF9C4;
		}
		#agrupadas table .td_header {
			background-color: #FFC107;
			color: #fff;
		}

		.noticias_principales {
			background:#FC0;
			padding:20px;
			padding-top:1px;
		}


		.noticias_varias {
			background:#ccc;
			padding:20px;
			margin-top:20px;
			padding-top:1px;	
		}

		.appendoButtons{
			font-size:26px;
		}
		#sortable { list-style-type: none; margin: 0; padding: 0; width: 60%; }
		#sortable li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1.4em; height: 18px; }
		#sortable li span { position: absolute; margin-left: -1.3em; }

		.botones_noticias a{
			background:#F30;
			color:#fff;
			padding:10px;
			font-weight:bold;
			text-decoration:none;
		}

		.botones_noticias a:hover{
			background:#606;
		}

		.lista_header {
			background:#E0E0E0!important; 
			color:#424242;
		}

		.boton_submit {

		}

		#contenedor_boton {
			width: 100%;
			text-align: right;
		}
		.clear {
			float: none;
			clear: both;
		}

		.contenido_cuerpo {
			margin-top: 55px;
		}
	</style>
</head>
<body>
	<header class="cd-main-header">
		<a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/" class="cd-logo">
			<img src="<?php echo $Servidor_url; ?>img/logo_blanco.png" alt="Wavi"></a>

			<nav class="cd-nav">

				<a id="btn_guardar" class="vc_btn_largo vc_btn_amarillo vc_btn_3d" style="margin-top: 3px; margin-right: 100px; display: none;" onclick="guardar()">
					<span class="fa-stack fa-lg pull-left">
						<i class="fa fa-circle fa-stack-2x"></i>
						<i class="fa fa-save fa-stack-1x fa-inverse"></i>
					</span>
					<p>Guardar</p>
				</a>
				<img id="img_cargando" src="<?php echo $Servidor_url; ?>img/loader.gif" width="50" style="margin-right: 5px; margin-top: 3px; display:none" />

			</nav>
		</header> <!-- .cd-main-header -->	
		<main class="cd-main-content contenido_cuerpo">

			<form id="myForm" action="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-promos/php/05-guardar-orden-imagenes.php" method="POST">
				<input type="hidden" name="promo" value="<?php echo $promo; ?>" />
				<input type="hidden" required value="" name="orden" id="input_resultado">

				<ul id="test-list">
					<?php foreach ($array_foto as $id_foto => $foto_nombre) { 
						$foto = $ruta_imagenes.'recortes/'.$array_recorte_foto_miniatura[$id_foto];
						?>
						<li id="listItem_<?php echo $id_foto; ?>" class="lista_header" >
							<img src="<?php echo $foto; ?>" alt="move" width="120" />
							<strong><?php echo $foto_nombre; ?></strong></li>
							<?php } ?>
						</ul>
					</main> 
					<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
					<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/popup.js"></script> <!-- Resource jQuery -->
					<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/jquery.ddslick.min.js"></script> <!-- Resource jQuery -->

					<script type="text/javascript">
						function guardar() {
							document.getElementById("myForm").submit();
						}

					</script>
				</body>
				</html>