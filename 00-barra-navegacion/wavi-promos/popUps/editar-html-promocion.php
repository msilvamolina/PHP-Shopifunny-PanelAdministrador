<?php include('../../../../paginas_include/variables-generales.php'); 
include('../../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../../php/verificar-permisos.php');

$promo = $_GET['promo'];

$promo = trim($_GET['promo']);
$theme = trim($_GET['theme']);

if(!$promo) {
	$redirigir = $Servidor_url."PANELADMINISTRADOR/00-barra-navegacion/wavi-promos/02-crear-promocion.php";
	header('location:'.$redirigir);
	exit;
}

conectar2('mywavi', 'sitioweb');

//consultar en la base de datos
$query_rs_promo = "SELECT promocion_titulo, promocion_ciudad, promocion_provincia, promocion_categoria, promocion_cuerpo FROM promociones WHERE id_promocion = $promo";
$rs_promo = mysql_query($query_rs_promo)or die(mysql_error());
$row_rs_promo = mysql_fetch_assoc($rs_promo);
$totalrow_rs_promo = mysql_num_rows($rs_promo);

$promocion_titulo = $row_rs_promo['promocion_titulo'];
$promocion_ciudad = $row_rs_promo['promocion_ciudad'];
$promocion_provincia = $row_rs_promo['promocion_provincia'];
$promocion_categoria = $row_rs_promo['promocion_categoria'];
$promocion_cuerpo = $row_rs_promo['promocion_cuerpo'];

desconectar();

?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php include('../../../includes/head-general.php'); ?>

	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form3.css"> <!-- Resource style -->

	<link rel=stylesheet href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/codeMirror/doc/docs.css">
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/codeMirror/lib/codemirror.css">
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/codeMirror/theme/3024-day.css">
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/codeMirror/theme/3024-night.css">
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/codeMirror/theme/abcdef.css">
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/codeMirror/theme/ambiance.css">
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/codeMirror/theme/base16-dark.css">
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/codeMirror/theme/bespin.css">
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/codeMirror/theme/base16-light.css">
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/codeMirror/theme/blackboard.css">
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/codeMirror/theme/cobalt.css">
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/codeMirror/theme/colorforth.css">
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/codeMirror/theme/dracula.css">
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/codeMirror/theme/duotone-dark.css">
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/codeMirror/theme/duotone-light.css">
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/codeMirror/theme/eclipse.css">
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/codeMirror/theme/elegant.css">
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/codeMirror/theme/erlang-dark.css">
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/codeMirror/theme/hopscotch.css">
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/codeMirror/theme/icecoder.css">
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/codeMirror/theme/isotope.css">
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/codeMirror/theme/lesser-dark.css">
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/codeMirror/theme/liquibyte.css">
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/codeMirror/theme/material.css">
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/codeMirror/theme/mbo.css">
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/codeMirror/theme/mdn-like.css">
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/codeMirror/theme/midnight.css">
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/codeMirror/theme/monokai.css">
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/codeMirror/theme/neat.css">
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/codeMirror/theme/neo.css">
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/codeMirror/theme/night.css">
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/codeMirror/theme/panda-syntax.css">
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/codeMirror/theme/paraiso-dark.css">
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/codeMirror/theme/paraiso-light.css">
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/codeMirror/theme/pastel-on-dark.css">
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/codeMirror/theme/railscasts.css">
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/codeMirror/theme/rubyblue.css">
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/codeMirror/theme/seti.css">
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/codeMirror/theme/solarized.css">
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/codeMirror/theme/the-matrix.css">
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/codeMirror/theme/tomorrow-night-bright.css">
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/codeMirror/theme/tomorrow-night-eighties.css">
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/codeMirror/theme/ttcn.css">
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/codeMirror/theme/twilight.css">
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/codeMirror/theme/vibrant-ink.css">
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/codeMirror/theme/xq-dark.css">
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/codeMirror/theme/xq-light.css">
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/codeMirror/theme/yeti.css">
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/codeMirror/theme/zenburn.css">
	<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/codeMirror/lib/codemirror.js"></script>
	<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/codeMirror/mode/javascript/javascript.js"></script>
	<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/codeMirror/addon/selection/active-line.js"></script>
	<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/codeMirror/addon/edit/matchbrackets.js"></script>
	<style type="text/css">
		.CodeMirror {border: 1px solid black; font-size:13px; height: auto; min-height: 1500px;}
		.contenido_cuerpo {
			margin-top: 55px;
		}
	</style>
</head>
<body>
	<header class="cd-main-header">
		<a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/" class="cd-logo">
			<img src="<?php echo $Servidor_url; ?>img/logo_blanco.png" alt="Wavi"></a>

			<p><select  style="float:left; margin-top:15px; margin-left: 15px" onchange="selectTheme()" id=select>
				<option>default</option>
				<option>3024-day</option>
				<option>3024-night</option>
				<option>abcdef</option>
				<option>ambiance</option>
				<option>base16-dark</option>
				<option>base16-light</option>
				<option>bespin</option>
				<option>blackboard</option>
				<option>cobalt</option>
				<option>colorforth</option>
				<option>dracula</option>
				<option>duotone-dark</option>
				<option>duotone-light</option>
				<option>eclipse</option>
				<option>elegant</option>
				<option>erlang-dark</option>
				<option>hopscotch</option>
				<option>icecoder</option>
				<option>isotope</option>
				<option>lesser-dark</option>
				<option>liquibyte</option>
				<option>material</option>
				<option>mbo</option>
				<option>mdn-like</option>
				<option>midnight</option>
				<option>monokai</option>
				<option>neat</option>
				<option>neo</option>
				<option>night</option>
				<option>panda-syntax</option>
				<option>paraiso-dark</option>
				<option>paraiso-light</option>
				<option>pastel-on-dark</option>
				<option>railscasts</option>
				<option>rubyblue</option>
				<option>seti</option>
				<option>solarized dark</option>
				<option>solarized light</option>
				<option>the-matrix</option>
				<option>tomorrow-night-bright</option>
				<option>tomorrow-night-eighties</option>
				<option>ttcn</option>
				<option>twilight</option>
				<option>vibrant-ink</option>
				<option>xq-dark</option>
				<option>xq-light</option>
				<option>yeti</option>
				<option>zenburn</option>
			</select>

		</p>
		<nav class="cd-nav">

			<a id="btn_guardar" class="vc_btn_largo vc_btn_amarillo vc_btn_3d" style="margin-top: 3px; margin-right: 100px" onclick="guardar()">
				<span class="fa-stack fa-lg pull-left">
					<i class="fa fa-circle fa-stack-2x"></i>
					<i class="fa fa-save fa-stack-1x fa-inverse"></i>
				</span>
				<p>Guardar</p>
			</a>
			<img id="img_cargando" src="<?php echo $Servidor_url; ?>img/loader.gif" width="50" style="margin-right: 5px; margin-top: 3px; display:none" />

		</nav>
	</header> <!-- .cd-main-header -->	
	<main class="cd-main-content contenido_cuerpo">

		<form id="myForm" action="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-promos/php/04-editar-html-promocion-db.php" method="POST">
			<input type="hidden" name="promo" value="<?php echo $promo; ?>" />
			<input type="hidden" name="theme" value="<?php echo $theme; ?>" id="input_theme" />

			<textarea id="code"  name="code"><?php echo $promocion_cuerpo; ?></textarea></form>
			<br>

		</main> 
		<?php include('../../../includes/pie-general.php');?>
		<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
		<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/popup.js"></script> <!-- Resource jQuery -->
		<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/jquery.ddslick.min.js"></script> <!-- Resource jQuery -->

		<script type="text/javascript">
		//con esto elegimos el tema por defecto
		location.hash = "#dracula";

		var editor = CodeMirror.fromTextArea(document.getElementById("code"), {
			lineNumbers: true,
			styleActiveLine: true,
			matchBrackets: true
		});
		var input = document.getElementById("select");
		function selectTheme() {
			var theme = input.options[input.selectedIndex].textContent;
			editor.setOption("theme", theme);
			location.hash = "#" + theme;
			$("#input_theme").val(theme);
		}
		var choice = (location.hash && location.hash.slice(1)) ||
		(document.location.search &&
			decodeURIComponent(document.location.search.slice(1)));
		if (choice) {
			input.value = choice;
			editor.setOption("theme", choice);
		}
		CodeMirror.on(window, "hashchange", function() {
			var theme = location.hash.slice(1);
			if (theme) { input.value = theme; selectTheme(); }
		});

		function guardar() {
			document.getElementById("myForm").submit();
		}

	</script>
</body>
</html>