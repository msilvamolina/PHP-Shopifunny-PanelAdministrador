<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');

?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/popup.css"> 
</head>
<style type="text/css">
	.btn_eliminar {
		text-align: right;
		width: 100%;
	}

	a {
		cursor: pointer;
	}
	.boton_verde a{
		background: #48b617;
		color: #fff;
	}
	.boton_verde a:hover {
		background: #235d09 !important;
		color: #f6ff05;
	}	
	.boton_rojo a{
		background: #c40000;
		color: #fff;
	}
	.boton_rojo a:hover {
		background: #9e0101 !important;
		color: #f6ff05;
	}
	h3 {
		margin-bottom: 5px;
		font-weight: bold;
	}

	.portada {
		color: #2E7D32;
		font-weight: bold;
	}

	.rojo {
		color: #F44336;
		font-weight: bold;
	}

	.verde {
		color: #2E7D32;
		font-weight: bold;
	}
	a {
		cursor: pointer;
	}
	.input_video {
		width: 100%;
		padding: 10px;
		margin-top: 15px;
	}

	.input_texto {
		width: 100%;
		height: 200px;
		padding: 10px;
		margin-top: 15px;
	}


	.video-container {
		position: relative;
		padding-bottom: 56.25%;
		padding-top: 30px; height: 0; overflow: hidden;
	}
	
	.video-container iframe,
	.video-container object,
	.video-container embed {
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
	}
	.video_youtube {
		width: 100%;
	}

	#formTexto {
		margin-top: -150px;
		padding: 20px;
	}	

	.img_delete {
		width: 30px;
	}

	.td_delete {
		padding: 10px;
		text-align: right;
		width: 30px;
	}
	.td_delete img {
		width: 30px;
		display: block;
	}
	.txt_categoria_elegida {
		padding: 10px;
		color: #2c97de;
		display: block;
	}
	.grupo_categoria {
		margin-left: -20px!important;
	}

	#section_categoria {
		background: #a7a7a7;
		padding: 30px;
		color: #fff;
	}

	#section_categoria h3 {
		font-size: 24px;
	}
	.select_class {
		background: #eeeeee !important;
	}
	.delete_categoria {
		float: right;
	}
	.delete_categoria img{
		width: 35px;
		margin-top: -5px;
	}
	.boton_amarillo {
		background: #f90;
	}
	.boton_amarillo:hover {
		background: red;
		color: #fff;
	}
</style>


</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper">

			<div class="contenedor">
				<div >					<!-- Contenido de la Pagina-->	
					<div class="cd-form floating-labels">
						<section id="crear_categoria" >							
							<fieldset >
								<form onsubmit="return validar_formulario()" id="myForm" action="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-promos/php/02-crear-promocion-db.php" method="POST">

									<legend id="txt_nueva_categoria">Nueva Promo</legend>

									<input type="hidden" value="-1" name="categoria" id="select_grupo_categoria" />

									<div id="demoBasic"  ></div>		

									<div class="icon">
										<label class="cd-label" for="cd-company">Titulo</label>
										<input class="company" type="text" name="titulo" id="titulo" required>
									</div> 	

								</fieldset>	
							</section>    	

							<a name="botonGuardar"></a>
							<div class="alinear_centro">
								<input type="submit" value="Crear Promo" id="btn_nueva_categoria">
							</div>
						</form>
					</div>
				</div> <!-- .content-wrapper -->
			</main> 
			<?php include('../../includes/pie-general.php');?>
			<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
			<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/popup.js"></script> <!-- Resource jQuery -->
			<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/jquery.ddslick.min.js"></script> <!-- Resource jQuery -->

			<script type="text/javascript">

//Dropdown plugin data

$.ajax({
	type: "GET",
	url: "<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/bender/json/02-categorias-json.php?main=1",
	data: {},
	success: function (data) {;
		$('#demoBasic').ddslick({
			data: data,
			width: 600,
			imagePosition: "left",
			selectText: "Elegí una categoría",
			onSelected: function (data) {
				var valor = data.selectedData.value;
				$('#select_grupo_categoria').val(valor);
			}
		});	
	}
});

function validar_formulario() {
	var error = null;	
	var categoria = $("#select_grupo_categoria").val();
	var titulo = $('#titulo').val();

	if(!titulo) {
		error = "Escribí un título";
	}
	if(categoria<1) {
		error = "Elegí una categoría";
	}
	if(error) {
		alert(error);
		return false;	
	} else {
		return true;
	}
}

</script>
</body>
</html>