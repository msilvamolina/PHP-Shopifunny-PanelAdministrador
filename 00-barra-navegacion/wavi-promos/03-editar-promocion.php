<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');

$promo = trim($_GET['promo']);
$imagen_cargada = trim($_GET['imagen_cargada']);
$negocio = trim($_GET['negocio']);

if(!$promo) {
	$redirigir = $Servidor_url."PANELADMINISTRADOR/00-barra-navegacion/wavi-promos/02-crear-promocion.php";
	header('location:'.$redirigir);
	exit;
}


conectar2('mywavi', 'sitioweb');

//consultar en la base de datos
$query_rs_promo = "SELECT promocion_titulo, promocion_ciudad, promocion_provincia, promocion_categoria, promocion_cuerpo FROM promociones WHERE id_promocion = $promo";
$rs_promo = mysql_query($query_rs_promo)or die(mysql_error());
$row_rs_promo = mysql_fetch_assoc($rs_promo);
$totalrow_rs_promo = mysql_num_rows($rs_promo);

$promocion_titulo = $row_rs_promo['promocion_titulo'];
$promocion_ciudad = $row_rs_promo['promocion_ciudad'];
$promocion_provincia = $row_rs_promo['promocion_provincia'];
$promocion_categoria = $row_rs_promo['promocion_categoria'];
$promocion_cuerpo = $row_rs_promo['promocion_cuerpo'];


$query_rs_imagen = "SELECT id_foto, nombre_foto, fecha_carga, recorte_foto_nombre, recorte_foto_miniatura FROM fotos_publicaciones WHERE id_promocion = $promo ORDER BY orden ASC ";
$rs_imagen = mysql_query($query_rs_imagen)or die(mysql_error());
$row_rs_imagen = mysql_fetch_assoc($rs_imagen);
$totalrow_rs_imagen = mysql_num_rows($rs_imagen);

if($imagen_cargada) {
	$id_foto = $row_rs_imagen['id_foto'];
	$redireccionar = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/wavi-noticias/03-recortar-foto.php?foto='.$id_foto.'&promo='.$promo;
	header('location:'.$redireccionar);
	exit;
}

do {
	$id_foto = $row_rs_imagen['id_foto'];
	$array_foto[$id_foto] =  $row_rs_imagen['nombre_foto'];
	$array_fecha_carga[$id_foto] = $row_rs_imagen['fecha_carga'];
	$array_recorte_foto_nombre[$id_foto] =  $row_rs_imagen['recorte_foto_nombre'];
	$array_recorte_foto_miniatura[$id_foto] =  $row_rs_imagen['recorte_foto_miniatura'];
} while($row_rs_imagen = mysql_fetch_assoc($rs_imagen));
desconectar();

conectar2('mywavi', 'WAVI');
//consultar en la base de datos
$query_rs_provincias = "SELECT id_provincia, provincia_nombre  FROM provincias ";
$rs_provincias = mysql_query($query_rs_provincias)or die(mysql_error());
$row_rs_provincias = mysql_fetch_assoc($rs_provincias);
$totalrow_rs_provincias = mysql_num_rows($rs_provincias);

do {
	$id_provincia = $row_rs_provincias['id_provincia'];
	$provincia_nombre = $row_rs_provincias['provincia_nombre'];

	$array_provincias[335] = "Todas las provincias";
	$array_provincias[$id_provincia] = $provincia_nombre;
} while ($row_rs_provincias = mysql_fetch_assoc($rs_provincias));

//consultar en la base de datos
$query_rs_vinculaciones_negocios = "SELECT vinculacion_negocio_promocion.id_vinculacion, vinculacion_negocio_promocion.id_negocio, negocios.negocio_nombre, negocios.negocio_ciudad, negocios.negocio_provincia FROM vinculacion_negocio_promocion, negocios WHERE vinculacion_negocio_promocion.id_promocion = $promo AND vinculacion_negocio_promocion.id_negocio = negocios.id_negocio ORDER BY vinculacion_negocio_promocion.orden ASC";
$rs_vinculaciones_negocios = mysql_query($query_rs_vinculaciones_negocios)or die(mysql_error());
$row_rs_vinculaciones_negocios = mysql_fetch_assoc($rs_vinculaciones_negocios);
$totalrow_rs_vinculaciones_negocios = mysql_num_rows($rs_vinculaciones_negocios);

//consultar en la base de datos
$query_rs_provincias = "SELECT id_provincia, provincia_nombre FROM provincias ORDER BY provincia_nombre ASC ";
$rs_provincias = mysql_query($query_rs_provincias)or die(mysql_error());
$row_rs_provincias = mysql_fetch_assoc($rs_provincias);
$totalrow_rs_provincias = mysql_num_rows($rs_provincias);
do {
	$id_provincia = $row_rs_provincias['id_provincia'];
	$provincia_nombre = $row_rs_provincias['provincia_nombre'];
	$array_provincias[$id_provincia] = $provincia_nombre;
} while($row_rs_provincias = mysql_fetch_assoc($rs_provincias));

//consultar en la base de datos
$query_rs_array_ciudades = "SELECT id_ciudad, ciudad_nombre, id_provincia  FROM ciudades ORDER BY ciudad_nombre ";
$rs_array_ciudades = mysql_query($query_rs_array_ciudades)or die(mysql_error());
$row_rs_array_ciudades = mysql_fetch_assoc($rs_array_ciudades);
$totalrow_rs_array_ciudades = mysql_num_rows($rs_array_ciudades);

do {
	$id_ciudad = $row_rs_array_ciudades['id_ciudad'];
	$ciudad_nombre = $row_rs_array_ciudades['ciudad_nombre'];
	$array_ciudades[$id_ciudad] = $ciudad_nombre;
} while($row_rs_array_ciudades = mysql_fetch_assoc($rs_array_ciudades));
desconectar();

$link_elegir_negocios = $Servidor_url."PANELADMINISTRADOR/00-barra-navegacion/wavi-general/07-grupos-categorias-negocios.php?regresar_a_promo=".$promo."&grupo_categoria=".$promocion_categoria."&subgrupo_categoria=&provincia=".$promocion_provincia."&ciudad=".$promocion_ciudad;

$link_negocio = $Servidor_url."PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/03-negocios-ficha.php?negocio=";

$ruta_imagenes = $Servidor_url.'APLICACION/Imagenes/promos/';

?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form3.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/popup.css"> <!-- Resource style -->
	<script src="<?php echo $Servidor_url;?>PANELADMINISTRADOR/js/nicEdit2/nicEdit.js" type="text/javascript"></script>

</head>
<style type="text/css">
	.btn_eliminar {
		text-align: right;
		width: 100%;
	}

	a {
		cursor: pointer;
	}
	.boton_verde a{
		background: #48b617;
		color: #fff;
	}
	.boton_verde a:hover {
		background: #235d09 !important;
		color: #f6ff05;
	}	
	.boton_rojo a{
		background: #c40000;
		color: #fff;
	}
	.boton_rojo a:hover {
		background: #9e0101 !important;
		color: #f6ff05;
	}
	h3 {
		margin-bottom: 5px;
		font-weight: bold;
	}

	.portada {
		color: #2E7D32;
		font-weight: bold;
	}

	.rojo {
		color: #F44336;
		font-weight: bold;
	}

	.verde {
		color: #2E7D32;
		font-weight: bold;
	}
	a {
		cursor: pointer;
	}
	.input_video {
		width: 100%;
		padding: 10px;
		margin-top: 15px;
	}

	.input_texto {
		width: 100%;
		height: 200px;
		padding: 10px;
		margin-top: 15px;
	}


	.video-container {
		position: relative;
		padding-bottom: 56.25%;
		padding-top: 30px; height: 0; overflow: hidden;
	}
	
	.video-container iframe,
	.video-container object,
	.video-container embed {
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
	}
	.video_youtube {
		width: 100%;
	}

	#formTexto {
		margin-top: -150px;
		padding: 20px;
	}	

	.img_delete {
		width: 30px;
	}

	.td_delete {
		padding: 10px;
		text-align: right;
		width: 30px;
	}
	.td_delete img {
		width: 30px;
		display: block;
	}
	.txt_categoria_elegida {
		padding: 10px;
		color: #2c97de;
		display: block;
	}
	.grupo_categoria {
		margin-left: -20px!important;
	}

	#section_categoria {
		background: #a7a7a7;
		padding: 30px;
		color: #fff;
	}

	#section_categoria h3 {
		font-size: 24px;
	}
	.select_class {
		background: #eeeeee !important;
	}
	.delete_categoria {
		float: right;
	}
	.delete_categoria img{
		width: 35px;
		margin-top: -5px;
	}
	.boton_amarillo {
		background: #f90;
	}
	.boton_amarillo:hover {
		background: red;
		color: #fff;
	}

	.areacuerpo {
		height: 500px;
	}
</style>

<script type="text/javascript">
	//bkLib.onDomLoaded(function() { nicEditors.({fullPanel : true}).panelInstance('area1') });

	bkLib.onDomLoaded(function() {
		new nicEditor({fullPanel : true}).panelInstance('area1');
	});

</script>	
</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper">
			<div class="cd-popup" id="popup_negocio" role="alert">
				<div class="cd-popup-container">
					<p>Escribí el ID del negocio<br>
						<input class="input_video" type="text" id="input_agregar_negocio" placeholder="Ej: 4456" value="<?php echo $negocio; ?>"/>
						<br><br>
						<a href="<?php echo $link_elegir_negocios; ?>">Elegir negocios de una lista</a>
					</p>
					<ul class="cd-buttons">
						<li><a onclick="confirmar_agregar_negocio()">Continuar</a></li>
						<li><a onclick="cerrar_popup()">Cancelar</a></li>
					</ul>
					<a href="#0" class="cd-popup-close img-replace"></a>
				</div> <!-- cd-popup-container -->
			</div> <!-- cd-popup -->
			<div class="cd-popup" id="popup_categoria" role="alert">
				<div class="cd-popup-container">
					<p>¿Estás seguro de querer borrar esta imagen?</p>
					<ul class="cd-buttons">
						<li id="btn_confirmar_categoria"><a onclick="">Sí</a></li>
						<li><a onclick="cerrar_popup()">No</a></li>
					</ul>
					<a href="#0" class="cd-popup-close img-replace"></a>
				</div> <!-- cd-popup-container -->
			</div>
			<div class="cd-popup" id="popup_borrar_negocio" role="alert">
				<div class="cd-popup-container">
					<p>¿Seguro querés desvincular este negocio?</p>
					<input type="hidden" id="input_borrar_negocio" />

					<ul class="cd-buttons">
						<li><a onclick="confirmar_borrar_negocio()">Continuar</a></li>
						<li><a onclick="cerrar_popup()">Cancelar</a></li>
					</ul>
					<a href="#0" class="cd-popup-close img-replace"></a>
				</div> <!-- cd-popup-container -->
			</div> <!-- cd-popup -->

			<div class="contenedor">

				<div >					<!-- Contenido de la Pagina-->	

					<div class="cd-form floating-labels">
						<section id="crear_categoria" >							
							<fieldset >
								<form onsubmit="return validar_formulario()" id="myForm" action="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-promos/php/03-editar-promocion-db.php" method="POST">

									<legend id="txt_nueva_categoria">Nueva Promo</legend>

									<input type="hidden" value="<?php echo $promo; ?>" name="promo"/>

									<input type="hidden" value="-1" name="categoria" id="select_grupo_categoria" />

									<div id="demoBasic"  ></div><br>		

									<div class="icon">
										<label class="cd-label" for="cd-company">Titulo</label>
										<input class="company" value="<?php echo $promocion_titulo; ?>" type="text" name="titulo" id="titulo" required>
									</div> 	
									<div>
										<h4>Provincia</h4>
										<p class="cd-select icon">
											<select name="provincia" class="budget" id="select_elegir_provincia" onchange="cargar_ciudades(this.value)">
												<option value="0" >Elegí una provincia</option>
												<?php foreach ($array_provincias as $id_provincia => $provincia_nombre) {
													$provincia_elegida=null;
													if($id_provincia == $promocion_provincia) {
														$provincia_elegida = "selected";
													}
													echo '<option value="'.$id_provincia.'" '.$provincia_elegida.'>'.$provincia_nombre.'</option>';
												}?>
											</select>
										</p>
									</div>
									<div>
										<h4>Ciudad</h4>
										<p class="cd-select icon">
											<select name="ciudad" class="budget" id="select_elegir_ciudad" >
												<option value="0" >Elegí una ciudad</option>
											</select>
										</p>
									</div>
									<br>

									<br>
									<legend id="txt_nueva_categoria">Imágenes</legend>
									<div id="direcciones">
										<table class="table table-striped">
											<tbody>
												<?php
												if($totalrow_rs_imagen) { 
													foreach ($array_foto as $id_foto => $nombre_foto) {
														$fecha_carga = $array_fecha_carga[$id_foto];
														$recorte_foto_nombre = $array_recorte_foto_nombre[$id_foto];
														$recorte_foto_miniatura = $array_recorte_foto_miniatura[$id_foto];
														?>
														<tr>
															<td width="200"><a target="_blank" href="<?php echo $ruta_imagenes.$nombre_foto; ?>"><img src="<?php echo $ruta_imagenes.'recortes/'.$recorte_foto_miniatura; ?>" width="200px"></a></td>
															<td>
																<p><a class="rojo" onclick="borrar_imagen(<?php echo $id_foto; ?>)">Borrar imagen</a></p>
																<p><b><?php echo $nombre_foto; ?></b> </p>

																<p><?php echo nombre_fecha($fecha_carga); ?><p><br>
																	<?php if($recorte_foto_miniatura) { ?>
																	<p><i class="fa fa-crop"></i> Recorte miniatura: <a target="_blank" href="<?php echo $ruta_imagenes.'recortes/'.$recorte_foto_miniatura; ?>"><?php echo $recorte_foto_miniatura; ?></a><p>
																		<?php } ?>
																		<?php if($recorte_foto_nombre) { ?>
																		<p><i class="fa fa-crop"></i> Recorte grande: <a target="_blank" href="<?php echo $ruta_imagenes.'recortes/'.$recorte_foto_nombre; ?>"><?php echo $recorte_foto_nombre; ?></a><p>
																			<?php } ?>
																			<p><a href="<?php echo $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/wavi-noticias/03-recortar-foto.php?foto='.$id_foto.'&promo='.$promo; ?>">Recortar foto</a><p>
																			</td>
																		</tr>
																		<?php } ?>

																		<? } else { ?>
																		<tr><td>No hay imágenes</td></tr>
																		<?php } ?>
																	</tbody>
																</table>	
																<center>
																	<p><a onclick="ordenar_imagenes()">Ordenar Imágenes</a></p>
																</center>
																<br><br><br>										
																<div class="alinear_centro">
																	<a class="boton_azul boton_amarillo" onclick="agregar_imagen()" id="btn_agregar_direccion">[+] Agregar Imagen</a>
																</div>
															</div>
															<br><br><br><br><br>
															<legend id="txt_nueva_categoria">Negocios Vinculados</legend>
															<div id="direcciones">
																<table class="table table-striped">
																	<tbody>
																		<?php  $imagen_bender = $Servidor_url.'PANELADMINISTRADOR/img/usuarios_cargadores/bender-circulo.png';
																		if($totalrow_rs_vinculaciones_negocios) {
																			do {
																				$id_vinculacion = $row_rs_vinculaciones_negocios['id_vinculacion'];
																				$id_negocio = $row_rs_vinculaciones_negocios['id_negocio'];
																				$id_grupo_categoria = $row_rs_vinculaciones_negocios['id_grupo_categoria'];
																				$id_subgrupo_categoria = $row_rs_vinculaciones_negocios['id_subgrupo_categoria'];
																				$negocio_nombre = $row_rs_vinculaciones_negocios['negocio_nombre'];
																				$negocio_ciudad = $row_rs_vinculaciones_negocios['negocio_ciudad'];
																				$negocio_provincia = $row_rs_vinculaciones_negocios['negocio_provincia'];

																				$negocio_ciudad = $array_ciudades[$negocio_ciudad];
																				$negocio_provincia = $array_provincias[$negocio_provincia];

																				$variable_grupo_nombre = null;
																				$variable_grupo_nombre .= '<img src="'.$imagen.'" class="usuario_avatar"/><br>';
																				$variable_grupo_nombre .= $grupo_nombre;
																				$variable_grupo_nombre .= '<br><b>'.$subgrupo_nombre.'</b>';
																				?>
																				<tr class="<?php echo $super_class; ?>" >
																					<td data-href="<?php echo $link_negocio.$id_negocio; ?>"><b style="color:red"><?php echo $id_negocio; ?></b></td>
																					<td width="20%" data-href="<?php echo $link_negocio.$id_negocio; ?>"><?php echo $negocio_nombre; ?></td>
																					<td data-href="<?php echo $link_negocio.$id_negocio; ?>"><strong><?php echo $negocio_ciudad; ?></strong>,<br><?php echo $negocio_provincia; ?></td>
																					<td style="text-align: center"><a onclick="borrar_vinculacion(<?php echo $id_vinculacion; ?>)">
																						<img width="30" src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/img/delete.png" />
																					</a></td>
																				</tr>
																				<?php } while($row_rs_vinculaciones_negocios = mysql_fetch_assoc($rs_vinculaciones_negocios)); }?>
																			</tbody>
																		</table>	
																		<div id="negocios_loader" style="display: none">
																			<center><img src="<?php echo $Servidor_url; ?>img/loader.gif" width="50" heigth="50" /></center>
																		</div>
																		<center>
																			<p><a onclick="ordenar_negocios()">Ordenar Negocios</a></p>
																		</center>
																		<br><br><br>										
																		<div class="alinear_centro">
																			<a class="boton_azul boton_amarillo" onclick="agregar_negocio()" id="btn_agregar_direccion">[+] Agregar Negocio</a>
																		</div>
																	</div>
																	<br><br><br>
																	<div id="cuerpo">
																		<h3>Cuerpo:</h3>
																		<textarea name="cuerpo" class="areacuerpo" id="area1"><?php echo $promocion_cuerpo; ?></textarea></td>

																		<br>
																		<center>
																			<p><a onclick="editar_html()">Editar HTML del cuerpo</a></p>
																		</center>
																		<br><br><br>
																	</fieldset>	
																</section>    	

																<a name="botonGuardar"></a>
																<div class="alinear_centro">
																	<input type="submit" value="Guardar" id="btn_nueva_categoria">
																</div>
															</form>
														</div>
													</div> <!-- .content-wrapper -->
												</main> 
												<?php include('../../includes/pie-general.php');?>
												<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
												<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/popup.js"></script> <!-- Resource jQuery -->
												<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/jquery.ddslick.min.js"></script> <!-- Resource jQuery -->

												<script type="text/javascript">

													$.ajax({
														type: "GET",
														url: "<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/bender/json/02-categorias-json.php?main=1&categoria=<?php echo $promocion_categoria; ?>",
														data: {},
														success: function (data) {;
															$('#demoBasic').ddslick({
																data: data,
																width: 600,
																imagePosition: "left",
																selectText: "Elegí una categoría",
																onSelected: function (data) {
																	var valor = data.selectedData.value;
																	$('#select_grupo_categoria').val(valor);
																}
															});	
														}
													});

													function cargar_ciudades(provincia) {	
														if(provincia) {
															$('#select_elegir_ciudad').html('<option value="0">Cargando ciudades...</option>');
															$.ajax({
																url: "<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/ajax/cargar-ciudades.php?provincia="+provincia+"&ciudad=<?php echo $promocion_ciudad;?>",
																success: function (resultado) {
																	$('#select_elegir_ciudad').html(resultado);
																}
															});
														}			
													}	

													<?php if($promocion_provincia) { ?>
														cargar_ciudades(<?php echo $promocion_provincia; ?>);
														<?php } ?>

														<?php if($negocio) { ?>
															confirmar_agregar_negocio();
															<?php } ?>
															function agregar_negocio() {
																$('#popup_negocio').addClass('is-visible');
															}
															function cerrar_popup() {
																$('.cd-popup').removeClass('is-visible');
															}

															function confirmar_agregar_negocio() {
																var id_negocio = $('#input_agregar_negocio').val();
																if(id_negocio>0) {
																	$("#negocios_loader").show();
																	$.ajax({
																		url: "<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-promos/ajax/vincular-negocio-promo.php?negocio="+id_negocio+"&promo=<?php echo $promo;?>",
																		success: function (resultado) {
																			var respuesta = resultado.trim();
																			if(respuesta == "OK") {
																				window.location.href = "<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-promos/03-editar-promocion.php?promo=<?php echo $promo; ?>";
																			} else {
																				alert(respuesta);
																				$("#negocios_loader").hide();
																			}
																		}
																	});
																}
																cerrar_popup();
															}

															function borrar_vinculacion(id_vinculacion) {
																$('#popup_borrar_negocio').addClass('is-visible');
																$('#input_borrar_negocio').val(id_vinculacion);
															}

															function confirmar_borrar_negocio() {
																var id_vinculacion = $('#input_borrar_negocio').val();
																if(id_vinculacion>0) {
																	$("#negocios_loader").show();
																	$.ajax({
																		url: "<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-promos/ajax/borrrar-vinculaacion-negocio-promo.php?vinculacion="+id_vinculacion,
																		success: function (resultado) {
																			var respuesta = resultado.trim();
																			if(respuesta == "OK") {
																				window.location.href = "<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-promos/03-editar-promocion.php?promo=<?php echo $promo; ?>";
																			} else {
																				alert(respuesta);
																				$("#negocios_loader").hide();
																			}
																		}
																	});
																}
																cerrar_popup();
															}
															$('td[data-href]').on("click", function() {
																var link = $(this).data('href');
																window.open(link);
//window.open();
});

															function validar_formulario() {
																var error = null;	
																var categoria = $("#select_grupo_categoria").val();
																var titulo = $('#titulo').val();
																var provincia = $('#select_elegir_provincia').val();
																var cuerpo = $('#area1').val();

																if(provincia<1) {
																	error = "Elegí una provincia";
																}

																if(!cuerpo) {
																	error = "Escribí un cuerpo";
																}

																if(!titulo) {
																	error = "Escribí un título";
																}
																if(categoria<1) {
																	error = "Elegí una categoría";
																}
																if(error) {
																	alert(error);
																	return false;	
																} else {
																	return true;
																}
															}

															function agregar_imagen() {
																window.open('<?php echo $Servidor_url;?>PANELADMINISTRADOR//00-barra-navegacion/wavi-noticias/popUps/01-cargar-imagen-promo.php?promo=<?php echo $promo; ?>&id_administrador=<?php echo $id_administrador;?>','popup','width=400,height=400');
															}

															function borrar_imagen(imagen) {
																$('#popup_categoria').addClass('is-visible');
																$('#btn_confirmar_categoria').html('<a href="<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/wavi-noticias/php/11-borrar-imagen-promo.php?promo=<?php echo $promo;?>&foto='+imagen+'">Sí</a>');
															}

															function ordenar_imagenes() {
																window.open('<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/wavi-promos/popUps/ordenar-imagenes-promocion.php?promo=<?php echo $promo; ?>','popup','width=800,height=600');
															}
															function ordenar_negocios() {
																window.open('<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/wavi-promos/popUps/ordenar-negocios-promocion.php?promo=<?php echo $promo; ?>','popup','width=800,height=600');	
															}
															function editar_html() {
																window.open('<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/wavi-promos/popUps/editar-html-promocion.php?promo=<?php echo $promo; ?>','popup','width=800,height=600');
															}

														</script>
													</body>
													</html>