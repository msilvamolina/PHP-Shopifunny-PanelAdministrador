<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');
session_start();
$direccion = $_SESSION['direccion'];

$latitud = $_SESSION['latitud'];
$longitud = $_SESSION['longitud'];

$categoria = trim($_GET['categoria']);


conectar2('mywavi', 'WAVI');

$query_rs_profesionales = "SELECT categoria_nombre, categoria_imagen, categoria_color, categoria_color_dark FROM grupo_categorias WHERE id_grupo_categoria = $categoria";
$rs_profesionales = mysql_query($query_rs_profesionales)or die(mysql_error());
$row_rs_profesionales = mysql_fetch_assoc($rs_profesionales);
$totalrow_rs_profesionales = mysql_num_rows($rs_profesionales);

$categoria_nombre = $row_rs_profesionales['categoria_nombre'];
$categoria_imagen = $row_rs_profesionales['categoria_imagen']; 
$categoria_color = $row_rs_profesionales['categoria_color']; 
$categoria_color_dark = $row_rs_profesionales['categoria_color_dark']; 

$url_imagen = $Servidor_url."APLICACION/Imagenes/categorias/grandes/";

$color_general = "#F44336";
$color_general_dark = "#D32F2F";

if($categoria_color) {
	$color_general = "#".$categoria_color;
}
if($categoria_color_dark) {
	$color_general_dark = "#".$categoria_color_dark;
}
desconectar();

$imagen = $Servidor_url.'PANELADMINISTRADOR/img/icono-imagen.png';
$nombre_imagen = '<span class="no_hay_imagen">(no hay imagen)</span>';
if($categoria_imagen) {
	$imagen = $url_imagen.$categoria_imagen;
	$nombre_imagen = $categoria_imagen;
}
?>
<!DOCTYPE html>
<html>
<head>
	<?php include('../../includes/head-general.php'); ?>

	<style type="text/css">
		/* Style the tab */
		* {
			padding: 0;
			margin: 0;
		}

		header {
			background: <?php echo $color_general; ?>;
			height: 230px;
			padding: 10px;
		}

		header .imagen {
			width: 130px;
			height: 130px;
		}

		header p {
			color: #fff;
			font-style: bold;
			margin-top: 5px;
			font-size: 22px;
		}
		.status_bar {
			width: 100%;
			background: <?php echo $color_general_dark; ?>;
		}
		.toolbar {
			height: 20px;
			float: right;
		}
		.clear {
			float: none;
			clear: both;
		}
	</style>
</head>
<body>
	<div class="status_bar">
		<img src="<?php echo $Servidor_url;?>PANELADMINISTRADOR/img/toolbar.png" class="toolbar" />
		<div class="clear"></div>
	</div>
	<header>
		<a onclick="window.history.back()">
			<img src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/img/backbutton.png" width="20"/>
		</a>

		<center>
			<img src="<?php echo $imagen; ?>" class="imagen"/>
			<p><?php echo $categoria_nombre; ?></p>
		</center>
	</header>
	<div class="cotenido">
	<h1>Dirección: <?php echo $direccion; ?></h1>
	<h1>Latitud: <?php echo $latitud; ?></h1>
	<h1>Longitud: <?php echo $longitud; ?></h1>

		<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,</p>
		<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,</p>
		<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,</p>
	</div>
</body>
</html>