<?php include('../../../../paginas_include/variables-generales.php'); 
include('../../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../../php/verificar-permisos.php');
require('GridLatLong.php');

$sala = trim($_POST['sala']);

$nombre = trim($_POST['nombre']);
$descripcion = trim($_POST['descripcion']);
$sitio_web = trim($_POST['sitio_web']);
$email = trim($_POST['email']);
$direccion_provincia = trim($_POST['direccion_provincia']);
$direccion_ciudad = trim($_POST['direccion_ciudad']);
$direccion_calle = trim($_POST['direccion_calle']);
$direccion_numero = trim($_POST['direccion_numero']);
$direccion_piso = trim($_POST['direccion_piso']);
$direccion_dpto = trim($_POST['direccion_dpto']);
$direccion_latitud = trim($_POST['direccion_latitud']);
$direccion_longitud = trim($_POST['direccion_longitud']);
$direccion_google_maps = trim($_POST['direccion_google_maps']);


if($direccion_latitud AND $direccion_longitud) {
	$gridlatlong = new JamieMBrown\GridLatLong\GridLatLong();
	$latitud_grief = $gridlatlong->getGridReferences($direccion_latitud, $direccion_longitud);
	$gridref = $latitud_grief[0];
}

$nombre = arreglar_datos_db($nombre);
$descripcion = arreglar_datos_db($descripcion);
$direccion_calle = arreglar_datos_db($direccion_calle);

conectar2('moebius', "ProyectoMoebius");

$fecha_actual = date('Y-m-d H:i:s');
mysql_query("UPDATE salas SET sala_nombre='$nombre', sala_descripcion='$descripcion', sala_sitio_web='$sitio_web', sala_email='$email', sala_direccion_google_maps='$direccion_google_maps', sala_direccion_calle='$direccion_calle', sala_direccion_numero='$direccion_numero', sala_direccion_piso='$direccion_piso', sala_direccion_dpto='$direccion_dpto', sala_ciudad='$direccion_ciudad', sala_provincia='$direccion_provincia', sala_latitud='$direccion_latitud', sala_longitud='$direccion_longitud', sala_gridref='$gridref', usuario_que_modifica='$id_administrador', ip_visitante_modificacion='$ip_visitante', fecha_modificacion='$fecha_actual'
	WHERE id_sala='$sala'");

$telefonos_para_agregar = null;
if($_POST['telefono']) {
	foreach ($_POST['telefono'] as $clave =>$valor) {
		if($valor) {
			$codigo_area = $_POST['cod_area'][$clave];
			$telefono_numero = $valor;
			$telefono_id = $_POST['telefonos_ids'][$clave];

			if($telefono_id) {
				mysql_query("UPDATE telefonos_salas SET telefono_caracteristica='$codigo_area', telefono_numero='$telefono_numero'
					WHERE id_telefono='$telefono_id'");
			} else {
				$query = "INSERT INTO telefonos_salas (id_sala, telefono_caracteristica, telefono_numero, usuario_que_crea, ip_visitante) 
				VALUES ('$sala','$codigo_area', '$telefono_numero','$id_administrador', '$ip_visitante')";
				$result = mysql_query($query);
			}
		}
	}
}

if($_POST['eliminar_telefono']) {
	foreach ($_POST['eliminar_telefono'] as $id_telefono) {
		if($id_telefono>0) {
			mysql_query("DELETE FROM telefonos_salas WHERE id_telefono=$id_telefono");
		}
	}
}
desconectar();

$redireccion = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/salas/01-editar-sala.php?sala='.$sala;
header('Location: '.$redireccion.'&ok=1');
exit;


?>