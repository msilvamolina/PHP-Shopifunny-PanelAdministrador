<?php include('../../../../paginas_include/variables-generales.php'); 
include('../../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../../php/verificar-permisos.php');

$sala = $_GET['sala'];


?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php include('../../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->
	<script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?key=AIzaSyA51ZllTKq7CDMNNYUro72e6TnJ8RtEQa4'></script>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/cargar-negocio-google-map.css"> 

	<style type="text/css">
		.cd-logo img {
			width: 40px !important;
		}
	</style>
</head>
<body>
	<header class="cd-main-header" style="background: #B71C1C">
		<a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/" class="cd-logo">
			<img src="<?php echo $Servidor_url; ?>img/logoredondo.png" /></a>

			
			<nav class="cd-nav">

				<a id="btn_guardar" class="vc_btn_largo vc_btn_amarillo vc_btn_3d" style="margin-top: 3px; margin-right: 100px" onclick="guardar()">
					<span class="fa-stack fa-lg pull-left">
						<i class="fa fa-circle fa-stack-2x"></i>
						<i class="fa fa-save fa-stack-1x fa-inverse"></i>
					</span>
					<p>Guardar</p>
				</a>
				<img id="img_cargando" src="<?php echo $Servidor_url; ?>img/loader.gif" width="50" style="margin-right: 5px; margin-top: 3px; display:none" />

			</nav>
		</header> <!-- .cd-main-header -->	
		<main class="cd-main-content contenido_cuerpo">

			<center>
				<div class="mapa" id="map_canvas1"></div>
			</center>	

		</main> 
		<?php include('../../../includes/pie-general.php');?>
		<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
		<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/popup.js"></script> <!-- Resource jQuery -->

		<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
		<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/popup.js"></script> <!-- Resource jQuery -->
		<script type="text/javascript">

			var infowindow = new google.maps.InfoWindow({});

			jQuery(document).ready(function(){
	     //obtenemos los valores en caso de tenerlos en un formulario ya guardado en la base de datos
	     //Inicializamos la función de google maps una vez el DOM este cargado
	     initialize(); 

	   	//si no hay información guardada, ejecuta automaticamente los botones con la direccion que ingreso anteriormente
	   	
	   });

			function initialize() {
				geocoder = new google.maps.Geocoder();

				
				var latlng1 = new google.maps.LatLng(-26.829375552257286, -65.20110603041076);
				var myOptions1 = {
					zoom: 18,
					center: latlng1,
					mapTypeId: google.maps.MapTypeId.ROADMAP
				}

				map1 = new google.maps.Map(document.getElementById("map_canvas1"), myOptions1);

				marker1 = new google.maps.Marker({
					position: latlng1,
					map: map1,
					draggable: true
				});

				updatePosition(latlng1, 1);
				google.maps.event.addListener(marker1, 'dragend', function(){
					updatePosition(marker1.getPosition(), 1);
				});  
				
			}

//funcion que traduce la direccion en coordenadas
function codeAddress(numero) {
	var address = document.getElementById("direccion"+numero).value;
	$.ajax({
		url: "https://maps.googleapis.com/maps/api/geocode/json?sensor=true&address="+address,
		dataType: "json",
		success: function (data) {
			response = JSON.stringify(data);
			var json = jQuery.parseJSON(response);

			if(numero==1) {
				mapa = map1;
				marcador = marker1;
			}  
			
			if(json.status=='OK') {
				var datos = json.results[0].geometry.location;

				mapa.setCenter(datos);
				mapa.setZoom(18);  
				marcador.setPosition(datos);    
		        //coloco el marcador en dichas coordenadas
		        //actualizo el formulario      
		        updatePosition(marcador.getPosition(), numero);
		    }
		}
	});
}

function codeAddress2(numero) {
    //obtengo la direccion del formulario
    var address = document.getElementById("direccion"+numero).value;

    geocoder.geocode( { 'address': address}, function(results, status) {

    	alert("sa: "+status);
    	
    	if(numero==1) {
    		mapa = map1;
    		marcador = marker1;
    	}  
    	
	    //si el estado de la llamado es OK
	    if (status == google.maps.GeocoderStatus.OK) {
	        //centro el mapa en las coordenadas obtenidas
	        mapa.setCenter(results[0].geometry.location);
	        mapa.setZoom(18);  
	        marcador.setPosition(results[0].geometry.location);    
	        //coloco el marcador en dichas coordenadas
	        //actualizo el formulario      
	        updatePosition(marcador.getPosition(), numero);
	    } 
	});
}

function updatePosition(latLng, numero)
{
	jQuery('#lat'+numero).val(latLng.lat());
	jQuery('#long'+numero).val(latLng.lng());

	jQuery('#lat_txt'+numero).html(latLng.lat());
	jQuery('#long_txt'+numero).html(latLng.lng());
}

function ocultar_direccion(numero) {
	$('#section_direccion'+numero).hide();
	$('#etiqueta_ocultar'+numero).hide();  	
	$('#etiqueta_mostrar'+numero).show();  	
	$('#tabla'+numero).hide();  
	jQuery('#direccion_activa'+numero).val(0);
}
function mostrar_direccion(numero) {
	$('#section_direccion'+numero).show();
	$('#etiqueta_ocultar'+numero).show();  	
	$('#etiqueta_mostrar'+numero).hide(); 
	jQuery('#direccion_activa'+numero).val(1);
	$('#section_direccion'+numero).css('visibility', 'visible');
	$('#section_direccion'+numero).css('height', 'auto');    
	$('#tabla'+numero).show();  
}  

</script>
</body>
</html>