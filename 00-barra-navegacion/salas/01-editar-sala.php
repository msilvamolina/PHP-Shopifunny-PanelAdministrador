<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');

$sala = trim($_GET['sala']);

if(!$sala) {
	$redireccion = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/salas/00-cargar-sala.php';
	header('Location: '.$redireccion);
	exit;
}


conectar2('moebius', "ProyectoMoebius");
//consultar en la base de datos
$query_rs_sala = "SELECT sala_nombre, sala_descripcion, sala_sitio_web, sala_email, sala_direccion_google_maps, sala_ciudad, sala_provincia, sala_latitud, sala_longitud, sala_direccion_calle, sala_direccion_numero, sala_direccion_piso, sala_direccion_dpto
FROM salas WHERE id_sala = $sala ";
$rs_sala = mysql_query($query_rs_sala)or die(mysql_error());
$row_rs_sala = mysql_fetch_assoc($rs_sala);
$totalrow_rs_sala = mysql_num_rows($rs_sala);

if(!$totalrow_rs_sala) {
	$redireccion = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/salas/00-cargar-sala.php';
	header('Location: '.$redireccion);
	exit;
}

$provincia = 25;
$ciudad = 1;


$sala_nombre = $row_rs_sala['sala_nombre'];
$sala_descripcion = $row_rs_sala['sala_descripcion'];
$sala_sitio_web = $row_rs_sala['sala_sitio_web'];
$sala_email = $row_rs_sala['sala_email'];
$sala_direccion_google_maps = $row_rs_sala['sala_direccion_google_maps'];
$sala_ciudad = $row_rs_sala['sala_ciudad'];
$sala_provincia = $row_rs_sala['sala_provincia'];
$sala_latitud = $row_rs_sala['sala_latitud'];
$sala_longitud = $row_rs_sala['sala_longitud'];

$sala_direccion_calle =$row_rs_sala['sala_direccion_calle'];
$sala_direccion_numero =$row_rs_sala['sala_direccion_numero'];
$sala_direccion_piso =$row_rs_sala['sala_direccion_piso'];
$sala_direccion_dpto =$row_rs_sala['sala_direccion_dpto'];

if(!$sala_latitud) {
	$sala_latitud = 0;
}
if(!$sala_longitud) {
	$sala_longitud = 0;
}

if($sala_provincia) {
	$provincia = $sala_provincia;
}

if($sala_ciudad) {
	$ciudad = $sala_ciudad;
}


//consultar en la base de datos
$query_rs_ciudad = "SELECT id_ciudad, ciudad_nombre, id_provincia FROM ciudades WHERE id_provincia = $provincia ORDER BY ciudad_nombre ASC ";
$rs_ciudad = mysql_query($query_rs_ciudad)or die(mysql_error());
$row_rs_ciudad = mysql_fetch_assoc($rs_ciudad);
$totalrow_rs_ciudad = mysql_num_rows($rs_ciudad);

do {
	$id_ciudad = $row_rs_ciudad['id_ciudad'];
	$ciudad_nombre = $row_rs_ciudad['ciudad_nombre'];
	$id_provincia = $row_rs_ciudad['id_provincia'];

	$array_ciudades[$id_ciudad] = $ciudad_nombre;

} while($row_rs_ciudad = mysql_fetch_assoc($rs_ciudad));

//consultar en la base de datos
$query_rs_provincias = "SELECT id_provincia, provincia_nombre FROM provincias ORDER BY provincia_nombre ASC ";
$rs_provincias = mysql_query($query_rs_provincias)or die(mysql_error());
$row_rs_provincias = mysql_fetch_assoc($rs_provincias);
$totalrow_rs_provincias = mysql_num_rows($rs_provincias);

do {
	$id_provincia = $row_rs_provincias['id_provincia'];
	$provincia_nombre = $row_rs_provincias['provincia_nombre'];

	$array_provincias[$id_provincia] = $provincia_nombre;
} while($row_rs_provincias = mysql_fetch_assoc($rs_provincias));

//consultar en la base de datos
$query_rs_telefonos = "SELECT id_telefono, telefono_caracteristica, telefono_numero  FROM telefonos_salas WHERE id_sala = $sala ";
$rs_telefonos = mysql_query($query_rs_telefonos)or die(mysql_error());
$row_rs_telefonos = mysql_fetch_assoc($rs_telefonos);
$totalrow_rs_telefonos = mysql_num_rows($rs_telefonos);


desconectar();
?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/popup.css"> <!-- Resource style -->

	<script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?key=AIzaSyA51ZllTKq7CDMNNYUro72e6TnJ8RtEQa4'></script>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/cargar-negocio-google-map.css"> 

	<style type="text/css">
		.contenedor{
			text-align: center;
			margin: 0 auto;
			padding-top: 40px;
		}
		.bender{
			width: 100%;
			max-width: 134px;
		}

		h2 {
			margin-top: 10px;
			font-size: 26px;
		}
		.td_delete {
			padding: 10px;
			text-align: right;
			width: 30px;
		}
		.td_delete img {
			width: 30px;
			display: block;
		}	
		.cd-form {
			text-align: left;
		}

		.grupo_categoria {
			margin-left: -20px!important;
		}

		#section_categoria {
			background: #a7a7a7;
			padding: 30px;
			color: #fff;
		}

		#section_categoria h3 {
			font-size: 24px;
		}
		.select_class {
			background: #eeeeee !important;
		}

		.section_informacion_bender {
			background: #a7a7a7;
			padding: 30px;
			color: #fff;		
		}

		#section_informacion_bender {
			line-height: 18px;
		}
		.bender_chiquito {
			width: 30px;
		}

		.info_bender {
			margin-bottom: 20px;
		}
		.delete_categoria {
			float: right;
		}
		.delete_categoria img{
			width: 35px;
			margin-top: -5px;
		}
		#negocio_repetido {
			width: 100%;
			padding: 30px;
			background: #464646;
			color:#fff;
		}
		#negocio_repetido b {
			color: #e6d461;
		}

		#negocio_repetido span {
			color: #f92672;
		}

		#negocio_repetido h3 {
			color: #a6db29;
			font-size: 32px;
			margin-bottom: 10px;
		}
	</style>
</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper">
			<div class="cd-popup" id="popup_geolocalizacion" role="alert">
				<div class="cd-popup-container">
					<p>Para activar la geolocalización, tenés que completar todos los datos de la dirección en rojo<br>

						<ul class="cd-buttons">
							<li style="width: 100%"><a onclick="cerrar_popup()">Bueno</li>
						</ul>
						<a href="#0" class="cd-popup-close img-replace"></a>
					</div> <!-- cd-popup-container -->
				</div> <!-- cd-popup -->

				<div class="contenedor">


					<div >					<!-- Contenido de la Pagina-->	

						<div class="cd-form floating-labels">
							<section id="crear_categoria" >							
								<fieldset >
									<?php if($_GET['ok']) { ?>
									<div class="alert alert-success" role="alert">
										Los cambios se guardaron correctamente a las <?php echo date('H:i:s'); ?>
									</div>
									<?php } ?>
									<form id="myForm" action="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/salas/php/01-editar-sala-db.php" method="POST">
										<input  type="hidden" id="sala" name="sala" value='<?php echo $sala; ?>'>

										<a href="<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/salas/03-ficha-sala.php?sala=<?php echo $sala;?>" class="vc_btn_largo vc_btn_rojo vc_btn_3d" style="width:250px;float:right">
											<span class="fa-stack fa-lg pull-left">
												<i class="fa fa-circle fa-stack-2x"></i>
												<i class="fa fa-calendar-o fa-stack-1x fa-inverse"></i>
											</span>
											<b>Ficha de la sala</b>
										</a>
										<legend id="txt_nueva_categoria">Editar Sala</legend>

										<div class="icon">
											<label class="cd-label" for="cd-company">Nombre de la sala</label>
											<input class="company" type="text" name="nombre" value="<?php echo $sala_nombre; ?>" id="nueva_categoria_nombre" required>
										</div> 			    
										<div class="icon">
											<label class="cd-label" for="cd-textarea">Descripción</label>
											<textarea class="message" name="descripcion" id="nueva_categoria_descripcion" ><?php echo $sala_descripcion; ?></textarea>
										</div>
										<div class="icon">
											<label class="cd-label" for="cd-email">Sitio web</label>
											<input class="email" type="text" value="<?php echo $sala_sitio_web; ?>" name="sitio_web" id="cd-email">
										</div>				    				
										<div class="icon">
											<label class="cd-label" for="cd-email">E-mail</label>
											<input class="email" type="email" value="<?php echo $sala_email;?>" name="email" id="cd-email">
										</div>

										<legend id="txt_nueva_categoria">Teléfonos</legend>
										<table width="100%"  id="telefonos">

											<?php 
											$i=1; 
											if($totalrow_rs_telefonos) { 
												do { 
													$id_telefono = $row_rs_telefonos['id_telefono'];
													$telefono_caracteristica = $row_rs_telefonos['telefono_caracteristica'];
													$telefono_numero = $row_rs_telefonos['telefono_numero'];

													?>
													<input name="telefonos_ids[]" type="hidden" value="<?php echo $id_telefono;?>" />
													<tr id="telefono_agregado_<?php echo $i; ?>">
														<td><input  type="text" name="cod_area[]" value="<?php echo $telefono_caracteristica; ?>" required placeholder="Código de área" ></td>
														<td><input  type="text" value="<?php echo $telefono_numero; ?>" name="telefono[]" required placeholder="Teléfono"></td>
														<td class="td_delete" width="60"><a onclick="eliminar_telefono(<?php echo $i; ?>, <?php echo $id_telefono; ?>)"><img src="<?php echo $Servidor_url;?>PANELADMINISTRADOR/img/delete.png"></a></td></tr>


														<?php $i++; } while($row_rs_telefonos = mysql_fetch_assoc($rs_telefonos)); } ?>										
													</table>

													<input  type="hidden" id="telefonos_contador" value='<?php echo $i; ?>'>

													<div class="alinear_centro">
														<a class="boton_azul boton_amarillo" onclick="agregar_telefonos()">[+] Agregar teléfono</a>
													</div>				    
													<br><br><br>
													<legend id="txt_nueva_categoria">Dirección</legend>

													<div id="direccion_agregada_<?php echo $contador; ?>">
														<table width="100%" >
															<tr>
																<td >
																	<p class="cd-select icon">
																		<select name="direccion_provincia" class="budget"  id="select_provincia" onchange="cargar_ciudades(this.value)">									
																			<?php foreach ($array_provincias as $id_provincia => $provincia_nombre) {
																				$elegido = null;
																				if($id_provincia==$provincia) {
																					$elegido = 'selected';
																				}
																				if($id_provincia) {
																					echo '<option value="'.$id_provincia.'" '.$elegido.'>'.$provincia_nombre.'</option>';
																				}
																			}
																			?>
																		</select></p>						    	
																	</td>
																	<td >
																		<p class="cd-select icon">
																			<select name="direccion_ciudad" class="budget" id="select_ciudad" >									
																				<?php foreach ($array_ciudades as $id_ciudad => $ciudad_nombre) {
																					$elegido = null;
																					if($id_ciudad==$ciudad) {
																						$elegido = 'selected';
																					}
																					if($id_ciudad) {
																						echo '<option value="'.$id_ciudad.'" '.$elegido.'>'.$ciudad_nombre.'</option>';
																					}
																				}
																				?>
																			</select></p>					    	
																		</td>					    				    		
																	</tr>

																</table>					
																<table width="100%" >
																	<tr>
																		<td width="40%">
																			<input  type="text" name="direccion_calle" id="direccion_calle" value="<?php echo $sala_direccion_calle;?>" required placeholder="Calle" >				
																		</td>					    			
																		<td width="20%">
																			<input  type="text" name="direccion_numero"  value="<?php echo $sala_direccion_numero;?>" id="direccion_numero" required placeholder="Número" >						
																		</td>
																		<td width="20%">
																			<input  type="text" value="<?php echo $sala_direccion_piso;?>" id="direccion_piso" name="direccion_piso"  placeholder="Piso" >						
																		</td>
																		<td width="20%">
																			<input  type="text"  value="<?php echo $sala_direccion_dpto;?>" id="direccion_dpto" name="direccion_dpto"  placeholder="Dpto" >					
																		</td>
																	</tr>
																	<tr><td>&nbsp;</td></tr>
																</table>

																<div id="etiquetas_activar_geolocalizacion">
																	<p class="etiqueta_direccion" style="" id="etiqueta_ocultar1"><a onclick="ocultar_direccion(1)"><i class="fa fa-close"></i> Quitar Geolocalización</a></p>			
																	<p class="etiqueta_direccion" style="display:none" id="etiqueta_mostrar1"><a onclick="activar_geolocalizacion()"><i class="fa fa-map-marker"></i> Activar Geolocalización</a></p>
																</div>
																<div id="cargando_geolocalizacion" style="display: none">
																	<center>
																		<img src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/img/loader.gif" />
																	</center>
																</div>

															</div>


															<input class="input_mapa" value="<?php echo $direccion_latitud; ?>" type="hidden" name="direccion_latitud" id="lat1">
															<input class="input_mapa" value="<?php echo $direccion_longitud; ?>" type="hidden"  name="direccion_longitud" id="long1" >
															<input id="direccion1" name="direccion_google_maps" value="<?php echo $sala_direccion_google_maps; ?>" type="hidden"/>

														</form>
													</fieldset>	
												</section>    	

											</div>
											<div class="contenedor_mapa" style="width: 600px; margin: -80px auto;">
												<div class="tabla_martin">
													<section id="section_direccion1" >
														<center>
															<a class="vc_btn_largo vc_btn_amarillo vc_btn_3d" style="max-width:300px" id="boton_mapa1" onclick="activar_geolocalizacion()">
																<span class="fa-stack fa-lg pull-left">
																	<i class="fa fa-circle fa-stack-2x"></i>
																	<i class="fa fa-map-marker fa-stack-1x fa-inverse"></i>
																</span>
																<p>Actualizar mapa</p>
															</a>
														</center>
														<br>

														<p style="text-align: left;">
															<b>Geolocalización encontrada para:</b><br><br>
															<span id="str_direccion"><?php echo $sala_direccion_google_maps; ?></span>
															<br><br>
															<b>Latitud: </b><span id="lat_txt1"></span>, <b>Longitud: </b><span id="long_txt1"></span></p><br>


															<center>
																<div class="mapa" id="map_canvas1" style="height: 500px"></div>
															</center>	
														</div>

														<br><br><br><br>
														<div class="alinear_centro">
															<a class="boton_azul boton_amarillo" onclick="validar_formulario()">Guardar Cambios</a>
														</div>
														<br><br><br><br><br>
													</div>

												</div> <!-- .content-wrapper -->


											</main> 
											<?php include('../../includes/pie-general.php');?>
											<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
											<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/popup.js"></script> <!-- Resource jQuery -->
											<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/jquery.ddslick.min.js"></script> <!-- Resource jQuery -->


											<script type="text/javascript">

												function cargar_ciudades(provincia) {	
													if(provincia) {
														$('#select_ciudad').html('<option value="0">Cargando ciudades...</option>');
														$.ajax({
															url: "<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/ajax/cargar-ciudades.php?provincia="+provincia,
															success: function (resultado) {
																$('#select_ciudad').html(resultado);
															}
														});
													}			
												}	

												function eliminar_direccion(direccion) {
													$('#direccion_agregada_'+direccion).html('');

												}
												function eliminar_telefono(telefono, id_telefono) {
													$('#telefono_agregado_'+telefono).html('');
													var variable = '<input type="hidden" name="eliminar_telefono[]" value="'+id_telefono+'">';
													$('#telefonos').append(variable);
												}	

												function eliminar_categoria(categoria) {
													$('#nueva_categoria_'+categoria).html('');
												}

												function agregar_telefonos() {
													var telefonos_contador = document.getElementById("telefonos_contador").value;
													var codigo_area = '0381';

													var variable = '<tr id="telefono_agregado_'+telefonos_contador+'">';
													variable = variable+'<td><input  type="text" name="cod_area[]" value="'+codigo_area+'" required placeholder="Código de área" ></td>';
													variable = variable+'<td><input  type="text" name="telefono[]" required placeholder="Teléfono"></td>';
													variable = variable+'<td class="td_delete" width="60"><a onclick="eliminar_telefono('+telefonos_contador+', 0)"><img src="<?php echo $Servidor_url;?>PANELADMINISTRADOR/img/delete.png"></a></td></tr>';

													document.getElementById("telefonos_contador").value = parseInt(telefonos_contador)+1;		
													$('#telefonos').append(variable);
												}			
												function agregar_direccion() {	
													window.open('<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/salas/popUps/agregar-direccion-sala.php?sala=<?php echo $sala;?>','popup','width=800,height=600');
												}


												function validar_formulario() {
													var error = null;	
													var direccion_provincia = $('#select_provincia').val();
													var direccion_ciudad = $('#select_ciudad').val();
													var direccion_calle = $('#direccion_calle').val();
													var direccion_numero = $('#direccion_numero').val();

													if(!direccion_provincia || !direccion_ciudad || !direccion_calle || !direccion_numero) {
														error = "Tenés que completar la dirección";
													}
													if(error) {
														alert(error);
													} else {
														document.getElementById("myForm").submit();
													}
												}

												var infowindow = new google.maps.InfoWindow({});

												jQuery(document).ready(function(){
	     //obtenemos los valores en caso de tenerlos en un formulario ya guardado en la base de datos
	     //Inicializamos la función de google maps una vez el DOM este cargado
	     //initialize();

	     <?php if(!$sala_latitud) { ?> 
	     	ocultar_direccion(1);
	     	<?php } else { ?>
	     		initialize();
	     		<?php } ?>
	   	//si no hay información guardada, ejecuta automaticamente los botones con la direccion que ingreso anteriormente
	   	
	   });

												function initialize() {
													geocoder = new google.maps.Geocoder();


													var latlng1 = new google.maps.LatLng(<?php echo $sala_latitud; ?>, <?php echo $sala_longitud; ?>);
													var myOptions1 = {
														zoom: 18,
														center: latlng1,
														mapTypeId: google.maps.MapTypeId.ROADMAP
													}

													map1 = new google.maps.Map(document.getElementById("map_canvas1"), myOptions1);

													marker1 = new google.maps.Marker({
														position: latlng1,
														map: map1,
														draggable: true
													});

													updatePosition(latlng1, 1);
													google.maps.event.addListener(marker1, 'dragend', function(){
														actualizarDireccionGoogle(marker1.getPosition());

														updatePosition(marker1.getPosition(), 1);
													});  

												}

//funcion que traduce la direccion en coordenadas
function codeAddress(numero) {
	var address = document.getElementById("direccion"+numero).value;
	$.ajax({
		url: "https://maps.googleapis.com/maps/api/geocode/json?sensor=true&address="+address,
		dataType: "json",
		success: function (data) {
			response = JSON.stringify(data);
			var json = jQuery.parseJSON(response);

			if(numero==1) {
				mapa = map1;
				marcador = marker1;
			}  
			
			if(json.status=='OK') {
				var datos = json.results[0].geometry.location;

				mapa.setCenter(datos);
				mapa.setZoom(18);  
				marcador.setPosition(datos);    
		        //coloco el marcador en dichas coordenadas
		        //actualizo el formulario      
		        updatePosition(marcador.getPosition(), numero);
		    }
		}
	});
}


function updatePosition(latLng, numero){
	jQuery('#lat'+numero).val(latLng.lat());
	jQuery('#long'+numero).val(latLng.lng());

	jQuery('#lat_txt'+numero).html(latLng.lat());
	jQuery('#long_txt'+numero).html(latLng.lng());
}

function actualizarDireccionGoogle(latLng) {
	$.ajax({
		url: "https://maps.googleapis.com/maps/api/geocode/json?sensor=true&latlng="+latLng.lat()+","+latLng.lng(),
		dataType: "json",
		success: function (data) {
			response = JSON.stringify(data);
			var json = jQuery.parseJSON(response);
			
			if(json.status=='OK') {
				var direccion = json.results[0].formatted_address;
				$("#direccion1").val(direccion);
				$("#str_direccion").text(direccion);
			}
		}
	});
}

function ocultar_direccion(numero) {
	//BORRAMOS LOS CAMPOS DE LATITUD Y ESO
	$('#lat'+numero).val(0);
	$('#long'+numero).val(0);
	$("#direccion"+numero).val("");


	$('#section_direccion'+numero).hide();
	$('#etiqueta_ocultar'+numero).hide();  	
	$('#etiqueta_mostrar'+numero).show();  	
	$('#tabla'+numero).hide();  
	jQuery('#direccion_activa'+numero).val(0);
}
function mostrar_direccion(numero) {
	$('#section_direccion'+numero).show();
	$('#etiqueta_ocultar'+numero).show();  	
	$('#etiqueta_mostrar'+numero).hide(); 
	jQuery('#direccion_activa'+numero).val(1);
	$('#section_direccion'+numero).css('visibility', 'visible');
	$('#section_direccion'+numero).css('height', 'auto');    
	$('#tabla'+numero).show();  
}  

function activar_geolocalizacion() {
	var direccion_provincia = $('#select_provincia').val();
	var direccion_ciudad = $('#select_ciudad').val();
	var direccion_calle = $('#direccion_calle').val();
	var direccion_numero = $('#direccion_numero').val();

	if(!direccion_provincia || !direccion_ciudad || !direccion_calle || !direccion_numero) {
		$('#popup_geolocalizacion').addClass('is-visible');
	} else {
		$("#cargando_geolocalizacion").show();
		$("#etiquetas_activar_geolocalizacion").hide();
		var sendInfo = {
			direccion_provincia: direccion_provincia,
			direccion_ciudad: direccion_ciudad,
			direccion_calle: direccion_calle,
			direccion_numero: direccion_numero
		};

		$.ajax({
			type: "POST",
			url: "<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/salas/ajax/consultar-direccion.php",
			dataType: "json",
			success: function (data) {
				response = JSON.stringify(data);
				var json = jQuery.parseJSON(response);

				var direccion = json.respuesta.trim();
				$("#direccion1").val(direccion);
				$("#str_direccion").text(direccion);
				mostrar_direccion(1);
				codeAddress(1);
				initialize();
				$("#cargando_geolocalizacion").hide();
				$("#etiquetas_activar_geolocalizacion").show();
			},

			data: sendInfo
		});
	}

}
function cerrar_popup() {
	$('.cd-popup').removeClass('is-visible');
}

</script>
</body>
</html>