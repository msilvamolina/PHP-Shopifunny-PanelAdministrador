<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');

$pagina= trim($_GET['pagina']);

$limite = 50;
$pagina_get = $pagina;
if(!$pagina_get) {
	$pagina_get=1;
}
if($pagina) {
	$pagina = $pagina-1;
}
$arranca = $pagina*$limite;

conectar2('moebius', "ProyectoMoebius");

//consultar en la base de datos
$query_rs_noticias = "SELECT salas.id_sala, salas.sala_nombre, salas.sala_latitud, salas.sala_longitud, salas.sala_direccion_calle, salas.sala_direccion_numero, salas.sala_direccion_piso, salas.sala_direccion_dpto, salas.sala_direccion_google_maps, salas.sala_ciudad, salas.sala_provincia FROM salas  ORDER BY salas.id_sala DESC LIMIT $arranca,$limite";
$rs_noticias = mysql_query($query_rs_noticias)or die(mysql_error());
$row_rs_noticias = mysql_fetch_assoc($rs_noticias);
$totalrow_rs_noticias = mysql_num_rows($rs_noticias);

$pagina_actual_variables = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/salas/02-salas.php?';
if($usuario) {
	$pagina_actual_variables = $pagina_actual_variables.'usuario='.$usuario.'&';
}
if($q) {
	$pagina_actual_variables = $pagina_actual_variables.'q='.$q.'&';
}
$pagina_siguiente = $pagina+2;
$pagina_anterior = $pagina;
$disabled_siguiente = null;
$disabled_anterior = null;
$link_siguiente = $pagina_actual_variables.'pagina='.$pagina_siguiente;
$link_anterior = $pagina_actual_variables.'pagina='.$pagina_anterior;
if($pagina_anterior<=0) {
	$disabled_anterior = 'disabled';
	$link_anterior = null;
}

if(!$totalrow_rs_noticias) {
	$disabled_siguiente = 'disabled';
	$link_siguiente = null;
}
//consultar en la base de datos
$query_rs_fotos = "SELECT id_foto, id_sala, recorte_foto_miniatura FROM fotos_publicaciones WHERE id_sala > 0";
$rs_fotos = mysql_query($query_rs_fotos)or die(mysql_error());
$row_rs_fotos = mysql_fetch_assoc($rs_fotos);
$totalrow_rs_fotos = mysql_num_rows($rs_fotos);

$ruta = $Servidor_url.'APLICACION/Imagenes/salas/recortes/';

do {
	$id_sala = $row_rs_fotos['id_sala'];
	$nombre_foto = $row_rs_fotos['recorte_foto_miniatura'];

	$array_fotos[$id_sala] = $ruta.$nombre_foto;
} while($row_rs_fotos = mysql_fetch_assoc($rs_fotos));

$query_rs_ciudad = "SELECT id_ciudad, ciudad_nombre, id_provincia FROM ciudades ORDER BY ciudad_nombre ASC ";
$rs_ciudad = mysql_query($query_rs_ciudad)or die(mysql_error());
$row_rs_ciudad = mysql_fetch_assoc($rs_ciudad);
$totalrow_rs_ciudad = mysql_num_rows($rs_ciudad);

do {
	$id_ciudad = $row_rs_ciudad['id_ciudad'];
	$ciudad_nombre = $row_rs_ciudad['ciudad_nombre'];

	$array_ciudades[$id_ciudad] = $ciudad_nombre;

} while($row_rs_ciudad = mysql_fetch_assoc($rs_ciudad));

//consultar en la base de datos
$query_rs_provincias = "SELECT id_provincia, provincia_nombre FROM provincias ORDER BY provincia_nombre ASC ";
$rs_provincias = mysql_query($query_rs_provincias)or die(mysql_error());
$row_rs_provincias = mysql_fetch_assoc($rs_provincias);
$totalrow_rs_provincias = mysql_num_rows($rs_provincias);

do {
	$id_provincia = $row_rs_provincias['id_provincia'];
	$provincia_nombre = $row_rs_provincias['provincia_nombre'];

	$array_provincias[$id_provincia] = $provincia_nombre;
} while($row_rs_provincias = mysql_fetch_assoc($rs_provincias));
desconectar();
?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/paginacion.css"> <!-- Resource style -->

	<style type="text/css">
		.rojo {
			color: #F44336;
			font-weight: bold;
		}

		.verde {
			color: #2E7D32;
			font-weight: bold;
		}
	</style>
</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper" style="max-width:100%">
			<!-- Contenido de la Pagina-->
			<nav role="navigation">
				<ul class="cd-pagination">
					<li class="button"><a class="<?php echo $disabled_anterior; ?>" href="<?php echo $link_anterior; ?>">Anterior</a></li>
					<li class="button"><a  class="<?php echo $disabled_siguiente; ?>"  href="<?php echo $link_siguiente; ?>">Siguiente</a></li>
				</ul>
			</nav> <!-- cd-pagination-wrapper -->	
			<div class="cd-form floating-labels" style="max-width:100%">
				<legend id="txt_nueva_categoria">Salas</legend>

				<?php if($totalrow_rs_noticias) { ?>
				<table class="table table-striped">
					<thead class="tabla_encabezado">
						<tr>
							<th><b>#</b></th>
							<th><b>Foto</b></th>
							<th><b>Nombre</b></th>
							<th><b>Ubicación</b></th>
						</tr>
					</thead>
					<tbody>
						<?php 

						do { 
							$id_sala = $row_rs_noticias['id_sala'];
							$sala_nombre = $row_rs_noticias['sala_nombre'];
							$sala_latitud = $row_rs_noticias['sala_latitud'];
							$sala_longitud = $row_rs_noticias['sala_longitud'];

							$sala_direccion_calle = $row_rs_noticias['sala_direccion_calle'];
							$sala_direccion_numero = $row_rs_noticias['sala_direccion_numero'];
							$sala_direccion_piso = $row_rs_noticias['sala_direccion_piso'];
							$sala_direccion_dpto = $row_rs_noticias['sala_direccion_dpto'];
							$sala_direccion_google_maps = $row_rs_noticias['sala_direccion_google_maps'];
							$sala_provincia = $row_rs_noticias['sala_provincia'];
							$sala_ciudad = $row_rs_noticias['sala_ciudad'];

							$direccion_a_mostrar = null;

							if($sala_direccion_calle) {
								$direccion_a_mostrar = $sala_direccion_calle.' '.$sala_direccion_numero;

								if($sala_direccion_piso) {
									$direccion_a_mostrar .= ', <b>Piso:</b> '.$sala_direccion_piso;
								}
								if($sala_direccion_dpto) {
									$direccion_a_mostrar .= ', <b>Dpto:</b> "'.$sala_direccion_dpto.'"';
								}

								$direccion_a_mostrar .=', '.$array_ciudades[$sala_ciudad].', '.$array_provincias[$sala_provincia];
							}


							$imagen = $Servidor_url.'PANELADMINISTRADOR/img/icono-imagen.png';
							$nombre_imagen = '<span class="no_hay_imagen">(no hay imagen)</span>';

							if($array_fotos[$id_sala]) {
								$imagen = $array_fotos[$id_sala];
							}

							$super_class = null;
							if($total_categorias_vinculadas!=$total_categorias_subgrupo) {
								$super_class = 'categorias_con_subgrupos';
							}

							$latitud_longitud = $sala_latitud.','.$sala_longitud;
							$url_ubicacion = "http://maps.googleapis.com/maps/api/staticmap?center=".$latitud_longitud."&zoom=16&size=200x150&scale=1&markers=color:red%7Clabel:%7C".$latitud_longitud;	
							?>
							<tr class="<?php echo $super_class; ?>" data-href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/salas/03-ficha-sala.php?sala=<?php echo $id_sala; ?>">
								<td><?php echo $id_sala; ?></td>
								<td><img src="<?php echo $imagen; ?>"  width="100"></td>
								<td><b><?php echo $sala_nombre; ?></b><br>
									<?php echo $direccion_a_mostrar;?></td>
									<td width="200">
										<?php if($sala_latitud) { ?>
										<img src="<?php echo $url_ubicacion; ?>" alt="<?php echo $sala_direccion_google_maps; ?>" title="<?php echo $sala_direccion_google_maps; ?>">
										<?php } ?>
									</td>
								</tr>		
								<?php } while($row_rs_noticias = mysql_fetch_assoc($rs_noticias)); ?>	          	
							</tbody>
						</table>		     
						<?php } else { ?>
						<p>No hay salas</p>
						<?php }?>           
					</div>
					<nav role="navigation">
						<ul class="cd-pagination">
							<li class="button"><a class="<?php echo $disabled_anterior; ?>" href="<?php echo $link_anterior; ?>">Anterior</a></li>
							<li class="button"><a  class="<?php echo $disabled_siguiente; ?>"  href="<?php echo $link_siguiente; ?>">Siguiente</a></li>
						</ul>
					</nav> <!-- cd-pagination-wrapper -->	
				</div> <!-- .content-wrapper -->
			</main> 
			<?php include('../../includes/pie-general.php');?>
			<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
			<script type="text/javascript">
				$('tr[data-href]').on("click", function() {
					document.location = $(this).data('href');
				});
			</script>
		</body>
		</html>