<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');
?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/popup.css"> <!-- Resource style -->
	<style type="text/css">
		.btn_eliminar {
			text-align: right;
			width: 100%;
		}

		a {
			cursor: pointer;
		}
	</style>
</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper">
			<div class="contenedor">
				<div >					<!-- Contenido de la Pagina-->	
					<div class="cd-form floating-labels">
						<section id="crear_categoria" >							
							<fieldset >
								<form action="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/salas/php/00-cargar-sala-db.php" method="POST">
									<input  type="hidden" id="elementos_contador" value="2">
									<input type="hidden" name="diario" id="diario" value="0" >

									<legend id="txt_nueva_categoria">Nueva Sala</legend>

									<div class="icon">
										<label class="cd-label" for="cd-company">Nombre</label>
										<input class="company" type="text" name="nombre" id="nueva_categoria_nombre" required>
									</div> 			    

									<div class="alinear_centro">
										<input type="submit" value="Continuar" id="btn_nueva_categoria">
									</div>
								</form>
							</fieldset>	
						</section>    	

					</div>
				</div>
			</div> <!-- .content-wrapper -->
		</main> 
		<?php include('../../includes/pie-general.php');?>
		<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
		<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/popup.js"></script> <!-- Resource jQuery -->
		<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/jquery.ddslick.min.js"></script> <!-- Resource jQuery -->

	</body>
	</html>