<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');

$sincronizar = trim($_GET['sincronizar']);


?>
<!doctype html>
<head>
	<!doctype html>
	<html lang="es" class="no-js">
	<head>
		<?php include('../../includes/head-general.php'); ?>
		<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->
		<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/popup.css"> <!-- Resource style -->
		<style type="text/css">
			.btn_eliminar {
				text-align: right;
				width: 100%;
			}

			a {
				cursor: pointer;
			}
		</style>
	</head>
	<body>
		<?php include('../../includes/header.php'); ?>
		<main class="cd-main-content">
			<?php include('../../includes/barra-navegacion.php'); ?>
			<div class="content-wrapper">
				<div class="contenedor">


					<h1>¿Estás seguro de querer sincronizar <b>"Noticias"</b> copiando nuestra base de datos a la base de datos en tiempo real de firebase?</h1>

					<center style="margin-top: -50px">
						<?php if($sincronizar==1) { ?>

						<div class="alert alert-success" role="alert">
							Los cambios se guardaron correctamente a las <?php echo date('H:i:s');?></div>

							<?php } ?>
							<a href="<?php echo $_SERVER['PHP_SELF'];?>?sincronizar=1" class="vc_btn_largo vc_btn_verde vc_btn_3d" style="width:300px">
								<span class="fa-stack fa-lg pull-left">
									<i class="fa fa-circle fa-stack-2x"></i>
									<i class="fa fa-refresh fa-stack-1x fa-inverse"></i>
								</span>
								<b>Sí, sincronicemos</b>
							</a>
						</center>


					</div>
				</div> <!-- .content-wrapper -->
			</main> 
			<?php include('../../includes/pie-general.php');?>
			<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
			<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/popup.js"></script> <!-- Resource jQuery -->
			<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/jquery.ddslick.min.js"></script> <!-- Resource jQuery -->
			<?php include('../../includes/firebase.php');?>

			<script type="text/javascript">
				<?php
				if($sincronizar==1) {
//consultar en la base de datos
					conectar2('congreso', "sitioweb");

					$query_rs_fotos_noticias = "SELECT id_foto, nombre_foto, recorte_foto_nombre  FROM fotos_publicaciones ";
					$rs_fotos_noticias = mysql_query($query_rs_fotos_noticias)or die(mysql_error());
					$row_rs_fotos_noticias = mysql_fetch_assoc($rs_fotos_noticias);
					$totalrow_rs_fotos_noticias = mysql_num_rows($rs_fotos_noticias);

					if($totalrow_rs_fotos_noticias) {
						$ruta_imagenes = $Servidor_url."imagenes/notas/";
						do {
							$id_foto = $row_rs_fotos_noticias['id_foto'];
							$nombre_foto = $row_rs_fotos_noticias['nombre_foto'];
							$recorte_foto_nombre = $row_rs_fotos_noticias['recorte_foto_nombre'];

							$imagen_mostrar = $nombre_foto;
							if($recorte_foto_nombre) {
								$imagen_mostrar = 'recortes/'.$recorte_foto_nombre;
							}

							$array_imagenes_noticias[$id_foto] = $imagen_mostrar;

						} while($row_rs_fotos_noticias = mysql_fetch_assoc($rs_fotos_noticias));
					}

					$query_rs_noticias = "SELECT id_noticia, noticia_titulo, noticia_bajada, noticia_palabras_claves,noticia_cuerpo_1, noticia_cuerpo, foto_portada, fecha_carga, fecha_modificacion  FROM noticias WHERE noticia_publicada = 1 ORDER BY id_noticia desc";
					$rs_noticias = mysql_query($query_rs_noticias)or die(mysql_error());
					$row_rs_noticias = mysql_fetch_assoc($rs_noticias);
					$totalrow_rs_noticias = mysql_num_rows($rs_noticias);

					if($totalrow_rs_noticias) {
						$i=0;
						do {
							$id_noticia = $row_rs_noticias['id_noticia'];
							$noticia_titulo = $row_rs_noticias['noticia_titulo'];
							$noticia_bajada = $row_rs_noticias['noticia_bajada'];
							$noticia_palabras_claves = $row_rs_noticias['noticia_palabras_claves'];
							$noticia_cuerpo_1 = $row_rs_noticias['noticia_cuerpo_1'];
							$noticia_cuerpo = $row_rs_noticias['noticia_cuerpo'];
							$foto_portada = $row_rs_noticias['foto_portada']; 

							$noticia_titulo = arreglar_datos_db($noticia_titulo);
							$noticia_bajada = arreglar_datos_db($noticia_bajada);
							$noticia_palabras_claves = arreglar_datos_db($noticia_palabras_claves);
							$noticia_cuerpo_1 = arreglar_datos_db($noticia_cuerpo_1);
							$noticia_cuerpo = arreglar_datos_db($noticia_cuerpo);

							if($foto_portada) {
								$foto_portada = $ruta_imagenes.$array_imagenes[$foto_portada]['grande'];
							} else {
								$foto_portada = "";
							} 
							?>
							firebase.database().ref('noticias/noticia<?php echo $id_noticia; ?>').set({
								id_noticia: "<?php echo $id_noticia; ?>",
								noticia_titulo: "<?php echo $noticia_titulo; ?>",
								noticia_bajada: "<?php echo $noticia_bajada ?>",
								noticia_palabras_claves: '<?php echo $noticia_palabras_claves ?>',
								noticia_cuerpo_1: '<?php echo $noticia_cuerpo_1 ?>',
								noticia_cuerpo: '<?php echo $noticia_cuerpo ?>',
								foto_portada: '<?php echo $foto_portada ?>'
							});

							<?php } while ($row_rs_noticias = mysql_fetch_assoc($rs_noticias));
						}

						desconectar();
					} ?>
				</script>
			</script>
		</body>
		</html>