<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');

$sincronizar = trim($_GET['sincronizar']);


?>
<!doctype html>
<head>
	<!doctype html>
	<html lang="es" class="no-js">
	<head>
		<?php include('../../includes/head-general.php'); ?>
		<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->
		<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/popup.css"> <!-- Resource style -->
		<style type="text/css">
			.btn_eliminar {
				text-align: right;
				width: 100%;
			}

			a {
				cursor: pointer;
			}
		</style>
	</head>
	<body>
		<?php include('../../includes/header.php'); ?>
		<main class="cd-main-content">
			<?php include('../../includes/barra-navegacion.php'); ?>
			<div class="content-wrapper">
				<div class="contenedor">


					<h1>¿Estás seguro de querer sincronizar <b>"Salas"</b> copiando nuestra base de datos a la base de datos en tiempo real de firebase?</h1>

					<center style="margin-top: -50px">
						<?php if($sincronizar==1) { ?>

						<div class="alert alert-success" role="alert">
							Los cambios se guardaron correctamente a las <?php echo date('H:i:s');?></div>

							<?php } ?>
							<a href="<?php echo $_SERVER['PHP_SELF'];?>?sincronizar=1" class="vc_btn_largo vc_btn_verde vc_btn_3d" style="width:300px">
								<span class="fa-stack fa-lg pull-left">
									<i class="fa fa-circle fa-stack-2x"></i>
									<i class="fa fa-refresh fa-stack-1x fa-inverse"></i>
								</span>
								<b>Sí, sincronicemos</b>
							</a>
						</center>


					</div>
				</div> <!-- .content-wrapper -->
			</main> 
			<?php include('../../includes/pie-general.php');?>
			<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
			<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/popup.js"></script> <!-- Resource jQuery -->
			<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/jquery.ddslick.min.js"></script> <!-- Resource jQuery -->
			<?php include('../../includes/firebase.php');?>

			<script type="text/javascript">
				<?php
				if($sincronizar==1) {
					conectar2('congreso', "aplicacion");

//consultar en la base de datos
					$query_rs_salas = "SELECT id_sala, sala_nombre, fecha_carga, fecha_modificacion FROM salas ";
					$rs_salas = mysql_query($query_rs_salas)or die(mysql_error());
					$row_rs_salas = mysql_fetch_assoc($rs_salas);
					$totalrow_rs_salas = mysql_num_rows($rs_salas);

					if($totalrow_rs_salas) {
						do {
							$id_sala = $row_rs_salas['id_sala'];
							$sala_nombre = $row_rs_salas['sala_nombre'];
							?>
							firebase.database().ref('salas/sala<?php echo $id_sala; ?>').set({
								id_sala: "<?php echo $id_sala; ?>",
								sala_nombre: "<?php echo $sala_nombre; ?>"
							});
							<?php } while ($row_rs_salas = mysql_fetch_assoc($rs_salas));	
						}
						desconectar();
					} ?>
				</script>
			</script>
		</body>
		</html>