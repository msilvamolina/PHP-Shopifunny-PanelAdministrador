<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');

$sincronizar = trim($_GET['sincronizar']);

conectar2('congreso', "aplicacion");
//consultar en la base de datos
$query_rs_usuarios = "SELECT * FROM noticias WHERE noticia_publicada = 1";
$rs_usuarios = mysql_query($query_rs_usuarios)or die(mysql_error());
$row_rs_usuarios = mysql_fetch_assoc($rs_usuarios);
$totalrow_rs_usuarios = mysql_num_rows($rs_usuarios);

//consultar en la base de datos
$query_rs_cuerpos = "SELECT * FROM noticias_cuerpo";
$rs_cuerpos = mysql_query($query_rs_cuerpos)or die(mysql_error());
$row_rs_cuerpos = mysql_fetch_assoc($rs_cuerpos);
$totalrow_rs_cuerpos = mysql_num_rows($rs_cuerpos);


//consultar en la base de datos
$query_rs_fotos_publicaciones = "SELECT id_foto, nombre_foto, recorte_foto_nombre, recorte_foto_miniatura  FROM fotos_publicaciones ";
$rs_fotos_publicaciones = mysql_query($query_rs_fotos_publicaciones)or die(mysql_error());
$row_rs_fotos_publicaciones = mysql_fetch_assoc($rs_fotos_publicaciones);
$totalrow_rs_fotos_publicaciones = mysql_num_rows($rs_fotos_publicaciones);

if($totalrow_rs_fotos_publicaciones) {
	do {
		$id_foto = $row_rs_fotos_publicaciones['id_foto'];
		$nombre_foto = $row_rs_fotos_publicaciones['nombre_foto'];
		$recorte_foto_nombre = $row_rs_fotos_publicaciones['recorte_foto_nombre'];
		$recorte_foto_miniatura = $row_rs_fotos_publicaciones['recorte_foto_miniatura'];

		$imagen_mostrar = $nombre_foto;
		if($recorte_foto_nombre) {
			$imagen_mostrar = 'recortes/'.$recorte_foto_nombre;
		}

		$array_imagenes[$id_foto]['grande'] = $imagen_mostrar;
		$array_imagenes[$id_foto]['miniatura'] = $recorte_foto_miniatura;

	} while($row_rs_fotos_publicaciones = mysql_fetch_assoc($rs_fotos_publicaciones));
}

desconectar();	



?>
<!doctype html>
<head>
	<!doctype html>
	<html lang="es" class="no-js">
	<head>
		<?php include('../../includes/head-general.php'); ?>
		<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->
		<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/popup.css"> <!-- Resource style -->
		<style type="text/css">
			.btn_eliminar {
				text-align: right;
				width: 100%;
			}

			a {
				cursor: pointer;
			}
		</style>
	</head>
	<body>
		<?php include('../../includes/header.php'); ?>
		<main class="cd-main-content">
			<?php include('../../includes/barra-navegacion.php'); ?>
			<div class="content-wrapper">
				<div class="contenedor">


					<h1>¿Estás seguro de querer sincronizar <b>"Información General"</b> copiando nuestra base de datos a la base de datos en tiempo real de firebase?</h1>

					<center style="margin-top: -50px">
						<?php if($sincronizar==1) { ?>

						<div class="alert alert-success" role="alert">
							Los cambios se guardaron correctamente a las <?php echo date('H:i:s');?></div>

							<?php } ?>
							<a href="<?php echo $_SERVER['PHP_SELF'];?>?sincronizar=1" class="vc_btn_largo vc_btn_verde vc_btn_3d" style="width:300px">
								<span class="fa-stack fa-lg pull-left">
									<i class="fa fa-circle fa-stack-2x"></i>
									<i class="fa fa-refresh fa-stack-1x fa-inverse"></i>
								</span>
								<b>Sí, sincronicemos</b>
							</a>
						</center>


					</div>
				</div> <!-- .content-wrapper -->
			</main> 
			<?php include('../../includes/pie-general.php');?>
			<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
			<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/popup.js"></script> <!-- Resource jQuery -->
			<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/jquery.ddslick.min.js"></script> <!-- Resource jQuery -->
			<?php include('../../includes/firebase.php');?>

			<script type="text/javascript">
				<?php
				if($sincronizar==1) {
					$ruta_imagenes = $Servidor_url.'APLICACION/Imagenes/notas/';

					do { 
						$id_noticia = $row_rs_usuarios["id_noticia"];
						$foto_portada = $row_rs_usuarios['foto_portada'];

						if($foto_portada) {
							$foto_portada = $ruta_imagenes.$array_imagenes[$foto_portada]['grande'];
						} else {
							$foto_portada = "";
						}

						?>
						firebase.database().ref('informacion-general/noticia<?php echo $id_noticia; ?>').set({
							id_noticia: "<?php echo $id_noticia; ?>",
							orden: "<?php echo $row_rs_usuarios['orden'] ?>",
							noticia_titulo: "<?php echo $row_rs_usuarios['noticia_titulo'] ?>",
							foto_portada: "<?php echo $foto_portada; ?>"
						});
						<?php } while($row_rs_usuarios = mysql_fetch_assoc($rs_usuarios));
						

						do {
							$id_cuerpo = $row_rs_cuerpos['id_cuerpo'];
							$id_noticia = $row_rs_cuerpos['id_noticia'];
							$orden = $row_rs_cuerpos['orden'];
							$cuerpo_tipo = $row_rs_cuerpos['cuerpo_tipo'];
							$contenido = $row_rs_cuerpos['contenido']; 
							$contenido = arreglar_datos_db($contenido);

							if($cuerpo_tipo=="imagen") {
								$contenido = $ruta_imagenes.$array_imagenes[$contenido]['grande'];
							}
							?>

							firebase.database().ref('informacion-general/noticia<?php echo $id_noticia; ?>/cuerpo/cuerpo<?php echo $id_cuerpo;?>').set({
								id_cuerpo: "<?php echo $id_cuerpo; ?>",
								orden: "<?php echo $orden; ?>",
								cuerpo_tipo: "<?php echo $cuerpo_tipo ?>",
								contenido: '<?php echo $contenido ?>'
							});

							<?php } while($row_rs_cuerpos = mysql_fetch_assoc($rs_cuerpos)); 
						} ?>
					</script>
				</script>
			</body>
			</html>