<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');
?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/popup.css"> <!-- Resource style -->
	<style type="text/css">
		.btn_eliminar {
			text-align: right;
			width: 100%;
		}

		a {
			cursor: pointer;
		}
	</style>
</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper">
			<div class="contenedor">
				<div >					<!-- Contenido de la Pagina-->	
					<div class="cd-form floating-labels">
						<section id="crear_categoria" >							
							<fieldset >

							</fieldset>	
						</section>    	

					</div>
				</div>
			</div> <!-- .content-wrapper -->
		</main> 
		<?php include('../../includes/pie-general.php');?>
		<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
		<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/popup.js"></script> <!-- Resource jQuery -->
		<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/jquery.ddslick.min.js"></script> <!-- Resource jQuery -->
		<?php include('../../includes/firebase.php');?>

		<script type="text/javascript">
		/*
			firebase.database().ref('users/' + "userId22244").set({
				username: "uri1231ta",
				email: "email",
				profile_picture : "imagen"
			});
	

			var newPostKey = firebase.database().ref('users/').push().key;
			*/

			firebase.auth().signInAnonymously().catch(function(error) {
				var errorCode = error.code;
				var errorMessage = error.message;
			});

			firebase.auth().onAuthStateChanged(function(user) {
				if (user) {
					var isAnonymous = user.isAnonymous;
					var uid = user.uid;
				} 
			});
			writeNewPost("123213123", "pepe", "picture", "title", "body");

			function writeNewPost(uid, username, picture, title, body) {
			  // A post entry.
			  var postData = {
			  	author: username,
			  	uid: uid,
			  	body: body,
			  	title: title,
			  	starCount: 0,
			  	authorPic: picture
			  };

			  // Get a key for a new Post.
			  var newPostKey = firebase.database().ref().push().key;

			  // Write the new post's data simultaneously in the posts list and the user's post list.
			  var updates = {};
			  updates['/pruebitas/' + newPostKey] = postData;

			  return firebase.database().ref().update(updates);
			}
		</script>
	</body>
	</html>