<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');
$sincronizar = trim($_GET['sincronizar']);

//consultar en la base de datos
conectar2('congreso', "sitioweb");

//consultar en la base de datos
$query_rs_usuarios = "SELECT id_usuario, fecha_acreditacion FROM usuarios";
$rs_usuarios = mysql_query($query_rs_usuarios)or die(mysql_error());
$row_rs_usuarios = mysql_fetch_assoc($rs_usuarios);
$totalrow_rs_usuarios = mysql_num_rows($rs_usuarios);

?>

<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/popup.css"> <!-- Resource style -->
	<style type="text/css">
		.btn_eliminar {
			text-align: right;
			width: 100%;
		}

		a {
			cursor: pointer;
		}
	</style>
</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper">
			<div class="contenedor">


				<h1>¿Estás seguro de querer sincronizar la <b>"Fecha de acreditacion de los Usuarios"</b> copiando los datos de firebase a la base de datos MYSQL?</h1>

				<center style="margin-top: -50px">
					<?php if($sincronizar==1) { ?>

					<div class="alert alert-success" role="alert">
						Los cambios se guardaron correctamente a las <?php echo date('H:i:s');?></div>

						<?php } ?>
					</center>
					<form action="https://www.congresodepublicidadunt.com/PANELADMINISTRADOR/00-barra-navegacion/firebase/php/00-sincronizar-usuarios-acreditados-db.php" method="POST">
						<div id="resultado"></div>

						<input type="submit" value="Guardar" />
					</form>

				</div>
			</div> <!-- .content-wrapper -->
		</main> 
		<?php include('../../includes/pie-general.php');?>
		<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
		<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/popup.js"></script> <!-- Resource jQuery -->
		<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/jquery.ddslick.min.js"></script> <!-- Resource jQuery -->
		<?php include('../../includes/firebase.php');?>

		<script type="text/javascript">
			
			<?php do { 
				$id_usuario = $row_rs_usuarios['id_usuario']; 
				$fecha_acreditacion = $row_rs_usuarios['fecha_acreditacion']; 

				if(!$fecha_acreditacion) {
					?>
					firebase.database().ref('usuarios/usuario<?php echo $id_usuario; ?>').once("value", function(snapshot) {
						var userData = snapshot.val();
				//alert(userData);
				    var id_usuario = snapshot.child("id_usuario").val(); // "last"
				   	var usuario_nombre = snapshot.child("usuario_nombre").val(); // "last"
				   	var usuario_apellido = snapshot.child("usuario_apellido").val(); // "last"

				    var fecha_acreditacion = snapshot.child("fecha_acreditacion").val(); // "last"

				    if(fecha_acreditacion) {
				    	var texto = id_usuario+" - "+usuario_nombre+" "+usuario_apellido+" - "+fecha_acreditacion+"<br><br>";
				    	$("#resultado").append(texto);   	

				    	var input = '<input type="hidden" name="usuario['+id_usuario+']" value="'+fecha_acreditacion+'" />';
				    	$("#resultado").append(input);   	

				    }
				});

					<?php } } while ($row_rs_usuarios = mysql_fetch_assoc($rs_usuarios)); ?>
				</script>
			</body>
			</html>