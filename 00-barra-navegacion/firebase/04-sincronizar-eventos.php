<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');

$sincronizar = trim($_GET['sincronizar']);


?>
<!doctype html>
<head>
	<!doctype html>
	<html lang="es" class="no-js">
	<head>
		<?php include('../../includes/head-general.php'); ?>
		<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->
		<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/popup.css"> <!-- Resource style -->
		<style type="text/css">
		.btn_eliminar {
			text-align: right;
			width: 100%;
		}

		a {
			cursor: pointer;
		}
	</style>
</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper">
			<div class="contenedor">


				<h1>¿Estás seguro de querer sincronizar <b>"Eventos"</b> copiando nuestra base de datos a la base de datos en tiempo real de firebase?</h1>

				<center style="margin-top: -50px">
					<?php if($sincronizar==1) { ?>

					<div class="alert alert-success" role="alert">
						Los cambios se guardaron correctamente a las <?php echo date('H:i:s');?></div>

						<?php } ?>
						<a href="<?php echo $_SERVER['PHP_SELF'];?>?sincronizar=1" class="vc_btn_largo vc_btn_verde vc_btn_3d" style="width:300px">
							<span class="fa-stack fa-lg pull-left">
								<i class="fa fa-circle fa-stack-2x"></i>
								<i class="fa fa-refresh fa-stack-1x fa-inverse"></i>
							</span>
							<b>Sí, sincronicemos</b>
						</a>
					</center>


				</div>
			</div> <!-- .content-wrapper -->
		</main> 
		<?php include('../../includes/pie-general.php');?>
		<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
		<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/popup.js"></script> <!-- Resource jQuery -->
		<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/jquery.ddslick.min.js"></script> <!-- Resource jQuery -->
		<?php include('../../includes/firebase.php');?>

		<script type="text/javascript">
			<?php
			if($sincronizar==1) {
//consultar en la base de datos
				conectar2('moebius', "ProyectoMoebius");

				$query_rs_eventos = "SELECT * FROM eventos";
				$rs_eventos = mysql_query($query_rs_eventos)or die(mysql_error());
				$row_rs_eventos = mysql_fetch_assoc($rs_eventos);
				$totalrow_rs_eventos = mysql_num_rows($rs_eventos);

				if($totalrow_rs_eventos) {
					do {
						$id_evento = $row_rs_eventos['id_evento'];

						$id_sala = $row_rs_eventos['id_sala'];
						$id_conferencista = $row_rs_eventos['id_conferencista'];
						$evento_nombre = $row_rs_eventos['evento_nombre'];
						$evento_descripcion = $row_rs_eventos['evento_descripcion'];
						$evento_fecha = $row_rs_eventos['evento_fecha'];
						$evento_hora_fin = $row_rs_eventos['evento_hora_fin'];
						$tomar_asistencia = $row_rs_eventos['tomar_asistencia'];


						$evento_nombre = arreglar_datos_db($evento_nombre);
						$evento_descripcion = arreglar_datos_db($evento_descripcion);

						$explorar_fecha = explode(' ', $evento_fecha);
						$fecha_sola = $explorar_fecha[0];
						$hora_comienzo = $explorar_fecha[1]; ?>

						var postData = {
							id_evento: "<?php echo $id_evento; ?>",
							id_sala: "<?php echo $id_sala; ?>"	
						};


						  // Get a key for a new Post.
						  var newPostKey = firebase.database().ref().push().key;

						  // Write the new post's data simultaneously in the posts list and the user's post list.
						  var updates = {};
						  updates['/eventos/' + newPostKey] = postData;

						  firebase.database().ref().update(updates);
			  <?php } while($row_rs_eventos = mysql_fetch_assoc($rs_eventos));
			}

			desconectar();
		} ?>
	</script>
</script>
</body>
</html>