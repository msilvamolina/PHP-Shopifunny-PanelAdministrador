<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');

?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/barra-pasos.css"> <!-- Resource style -->
	<style type="text/css">
		.campo_flotante {}
	</style>
</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper">
			<!-- Contenido de la Pagina-->
			<div class="cd-form floating-labels">
				
				<section id="crear_categoria" >							
					<fieldset >
						<legend id="txt_nueva_categoria">Negocio cargado con éxito</legend>
						<a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/02-cargar-negocio-paso3-1.php">
							<center><img src="<?php echo $Servidor_url; ?>img/restart.jpg"><br><br>
								<p>Cargar otro negocio</p></center>
							</a>
						</fieldset>	
					</section>    	
					
				</div>
			</div> <!-- .content-wrapper -->
		</main> 
		<?php include('../../includes/pie-general.php');?>
		<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->

	</body>
	</html>