<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');
$negocio = trim($_GET['negocio']);
$redirigir = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/03-negocios.php';
$source = trim($_GET['source']);
if(!$negocio) {
	header('location:'.$redirigir);
	exit;
} 
conectar2('mywavi', 'WAVI');
//consultar en la base de datos

//consultar en la base de datos
$query_rs_ciudad = "SELECT id_ciudad, ciudad_nombre FROM ciudades ";
$rs_ciudad = mysql_query($query_rs_ciudad)or die(mysql_error());
$row_rs_ciudad = mysql_fetch_assoc($rs_ciudad);
$totalrow_rs_ciudad = mysql_num_rows($rs_ciudad);
do {
	$id_ciudad_1 = $row_rs_ciudad['id_ciudad'];
	$ciudad_nombre_1 = $row_rs_ciudad['ciudad_nombre'];
	$array_ciudades[$id_ciudad_1] = $ciudad_nombre_1;
} while($row_rs_ciudad = mysql_fetch_assoc($rs_ciudad));
$ciudad_nombre = $array_ciudades[$id_ciudad];
	//consultar en la base de datos
$query_rs_provincia = "SELECT id_provincia, provincia_nombre  FROM provincias ";
$rs_provincia = mysql_query($query_rs_provincia)or die(mysql_error());
$row_rs_provincia = mysql_fetch_assoc($rs_provincia);
$totalrow_rs_provincia = mysql_num_rows($rs_provincia);
do {
	$id_provincia_1 = $row_rs_provincia['id_provincia'];
	$provincia_nombre_1 = $row_rs_provincia['provincia_nombre'];
	$array_provincias[$id_provincia_1] = $provincia_nombre_1;
} while($row_rs_provincia = mysql_fetch_assoc($rs_provincia));
$provincia_nombre = $array_provincias[$id_provincia];

//consultar en la base de datos
$query_rs_direcciones = "SELECT direccion_gps_activa, fecha_modificacion, direccion_nombre_google_maps, id_direccion, direccion_nombre, direccion_numero, direccion_piso, direccion_latitud, direccion_longitud, direccion_ciudad, direccion_provincia FROM direcciones WHERE id_negocio = $negocio ORDER BY id_negocio ASC";
$rs_direcciones = mysql_query($query_rs_direcciones)or die(mysql_error());
$row_rs_direcciones = mysql_fetch_assoc($rs_direcciones);
$totalrow_rs_direcciones = mysql_num_rows($rs_direcciones);

if(!$totalrow_rs_direcciones) {
	$redirigir = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/02-cargar-negocio-paso-ok.php';
	if($source=='bender') {
		$redirigir = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/bender/03-bender-editar-negocios.php';
	}
	header('location: '.$redirigir);
	exit;
}
desconectar();

?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->
	<script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?key=AIzaSyBcvJODUo2VKA-QuRMAEu8EwYtSNg1cJV0'></script>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/cargar-negocio-google-map.css"> 

</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">

		<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper" >
			<!-- Contenido de la Pagina-->
			<fieldset >	
				<form  action="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/php/02-cargar-negocio-paso4-mapas-db.php" method="POST">
					<input type="hidden" value="<?php echo $negocio; ?>" name="id_negocio">
					<input type="hidden" value="<?php echo $source; ?>" name="source">

					<h1>Establecer Ubicaciones</h1>

					<div class="alerta_importante">
						<div class="alert alert-info" role="alert">
							<strong>Nota importante!</strong> Los marcadores rojos ubicados en los mapas, se pueden mover al hacerles clic y arrastrar el mouse. Esto sirve para una mejor precisión a la hora de cargar las latitudes
						</div>
						<br><br><br>
					</div>  
					<?php if($totalrow_rs_direcciones) { ?>
					<div class="tabla_martin">
						<?php $i=1;
						do {
							$id_direccion = $row_rs_direcciones['id_direccion'];
							$direccion_nombre = $row_rs_direcciones['direccion_nombre'];
							$direccion_numero = $row_rs_direcciones['direccion_numero'];
							$direccion_piso = $row_rs_direcciones['direccion_piso'];
							$direccion_latitud = $row_rs_direcciones['direccion_latitud'];
							$direccion_longitud = $row_rs_direcciones['direccion_longitud'];
							$direccion_ciudad = $row_rs_direcciones['direccion_ciudad'];
							$direccion_provincia = $row_rs_direcciones['direccion_provincia'];	
							$direccion_nombre_google_maps = $row_rs_direcciones['direccion_nombre_google_maps'];	
							$fecha_modificacion = $row_rs_direcciones['fecha_modificacion'];	
							$direccion_gps_activa = $row_rs_direcciones['direccion_gps_activa'];	

							$array_direccion_gps_activa[$i] = $direccion_gps_activa;

							$mostrar_direccion = $direccion_nombre.', '.$array_ciudades[$direccion_ciudad].', '.$array_provincias[$direccion_provincia];

							if($direccion_longitud&&$direccion_latitud) {
								$array_longitudes[$i] = $direccion_longitud;
								$array_latitudes[$i] = $direccion_latitud;
							}		
							$nuevo_array_direcciones[$i] = $id_direccion;	

							if($fecha_modificacion) {
								$array_fecha_modificacion[$i] = $fecha_modificacion;
							}
							$etiqueta_ocultar = 'display:none';
							$etiqueta_mostrar = 'display:none';

							if($direccion_gps_activa) {
								$etiqueta_ocultar = null;	
							} else {
								$etiqueta_mostrar = null;					
							}				
							?>
							<legend  class="txt_rojo"><?php echo $mostrar_direccion; ?></legend>		

							<p class="etiqueta_direccion" style="<?php echo $etiqueta_ocultar; ?>" id="etiqueta_ocultar<?php echo $i; ?>"><a onclick="ocultar_direccion(<?php echo $i; ?>)"><i class="fa fa-close"></i> No agregar latitud y longitud</a></p>			
							<p class="etiqueta_direccion" style="<?php echo $etiqueta_mostrar; ?>" id="etiqueta_mostrar<?php echo $i; ?>"><a onclick="mostrar_direccion(<?php echo $i; ?>)"><i class="fa fa-map-marker"></i> Agregar latitud y longitud</a></p>			

							<input class="input_mapa" type="hidden" value="<?php echo $direccion_gps_activa; ?>" name="direccion_activa[<?php echo $id_direccion; ?>]" id="direccion_activa<?php echo $i; ?>">

							<section id="section_direccion<?php echo $i; ?>" >
								<div clas="tabla_martin_fila" id="tabla<?php echo $i; ?>">
									<div class="celda1">
										<input class="input_mapa" name="nombre_direccion[<?php echo $id_direccion; ?>]" id="direccion<?php echo $i;?>" value="<?php echo $mostrar_direccion ?>">
									</div>
									<div class="celda2">
										<a class="vc_btn_largo vc_btn_amarillo vc_btn_3d" style="max-width:100%" id="boton_mapa<?php echo $i; ?>" onclick="codeAddress(<?php echo $i; ?>)">
											<span class="fa-stack fa-lg pull-left">
												<i class="fa fa-circle fa-stack-2x"></i>
												<i class="fa fa-map-marker fa-stack-1x fa-inverse"></i>
											</span>
											<p>Actualizar Mapa</p>
										</a>
									</div>	
								</div>
								<input class="input_mapa" type="hidden" name="latitud[<?php echo $id_direccion; ?>]" id="lat<?php echo $i; ?>">
								<input class="input_mapa"  type="hidden"  name="longitud[<?php echo $id_direccion; ?>]" id="long<?php echo $i; ?>" >

								<div clas="tabla_martin_fila sin_fondo">
									<div class="celdaL1">
										<p><b>Latitud: </b><span id="lat_txt<?php echo $i; ?>"></span></p>
									</div>	
									<div class="celdaL1">
										<p><b>Longitud: </b><span id="long_txt<?php echo $i; ?>"></span></p>
									</div>
									<div class="clear"></div>
									<br>
									<center>
										<div class="mapa" id="map_canvas<?php echo $i; ?>"></div>
									</center>	
									<hr class="style-four">
									<br>
								</div>		
							</section>									
							<?php $i++; } while($row_rs_direcciones = mysql_fetch_assoc($rs_direcciones)); ?>
							<?php } ?>	
							<div class="alinear_centro">
								<input type="submit" class="boton_azul" value="Guardar Ubicaciones">
							</div>

						</div>
					</div>
				</div>

			</form>
		</fieldset>
	</div> <!-- .content-wrapper -->
</main> 
<?php include('../../includes/pie-general.php');?>
<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/popup.js"></script> <!-- Resource jQuery -->
<script type="text/javascript">

	var infowindow = new google.maps.InfoWindow({});

	jQuery(document).ready(function(){
	     //obtenemos los valores en caso de tenerlos en un formulario ya guardado en la base de datos
	     //Inicializamos la función de google maps una vez el DOM este cargado
	     initialize(); 

	   	//si no hay información guardada, ejecuta automaticamente los botones con la direccion que ingreso anteriormente
	   	<?php if((!$array_longitudes)&&(!$array_fecha_modificacion)) { 
	   		foreach ($nuevo_array_direcciones as $i => $direccion) {?>
	   			$('#boton_mapa<?php echo $i; ?>').trigger('click');
	   			<?php } }?>		
	   		});

	function initialize() {
		geocoder = new google.maps.Geocoder();

		<?php  foreach ($nuevo_array_direcciones as $i => $direccion) {

			$latitud = $array_latitudes[$i];
			$longitud = $array_longitudes[$i];

	//por defecto
			$lat = -26.816642034805092;
			$long = -65.3094543067383;
			$zoom = 12;
			if($latitud && $longitud) {
				$lat = $latitud;
				$long = $longitud;
				$zoom = 18;		
			}
			?>

			var latlng<?php echo $i;?> = new google.maps.LatLng(<?php echo $lat; ?>, <?php echo $long; ?>);
			var myOptions<?php echo $i;?> = {
				zoom: <?php echo $zoom; ?>,
				center: latlng<?php echo $i;?>,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			}

			map<?php echo $i;?> = new google.maps.Map(document.getElementById("map_canvas<?php echo $i;?>"), myOptions<?php echo $i;?>);

			marker<?php echo $i;?> = new google.maps.Marker({
				position: latlng<?php echo $i;?>,
				map: map<?php echo $i;?>,
				draggable: true
			});

			updatePosition(latlng<?php echo $i;?>, <?php echo $i;?>);
			google.maps.event.addListener(marker<?php echo $i;?>, 'dragend', function(){
				updatePosition(marker<?php echo $i;?>.getPosition(), <?php echo $i;?>);
			});  
			<?php } ?>

		}

//funcion que traduce la direccion en coordenadas
function codeAddress(numero) {
	var address = document.getElementById("direccion"+numero).value;
	$.ajax({
		url: "https://maps.googleapis.com/maps/api/geocode/json?sensor=true&address="+address,
		dataType: "json",
		success: function (data) {
			response = JSON.stringify(data);
			var json = jQuery.parseJSON(response);

			<?php foreach ($nuevo_array_direcciones as $clave => $valor) { ?>
				if(numero==<?php echo $clave; ?>) {
					mapa = map<?php echo $clave; ?>;
					marcador = marker<?php echo $clave; ?>;
				}  
				<?php } ?> 
				if(json.status=='OK') {
					var datos = json.results[0].geometry.location;

					mapa.setCenter(datos);
					mapa.setZoom(18);  
					marcador.setPosition(datos);    
		        //coloco el marcador en dichas coordenadas
		        //actualizo el formulario      
		        updatePosition(marcador.getPosition(), numero);
		    }
		}
	});
}

function codeAddress2(numero) {
    //obtengo la direccion del formulario
    var address = document.getElementById("direccion"+numero).value;

    geocoder.geocode( { 'address': address}, function(results, status) {

    	alert("sa: "+status);
    	<?php foreach ($nuevo_array_direcciones as $clave => $valor) { ?>

    		if(numero==<?php echo $clave; ?>) {
    			mapa = map<?php echo $clave; ?>;
    			marcador = marker<?php echo $clave; ?>;
    		}  
    		<?php } ?> 
	    //si el estado de la llamado es OK
	    if (status == google.maps.GeocoderStatus.OK) {
	        //centro el mapa en las coordenadas obtenidas
	        mapa.setCenter(results[0].geometry.location);
	        mapa.setZoom(18);  
	        marcador.setPosition(results[0].geometry.location);    
	        //coloco el marcador en dichas coordenadas
	        //actualizo el formulario      
	        updatePosition(marcador.getPosition(), numero);
	    } 
	});
}

function updatePosition(latLng, numero)
{
	jQuery('#lat'+numero).val(latLng.lat());
	jQuery('#long'+numero).val(latLng.lng());

	jQuery('#lat_txt'+numero).html(latLng.lat());
	jQuery('#long_txt'+numero).html(latLng.lng());
}

function ocultar_direccion(numero) {
	$('#section_direccion'+numero).hide();
	$('#etiqueta_ocultar'+numero).hide();  	
	$('#etiqueta_mostrar'+numero).show();  	
	$('#tabla'+numero).hide();  
	jQuery('#direccion_activa'+numero).val(0);
}
function mostrar_direccion(numero) {
	$('#section_direccion'+numero).show();
	$('#etiqueta_ocultar'+numero).show();  	
	$('#etiqueta_mostrar'+numero).hide(); 
	jQuery('#direccion_activa'+numero).val(1);
	$('#section_direccion'+numero).css('visibility', 'visible');
	$('#section_direccion'+numero).css('height', 'auto');    
	$('#tabla'+numero).show();  
}  
<?php foreach ($array_direccion_gps_activa as $numero => $valor) { 
	if(!$valor){?>
		$('#tabla'+<?php echo $numero; ?>).hide();  
		$('#section_direccion'+<?php echo $numero; ?>).css('visibility', 'hidden');
		$('#section_direccion'+<?php echo $numero; ?>).css('height', '1px');	
		<?php } } ?>	  
	</script>
</body>
</html>