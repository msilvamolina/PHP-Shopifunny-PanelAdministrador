<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');

$lista_de_negocios = $_GET['lista'];


if(!$lista_de_negocios) {
	$lista_de_negocios = 0;
}
conectar2('mywavi', 'WAVI');

//consultar en la base de datos
$query_rs_listas = "SELECT id_lista, lista_nombre FROM lista_negocios_sin_conexion ORDER BY id_lista DESC ";
$rs_listas = mysql_query($query_rs_listas)or die(mysql_error());
$row_rs_listas = mysql_fetch_assoc($rs_listas);
$totalrow_rs_listas = mysql_num_rows($rs_listas);


//consultar en la base de datos
$query_rs_provincias = "SELECT id_provincia, provincia_nombre FROM provincias ORDER BY provincia_nombre ASC ";
$rs_provincias = mysql_query($query_rs_provincias)or die(mysql_error());
$row_rs_provincias = mysql_fetch_assoc($rs_provincias);
$totalrow_rs_provincias = mysql_num_rows($rs_provincias);
do {
	$id_provincia = $row_rs_provincias['id_provincia'];
	$provincia_nombre = $row_rs_provincias['provincia_nombre'];
	$array_provincias[$id_provincia] = $provincia_nombre;
} while($row_rs_provincias = mysql_fetch_assoc($rs_provincias));

//consultar en la base de datos
$query_rs_array_ciudades = "SELECT id_ciudad, ciudad_nombre, id_provincia  FROM ciudades ORDER BY ciudad_nombre ";
$rs_array_ciudades = mysql_query($query_rs_array_ciudades)or die(mysql_error());
$row_rs_array_ciudades = mysql_fetch_assoc($rs_array_ciudades);
$totalrow_rs_array_ciudades = mysql_num_rows($rs_array_ciudades);

do {
	$ciudad_provincia = $row_rs_array_ciudades['id_provincia'];
	$id_ciudad = $row_rs_array_ciudades['id_ciudad'];
	$ciudad_nombre = $row_rs_array_ciudades['ciudad_nombre'];
	$array_ciudades[$id_ciudad] = $ciudad_nombre;

	if(!$array_provincias_ciudades[$ciudad_provincia]) {
		$array_provincias_ciudades[$ciudad_provincia] = $id_ciudad;
	} else {
		$array_provincias_ciudades[$ciudad_provincia] .= '-'.$id_ciudad;
	}
	
} while($row_rs_array_ciudades = mysql_fetch_assoc($rs_array_ciudades));

//consultar en la base de datos
$query_rs_grupo_categoria = "SELECT id_grupo_categoria, categoria_nombre, categoria_imagen FROM grupo_categorias ORDER BY categoria_nombre ASC";
$rs_grupo_categoria = mysql_query($query_rs_grupo_categoria)or die(mysql_error());
$row_rs_grupo_categoria = mysql_fetch_assoc($rs_grupo_categoria);
$totalrow_rs_grupo_categoria = mysql_num_rows($rs_grupo_categoria);

do {
	$id_grupo_categoria = $row_rs_grupo_categoria['id_grupo_categoria'];
	$categoria_nombre = $row_rs_grupo_categoria['categoria_nombre'];
	$categoria_imagen = $row_rs_grupo_categoria['categoria_imagen'];

	$array_categoria_nombre[$id_grupo_categoria] = $categoria_nombre;
	$array_categoria_imagen[$id_grupo_categoria] = $categoria_imagen;

} while($row_rs_grupo_categoria = mysql_fetch_assoc($rs_grupo_categoria));

//consultar en la base de datos
$query_rs_subgrupos = "SELECT id_subgrupo, subgrupo_nombre FROM subgrupos_categorias ";
$rs_subgrupos = mysql_query($query_rs_subgrupos)or die(mysql_error());
$row_rs_subgrupos = mysql_fetch_assoc($rs_subgrupos);
$totalrow_rs_subgrupos = mysql_num_rows($rs_subgrupos);

do {
	$id_subgrupo = $row_rs_subgrupos['id_subgrupo'];
	$subgrupo_nombre = $row_rs_subgrupos['subgrupo_nombre'];

	$array_subgrupo_nombre[$id_subgrupo] = $subgrupo_nombre;
} while($row_rs_subgrupos = mysql_fetch_assoc($rs_subgrupos));


if($lista_de_negocios) {
	//consultar en la base de datos
	$query_rs_lista_elegida = "SELECT lista_ciudad, lista_provincia, lista_nombre_archivo_guardado, lista_fecha_archivo_guardado FROM lista_negocios_sin_conexion WHERE id_lista = $lista_de_negocios ";
	$rs_lista_elegida = mysql_query($query_rs_lista_elegida)or die(mysql_error());
	$row_rs_lista_elegida = mysql_fetch_assoc($rs_lista_elegida);
	$totalrow_rs_lista_elegida = mysql_num_rows($rs_lista_elegida);
	
	$ciudad = $row_rs_lista_elegida['lista_ciudad'];
	$provincia = $row_rs_lista_elegida['lista_provincia'];

	$lista_nombre_archivo_guardado = $row_rs_lista_elegida['lista_nombre_archivo_guardado'];
	$lista_fecha_archivo_guardado = $row_rs_lista_elegida['lista_fecha_archivo_guardado'];
	
	//consultar en la base de datos
	$query_rs_negocios = "SELECT negocios.id_negocio, negocios.id_bender_negocio, negocios.fecha_carga, negocios.negocio_nombre, negocios.negocio_descripcion, negocios.negocio_provincia, negocios.negocio_ciudad, negocios.usuario_que_carga, vinculacion_negocio_lista.id_grupo_categoria, vinculacion_negocio_lista.id_subgrupo_categoria FROM negocios, vinculacion_negocio_lista WHERE vinculacion_negocio_lista.id_negocio = negocios.id_negocio AND vinculacion_negocio_lista.id_lista = $lista_de_negocios ORDER BY negocios.negocio_nombre ASC";
	$rs_negocios = mysql_query($query_rs_negocios)or die(mysql_error());
	$row_rs_negocios = mysql_fetch_assoc($rs_negocios);
	$totalrow_rs_negocios = mysql_num_rows($rs_negocios);
	
	do {
		$id_negocio = $row_rs_negocios['id_negocio'];
		$negocio_nombre = $row_rs_negocios['negocio_nombre'];
		$negocio_descripcion = $row_rs_negocios['negocio_descripcion'];
		$negocio_provincia = $row_rs_negocios['negocio_provincia'];
		$negocio_ciudad = $row_rs_negocios['negocio_ciudad'];
		$usuario_que_carga = $row_rs_negocios['usuario_que_carga'];
		$id_grupo_categoria = $row_rs_negocios['id_grupo_categoria'];
		$id_subgrupo_categoria = $row_rs_negocios['id_subgrupo_categoria'];

		$negocio_sin_conexion_prioridad = $row_rs_negocios['negocio_sin_conexion_prioridad'];

		$fecha_carga = $row_rs_negocios['fecha_carga'];

		$id_bender_negocio = $row_rs_negocios['id_bender_negocio'];


		if(!$array_grupo_categorias[$id_grupo_categoria]) {
			$array_grupo_categorias[$id_grupo_categoria] = $id_negocio;
		} else {
			$array_grupo_categorias[$id_grupo_categoria] .= '-'.$id_negocio;
		}

		$array_negocios_nombre[$id_negocio] = $negocio_nombre;
		$array_negocios_bender[$id_negocio] = $id_bender_negocio;
		$array_negocios_descripcion[$id_negocio] = $negocio_descripcion;
		$array_negocios_provincia[$id_negocio] = $negocio_provincia;
		$array_negocios_ciudad[$id_negocio] = $negocio_ciudad;
		$array_negocios_usuario[$id_negocio] = $usuario_que_carga;
		$array_negocios_grupo[$id_negocio] = $id_grupo_categoria;
		$array_negocios_subgrupo[$id_negocio] = $id_subgrupo_categoria;
		$array_negocios_fecha_carga[$id_negocio] = $fecha_carga;

		$array_negocios_negocio_sin_conexion_prioridad[$id_negocio] = $negocio_sin_conexion_prioridad;


	} while($row_rs_negocios = mysql_fetch_assoc($rs_negocios));
	
}

//consultar en la base de datos
$query_rs_usuarios = "SELECT id_usuario, usuario_nombre, usuario_apellido FROM usuarios_cargadores ";
$rs_usuarios = mysql_query($query_rs_usuarios)or die(mysql_error());
$row_rs_usuarios = mysql_fetch_assoc($rs_usuarios);
$totalrow_rs_usuarios = mysql_num_rows($rs_usuarios);

do {
	$id_usuario = $row_rs_usuarios['id_usuario'];
	$usuario_nombre = $row_rs_usuarios['usuario_nombre'];
	$usuario_apellido = $row_rs_usuarios['usuario_apellido'];

	$array_usuarios[$id_usuario] = $usuario_nombre;
} while($row_rs_usuarios = mysql_fetch_assoc($rs_usuarios));

desconectar();

$url_imagen = $Servidor_url."APLICACION/Imagenes/categorias/grandes/";

?>
<!doctype html>
<html lang="es" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/negocios.css"> <!-- Resource style -->

	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/paginacion.css"> <!-- Resource style -->

	<title>Sistema Paradigma 2.0</title>
	<style type="text/css">
		.td_delete {
			padding: 10px;
			text-align: right;
			width: 30px;
		}
		.td_delete img {
			width: 30px;
			display: block;
		}

		.tabla {
			width: 100%;
		}
		.tabla tr td{
			padding: 10px;
		}	

		.tabla tr:nth-of-type(2n) {
			background: #f5e5f2;
		}
		.no_hay_imagen{
			color: #acacac;
		}
		.tabla_encabezado {
			color: red;
		}

		tr {
			cursor: pointer;
		}

		.categorias_con_subgrupos {
			background: #f90 !important;
			color: #fff !important;
		}

		.grupo_imagen {
			width: 60px;
			border-radius: 50%;
		}

		.usuario_avatar {
			width: 50px;
			border-radius: 50%;
		}
		td {
			cursor: pointer;
		}

		.table {
			margin-left: 60px;
		}

		a {
			cursor: pointer;
		}
	</style>
</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<!-- Contenido de la Pagina-->	
		<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper">
			<!-- Contenido de la Pagina-->
			<div class="cd-form floating-labels" style="max-width:1600px">
				<legend id="txt_nueva_categoria">
					<b>Negocios sin conexión</b></legend>

					<br><br>
					<h2><b>Lista de negocios sin conexión</b></h2>
					<br><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/04-nueva-lista-sin-conexion.php">[+]Nueva Lista</a><br><br>
					<form method="get" action="<?php echo $Servidor_url_documento;?>" name="form2">

						<p class="cd-select">
							<select name="lista" class="select_class" id="select_subgrupo_1" onchange="document.forms.form2.submit()">
								<option value="0">Ninguna lista seleccionada</option>	

								<?php do { 
									$id_lista = $row_rs_listas['id_lista'];
									$lista_nombre = $row_rs_listas['lista_nombre'];
									$selected = null;

									if($lista_de_negocios==$id_lista) {
										$selected = 'selected';
									}
									if($lista_nombre) {
										?>
										<option <?php echo $selected; ?> value="<?php echo $id_lista; ?>"><?php echo $lista_nombre; ?></option>	
										<?php } } while($row_rs_listas = mysql_fetch_assoc($rs_listas)); ?>

									</select></p>
								</form>
								<br><br>

								<?php
								if($lista_de_negocios) { ?>

								<div id="div_json">
									<?php if($lista_nombre_archivo_guardado) { ?>
									<p><b>Archivo Json:</b> <a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/guia-de-servicios/json/listas-sin-conexion/<?php echo $lista_nombre_archivo_guardado; ?>" target="_blank"><?php echo $lista_nombre_archivo_guardado; ?></a>, generado por última vez: <b><?php echo nombre_fecha($lista_fecha_archivo_guardado); ?></b></p><br>
									<?php } ?>

									<p><b><a onclick="GenerarJson()">Guardar lista en un archivo Json</a></b> </p>
								</div>
								<br><br>
								<?php

								foreach ($array_categoria_nombre as $id_grupo_categoria => $categoria_nombre) {

									$categoria_imagen = $array_categoria_imagen[$id_grupo_categoria];

									$imagen = $Servidor_url.'PANELADMINISTRADOR/img/icono-imagen.png';
									$nombre_imagen = '<span class="no_hay_imagen">(no hay imagen)</span>';
									if($categoria_imagen) {
										$imagen = $url_imagen.$categoria_imagen;
										$nombre_imagen = $categoria_imagen;
									}

									$link_agregar_negocios = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/05-grupos-categorias-negocios.php?grupo_categoria='.$id_grupo_categoria.'&provincia='.$provincia.'&ciudad='.$ciudad;

									?>

									<legend id="txt_nueva_categoria"><img src="<?php echo $imagen; ?>"  class="grupo_imagen"><?php echo $categoria_nombre; ?></legend>

									<a href="<?php echo $link_agregar_negocios; ?>" target="_blank" style="margin-left:60px">[+] Agregar negocio</a>

									<?php if($array_grupo_categorias[$id_grupo_categoria]) { ?>
									<div class="contenedor_tabla">
										<table class="table table-striped">
											<thead class="tabla_encabezado">
												<tr>
													<th><b>#</b></th>
													<th><b>Negocio</b></th>
													<th style="text-align:center"><b>Categoría</b></th>
													<th><b>Ubicación</b></th>
													<th><b>Usuario</b></th>
													<th><b>Fecha Carga</b></th>

												</tr>
											</thead>
											<tbody>
												<?php  $imagen_bender = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/bender/img/bender-circulo.png';

												$explorar_categorias = explode('-', $array_grupo_categorias[$id_grupo_categoria]);

												foreach ($explorar_categorias as $id_negocio) {

													$id_grupo_categoria = $array_negocios_grupo[$id_negocio];
													$id_subgrupo_categoria = $array_negocios_subgrupo[$id_negocio];

													$id_bender_negocio = $array_negocios_bender[$id_negocio];
													$negocio_nombre = $array_negocios_nombre[$id_negocio];

													$grupo_nombre = $array_categoria_nombre[$id_grupo_categoria];
													$subgrupo_nombre = $array_subgrupo_nombre[$id_subgrupo_categoria];

													$negocio_ciudad = $array_negocios_ciudad[$id_negocio];
													$negocio_provincia = $array_negocios_provincia[$id_negocio];

													$negocio_ciudad = $array_ciudades[$negocio_ciudad];
													$negocio_provincia = $array_provincias[$negocio_provincia];

													$negocio_prioridad = $array_negocios_negocio_sin_conexion_prioridad[$id_negocio];
													$usuario_que_carga = $array_negocios_usuario[$id_negocio];

													$categoria_imagen = $array_categoria_imagen[$id_grupo_categoria];

													$fecha_carga = $array_negocios_fecha_carga[$id_negocio];
													$fecha_carga = cuantoHace($fecha_carga);
													$usuario_nombre_txt = $array_usuarios[$usuario_que_carga];
													$variable_grupo_nombre = null;
													$variable_grupo_nombre .= '<img src="'.$imagen.'" class="usuario_avatar"/><br>';
													$variable_grupo_nombre .= $grupo_nombre;
													$variable_grupo_nombre .= '<br><b>'.$subgrupo_nombre.'</b>';
													?>
													<tr class="<?php echo $super_class; ?>" data-href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/03-negocios-ficha.php?negocio=<?php echo $id_negocio; ?>">
														<td><b style="color:red"><?php echo $id_negocio; ?></b></td>
														<td width="20%"><?php echo $negocio_nombre; ?></td>
														<td style="text-align:center"><?php echo $variable_grupo_nombre; ?></td>
														<td><strong><?php echo $negocio_ciudad; ?></strong>,<br><?php echo $negocio_provincia; ?></td>
														<td>
															<?php if($id_bender_negocio) { ?>
															<img class="usuario_avatar" src="<?php echo $imagen_bender; ?>">
															<?php 
															$usuario_nombre_txt = 'Bender y '.$usuario_nombre_txt;
														} ?>
														<img class="usuario_avatar" src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/img/usuarios_cargadores/<?php echo $usuario_que_carga; ?>.jpg">
														<br><b><?php echo $usuario_nombre_txt; ?></b></td>
														<td><strong><?php echo $fecha_carga; ?></strong></td>
													</tr>
													<?php }  ?>
												</tbody>
											</table>
										</div>  	

										<?php } else { ?>
										<br><br><br>
										<?php } } }?>       

									</div>
								</div> <!-- .content-wrapper -->
							</main> 
							<?php include('../../includes/pie-general.php');?>
							<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
							<script type="text/javascript">
								$('tr[data-href]').on("click", function() {
									document.location = $(this).data('href');
								});

								function GenerarJson() {
									$('#div_json').html("<b>Generando Json</b>, esperá unos segundos");
									var lista = <?php echo $lista_de_negocios; ?>;
									$.ajax({
										url: "<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/php/11-negocios-sin-conexion-guardar.php?lista="+lista,
										success: function (resultado) {
											$('#div_json').html(resultado);										
										}
									});	
								}
							</script>
						</body>
						</html>