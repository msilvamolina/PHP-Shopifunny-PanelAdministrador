<?php include('../../../../paginas_include/variables-generales.php'); 
include('../../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../../php/verificar-permisos.php');

$provincia = trim($_GET['provincia']);
$ciudad = trim($_GET['ciudad']);
$contador = trim($_GET['contador']);

if(!$provincia) {
	$provincia = 2;
}

conectar2('mywavi', 'WAVI');
//consultar en la base de datos
$query_rs_ciudad = "SELECT id_ciudad, ciudad_nombre, id_provincia FROM ciudades WHERE id_provincia = $provincia ORDER BY ciudad_nombre ASC ";
$rs_ciudad = mysql_query($query_rs_ciudad)or die(mysql_error());
$row_rs_ciudad = mysql_fetch_assoc($rs_ciudad);
$totalrow_rs_ciudad = mysql_num_rows($rs_ciudad);

do {
	$id_ciudad = $row_rs_ciudad['id_ciudad'];
	$ciudad_nombre = $row_rs_ciudad['ciudad_nombre'];
	$id_provincia = $row_rs_ciudad['id_provincia'];

	$array_ciudades[$id_ciudad] = $ciudad_nombre;

} while($row_rs_ciudad = mysql_fetch_assoc($rs_ciudad));

//consultar en la base de datos
$query_rs_provincias = "SELECT id_provincia, provincia_nombre FROM provincias ORDER BY provincia_nombre ASC ";
$rs_provincias = mysql_query($query_rs_provincias)or die(mysql_error());
$row_rs_provincias = mysql_fetch_assoc($rs_provincias);
$totalrow_rs_provincias = mysql_num_rows($rs_provincias);

do {
	$id_provincia = $row_rs_provincias['id_provincia'];
	$provincia_nombre = $row_rs_provincias['provincia_nombre'];

	$array_provincias[$id_provincia] = $provincia_nombre;
} while($row_rs_provincias = mysql_fetch_assoc($rs_provincias));


desconectar();
?>
<div id="direccion_agregada_<?php echo $contador; ?>">
	<table width="100%" >
		<tr>
			<td >
				<p class="cd-select icon">
					<select name="select_provincia[]" class="budget"  onchange="cargar_ciudades(this.value,<?php echo $contador; ?>)">									
						<?php foreach ($array_provincias as $id_provincia => $provincia_nombre) {
							$elegido = null;
							if($id_provincia==$provincia) {
								$elegido = 'selected';
							}
							if($id_provincia) {
								echo '<option value="'.$id_provincia.'" '.$elegido.'>'.$provincia_nombre.'</option>';
							}
						}
						?>
					</select></p>						    	
				</td>
				<td >
					<p class="cd-select icon">
						<select name="select_ciudad[]" class="budget" id="select_ciudad_<?php echo $contador; ?>" >									
							<?php foreach ($array_ciudades as $id_ciudad => $ciudad_nombre) {
								$elegido = null;
								if($id_ciudad==$ciudad) {
									$elegido = 'selected';
								}
								if($id_ciudad) {
									echo '<option value="'.$id_ciudad.'" '.$elegido.'>'.$ciudad_nombre.'</option>';
								}
							}
							?>
						</select></p>					    	
					</td>					
					<td class="td_delete"><a onclick="eliminar_direccion(<?php echo $contador; ?>)"><img src="<?php echo $Servidor_url;?>PANELADMINISTRADOR/img/delete.png"></a></td>	    				    		
				</tr>

			</table>					
			<table width="100%" >
				<tr>
					<td width="60%">
						<input  type="text" name="direccion_nombre[]" required placeholder="Dirección" >							

					</td>					    				    		
				</tr>
				<tr><td>&nbsp;</td></tr>
			</table>
		</div>