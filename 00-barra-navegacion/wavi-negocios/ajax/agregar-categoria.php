<?php include('../../../../paginas_include/variables-generales.php'); 
include('../../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../../php/verificar-permisos.php');

$numero = trim($_GET['numero']); ?>
<div id="nueva_categoria_<?php echo $numero;?>">
	<input type="hidden" value="0" name="select_grupo_categoria[]" id="select_grupo_categoria_<?php echo $numero; ?>" />
	<section id="section_categoria">
		<a class="delete_categoria" onclick="eliminar_categoria(<?php echo $numero;?>)"><img src="<?php echo $Servidor_url;?>PANELADMINISTRADOR/img/delete.png"></a>    				    				    		
		<h3>Elegí una categoría y subcategoría</h3>

		<div id="demoBasic<?php echo $numero; ?>"  ></div>		

		<p class="cd-select">
			<select name="subgrupo[]" class="select_class" id="select_subgrupo_<?php echo $numero; ?>" >
				<option value="0">Elegí una subcategoría</option>						
			</select></p>
		</section>
		<script type="text/javascript">
			$('#demoBasic<?php echo $numero; ?>').ddslick({
				data: ddData,
				width: 540,
				imagePosition: "left",
				selectText: "Elegí una categoría",
				onSelected: function (data) {
					var valor = data.selectedData.value;
					cargar_subgrupo(valor, <?php echo $numero; ?>);
				}
			});	
		</script>
		<br><br>
	</div>