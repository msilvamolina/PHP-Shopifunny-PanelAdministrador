<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');
$id_negocio = trim($_GET['negocio']);
$source = trim($_GET['source']);

if(!$id_negocio) {
	$link = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/02-cargar-negocio.php';
	header('location: '.$link);
	exit;
}
conectar2('mywavi', 'WAVI');
//consultar en la base de datos
$query_rs_direcciones = "SELECT id_direccion, direccion_nombre, direccion_numero, direccion_latitud, direccion_longitud, direccion_ciudad, direccion_provincia, direccion_nombre_google_maps  FROM direcciones WHERE id_negocio = $id_negocio ";
$rs_direcciones = mysql_query($query_rs_direcciones)or die(mysql_error());
$row_rs_direcciones = mysql_fetch_assoc($rs_direcciones);
$totalrow_rs_direcciones = mysql_num_rows($rs_direcciones);

//consultar en la base de datos
$query_rs_ciudades = "SELECT id_ciudad, ciudad_nombre, id_provincia FROM ciudades ORDER BY ciudad_nombre ASC";
$rs_ciudades = mysql_query($query_rs_ciudades)or die(mysql_error());
$row_rs_ciudades = mysql_fetch_assoc($rs_ciudades);
$totalrow_rs_ciudades = mysql_num_rows($rs_ciudades);
do {
	$id_ciudad = $row_rs_ciudades['id_ciudad'];
	$ciudad_nombre = $row_rs_ciudades['ciudad_nombre'];
	$id_provincia = $row_rs_ciudades['id_provincia'];
	$array_ciudades[$id_ciudad] = $ciudad_nombre;
	if($id_ciudad) {
		$array_ciudades[$id_ciudad] = $ciudad_nombre;
		if(!$array_dependencias_ciudades[$id_provincia]) {
			$array_dependencias_ciudades[$id_provincia] = $id_ciudad;
		} else {
			$array_dependencias_ciudades[$id_provincia] = $array_dependencias_ciudades[$id_provincia].'-'.$id_ciudad;		
		}
	}	
} while($row_rs_ciudades = mysql_fetch_assoc($rs_ciudades));
//consultar en la base de datos
$query_rs_provincias = "SELECT id_provincia, provincia_nombre FROM provincias ";
$rs_provincias = mysql_query($query_rs_provincias)or die(mysql_error());
$row_rs_provincias = mysql_fetch_assoc($rs_provincias);
$totalrow_rs_provincias = mysql_num_rows($rs_provincias);
do {
	$id_provincia = $row_rs_provincias['id_provincia'];
	$provincia_nombre = $row_rs_provincias['provincia_nombre'];
	$array_provincias[$id_provincia] = $provincia_nombre;
} while($row_rs_provincias = mysql_fetch_assoc($rs_provincias));
//consultar en la base de datos
$query_rs_telefonos = "SELECT id_telefono, telefono_caracteristica, telefono_numero, telefono_direccion FROM telefonos WHERE id_negocio = $id_negocio ";
$rs_telefonos = mysql_query($query_rs_telefonos)or die(mysql_error());
$row_rs_telefonos = mysql_fetch_assoc($rs_telefonos);
$totalrow_rs_telefonos = mysql_num_rows($rs_telefonos);

if((!$totalrow_rs_telefonos)OR(!$totalrow_rs_direcciones)) {
	$redirigir = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/02-cargar-negocio-paso-ok.php';
	if($source=='bender') {
		$redirigir = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/bender/03-bender-editar-negocios.php';
	}
	header('location: '.$redirigir);
	exit;
}
desconectar();

do {
	$id_direccion = $row_rs_direcciones['id_direccion'];
	$direccion_nombre = $row_rs_direcciones['direccion_nombre'];
	$direccion_numero = $row_rs_direcciones['direccion_numero'];

	$direccion_latitud = $row_rs_direcciones['direccion_latitud'];
	$direccion_longitud = $row_rs_direcciones['direccion_longitud'];
	$direccion_ciudad = $row_rs_direcciones['direccion_ciudad'];
	$direccion_provincia = $row_rs_direcciones['direccion_provincia'];

	$direccion_nombre_google_maps = $row_rs_direcciones['direccion_nombre_google_maps'];
	
	$mostrar_direccion = $direccion_nombre.' '.$direccion_numero.' '.$direccion_piso.', '.$array_ciudades[$direccion_ciudad].', '.$array_provincias[$direccion_provincia];
	
	if($direccion_nombre_google_maps) {
		$mostrar_direccion = $direccion_nombre_google_maps;
	}

	$array_direcciones[$id_direccion] = $mostrar_direccion;

} while ($row_rs_direcciones = mysql_fetch_assoc($rs_direcciones) ); ?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/barra-pasos.css"> <!-- Resource style -->
	<style type="text/css">
		.txt_rojo {
			color: red;
		}
	</style>
</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper">
			<!-- Contenido de la Pagina-->
			<?php if($source == 'editar') { ?>
			<br><br><br><br>
			<div class="alinear_centro">
				<a class="boton_azul " href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/03-negocios-ficha.php?negocio=<?php echo $id_negocio; ?>">Volver a Ficha de Negocio</a>
			</div>		
			<?php } ?>
			<div class="cd-form floating-labels">
				<section id="crear_categoria" >							
					<fieldset >
						<form onsubmit="return comprobar_formulario()" action="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/php/02-cargar-negocio-paso5-1-db.php" method="POST">
							<input  type="hidden" name="id_negocio" value='<?php echo $id_negocio; ?>'>
							<input  type="hidden" name="source" value='<?php echo $source; ?>'>

							<br><br>
							<legend>Vincular teléfonos</legend>
							<?php
							if($totalrow_rs_telefonos) {
								do {
									$id_telefono = $row_rs_telefonos['id_telefono'];
									$telefono_caracteristica = $row_rs_telefonos['telefono_caracteristica'];
									$telefono_numero = $row_rs_telefonos['telefono_numero'];
									$telefono_direccion = $row_rs_telefonos['telefono_direccion'];
									$array_telefono_direccion[$id_telefono] = $telefono_direccion;
									$mostrar_telefono = '('.$telefono_caracteristica.') '.$telefono_numero; 

									?>
									<table width="100%" id="telefonos">
										<tr>
											<td><?php echo $mostrar_telefono; ?></td>
											<td style="padding-left:8px">
												<p class="cd-select icon">
													<select name="select_direccion[<?php echo $id_telefono; ?>]" class="budget" id="select_ciudad_0" >									
														<option value="0">Elegí una dirección</option>
														<?php foreach ($array_direcciones as $id_direccion => $direccion_nombre) {
															if($id_direccion) {
																$elegido = null;
																if($telefono_direccion==$id_direccion) {
																	$elegido='selected';
																}
																echo '<option value="'.$id_direccion.'" '.$elegido.'>'.$direccion_nombre.'</option>';
															}	
														} ?>
													</select></p>
												</td>				    		
											</tr>
											<tr><td>&nbsp;</td></tr>
										</table>
										<?php }while($row_rs_telefonos = mysql_fetch_assoc($rs_telefonos)); }?>
										<br><br>
										<div class="alinear_centro">
											<input type="submit" value="Continuar" id="btn_nueva_categoria">
										</div>				    
									</form>
								</fieldset>	
							</section>    	

						</div>
					</div> <!-- .content-wrapper -->
				</main> 
				<?php include('../../includes/pie-general.php');?>
				<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
				<script type="text/javascript">

				</script>
			</body>
			</html>