<?php include('../../../../paginas_include/variables-generales.php'); 
include('../../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../../php/verificar-permisos.php');

//aca recibimos todos los datos que nos pasa bender y analizamos

function irse_a_la_mierda($redirigir) {
	header('location:'.$redirigir);
	exit;
}

$agregar_solo_direccion_telefono = $_POST['agregar_solo_direccion_telefono'];
$source = $_POST['source'];

$array_grupos = $_POST['select_grupo_categoria'];
$array_subgrupos = $_POST['subgrupo'];

$nombre = trim($_POST['nombre']);
$descripcion = trim($_POST['descripcion']);
$sitio_web = trim($_POST['sitio_web']);

$palabras_claves = trim($_POST['palabras_claves']);
$email = trim($_POST['email']);

$latitud = trim($_POST['latitud']);
$longitud = trim($_POST['longitud']);

$array_direcciones = $_POST['direccion_nombre'];

$array_cod_area = $_POST['cod_area'];
$array_telefono = $_POST['telefono'];

$pagina_que_manda = trim($_POST['pagina_que_manda']);

//si recibimos de bender
$id_bender_negocio = trim($_POST['id_bender_negocio']);

$redirigir = $Servidor_url.$pagina_que_manda;

$provincia = $_POST['provincia'];
$ciudad = $_POST['ciudad'];

$fecha_hoy = date('Y-m-d H:i:s');

conectar2('mywavi', 'WAVI');

if(!$agregar_solo_direccion_telefono) {
	if((!$provincia)||(!$ciudad)) {
		$redirigir .= '?error=provincia';
		irse_a_la_mierda($redirigir); 
	}

	$descripcion = arreglar_datos_db($descripcion);
	$palabras_claves = arreglar_datos_db($palabras_claves);

	//ahora analizamos todo esto
	foreach ($array_grupos as $id_grupo => $valor_grupo) {
		if($valor_grupo) {
			//aramamos un array con el grupo y el subgrupo
			$subgrupo = $array_subgrupos[$id_grupo];
			$db_array_grupo_subgrupo[] = $valor_grupo.'=>'.$subgrupo;
		} else {
			$redirigir .= '?error=categorias';
			irse_a_la_mierda($redirigir); 
		}
	}
	$db_array_grupo_subgrupo = array_unique($db_array_grupo_subgrupo);


		//consultar en la base de datos
	$query_rs_negocios = "SELECT id_negocio FROM negocios WHERE negocio_nombre='$nombre' AND negocio_provincia='$provincia' AND negocio_ciudad='$ciudad' ";
	$rs_negocios = mysql_query($query_rs_negocios)or die(mysql_error());
	$row_rs_negocios = mysql_fetch_assoc($rs_negocios);
	$totalrow_rs_negocios = mysql_num_rows($rs_negocios);

	if($totalrow_rs_negocios) {
		$redirigir .= '?error=negocio_repetido';
		irse_a_la_mierda($redirigir); 
	}

	$query = "INSERT INTO negocios (id_bender_negocio, negocio_nombre, negocio_descripcion, negocio_palabras_claves, negocio_emails, negocio_sitio_web, usuario_que_carga, ip_visitante, negocio_provincia, negocio_ciudad) 
	VALUES ('$id_bender_negocio','$nombre','$descripcion','$palabras_claves', '$email','$sitio_web', '$id_administrador', '$ip_visitante', '$provincia', '$ciudad')";
	$result = mysql_query($query);
	$id_negocio = mysql_insert_id();

	    //una vez que cargamos el negocio, lo vinculamos con las categorias

	if($db_array_grupo_subgrupo) {
		foreach ($db_array_grupo_subgrupo as $valor) {
			if($valor) {
				$explorar_categoria = explode('=>', $valor);
				$categoria = $explorar_categoria[0];
				$subcategoria = $explorar_categoria[1];

				$query = "INSERT INTO vinculacion_negocio_categorias (id_negocio, id_grupo_categoria, id_subgrupo_categoria, usuario_que_crea, ip_visitante) 
				VALUES ('$id_negocio','$categoria','$subcategoria','$id_administrador', '$ip_visitante')";
				$result = mysql_query($query);
			}
		}
	}
} //agregar_solo_direccion_telefono
else {
	//si es verdadero, es que solo queremos cargar las direcciones a este negocio
	$id_negocio = $agregar_solo_direccion_telefono;
	//consultar en la base de datos
	$query_rs_analizar_direcciones = "SELECT direccion_nombre, direccion_numero, direccion_piso, direccion_ciudad, direccion_provincia  FROM direcciones WHERE id_negocio = $id_negocio ";
	$rs_analizar_direcciones = mysql_query($query_rs_analizar_direcciones)or die(mysql_error());
	$row_rs_analizar_direcciones = mysql_fetch_assoc($rs_analizar_direcciones);
	$totalrow_rs_analizar_direcciones = mysql_num_rows($rs_analizar_direcciones);
	
	do {
		$direccion_nombre = $row_rs_analizar_direcciones['direccion_nombre'];
		$direccion_numero = $row_rs_analizar_direcciones['direccion_numero'];
		$direccion_piso = $row_rs_analizar_direcciones['direccion_piso'];
		$direccion_ciudad = $row_rs_analizar_direcciones['direccion_ciudad'];
		$direccion_provincia = $row_rs_analizar_direcciones['direccion_provincia'];

		$direccion_mostrar = $direccion_nombre.' '.$direccion_numero.' '.$direccion_piso;
		$direccion_mostrar = trim($direccion_mostrar);

		if($direccion_mostrar) {
			$array_direcciones[] = $direccion_mostrar;
			$array_direcciones_ubicacion[] = $direccion_provincia.'-'.$direccion_ciudad;
		}
	} while($row_rs_analizar_direcciones = mysql_fetch_assoc($rs_analizar_direcciones));

	//consultar en la base de datos
	$query_rs_analizar_telefonos = "SELECT telefono_numero, telefono_caracteristica FROM telefonos WHERE id_negocio = $id_negocio ";
	$rs_analizar_telefonos = mysql_query($query_rs_analizar_telefonos)or die(mysql_error());
	$row_rs_analizar_telefonos = mysql_fetch_assoc($rs_analizar_telefonos);
	$totalrow_rs_analizar_telefonos = mysql_num_rows($rs_analizar_telefonos);
	
	do {
		$telefono_numero = $row_rs_analizar_telefonos['telefono_numero'];
		$telefono_caracteristica = $row_rs_analizar_telefonos['telefono_caracteristica'];

		if($telefono_numero) {
			$array_telefono[] = $telefono_numero;
			$array_telefono_codigo[] = $telefono_caracteristica;
		}

	}while(	$row_rs_analizar_telefonos = mysql_fetch_assoc($rs_analizar_telefonos));
	
}		
$direcciones_para_agregar = null;
if($_POST['direccion_nombre']) {
	foreach ($_POST['direccion_nombre'] as $clave => $valor) {
		$valor = arreglar_datos_db($valor);
		$provincia_direccion = $_POST['select_provincia'][$clave];
		$ciudad_direccion =$_POST['select_ciudad'][$clave];

		$direccion_latitud = null;
		$direccion_longitud = null;

		$ubicacion_a_cargar = $provincia_direccion.'-'.$ciudad_direccion;
		$direccion_repetida = null;
		foreach ($array_direcciones as $clave2 => $valor2) {
			$ubicacion = $array_direcciones_ubicacion[$clave2];
			if($valor==$valor2) {
				if($ubicacion==$ubicacion_a_cargar) {
					$direccion_repetida = 1;
				}
			}
		}

		if(!$direccion_repetida) {
			if($clave==0) {
						//la longitud y la latitud de las paginas amarillas, las cargamos en la primera direccion
				$direccion_latitud = $latitud;
				$direccion_longitud = $longitud;	
			}
			if($valor) {
				if(!$direcciones_para_agregar) {
					$direcciones_para_agregar = "('".$id_negocio."', '".$valor."', '".$ciudad_direccion."', '".$provincia_direccion."', '".$direccion_latitud."', '".$direccion_longitud."', '".$id_administrador."', '".$ip_visitante."')";
				} else {
					$direcciones_para_agregar = $direcciones_para_agregar.", ('".$id_negocio."', '".$valor."', '".$ciudad_direccion."', '".$provincia_direccion."', '".$direccion_latitud."', '".$direccion_longitud."', '".$id_administrador."', '".$ip_visitante."')";				
				}
			}
		}
	}
}

if($direcciones_para_agregar) {
	$query2 = "INSERT INTO direcciones (id_negocio, direccion_nombre, direccion_ciudad, direccion_provincia, direccion_latitud, direccion_longitud, usuario_que_crea, ip_visitante) 
	VALUES ".$direcciones_para_agregar;
	$result = mysql_query($query2);
}

	//cargamos las direcciones
$telefonos_para_agregar = null;
if($_POST['telefono']) {
	foreach ($_POST['telefono'] as $clave =>$valor) {
		if($valor) {
			$codigo_area = $_POST['cod_area'][$clave];

			$telefonos_repetidos = null;
			foreach ($array_telefono as $clave2 => $valor2) {
				$codigo2 = $array_telefono_codigo[$clave2];
				if($valor==$valor2) {
					if($codigo_area==$codigo2) {
						$telefonos_repetidos = 1;
					}
				}
			}
			if(!$telefonos_repetidos) {
				if(!$telefonos_para_agregar) {
					$telefonos_para_agregar = "('".$id_negocio."', '".$codigo_area."','".$valor."', '".$ciudad."', '".$provincia."', '".$id_administrador."', '".$ip_visitante."')";
				} else {
					$telefonos_para_agregar = $telefonos_para_agregar.", ('".$id_negocio."', '".$codigo_area."','".$valor."', '".$ciudad."', '".$provincia."', '".$id_administrador."', '".$ip_visitante."')";
				}
			}
		}
	}
}
if($telefonos_para_agregar) {
	$query3 = "INSERT INTO telefonos (id_negocio, telefono_caracteristica, telefono_numero, telefono_ciudad, telefono_provincia, usuario_que_crea, ip_visitante) 
	VALUES ".$telefonos_para_agregar;
	$result = mysql_query($query3);
}	

desconectar();

if($id_bender_negocio) {

	conectar2('mywavi', 'bender');
	mysql_query("UPDATE bender_negocios SET negocio_corregido='$fecha_hoy' WHERE id_bender_negocio = $id_bender_negocio ");
	desconectar();
}

$notificacion_mensaje = $administrador_mostrar_como.' cargó un negocio';
$notificacion_dato = 'id_negocio:'.$negocio;
include('../../../php/mandar-notificacion-include-nuevo.php');

$redirigir = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/02-cargar-negocio-paso4.php?source='.$source.'&negocio='.$id_negocio;
header('location:'. $redirigir);
exit;
?>