<?php
require_once('OpenGraph.php');

//header("HTTP/1.1 200 OK");

$link = trim($_GET['link']);

$graph = OpenGraph::fetch($link);

foreach ($graph as $key => $value) {
	$array_post[$key] = $value;
}

if(!$array_post['url']) {
	$array_post['url'] = $link;
}


function get_content($url) {
	$ch = curl_init();
	curl_setopt ($ch, CURLOPT_URL, $url);
	curl_setopt ($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
	ob_start();
	curl_exec ($ch);
	curl_close ($ch);
	$string = ob_get_contents();
	ob_end_clean();
	return $string;
}

$contenido = get_content($link);
$contenido = utf8_encode($contenido);

function averiguar_imagen($contenido, $averiguar) {
	$explorar = explode($averiguar, $contenido);

	$retornar = null;
	if($explorar[1]) {

		$explorar2 = explode(">", $explorar[1]);
		$caracteres = array('"',"'", "content=", "/");
		$imagen = $explorar2[0];

		$imagen = str_replace($caracteres, "", $imagen);
		
		$retornar = trim($imagen);
	}
	return $retornar;
}

if(!$array_post['image']) {
	$nueva_imagen = averiguar_imagen($contenido, "og:image");
	if($nueva_imagen) {
		$array_post['image'] = $nueva_imagen;
	}
}

$keywords = averiguar_imagen($contenido, "keywords");

if(!$keywords) {
	$explorar = explode('http://', $contenido);
	$agregar = 'http://';
	if(!$explorar[1]) {
		$explorar = explode('https://', $contenido);
		$agregar = 'https://';
	}
	$explorar3 = explode('"', $explorar[1]);

	$nuevo_link = $agregar.$explorar3[0];

	$array_post['nuevo_link'] = $nuevo_link;
	$contenido = get_content($nuevo_link);

	$keywords = averiguar_imagen($contenido, "keywords");
}


if($keywords) {
	$array_post['keywords'] = $keywords;
}

//$array_post['contenido'] = $contenido;

//print_r($contenido);

/*
if($array_post['url']) {
	$contenido = get_content($array_post['url']);

} else {

//traemos el contenido del link
	$contenido = get_content($link);

	if($contenido) {
		$explorar = explode('http://', $contenido);
		$agregar = 'http://';
		if(!$explorar[1]) {
			$explorar = explode('https://', $contenido);
			$agregar = 'https://';
		}
		$explorar2 = explode('?', $explorar[1]);
		$explorar3 = explode('"', $explorar2[0]);

		$nuevo_link = $agregar.$explorar3[0];

		$array_post['nuevo_link'] = $nuevo_link;
		$nuevo_contenido = get_content($nuevo_link);
	}	
}
$array_post['contenido'] = $contenido;
$array_post['nuevo_contenido'] = $nuevo_contenido;
*/
header('Content-Type: application/json');
print_r(json_encode($array_post));

?>