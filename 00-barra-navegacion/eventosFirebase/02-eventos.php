<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');

?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/paginacion.css"> <!-- Resource style -->

	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/popup.css"> <!-- Resource style -->

	<style type="text/css">
	.rojo {
		color: #F44336;
		font-weight: bold;
	}

	.verde {
		color: #2E7D32;
		font-weight: bold;
	}

	.evento_nombre {
		color: #E91E63;
	}

	.verde2 {
		color: #8BC34A;
		font-weight: bold;
	}
</style>
</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper" style="max-width:100%">
			<!-- Contenido de la Pagina-->
			<div class="cd-form"  style="max-width:100%">
				<form id="form-evento">
					<legend id="txt_nueva_categoria">Nuevo Evento</legend>
					<div class="icon">
						<label class="cd-label" for="cd-company">Nombre</label>
						<input class="company" type="text" name="nombreEvento" id="nombreEvento" required>
					</div> 			    

					<div class="alinear_centro">
						<input type="submit" value="Crear Evento" id="botonEvento">
					</div>
				</form>

				<table id="tabla-Convalidaciones"></table>

				<legend id="txt_nueva_categoria">Eventos</legend>

				<table class="table table-striped">
					<thead class="tabla_encabezado">
						<tr>
							<th><b>#</b></th>
							<th><b>Foto</b></th>
							<th><b>Ubicación</b></th>
							<th><b>Nombre</b></th>
							<th><b>Fechadel evento</b></th>
						</tr>
					</thead>
					<tbody>
						<tr class="<?php echo $super_class; ?>" data-href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/eventos/03-ficha-evento.php?evento=<?php echo $id_evento; ?>">
							<td><?php echo $id_evento; ?></td>
							<td><img src="<?php echo $imagen; ?>"  width="100"></td>
							<td width="150">
								<?php if($sala_latitud) { ?>
								<img src="<?php echo $url_ubicacion; ?>" alt="<?php echo $sala_direccion_google_maps; ?>" title="<?php echo $sala_direccion_google_maps; ?>">
								<?php } ?>
							</td>
							<td><b><span class="evento_nombre"><?php echo $evento_nombre; ?></span></b><br>
								<b><?php echo $sala_nombre; ?></b><br>
								<?php echo $direccion_a_mostrar;?></td>


								<td class="verde2"><?php echo nombre_fecha($evento_fecha); ?></td>
							</tr>		
						</tbody>
					</table>	       
				</div>
				<nav role="navigation">
					<ul class="cd-pagination">
						<li class="button"><a class="<?php echo $disabled_anterior; ?>" href="<?php echo $link_anterior; ?>">Anterior</a></li>
						<li class="button"><a  class="<?php echo $disabled_siguiente; ?>"  href="<?php echo $link_siguiente; ?>">Siguiente</a></li>
					</ul>
				</nav> <!-- cd-pagination-wrapper -->	
			</div> <!-- .content-wrapper -->
		</main> 
		<?php include('../../includes/pie-general.php');?>
		<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->

		<?php include('../../includes/firebase.php');?>

		<script type="text/javascript">
			window.onload = inicializar;

			var formConvalidaciones;
			var refConvalidaciones; 
			var tablaConvalidaciones;
			var CREATE = "Añadir Convalidación";
			var UPDATE = "Modificar Convalidación";
			var modo = CREATE;
			var refConvalidacionAEditar;

			function inicializar () {
				formConvalidaciones = document.getElementById("form-evento");
				formConvalidaciones.addEventListener("submit", enviarDatoAServidor, false);

				tablaConvalidaciones = document.getElementById("tabla-Convalidaciones");
				refConvalidaciones = firebase.database().ref().child("Eventos");

				mostrarConvalidacionesDeFirebase();
			}


			/*
			function enviarDatoAServidor(event) {
				event.preventDefault();

				$('#botonEvento').addClass('boton_trabajando');			
				document.getElementById("botonEvento").disabled = true;

				var sendInfo = {
					nombreEvento: event.target.nombreEvento.value
				};

				$.ajax({
					type: "POST",
					url: "<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/eventosFirebase/json/00-cargar-evento-json.php",
					dataType: "json",
					success: function (data) {
						response = JSON.stringify(data);
						var json = jQuery.parseJSON(response);

						$('#botonEvento').removeClass('boton_trabajando');			
						document.getElementById("botonEvento").disabled = false;
						alert(json.respuesta);
					},
					data: sendInfo
				});
			}	
			*/

			$('tr[data-href]').on("click", function() {
				document.location = $(this).data('href');
			});
		</script>
	</body>
	</html>