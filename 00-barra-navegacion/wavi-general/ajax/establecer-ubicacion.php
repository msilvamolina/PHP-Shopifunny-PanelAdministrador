<?php include('../../../../paginas_include/variables-generales.php'); 
include('../../../includes/permisos-usuarios.php');
$permisos_pagina = '';

	session_start();

$direccion = trim($_POST['direccion']);
$latitud = trim($_POST['latitud']);
$longitud = trim($_POST['longitud']);

$_SESSION['direccion'] = $direccion;
$_SESSION['latitud'] = $latitud;
$_SESSION['longitud'] = $longitud;

$array_post['respuesta'] = "OK";
header('Content-Type: application/json');
print_r(json_encode($array_post));
?>