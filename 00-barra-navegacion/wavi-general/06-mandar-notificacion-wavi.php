<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');

?>
<!doctype html>
<html lang="es" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/barra-pasos.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/negocios.css"> <!-- Resource style -->

	<title>Sistema Paradigma 2.0</title>
	<style type="text/css">
		.td_delete {
			padding: 10px;
			text-align: right;
			width: 30px;
		}
		.td_delete img {
			width: 30px;
			display: block;
		}

		.tabla {
			width: 100%;
		}
		.tabla tr td{
			padding: 10px;
		}	

		.tabla tr:nth-of-type(2n) {
			background: #f5e5f2;
		}
	</style>
</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<!-- Contenido de la Pagina-->	
		<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper">
			<!-- Contenido de la Pagina-->
			<div class="cd-form floating-labels" style="max-width:1600px">
				<div style="max-width:600px; margin:0 auto;">
					<section id="crear_categoria" >							
						<fieldset >
						<form action="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-general/php/06-mandar-notificacion-wavi-db.php" method="post">
								<legend id="txt_nueva_categoria">Mandar Notificación Wavi</legend>	
								<div class="icon">
									<label class="cd-label" for="cd-company">Notificación</label>
									<input class="company" type="text" name="notificacion" id="nueva_categoria_nombre" required>
								</div> 

								<div class="alinear_centro">
									<button class="boton_azul" id="btn_continuar" >Mandar</button>
								</div>	
							</form>

						</fieldset>	
					</section>    	
				</div>	

			</div>
		</div> <!-- .content-wrapper -->
	</main> 
	<?php include('../../includes/pie-general.php');?>
	<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->

</body>
</html>