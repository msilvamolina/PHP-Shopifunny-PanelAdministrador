<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');

//eliminamos los temporales

conectar2('mywavi', 'WAVI');

//consultar en la base de datos
$query_rs_reportes = "SELECT * FROM negocios_reportados WHERE problema_solucionado IS NULL ORDER BY id_reporte DESC ";
$rs_reportes = mysql_query($query_rs_reportes)or die(mysql_error());
$row_rs_reportes = mysql_fetch_assoc($rs_reportes);
$totalrow_rs_reportes = mysql_num_rows($rs_reportes);


//consultar en la base de datos
$query_rs_negocio = "SELECT id_negocio, negocio_nombre, usuario_que_carga, fecha_modificacion FROM negocios ";
$rs_negocio = mysql_query($query_rs_negocio)or die(mysql_error());
$row_rs_negocio = mysql_fetch_assoc($rs_negocio);
$totalrow_rs_negocio = mysql_num_rows($rs_negocio);

do {
	$id_negocio = $row_rs_negocio['id_negocio'];
	$negocio_nombre = $row_rs_negocio['negocio_nombre'];
	$usuario_que_carga = $row_rs_negocio['usuario_que_carga'];
	$fecha_carga = $row_rs_negocio['fecha_modificacion'];

	$array_negocio[$id_negocio] = $negocio_nombre;
	$array_negocio_usuario[$id_negocio] = $usuario_que_carga;
	$array_negocio_fecha[$id_negocio] = $fecha_carga;

} while($row_rs_negocio = mysql_fetch_assoc($rs_negocio));

desconectar();

$url_imagen = $Servidor_url.'PANELADMINISTRADOR/img/usuarios_cargadores/';

function cortar_cadena($cadena) {
	$limite = 30;

	$cadena_cortada = substr($cadena, 0, $limite);

	if($cadena_cortada!=$cadena) {
		$cadena_cortada = $cadena_cortada.'...';
	}

	return $cadena_cortada;
}

$total_negocios_verdes = 0;
do { 
	$id_reporte = $row_rs_reportes['id_reporte'];
	$id_negocio = $row_rs_reportes['id_negocio'];

	$problema = $row_rs_reportes['problema'];
	$usuario_que_crea = $row_rs_reportes['usuario_que_reporta'];
	$fecha_carga = formatear_fecha($row_rs_reportes['fecha_reporte']);


	$fecha_carga_negocio = $array_negocio_fecha[$id_negocio];
	if($fecha_carga_negocio) {
		$fecha_carga_negocio = formatear_fecha($fecha_carga_negocio);
	}	
	$fecha1 = strtotime($row_rs_reportes['fecha_reporte']);
	$fecha2 = strtotime($array_negocio_fecha[$id_negocio]);

	$txt_problema = null;

	if($problema_solucionado) {
		$txt_problema = '(Solucionado)';
	} 
	$negocio_actualizado = null;

	if( ($fecha2) > ($fecha1) ) {
		$negocio_actualizado = 'negocio_actualizado';
		$total_negocios_verdes++;
	}

	$array_reporte1[$id_reporte] = $id_negocio;
	$array_reporte2[$id_reporte] = $url_imagen.$usuario_que_crea;
	$array_reporte3[$id_reporte] = cortar_cadena($problema);
	$array_reporte4[$id_reporte] = $fecha_carga;
	$array_reporte5[$id_reporte] = $url_imagen.$array_negocio_usuario[$id_negocio];
	$array_reporte6[$id_reporte] = $array_negocio[$id_negocio];
	$array_reporte7[$id_reporte] = $fecha_carga_negocio;
	$array_reporte8[$id_reporte] = $negocio_actualizado;

} while($row_rs_reportes = mysql_fetch_assoc($rs_reportes)); 

$_SESSION['total_negocios_verdes'] = $total_negocios_verdes;
?>	 
<!doctype html>
<html lang="es" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/barra-pasos.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/negocios.css"> <!-- Resource style -->
	<script type="text/javascript" src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/jszip.js"></script>
	<script type="text/javascript" src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/jszip-utils.js"></script>
	<script type="text/javascript" src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/FileSaver.js"></script>

	<title>Sistema Paradigma 2.0</title>
	<style type="text/css">
		.td_delete {
			padding: 10px;
			text-align: right;
			width: 30px;
		}
		.td_delete img {
			width: 30px;
			display: block;
		}

		.tabla {
			width: 100%;
		}
		.tabla tr td{
			padding: 10px;
		}	

		.tabla tr:nth-of-type(2n) {
			background: #f5e5f2;
		}
		.no_hay_imagen{
			color: #acacac;
		}
		.tabla_encabezado {
			color: red;
		}

		tr {
			cursor: pointer;
		}

		h2 {
			font-size: 36px;
			margin: 0 auto;
			text-align: center;
		}

		.negocio_actualizado {
			background-color: green !important;
			color: #fff79f !important;
		}
	</style>
</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<!-- Contenido de la Pagina-->	
		<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper">
			<!-- Contenido de la Pagina-->
			<div class="cd-form floating-labels" style="max-width:1600px">
				<h2>Negocios Reportados (<?php echo $totalrow_rs_reportes; ?>)</h2><br><br><br><br>
				<h2>Hay <span class="label label-success"><?php echo $total_negocios_verdes;?></span> negocios editados</h2>
				<div class="alert alert-success" role="alert">
					Los negocios que fueron editados después de haberse generado el reporte, están marcados en verde
				</div>
				<?php if(!$totalrow_rs_reportes) { ?>
				<center>
					<p>No hay negocios reportados</p>
				</center>
				<?php } else { ?>
				<table class="table table-striped">
					<thead class="tabla_encabezado">
						<tr>
							<th><b>#</b></th>
							<th><b>Quién reportó</b></th>
							<th><b>Problema</b></th>
							<th><b>Fecha reporte</b></th>
							<th><b>Quién cargó</b></th>
							<th><b>Negocio</b></th>
							<th><b>Última modificación</b></th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($array_reporte1 as $id_reporte => $id_negocio) { ?>
						<tr class="<?php echo $array_reporte8[$id_reporte]; ?>" data-href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/03-negocios-ficha.php?negocio=<?php echo $id_negocio; ?>">
							<td><?php echo $id_reporte; ?></td>
							<td><img src="<?php echo $array_reporte2[$id_reporte]; ?>.jpg"  height="40"></td>
							<td><?php echo $array_reporte3[$id_reporte]; ?></td>
							<td><?php echo $array_reporte4[$id_reporte]; ?></td>	
							<td><img src="<?php echo $array_reporte5[$id_reporte]; ?>.jpg"  height="40"></td>
							<td><?php echo $array_reporte6[$id_reporte]; ?></td>
							<td><?php echo $array_reporte7[$id_reporte]; ?></td>		                
						</tr>		
						<?php } ?>	          
					</tbody>
				</table>				 
				<?php } ?>
			</div>
		</div> <!-- .content-wrapper -->
	</main> 
	<?php include('../../includes/pie-general.php');?>
	<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
	<script type="text/javascript">
		$('tr[data-href]').on("click", function() {
			document.location = $(this).data('href');
		});
	</script>
</body>
</html>