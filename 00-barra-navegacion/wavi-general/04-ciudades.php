<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');
$categoria = 0;
if(!$categoria) {
	$continuar = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/02-cargar-negocio-paso2.php';

}
conectar2('mywavi', 'WAVI');
//consultar en la base de datos
$query_rs_provincias = "SELECT id_provincia, provincia_nombre FROM provincias ";
$rs_provincias = mysql_query($query_rs_provincias)or die(mysql_error());
$row_rs_provincias = mysql_fetch_assoc($rs_provincias);
$totalrow_rs_provincias = mysql_num_rows($rs_provincias);
//consultar en la base de datos
$query_rs_ciudades = "SELECT id_ciudad, id_provincia, ciudad_nombre FROM ciudades ORDER BY ciudad_nombre ASC";
$rs_ciudades = mysql_query($query_rs_ciudades)or die(mysql_error());
$row_rs_ciudades = mysql_fetch_assoc($rs_ciudades);
$totalrow_rs_ciudades = mysql_num_rows($rs_ciudades);
do {
	$id_provincia = $row_rs_provincias['id_provincia'];
	$provincia_nombre = $row_rs_provincias['provincia_nombre'];
	$array_provincias[$id_provincia] = $provincia_nombre;
} while($row_rs_provincias = mysql_fetch_assoc($rs_provincias)); 
do {
	$id_ciudad = $row_rs_ciudades['id_ciudad'];
	$id_provincia = $row_rs_ciudades['id_provincia'];
	$ciudad_nombre = $row_rs_ciudades['ciudad_nombre'];
	if($id_ciudad) {
		$array_ciudades[$id_ciudad] = $ciudad_nombre;
		if(!$array_dependencias_ciudades[$id_provincia]) {
			$array_dependencias_ciudades[$id_provincia] = $id_ciudad;
		} else {
			$array_dependencias_ciudades[$id_provincia] = $array_dependencias_ciudades[$id_provincia].'-'.$id_ciudad;		
		}
	}
} while($row_rs_ciudades = mysql_fetch_assoc($rs_ciudades));
desconectar();
?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/barra-pasos.css"> <!-- Resource style -->
	<title>Sistema Paradigma 2.0</title>
	<style type="text/css">
		.campo_flotante {}
	</style>
</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper">
			<!-- Contenido de la Pagina-->

			<div class="cd-form floating-labels">
				
				<section id="crear_categoria" >							
					<fieldset >
						<form action="javascript:mandar_ciudad()">
							<input  type="hidden" id="ciudad_elegida" value='1'>
							<legend id="txt_nueva_categoria">Elegí una ciudad</legend>
							<div>
								<h4>Provincia</h4>
								<p class="cd-select icon">
									<select class="budget" id="select_elegir_provincia" onchange="ciudades_dependientes(this.value)">
										<option value="0" >Elegí una provincia</option>
										<?php foreach ($array_provincias as $id_provincia => $provincia_nombre) {
											$provincia_elegida=null;
											if($id_provincia==25) {
												$provincia_elegida="selected=";
											}
											echo '<option value="'.$id_provincia.'" '.$provincia_elegida.'>'.$provincia_nombre.'</option>';
										}?>
									</select>
								</p>
							</div>
							<?php foreach ($array_provincias as $id_provincia => $provincia_nombre) {
								$display = 'style="display:none"';
								$elegido = 'selected=""';
								if($id_provincia==25){
									$display = null;
									$elegido = null;
								}
								?>
								<div id="dependientes_de_<?php echo $id_provincia; ?>" class="select_ciudades" <?php echo $display; ?>>
									<h4>Ciudades de <?php echo $provincia_nombre; ?></h4>
									<p class="cd-select icon">
										<select class="budget" id="select_elegir_categoria" onchange="elegir_ciudad(this.value)">
											<option value="0" <?php echo $elegido; ?>>Elegí una ciudad</option>
											<?php if($verificar_permiso['permisos_wavi_administrador']) { ?> 	
											<option value="agregar_<?php echo $id_provincia; ?>" >[+] Agregar una ciudad</option>
											<?php } ?>
											<?php 
											$explorar_ciudad = explode('-', $array_dependencias_ciudades[$id_provincia]);
											foreach ($explorar_ciudad as $id_ciudad) {
												if($id_ciudad) {
													$ciudad_elegida=null;
													if($id_ciudad==1) {
														$ciudad_elegida="selected=";
													}
													echo '<option value="'.$id_ciudad.'" '.$ciudad_elegida.'>'.$array_ciudades[$id_ciudad].'</option>';										
												}
											}
											?>
										</select>
									</p>
								</div>
								<?php } ?>	
								<div class="alinear_centro" id="continuar_formulario">
									<?php if($verificar_permiso['permisos_wavi_administrador']) { ?> 	
									<a class="boton_azul" onclick="editar_ciudad()">Editar ciudad</a><br><br><br><br>
									<?php } ?>  	
									<a class="boton_azul" onclick="negocios_ciudad()">Ver negocios vinculados</a>
								</div>
							</form>
						</fieldset>	
					</section>    	
					<?php if($verificar_permiso['permisos_wavi_administrador']) { ?> 					
					<section id="crear_ciudad" style="display:none">							
						<fieldset >
							<form action="javascript:cargar_ciudad()">
								<input type="hidden" value="0" id="nueva_ciudad_provincia">
								<legend id="txt_nueva_categoria">Nueva ciudad</legend>
								<div class="icon">
									<label class="cd-label" for="cd-company">ID de Facebook</label>
									<input class="company" type="text" name="facebook" id="facebook_nueva_ciudad" >
								</div> 		
								<div class="icon">
									<label class="cd-label" for="cd-company">Nombre de la ciudad</label>
									<input class="company" type="text" name="nombre" id="nombre_nueva_ciudad" required>
								</div> 			    
								<div class="alinear_centro">
									<input type="submit" value="Continuar" id="btn_nueva_ciudad">
								</div>
							</form>
						</fieldset>	
					</section>    	
					<?php } ?>			
				</div>
			</div> <!-- .content-wrapper -->
		</main> 
		<?php include('../../includes/pie-general.php');?>
		<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
		<script type="text/javascript">
			function ciudades_dependientes(provincia) {
				$('.select_ciudades').hide();
				$('#dependientes_de_'+provincia).show();
				$('#crear_ciudad').hide();
			}
			function elegir_ciudad(ciudad) {
				var analizar_categoria = ciudad.split('_');
				var categoria = analizar_categoria[0];
				if(categoria=='agregar') {
					var agragar_a = analizar_categoria[1];
					document.getElementById("nueva_ciudad_provincia").value = agragar_a;
					$('#crear_ciudad').show();
					$('#continuar_formulario').hide();
				} else {
					document.getElementById("ciudad_elegida").value = ciudad;
					$('#continuar_formulario').show();
					$('#crear_ciudad').hide();			
				}
			}
			function cargar_ciudad() {
				var id_facebook = document.getElementById("facebook_nueva_ciudad").value;
				var nueva_ciudad = document.getElementById("nombre_nueva_ciudad").value;
				var provincia = document.getElementById("nueva_ciudad_provincia").value;
				$('#btn_nueva_ciudad').addClass('boton_trabajando');			
				document.getElementById("btn_nueva_ciudad").disabled = true;
				$.ajax({
					url: "<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/guia-de-servicios/php/nueva-ciudad.php?ciudad="+nueva_ciudad+'&provincia='+provincia+'&id_facebook='+id_facebook,
					success: function (resultado) {
						alert(resultado);
						window.location.reload();			

					}
				});	
			}

			function mandar_ciudad() {
				var categoria = <?php echo $categoria; ?>;
				var ciudad = document.getElementById("ciudad_elegida").value;
				var provincia = document.getElementById("select_elegir_provincia").value;		
				window.open('<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/guia-de-servicios/02-cargar-negocio-paso3-1.php?categoria='+categoria+'&provincia='+provincia+'&ciudad='+ciudad, '_self');
			}
			function editar_ciudad() {
				var categoria = <?php echo $categoria; ?>;
				var ciudad = document.getElementById("ciudad_elegida").value;
				var provincia = document.getElementById("select_elegir_provincia").value;		
				window.open('<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/guia-de-servicios/08-editar-ciudad.php?categoria='+categoria+'&provincia='+provincia+'&ciudad='+ciudad, '_self');
			}
			function negocios_ciudad() {		
				var ciudad = document.getElementById("ciudad_elegida").value;
				window.open('<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/03-negocios.php?ciudad='+ciudad, '_self');
			}		
		</script>
	</body>
	</html>