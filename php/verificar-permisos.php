<?php
//si esto está en 1, solo Martín puede entrar al sistema
$pagina_en_construccion = 0;

session_start();

if(!$_SESSION['id_administrador']) {
	if($_COOKIE['id_administrador']) {
		$_SESSION['id_administrador'] = $_COOKIE['id_administrador'];
		$_SESSION['administrador_usuario'] = $_COOKIE['administrador_usuario'];
		$_SESSION['administrador_nombre'] = $_COOKIE['administrador_nombre'];
		$_SESSION['administrador_mostrar_como'] = $_COOKIE['administrador_mostrar_como'];
		$_SESSION['administrador_apellido'] = $_COOKIE['administrador_apellido'];	
		$_SESSION['administrador_sexo'] = $_COOKIE['administrador_sexo'];
		$_SESSION['administrador_nivel'] = $_COOKIE['administrador_nivel'];
		$_SESSION['administrador_rol'] = $_COOKIE['administrador_rol'];
		$_SESSION['administrador_permisos'] = $_COOKIE['administrador_permisos'];

		$_SESSION['administrador_email'] = $_COOKIE['administrador_email'];
		$_SESSION['administrador_telefono'] = $_COOKIE['administrador_telefono'];
		$_SESSION['administrador_fecha_nacimiento'] = $_COOKIE['administrador_fecha_nacimiento'];
		$_SESSION['pedir_cambio_contrasena'] = $_COOKIE['pedir_cambio_contrasena'];	
		$_SESSION['iniciar_nuevo_sistema'] = $_COOKIE['iniciar_nuevo_sistema'];	
		$_SESSION['token'] = $_COOKIE['token'];	

	}

}

if(!$_SESSION['id_administrador']) {
		$redirigir = $_SERVER['PHP_SELF'];	
		$cad = $Servidor_url;
		$cad = substr ($cad, 0, strlen($cad) - 1);
		
		$redireccion = $Servidor_url.'PANELADMINISTRADOR/login.php?redirigir='.$cad.$redirigir;
		header('Location: '.$redireccion);
		exit;
} else {
	$id_administrador = $_SESSION['id_administrador'];
	$administrador_usuario = $_SESSION['administrador_usuario'];
	$administrador_nombre = $_SESSION['administrador_nombre'];
	$administrador_mostrar_como = $_SESSION['administrador_mostrar_como'];
	$administrador_nombre = $_SESSION['administrador_nombre'];
	$administrador_sexo = $_SESSION['administrador_sexo'];		
	$administrador_nivel = $_SESSION['administrador_nivel'];
	$administrador_rol = $_SESSION['administrador_rol'];
	$administrador_permisos = $_SESSION['administrador_permisos'];

	$administrador_email = $_SESSION['administrador_email'];		
	$administrador_telefono = $_SESSION['administrador_telefono'];
	$administrador_fecha_nacimiento = $_SESSION['administrador_fecha_nacimiento'];
	$pedir_cambio_contrasena = $_SESSION['pedir_cambio_contrasena'];	
	$iniciar_nuevo_sistema = $_SESSION['iniciar_nuevo_sistema'];
		
	$token = $_SESSION['token'];	

	$administrador_mostrar_como = $administrador_nombre;
	$analizar_permisos = explode(';', $administrador_permisos);

	foreach ($analizar_permisos as $valor) {
	  $verificar_permiso['permisos_'.$valor] = 1;
	}

	//verificamos si cuenta con los permisos necesarios
	if($permisos_pagina) {	
			if(!$verificar_permiso[$permisos_pagina]) {
			$redireccion = $Servidor_url.'PANELADMINISTRADOR/errorpermisos.php';
			header('Location: '.$redireccion);
			exit;
		}
	}
}

if(!$cambiar_contrasena) {
	if($pedir_cambio_contrasena) {
		$redireccion = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/usuario/01-cambiar-contrasena.php?sistema=1';
		header('Location: '.$redireccion);
		exit;	
	}
}



if($pagina_en_construccion==1) {
	if($id_administrador!=1) {
		$redireccion = $Servidor_url.'PANELADMINISTRADOR/pagina-contruccion.php';
		header('Location: '.$redireccion);
		exit;		
	}
}
?>