<?php include('../paginas_include/variables-generales.php'); 
$permisos_pagina = '';
include('php/verificar-permisos.php');

$url_panel = $Servidor_url.'PANELADMINISTRADOR/';
?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php include('includes/head-general.php'); ?>
	<style type="text/css">
		.mensaje { 
			margin: 50px auto;
			width: 100%;
			max-width: 550px;
		 }
	</style>
</head>
<body>
	<main class="cd-main-content">
		<?php include('includes/barra-navegacion.php'); ?>
		<div class="content-wrapper">
			<center>
			<h1>Hola <?php echo $administrador_nombre;?>, ¿cómo estás hoy?</h1>
			</center>
		</div> <!-- .content-wrapper -->
	</main> <!-- .cd-main-content -->
	<?php include('includes/pie-general.php'); ?>

</body>
</html>