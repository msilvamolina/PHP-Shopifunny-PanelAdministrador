<?php
$Servidor_url = 'https://www.congresodepublicidadunt.com/';
$url_imagenes = 'https://www.congresodepublicidadunt.com/';
if ($_SERVER['HTTP_HOST'] == 'localhost') {
	$Servidor_url = 'http://localhost/revistambp/';
}
$Servidor_url_link = $Servidor_url.'link=';
//Ubicacion Index por defecto
$ubicacion_server = '';
if($panel_administrador == 'panel_administrador') {
	$ubicacion_server = '../';
}
if (!function_exists("amigable")) {
  function amigable($nom_archivo) 
  { 
    $arr_busca = array(' ','�','�','�','�','�','�','�', 
      '�','�', '�','�','�','�','�','�','�','�','�','�', 
      '�','�','�','�','�', '�','�','�','�','�','�','�', 
      '�','�','�','�','�','�','�','�','�','�','�'); 
    $arr_susti = array('-','a','a','a','a','a','A','A', 
      'A','A','e','e','e','E','E','E','i','i','i','I','I', 
      'I','o','o','o','o','o','O','O','O','O','u','u','u','u', 
      'U','U','U','U','c','C','N','n'); 
    $nom_archivo = str_replace($arr_busca, $arr_susti, $nom_archivo);
    $nom_archivo = trim(preg_replace('/[^a-zA-Z0-9\s\-]/', '', $nom_archivo));
    return $nom_archivo;
  }
}
if (!function_exists("amigable_acentos")) {
  function amigable_acentos($nom_archivo) 
  { 
    $arr_busca = array('�','�','�','�','�','�','�', 
      '�','�', '�','�','�','�','�','�','�','�','�','�', 
      '�','�','�','�','�', '�','�','�','�','�','�','�', 
      '�','�','�','�','�','�','�','�','�','�','�'); 
    $arr_susti = array('a','a','a','a','a','A','A', 
      'A','A','e','e','e','E','E','E','i','i','i','I','I', 
      'I','o','o','o','o','o','O','O','O','O','u','u','u','u', 
      'U','U','U','U','c','C','N','n'); 
    $nom_archivo = str_replace($arr_busca, $arr_susti, $nom_archivo);
    return $nom_archivo;
  }
}
if (!function_exists("getRealIP")) {
  function getRealIP()
  {
   
   if(@$_SERVER['HTTP_X_FORWARDED_FOR'] != '' )
   {
    $client_ip = 
    ( !empty($_SERVER['REMOTE_ADDR']) ) ? 
    $_SERVER['REMOTE_ADDR'] 
    : 
    ( ( !empty($_ENV['REMOTE_ADDR']) ) ? 
     $_ENV['REMOTE_ADDR'] 
     : 
     "unknown" );
    
      // los proxys van a�adiendo al final de esta cabecera
      // las direcciones ip que van "ocultando". Para localizar la ip real
      // del usuario se comienza a mirar por el principio hasta encontrar 
      // una direcci�n ip que no sea del rango privado. En caso de no 
      // encontrarse ninguna se toma como valor el REMOTE_ADDR
    
    $entries = split('[, ]', $_SERVER['HTTP_X_FORWARDED_FOR']);
    
    reset($entries);
    while (list(, $entry) = each($entries)) 
    {
     $entry = trim($entry);
     if ( preg_match("/^([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)/", $entry, $ip_list) )
     {
            // http://www.faqs.org/rfcs/rfc1918.html
      $private_ip = array(
        '/^0\./', 
        '/^127\.0\.0\.1/', 
        '/^192\.168\..*/', 
        '/^172\.((1[6-9])|(2[0-9])|(3[0-1]))\..*/', 
        '/^10\..*/');
      
      $found_ip = preg_replace($private_ip, $client_ip, $ip_list[1]);
      
      if ($client_ip != $found_ip)
      {
       $client_ip = $found_ip;
       break;
     }
   }
 }
}
else
{
  $client_ip = 
  ( !empty($_SERVER['REMOTE_ADDR']) ) ? 
  $_SERVER['REMOTE_ADDR'] 
  : 
  ( ( !empty($_ENV['REMOTE_ADDR']) ) ? 
   $_ENV['REMOTE_ADDR'] 
   : 
   "unknown" );
}

return $client_ip;

}
//Ip del visitante de la p�gina
$ip_visitante = getRealIP();
}
//Conectar con las bases de datos
if (!function_exists("conectar")) {
  function conectar($db) {
   $db = "revistam_".$db;
   mysql_connect("localhost", "revistam_martin", "birlibirloqueGB335");
   mysql_select_db($db);
 }
 function desconectar() {
   mysql_close();
 }
}

function conectar2($usuario, $db) {
  $db = $usuario."_".$db;
  mysql_connect("localhost", $usuario."_martin", "birlibirloqueGB335");
  mysql_select_db($db);
}

if (!function_exists("conectar_amor")) {
  function conectar_amor($db) {
   $db = "amor_".$db;
   mysql_connect("localhost", "amor_martin", "birlibirloqueGB335");
   mysql_select_db($db);
 }
}
if (!function_exists("conectar_paradigma")) {
  function conectar_paradigma($db) {
    $db = "paradigm_".$db;
    mysql_connect("localhost", "paradigm_martin", "birlibirloqueGB335");
    mysql_select_db($db);
  }
}
if (!function_exists("cuantoHace")) {
  function cuantoHace($PublishDate){
/*Calcula el tiempo que hace desde $PublishDate hasta la fecha actual, con arreglo a un
formato personalizado: Muestra segundos, minutos, horas, o dias transcurridos.*/
$PublishDate = strtotime($PublishDate);
$actual=time();
$dif=$actual-$PublishDate;
$segundos = $dif;
$segundos > 0 ? $segundos_text = $segundos." segundos " : $segundos_text = "";
$minutos = floor($dif/(60));
$minutos > 1 ? $minutos_text=$minutos." minutos " : $minutos_text = "";
$minutos == 1 ? $minutos_text = $minutos." minuto " : $minutos_text = $minutos_text;
$horas = floor($dif/(60*60));
$horas > 1 ? $horas_text = $horas." horas ":$horas_text = "";
$horas == 1 ? $horas_text = $horas." hora " : $horas_text = $horas_text;
$dias = floor($dif/(60*60*24));
$dias > 1 ? $dias_text = $dias." dias " : $dias_text = "";
$dias == 1 ? $dias_text = $dias." dia " : $dias_text = $dias_text;
/*El texto a mostrar para saber el tiempo transcurrido entre la fecha dada y el momento actual�..*/
$time_dif_text="";
$segundos > 0 ? $time_dif_text=$segundos_text : $time_dif_text = $time_dif_text;
$minutos > 0 ? $time_dif_text = $minutos_text : $time_dif_text = $time_dif_text;
$horas > 0 ? $time_dif_text = $horas_text : $time_dif_text = $time_dif_text;
$dias > 0 ? $time_dif_text = $dias_text : $time_dif_text = $time_dif_text;
$dias > 5 ? $time_dif_text = " el ".date("d M Y",$PublishDate) : $time_dif_text = " hace ".$time_dif_text;
if($time_dif_text == ' hace ') {
	$time_dif_text = ' hace instantes';
}
return $time_dif_text;
}
}
$img_cargando = '<center style="margin-top:5px"><img src="'.$Servidor_url.'imagenes/cargando.gif" /></center>';
//si la pagina esta en construccion, redirigimos al error 404 a menos que el ip este identificado
if($pagina_en_construccion) {
	conectar('sitioweb');
	
	$query_rs_verificar_ip = "SELECT direccion_ip FROM direcciones_ip_identificadas WHERE direccion_ip = '$ip_visitante'";
	$rs_verificar_ip = mysql_query($query_rs_verificar_ip)or die(mysql_error());
	$row_rs_verificar_ip = mysql_fetch_assoc($rs_verificar_ip);
	$totalrow_rs_verificar_ip = mysql_num_rows($rs_verificar_ip);
	if(!$totalrow_rs_verificar_ip) {
		$redireccion = $Servidor_url.'error404';
		header('Location: '.$redireccion);
		exit;	
	}
	desconectar();
}


function nombre_fecha($fecha_actual) {

  $dias = array('Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado','Domingo');

  $meses['01'] = 'Enero';
  $meses['02'] = 'Febrero';
  $meses['03'] = 'Marzo';
  $meses['04'] = 'Abril';
  $meses['05'] = 'Mayo';
  $meses['06'] = 'Junio';
  $meses['07'] = 'Julio';
  $meses['08'] = 'Agosto';
  $meses['09'] = 'Septiembre';
  $meses['10'] = 'Octubre';
  $meses['11'] = 'Noviembre';
  $meses['12'] = 'Diciembre';

  $nueva_fecha = explode(' ', $fecha_actual);

  $fecha_actual = $nueva_fecha[0];

  $hora = $nueva_fecha[1];

  $saber_dia = $dias[date(N, strtotime($fecha_actual))];
  $saber_dia_corto = $dias_corto[date(N, strtotime($fecha_actual))];

  $explorar_fecha = explode('-', $fecha_actual);

  if($explorar_fecha[1]){
    $explorar_ano = $explorar_fecha[0];
    $explorar_mes = $explorar_fecha[1];
    $explorar_dia = $explorar_fecha[2];
  }else{
    $explorar_fecha = explode('/', $fecha_actual);
    $explorar_ano = $explorar_fecha[2];
    $explorar_mes = $explorar_fecha[1];
    $explorar_dia = $explorar_fecha[0];
  }
  $saber_mes = $meses[$explorar_mes];

  $explorar_dia=(string)(int)$explorar_dia;

  $saber_dia_tablet = substr($saber_dia , 0, 3); 
  $saber_mes_tablet = substr($saber_mes , 0, 3);

  $mostrar_fecha = $saber_dia.', '.$explorar_dia.' de '.$saber_mes.' de '.$explorar_ano;

  if(!$explorar_dia) {
    $mostrar_fecha = $saber_mes.' de '.$explorar_ano;
  }

  if($hora) {
    $explorar_hora = explode(':', $hora);
    $hora = $explorar_hora[0].':'.$explorar_hora[1];
    $mostrar_fecha = $mostrar_fecha.' a las '.$hora;
  }

  return $mostrar_fecha;
}
function nombre_fecha_min($fecha_actual) {

  $dias = array('Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado','Domingo');

  $meses['01'] = 'Enero';
  $meses['02'] = 'Febrero';
  $meses['03'] = 'Marzo';
  $meses['04'] = 'Abril';
  $meses['05'] = 'Mayo';
  $meses['06'] = 'Junio';
  $meses['07'] = 'Julio';
  $meses['08'] = 'Agosto';
  $meses['09'] = 'Septiembre';
  $meses['10'] = 'Octubre';
  $meses['11'] = 'Noviembre';
  $meses['12'] = 'Diciembre';

  $nueva_fecha = explode(' ', $fecha_actual);

  $fecha_actual = $nueva_fecha[0];

  $hora = $nueva_fecha[1];

  $saber_dia = $dias[date(N, strtotime($fecha_actual))];
  $saber_dia_corto = $dias_corto[date(N, strtotime($fecha_actual))];

  $explorar_fecha = explode('-', $fecha_actual);

  if($explorar_fecha[1]){
    $explorar_ano = $explorar_fecha[0];
    $explorar_mes = $explorar_fecha[1];
    $explorar_dia = $explorar_fecha[2];
  }else{
    $explorar_fecha = explode('/', $fecha_actual);
    $explorar_ano = $explorar_fecha[2];
    $explorar_mes = $explorar_fecha[1];
    $explorar_dia = $explorar_fecha[0];
  }
  $saber_mes = $meses[$explorar_mes];

  $explorar_dia=(string)(int)$explorar_dia;

  $saber_dia_tablet = substr($saber_dia , 0, 3); 
  $saber_mes_tablet = substr($saber_mes , 0, 3);

  $mostrar_fecha = $saber_dia.', '.$explorar_dia.' de '.$saber_mes;

  if(!$explorar_dia) {
    $mostrar_fecha = $saber_mes;
  }

  $explorar_hora = explode(':', $hora);
  $hora = $explorar_hora[0].':'.$explorar_hora[1];
  if($hora) {
    $mostrar_fecha = $mostrar_fecha.' a las '.$hora;
  }

  return $mostrar_fecha;
}


function formatear_fecha ($fecha) {
  $explorar_fecha_hora = explode(' ', $fecha);
  $explorar_fecha = explode('-', $explorar_fecha_hora[0]);
  if($explorar_fecha[1]) {
    $ano = $explorar_fecha[0];
    $mes = $explorar_fecha[1];
    $dia = $explorar_fecha[2];
    
    $nueva_fecha = $dia.'/'.$mes.'/'.$ano;
  } else {
    $nueva_fecha = $fecha;
  }

  if($explorar_fecha_hora[1]) {
    $hora = explode(':', $explorar_fecha_hora[1]);
    $nueva_fecha .= ' '.$hora[0].':'.$hora[1];
  }
  return $nueva_fecha;
}

function arreglar_datos_db($dato) {
  $dato = str_replace('"',"&#34;", $dato);
  $dato = str_replace("'","&#39;", $dato);

  $dato = eregi_replace("[\n|\r|\n\r]", '<br>', $dato);

  return $dato;
}
?>