<?php 
function comprobar_active($carpeta) {
	$url = $_SERVER['REQUEST_URI']; 
	$explorar_url = explode('/', $url);
	$url_actual = $explorar_url[3];
	$return=NULL;
	if($url_actual==$carpeta) {
		$return = 'active';
	}
	return $return;
}

$total_reportes = $_SESSION['total_reportes'];
$total_negocios_verdes = $_SESSION['total_negocios_verdes'];

?>
<nav class="cd-side-nav">
	<ul>
		<li>
			<img src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/img/logo-grande2.png" width="100%" style="padding:10px" alt="Shopifunny">

		</li>

		<li class="cd-label">Cuenta</li>
		<?php $active = comprobar_active('usuario'); ?>		
		<li class="has-children <?php echo $active; ?>">
			<a href="#0">Usuario</a>
		<!-- ejemplo de como seria con un contador al lado
			<a href="#0">Guía de Servicios<span class="count">3</span></a>
		-->	
		<ul>
			<li><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/usuario/01-cambiar-contrasena.php">Cambiar Contraseña</a></li>
			<li><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/usuario/02-modificar-datos.php">Modificar Datos</a></li>
		</ul>
	</li>
	<li class="cd-label">Productos</li>
	<?php $active = comprobar_active('productos'); ?>		
	<li class="has-children bookmarks <?php echo $active; ?>">
		<a href="#0">Productos</a>
		<ul>				
			<li><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/productos/00-cargar-producto.php">Cargar producto</a></li>
			<li><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/productos/04-productos.php">Productos</a></li>
		</ul>
	</li>
	
	<li class="cd-label">Sesión</li>
	<li class="has-children">
		<a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/php/logout.php">Cerrar Sesión</a>
	</ul>
</nav>