<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/recursos/bootstrap/css/bootstrap.min.css" />

<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/reset2.css?v=1"> <!-- CSS reset -->
<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/style3.css?v=3"> <!-- Resource style -->
<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/modernizr.js"></script> <!-- Modernizr -->
<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/font-awesome-4.7.0/css/font-awesome.min.css" />
<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/botones.css"> <!-- Resource style -->
<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/recursos/bootstrap/js/bootstrap.min.js"></script>

<!-- Web APP iOS/Android -->
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">

<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta content="Web App" name="apple-mobile-web-app-title">

<link rel="apple-touch-icon" sizes="57x57" href="<?php echo $Servidor_url; ?>apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="<?php echo $Servidor_url; ?>apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo $Servidor_url; ?>apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo $Servidor_url; ?>apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo $Servidor_url; ?>apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo $Servidor_url; ?>apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo $Servidor_url; ?>apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo $Servidor_url; ?>apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo $Servidor_url; ?>apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo $Servidor_url; ?>android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo $Servidor_url; ?>favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="<?php echo $Servidor_url; ?>favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo $Servidor_url; ?>favicon-16x16.png">
<link rel="manifest" href="<?php echo $Servidor_url; ?>manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="<?php echo $Servidor_url; ?>ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">

<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title>Shopifunny Administrador</title>
<meta name="HandheldFriendly" content="true">
<meta name="MobileOptimized" content="width">
<style type="text/css">
	a:hover, a:active {
		text-decoration: none !important;
	}

	.cd-side-nav a:hover, .cd-side-nav a:active  {
		color: #ffde00 !important;
		text-decoration: none !important;
	}
</style>