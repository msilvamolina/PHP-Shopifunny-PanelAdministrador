<?php
require_once '../ThumbLib.inc.php';
$ruta = '../../../../APLICACION/Imagenes/productos/';
$ruta_recorte = $ruta.'recortes/';

$imagen_recorte = explode('.', $_GET['imagen']);

$tipo_recorte = "-miniatura.";
$redimensionar_1 = "200";
$redimensionar_2 = "200";
if($_GET['tipo'] == 'recorte') {
	$tipo_recorte = "-recorte.";
	$redimensionar_1 = "500";
	$redimensionar_2 = "500";	
}

if($_GET['tipo'] == 'zoom') {
	$tipo_recorte = ".";
	$redimensionar_1 = "1000";
	$redimensionar_2 = "1000";	
}

$imagen_nombre = $imagen_recorte[0].$tipo_recorte.$imagen_recorte[1];
$imagen = $ruta.trim($_GET['imagen']);

$tamano = explode('x', trim($_GET['tamano']));
$thumb = PhpThumbFactory::create($imagen);
$thumb->crop($tamano[0],$tamano[1],$tamano[2],$tamano[3]);
$thumb->resize($redimensionar_1, $redimensionar_2);
$thumb->save($ruta_recorte.$imagen_nombre);
$thumb->show();

?>
