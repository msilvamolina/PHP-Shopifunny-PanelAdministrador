<?php
require_once '../ThumbLib.inc.php';

$carpeta = trim($_GET['carpeta']);
$ruta = '../../../imagenes-subidas-desde-web/'.$carpeta.'/';
$ruta_temp = $ruta.'temp/';
$imagen = $ruta_temp.trim($_GET['imagen']);

$tamano = explode('x', trim($_GET['tamano']));
$thumb = PhpThumbFactory::create($imagen);
$thumb->resize($tamano[0],$tamano[1]);
$thumb->save($ruta.$_GET['imagen']);
$thumb->show();
?>