<?php

require_once '../ThumbLib.inc.php';


$ruta = '../../../usuarios/fotos_publicaciones/';
$imagen = $ruta.trim($_GET['imagen']);

$tamano = explode('x', trim($_GET['tamano']));
$thumb = PhpThumbFactory::create($imagen);
$thumb->resize($tamano[0],$tamano[1]);
$thumb->show();

?>
