jQuery(document).ready(function(){
	var accordionsMenu = $('.cd-accordion-menu');

	if( accordionsMenu.length > 0 ) {
		
		accordionsMenu.each(function(){
			var accordion = $(this);
			//detect change in the input[type="checkbox"] value
			accordion.on('change', 'input[type="checkbox"]', function(){
				var checkbox = $(this);
				var id = checkbox.prop('id');
				cargar_categorias(id);

				console.log(checkbox.prop('checked'));
				( checkbox.prop('checked') ) ? checkbox.siblings('ul').attr('style', 'display:none;').slideDown(300) : checkbox.siblings('ul').attr('style', 'display:block;').slideUp(300);
			});
		});
	}

});

function cargar_categorias(categoria) {
	$.ajax({
		url: "http://www.paradigmapublicidad.com/sistemaV3/00-barra-navegacion/guia-de-servicios/php/00-elegir-categoria-db.php?categoria="+categoria,
		success: function (resultado) {
			$('#categoria_'+categoria).html(resultado);
		}
	});
}